<cfsilent>

	<cfparam name="rc.bodyID" default=""/>
	<cfparam name="rc.pageTitle" default="Must Remember That"/>
    <cfparam name="rc.keywords" default=""/>
    <cfparam name="rc.errors" default="#arrayNew(1)#">
    <cfparam name="rc.successMessage" default="#arrayNew(1)#">
    <cfparam name="rc.registerMessage" default="#arrayNew(1)#">
    <cfparam name="rc.message" default="#arrayNew(1)#">
    <cfparam name="rc.infoMessage" default="#arrayNew(1)#">
    
    <!--- get a list of all the businesses the user ownes or is an administrator for --->
    <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
        <cfquery name="rc.getBusinesses">
            SELECT tbl_businesses.*
            FROM tbl_businesses
            WHERE ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            UNION
            SELECT tbl_businesses.*
            FROM tbl_businesses
            INNER JOIN tbl_businessAdmins ON tbl_businessAdmins.businessId = tbl_businesses.businessId
            WHERE tbl_businessAdmins.userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>
    </cfif>
        
</cfsilent>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="apple-itunes-app" content="app-id=1121692955"/>
	<meta name="keywords" content="<cfoutput>#rc.keywords#</cfoutput>">
        <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon'/ >
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><cfoutput>#rc.pageTitle#</cfoutput></title>

        <!-- Bootstrap -->
        <link href="/assets/css/bootstrap.css?v=1" rel="stylesheet">
        <link href="/assets/css/datepicker.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="<cfoutput>#rc.bodyID#</cfoutput>" role="document">
        <div id="fb-root"></div>
        <nav class="navbar navbar-inverse mainNav" style="margin-bottom:0px; min-height:85px;">
            <div class="container-fluid">
                <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
                    <div class="navbar-header">
                        <a href="/" class="navbar-brand"><img class="img-responsive" src="/assets/logo-mrt.png"></a>
                        <span class="navbar_businessName">
                            <cfoutput>#rc.getBusiness.title#</cfoutput>
                        </span>
                    </div>
                    
                    <div id="bs-example-navbar-collapse-2" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right loggedOut">
                            <li class="active">Hi <cfoutput>#session.auth.firstname#</cfoutput> - <a style="display:inline;" href="<cfoutput>#buildUrl("main.logout")#</cfoutput>">logout</a></li>
                        </ul>
                    </div>
                <cfelse>
                    <div class="navbar-header">
                        <button data-target="#bs-example-navbar-collapse-2" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            menu
                        </button>
                        <div class="col-xs-2 visible-xs" style="padding:0px;">
                            <a href="/" class="navbar-brand"><img style="padding:0px;" class="img-responsive" src="/assets/pin.png"></a>
                        </div>
                        <a href="/" class="navbar-brand hidden-xs"><img class="img-responsive" src="/assets/logo-mrt.png"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="hideMob"><p class="navbar-text">Already have an account?</p></li>
                            <li id="mobLogin" class="dropdown">
                                <a href="#" class="dropdown-toggle hideMob" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                                <ul id="login-dp" class="dropdown-menu">
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!--- Login via
                                                <div class="social-buttons">
                                                    <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                                </div>
                                                or --->
                                                <form class="form" role="form" method="post" action="<cfoutput>#buildUrl("main.default")#</cfoutput>" accept-charset="UTF-8" id="login-nav">
                                                    <input type="hidden" name="update" value="loginUser"/>
                                                    <input type="hidden" name="forwardAddress" value="<cfoutput>#rc.getBusiness.urlSlug#</cfoutput>"/>
                                                    <div class="form-group">
                                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                        <input type="email" name="emailAddress" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                        <input name="password" type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                                        <div class="help-block">
                                                            <input name="rememberMe" type="checkbox" checked> remember me
                                                            <a href="" style="float:right; margin-top:4px;" class="text-right" data-toggle="modal" data-target="#passwordModal">Forgotten password?</a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                                    </div>
                                                    <!--- <div class="form-group">
                                                        <a href="#" onclick="authUser();" class="btn btn-block btn-fb"><i class="fa fa-facebook"></i> Sign in with Facebook</a>
                                                    </div>
                                                    
                                                        <div class="form-group">
                                                            <button class="btn btn-block btn-google" type="button" id="googleSignup"><i class="fa fa-google"></i> Sign Up with Google</button>
                                                            <div style="opacity:0; height:10px;" id="my-signin2"></div>
                                                        </div>
                                                    --->
                                                </form>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </cfif>
            </div>
        </nav>

        <div class="container mobPadding" style="width:100%;">

            

            <cfif not arrayIsEmpty(rc.errors)>
                <div class="alert alert-danger fade in m-b-15">
                    <span data-dismiss="alert" class="close">x</span>
                    <cfloop array="#rc.errors#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif>

            <cfif not arrayIsEmpty(rc.successMessage)>
                <div class="alert alert-success fade in m-b-15">
                    <strong>Success!</strong>
                    <span data-dismiss="alert" class="close">x</span><br/>
                    <cfloop array="#rc.successMessage#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif>

            <cfif not arrayIsEmpty(rc.registerMessage)>
                <div class="alert alert-success fade in m-b-15">
                    <span data-dismiss="alert" class="close">x</span>
                    <cfloop array="#rc.registerMessage#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif>

            <cfif not arrayIsEmpty(rc.infoMessage)>
                <div class="alert alert-info fade in m-b-15">
                    <strong>Information!</strong>
                    <span data-dismiss="alert" class="close">x</span><br/>
                    <cfloop array="#rc.infoMessage#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif>

            <!--- main body content --->
    		<cfoutput>#body#</cfoutput>

		</div>

        <footer class="white navbar hidden-xs <cfif structKeyExists(RC, 'footerBottom')>navbar-fixed-bottom</cfif>">
            <div class="row-fluid">
                <div class="col-lg-6 footerlinks">
                    <a href="terms.cfm">Terms &amp; Conditions</a> | <a href="privacy.cfm">Privacy Policy</a> | <a href="acceptance.cfm">Acceptable Use Policy</a> | <a href="cookie.cfm">Cookies</a> |  <a href="contact.cfm">Contact us</a>
                </div>
                <div class="col-lg-6">
                    <div class="row-fluid">
                        <div style="text-align:center; margin-top:15px;" class="col-lg-6">Must Remember That &copy; 2016</div>
                        <div class="col-lg-6">
                            <div style="text-align:center">
                                <a onmouseover="MM_swapImage('facebook','','/assets/social-facebook.png',1)" onmouseout="MM_swapImgRestore()" href="https://www.facebook.com/mustrememberthat"><img alt="" src="/assets/social-facebook-grey.png" id="facebook"></a>
                                <a onmouseover="MM_swapImage('twitter','','/assets/social-twitter.png',1)" onmouseout="MM_swapImgRestore()" href="https://twitter.com/mustremember2"><img alt="" src="/assets/social-twitter-grey.png" id="twitter"></a>
                                <a onmouseover="MM_swapImage('youtube','','/assets/social-youtube.png',1)" onmouseout="MM_swapImgRestore()" href="https://www.youtube.com/channel/UCAJGx3uRrnFkDg-fyphsMDg"><img alt="" src="/assets/social-youtube-grey.png" id="youtube"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
            <footer class="stickyfooter visible-xs">
                <div class="row-fluid">
                    <div class="mobFooterContainer"><a href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>"><img src="/assets/footerPin.png" style="width:85%"><br/>Reminders</a></div>
                    <div class="mobFooterContainer"><a href="<cfoutput>#buildUrl("users.subscriptions")#</cfoutput>"><img src="/assets/footerSubscriptions.png" style="width:85%"><br/>Subscriptions</a></div>
                    <div class="mobFooterContainer"><a href="<cfoutput>#buildUrl("users.groups")#</cfoutput>"><img src="/assets/footerUsers.png" style="width:85%"><br/>Groups</a></div>
                    <div class="mobFooterContainer"><a href="<cfoutput>#buildUrl("users.suggestedReminders")#</cfoutput>"><img src="/assets/footerLightbulb.png" style="width:85%"><br/>Suggested</a></div>
                    <div class="mobFooterContainer"><a href="<cfoutput>#buildUrl("users.myAccount")#</cfoutput>"><img src="/assets/footerUser.png" style="width:85%"><br/>My Profile</a></div>
                </div>
            </footer>
        </cfif>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/assets/js/bootstrap.min.js"></script>
        
        <cftry>

            <cfset viewsPath = expandPath('.') & '/views/' />       <!--- get path to views --->
            <cfset jsView = replace(rc.action,'.','/jspath/' ) >    <!--- set js path to file name aka main/jspath/default.cfm to main/jspath/default view --->
            <cfset jsFile = viewsPath & jsView & '.cfm' />          <!--- create path file name to check against --->

            <cfif FileExists(jsFile)>
                
                <cfoutput>#view(jsView)#</cfoutput>
            
            </cfif>
            <cfcatch type="any"></cfcatch>

        </cftry>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-64228773-1', 'auto');
          ga('send', 'pageview');

        </script>
        
        <!--- facebook code --->
        <script>
        function MM_swapImgRestore() { //v3.0
          var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }

        function MM_findObj(n, d) { //v4.01
          var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
          if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
          for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
          if(!x && d.getElementById) x=d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
          var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
           if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
        
        window.fbAsyncInit = function() {
            FB.init({appId: '864511866924978', status: true, cookie: true,
            xfbml: true});
        };
        (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
            '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());

        $( document ).ready(function() {
            
            // this code gets run every 5 mins to check if the users session is still valid
            <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
            window.chkSessionStatus(function(){
                chkSessionStatus();
            }, 300*1000);
            </cfif>
            
            $('input#getBusinessReminders').on('keyup',function(){
                var charCount = $(this).val().replace(/\s/g, '').length;
                alert(charCount);
                if(charCount >= 2){
                    var searchString = $('input#getBusinessReminders').val();
                    $.ajax({
                        type: "POST",
                        url: "<cfoutput>#buildUrl("ajax.searchReminders")#</cfoutput>",
                        data: { searchString : searchString, reminderKey : '<cfoutput>#rc.getBusiness.businessId#</cfoutput>'},
                        success: function(result){
                            $('ul.list-group').html(result);
                        }
                                    
                    });

                }

            });

           $(window).bind('resize load', function() {
                if ($(this).width() < 767) {
                    $('.hideMob').remove();
                    $('#mobLogin').addClass('open');
                } 
            });

            $(document.body).on("click", "#retrievePassword", function(){
                var emailAddress = $('#FPEmail').val();

                // hide any previous error or success messages
                $('.passwordError, .passwordSuccess').hide();
                
                $.ajax({
                    type: "POST",
                    url: "<cfoutput>#buildUrl("ajax.resetPassword")#</cfoutput>",
                    dataType: "json",
                    data: { emailAddress : emailAddress }
                }).done(function( html ) {
                    if(html['errorMessage']){
                        $(".passwordError").html( html.errorMessage );
                        $(".passwordError").show();
                    }else if(html['successMessage']){
                        $( ".passwordSuccess" ).html( html.successMessage );
                        $(".passwordSuccess").show();
                    }
                });
                
                return false;   
            });
        });
        
        function chkSessionStatus(){
            $.ajax({
                type: "get",
                url: "/controllers/ajax.cfc?method=checkSessionExists",
                success: function(data, textStatus, jqXHR){
                    // all good here
                    console.log(jqXHR);
                },
                error: function(jqXHR){
                    console.log(jqXHR.status);
                    window.location = "/";
                }
            });
        }

        $(document).ready(function() {   
            var sideslider = $('[data-toggle=collapse-side]');
            var sel = sideslider.attr('data-target-2');
            
            sideslider.click(function(event){
                $(sel).toggleClass('in');
            });
        });
        
        </script>

        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="828595320587-dfi9kuet9kihch91jas93jd1dm1ht39l.apps.googleusercontent.com">
                                                       
        <!---  cg29RqH4XCDL-9fOzefFptfa  --->
    
        <script>
            function onSuccess(googleUser) {
      
                var profile = googleUser.getBasicProfile();

                var params = "GName="+profile.getName()+"&GId="+profile.getId()+"&GEmail="+profile.getEmail()+"&rid="+getUrlParameter('rid');
                $.ajax({
                       type: "POST",
                       url: "/index.cfm?action=ajax.googleConnect",
                       data: params,
                       dataType: "html",
                       success: function(resp)
                       {
                            window.location = '/index.cfm?action=users.reminders'   

                       },
                       error: function (json, status, e)
                       {
                            alert("There was a problem login in. Please try again");                
                       }
                 });

            }
            function onFailure(error) {
              console.log(error);
            }
            function renderButton() {
              gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 0,
                'height': 0,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
              });
            }

            $(document.body).on("click", "#googleSignup", function(){
                renderButton()
            });

            $(document).ready(function() {   
                var sideslider = $('[data-toggle=collapse-side]');
                var sel = sideslider.attr('data-target-2');
                
                sideslider.click(function(event){
                    $(sel).toggleClass('in');
                });
            });

        </script>

        <!--- Password Modal --->
            <!-- Modal -->
            <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" 
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" 
                               data-dismiss="modal">
                                   <span aria-hidden="true">&times;</span>
                                   <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Forgotten Password</h4>
                        </div>
                        
                        <!-- Modal Body -->
                        <div class="modal-body">
                            
                            <div style="display:none;" class='alert alert-danger passwordError'></div>
                            <div style="display:none;" class='alert alert-success passwordSuccess'></div>

                            <div id="FPContent">
                            <p>Give us you're email address and we will send you a email that will allow you to reset your password</p>
                                <form role="form">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                      <input type="email" class="form-control"
                                      id="FPEmail" placeholder="Enter email"/>
                                  </div>
                                </form>
                            </div>
                        </div>
                        
                        <!-- Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="retrievePassword" class="btn btn-primary">Retrieve</button>
                        </div>
                    </div>
                </div>
            </div>
    
    </body>
</html>