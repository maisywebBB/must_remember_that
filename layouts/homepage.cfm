<cfsilent>

	<cfparam name="rc.bodyID" default=""/>
	<cfparam name="rc.pageTitle" default="Must Remember That"/>
    <cfparam name="rc.keywords" default=""/>
    <cfparam name="rc.errors" default="#arrayNew(1)#">
    <cfparam name="rc.successMessage" default="#arrayNew(1)#">
    <cfparam name="rc.registerMessage" default="#arrayNew(1)#">
    <cfparam name="rc.message" default="#arrayNew(1)#">
    <cfparam name="rc.infoMessage" default="#arrayNew(1)#">
    <cfparam name="rc.isArchive" default="0">
    
    <!--- get a list of all the businesses the user ownes or is an administrator for --->
    <cfif structKeyExists(SESSION, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
        <cfquery name="rc.getBusinesses">
            SELECT tbl_businesses.*
            FROM tbl_businesses
            WHERE ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            UNION
            SELECT tbl_businesses.*
            FROM tbl_businesses
            INNER JOIN tbl_businessAdmins ON tbl_businessAdmins.businessId = tbl_businesses.businessId
            WHERE tbl_businessAdmins.userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

        <cfquery name="rc.getGroups">
           SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            WHERE tbl_groups.ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            UNION
            SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            INNER JOIN tbl_groupUsers ON tbl_groupUsers.groupId = tbl_groups.groupId
            WHERE tbl_groupUsers.userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>
    </cfif>

</cfsilent>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="apple-itunes-app" content="app-id=1121692955"/>
        <meta name="keywords" content="<cfoutput>#rc.keywords#</cfoutput>">
        <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon'/ >
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><cfoutput>#rc.pageTitle#</cfoutput></title>
        <meta name="description" content="Have you ever forgotten to renew your car tax, pay the TV license or missed an important appointment? Must Remember That keeps your life organised with reminders sent direct to you.">

        
        <!-- Facebook -->
        <meta property="og:url" content="https://www.mustrememberthat.com" />
		<meta property="og:image" content="http://www.mustrememberthat.com/assets/Must-Remember-That-FB-mets.png" />
		<meta property="og:title" content="Must Remember That! - Never Forget Again" />
		<meta property="og:description" content="Have you ever forgotten to renew your car tax, pay the TV license or missed an important appointment? Must Remember That keeps your life organised with reminders sent direct to you" />

        <!-- Bootstrap -->
        <link href="/assets/css/bootstrap.css?v=1" rel="stylesheet">
        <link href="/assets/css/datepicker.css" rel="stylesheet" type="text/css">
        <style>
            body {
                height: 100%;
                width: 100%;
            }
            html {
                height: 100%;
                width: 100%;
            }
        </style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="<cfoutput>#rc.bodyID#</cfoutput>" role="document">
        <div id="fb-root"></div>
        <div id="headerFP">
            <nav class="navbar navbar-inverse mainNav navbar-fixed-top">
                <div class="container-fluid">
                    <cfif structKeyExists(SESSION, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
                        <div class="navbar-header">
                            <button data-target="#bs-example-navbar-collapse-2" data-target-2=".side-collapse" data-toggle="collapse-side" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                Login
                            </button>

                            <div class="col-xs-2 visible-xs" style="padding:0px;">
                                <a href="/" class="navbar-brand"><img style="padding:0px;" class="img-responsive" src="/assets/pin.png"></a>
                            </div>
                            <div class="visible-xs col-xs-7 mobReminderContainer btn-group" style="padding:0px; text-align: center; padding-left:13%">
                                <cfif listFindNoCase('main.privacy,main.terms,main.acceptance,main.cookie,main.contact,business.business',request.action)>
                                    <cfif rc.isArchive>
                                        <a class="btn btn-default mobReminderType" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&">Active</a>
                                        <a class="btn btn-primary mobReminderType" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&isArchive=1">Expired</a>
                                    <cfelse>
                                        <a class="btn btn-primary mobReminderType" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&">Active</a>
                                        <a class="btn btn-default mobReminderType" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&isArchive=1">Expired</a>
                                    </cfif>
                                </cfif>
                                <cfif listFindNoCase('users.updateReminder,users.subscriptions,users.groups,users.suggestedReminders,users.myAccount',request.action)>
                                    <a class="btn btn-primary mobReminderType" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>">Active</a>
                                    <a class="btn btn-default mobReminderType" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&isArchive=1">Expired</a>
                                </cfif>
                            </div>
                            <a href="#regForm" class="navbar-brand hidden-xs"><img class="img-responsive" src="/assets/logo-mrt.png"></a>
                        </div>
                        <div id="bs-example-navbar-collapse-2" class="side-collapse in">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active"><a href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span> My Reminders<span class="sr-only">(current)</span></a></li>
                                <!---<li><a href="<cfoutput>#buildUrl("business.addABusiness")#</cfoutput>"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Add a page</a></li>--->
                                <li class="dropdown">
                                    <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <cfoutput>#session.auth.firstname# #session.auth.lastname#</cfoutput>  <span class="caret"></span></a>
                                    <ul role="menu" class="dropdown-menu">
                                        <cfoutput query="rc.getBusinesses">
                                            <li><a href="#buildUrl("business.reminders")#&businessId=#rc.getBusinesses.businessId#">#rc.getBusinesses.title#</a></li>
                                        </cfoutput>
                                        <cfif rc.getBusinesses.recordcount><li><hr></li></cfif>
                                        <cfoutput query="rc.getGroups">
                                            <li><a href="#buildUrl("groups.edit")#&groupId=#rc.getGroups.groupId#">#rc.getGroups.title#</a></li>
                                        </cfoutput>
                                        <cfif rc.getGroups.recordcount><li><hr></li></cfif>
                                        <li><a href="<cfoutput>#buildUrl("users.myAccount")#</cfoutput>">My Profile</a></li>
                                        <li><a href="<cfoutput>#buildUrl("business.addABusiness")#</cfoutput>">Add a page</a></li>
                                        <li><a href="<cfoutput>#buildUrl("groups.addAGroup")#</cfoutput>">Add a group</a></li>
                                        <li><hr></li>
                                        <li><a href="terms.cfm">Terms &amp; Conditions</a></li>
                                        <li><a href="privacy.cfm">Privacy Policy</a></li>
                                        <li><a href="acceptance.cfm">Acceptable Use Policy</a></li>
                                        <li><a href="cookie.cfm">Cookies</a></li>
                                        <li><a href="contact.cfm">Contact us</a></li>
                                        <li><hr></li>
                                        <li><a href="<cfoutput>#buildUrl("main.logout")#</cfoutput>">Logout</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                    <cfelse> 
                        <div class="navbar-header">
                            <button data-target="#bs-example-navbar-collapse-2" data-target-2=".side-collapse" data-toggle="collapse-side" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                Login
                            </button>
                            <a href="#regForm" class="navbar-brand  page-scroll"><img class="img-responsive" src="/assets/logo-mrt.png"></a>
                        </div>
                        <div id="bs-example-navbar-collapse-2" class="side-collapse in">
                            <ul class="nav navbar-nav navbar-right">
                                <!--- <li class="hideMob"><p class="navbar-text">Already have an account?</p></li> --->
                                <li><a data-anchor="howitworks" href="#howitworks" class="smallPadding page-scroll">How It Works</a></li>
                                <li><a data-anchor="business" href="#business" class="smallPadding page-scroll" >Business</a></li>
                                <li><a data-anchor="ourapp" href="#ourapp" class="smallPadding page-scroll">App</a></li>
                                <li><a data-anchor="reminders" href="#reminders" class="smallPadding page-scroll" >Reminders</a></li>
                                <li><a data-anchor="aboutus" href="#aboutus" class="smallPadding page-scroll">About Us</a></li>
                                <li id="mobLogin" class="dropdown">
                                    <a href="#" class="dropdown-toggle btn btn-default smallPadding hideMob" style="margin-right:15px;" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                                    <ul id="login-dp" class="dropdown-menu">
                                        <li>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    
                                                    <form class="form" role="form" method="post" action="<cfoutput>#buildUrl("main.default")#</cfoutput>" accept-charset="UTF-8" id="login-nav">
                                                        <input type="hidden" name="update" value="loginUser"/>
                                                        <cfif structKeyExists(URL, 'msg') AND url.msg EQ 'login' AND structKeyExists(URL, 'rid')>
                                                            <input type="hidden" name="rid" value="<cfoutput>#url.rid#</cfoutput>"/>
                                                        </cfif>
                                                        <cfif structKeyExists(URL, 'slug')>
                                                            <input type="hidden" name="slug" value="<cfoutput>#url.slug#</cfoutput>"/>
                                                        </cfif>
                                                        <div class="form-group">
                                                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                            <input type="email" name="emailAddress" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                            <input name="password" type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                                            <div class="help-block">
                                                                <input name="rememberMe" type="checkbox" checked> remember me
                                                                <a href="" style="float:right; margin-top:4px;" class="text-right" data-toggle="modal" data-target="#passwordModal">Forgotten password?</a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                                        </div>
                                                        <!--- 
                                                        <div class="form-group">
                                                            <a href="#" onclick="authUser();" class="btn btn-block btn-fb"><i class="fa fa-facebook"></i> Sign in with Facebook</a>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <button class="btn btn-block btn-google" type="button" id="googleSignup"><i class="fa fa-google"></i> Sign Up with Google</button>
                                                            <div style="opacity:0; height:10px;" id="my-signin2"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <a href="<cfoutput>#buildUrl("main.registration")#</cfoutput>" class="btn btn-block btn-primary">Sign Up</a>
                                                        </div>
                                                         --->
                                                    </form>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="hideMob" style="margin-right:10px;"><div class="form-group"><a href="<cfoutput>#buildUrl("main.registration")#</cfoutput>" class="btn btn-primary smallPadding btn-block">Sign Up</a></div></li>
                            </ul>
                        </div>
                    </cfif>
                </div>
            </nav>
        </div>

        

            <cfif structKeyExists(URL, 'msg') AND url.msg EQ 'login' AND structKeyExists(URL, 'rid')>
                
                <cfquery name="rc.getReminder">
                    SELECT title
                    FROM  tbl_reminders
                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
                </cfquery>
                
                <cfif NOT arrayLen(rc.successMessage)>
                    <cfset arrayAppend(rc.registerMessage, "Please register or login to add your new reminder - #rc.getReminder.title#")>
                </cfif>
                
            </cfif>

            <cfif structKeyExists(URL, 'msg') AND url.msg EQ 'login' AND NOT structKeyExists(URL, 'rid')>
                <cfset arrayAppend(rc.registerMessage, "To set your reminders please login or if you are not a member please complete our sign up form which take just 2 minutes, your reminders will be automatically added to your new FREE account.")>
            </cfif>

            <!--- <cfif not arrayIsEmpty(rc.errors)>
                <div class="alert alert-danger fade in m-b-15">
                    <a href="##" data-dismiss="alert" class="close">x</a>
                    <cfloop array="#rc.errors#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif> --->

            <cfif not arrayIsEmpty(rc.registerMessage)>
                <div class="alert alert-success fade in m-b-15">
                    <span data-dismiss="alert" class="close">x</span>
                    <cfloop array="#rc.registerMessage#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif>

            <!---
            <cfif not arrayIsEmpty(rc.successMessage)>
                <div class="alert alert-success fade in m-b-15">
                    <strong>Success!</strong>
                    <a href="##" data-dismiss="alert" class="close">x</a><br/>
                    <cfloop array="#rc.successMessage#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif>
            --->

            <cfif not arrayIsEmpty(rc.infoMessage)>
                <div class="alert alert-info fade in m-b-15">
                    <strong>Information!</strong>
                    <a href="##" data-dismiss="alert" class="close">x</a><br/>
                    <cfloop array="#rc.infoMessage#" index="msg">
                        <cfoutput>#msg#</cfoutput></br>
                    </cfloop>
                </div>
            </cfif>

<div id="status">
</div>

            <!--- main body content --->
    		<cfoutput>#body#</cfoutput>


        <cfif NOT structKeyExists(RC, 'noFooter')>
            
            <footer class="white navbar <cfif structKeyExists(RC, 'footerBottom')>navbar-fixed-bottom</cfif>">
                <div class="row-fluid">
                    <div class="col-lg-6 footerlinks">
                        <a href="terms.cfm">Terms &amp; Conditions</a> | <a href="privacy.cfm">Privacy Policy</a> | <a href="acceptance.cfm">Acceptable Use Policy</a> | <a href="cookie.cfm">Cookies</a> |  <a href="contact.cfm">Contact us</a>
                    </div>
                    <div class="col-lg-6">
                        <div class="row-fluid">
                            <div style="text-align:center; margin-top:15px;" class="col-lg-6">Must Remember That &copy; 2016</div>
                            <div class="col-lg-6">
                                <div style="text-align:center">
                                    <a onmouseover="MM_swapImage('facebook','','/assets/social-facebook.png',1)" onmouseout="MM_swapImgRestore()" href="https://www.facebook.com/mustrememberthat"><img alt="" src="/assets/social-facebook-grey.png" id="facebook"></a>
                                    <a onmouseover="MM_swapImage('twitter','','/assets/social-twitter.png',1)" onmouseout="MM_swapImgRestore()" href="https://twitter.com/mustremember2"><img alt="" src="/assets/social-twitter-grey.png" id="twitter"></a>
                                    <a onmouseover="MM_swapImage('youtube','','/assets/social-youtube.png',1)" onmouseout="MM_swapImgRestore()" href="https://www.youtube.com/channel/UCAJGx3uRrnFkDg-fyphsMDg"><img alt="" src="/assets/social-youtube-grey.png" id="youtube"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </cfif>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/jquery.easing.min.js"></script>
        <script src="/assets/js/scrolling-nav.js"></script>
        <cftry>

            <cfset viewsPath = expandPath('.') & '/views/' />       <!--- get path to views --->
            <cfset jsView = replace(rc.action,'.','/jspath/' ) >    <!--- set js path to file name aka main/jspath/default.cfm to main/jspath/default view --->
            <cfset jsFile = viewsPath & jsView & '.cfm' />          <!--- create path file name to check against --->

            <cfif FileExists(jsFile)>
                
                <cfoutput>#view(jsView)#</cfoutput>
            
            </cfif>
            <cfcatch type="any"></cfcatch>

        </cftry>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-64228773-1', 'auto');
          ga('send', 'pageview');

        </script>
        
        <!--- facebook code --->
        <script>
        function MM_swapImgRestore() { //v3.0
          var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }

        function MM_findObj(n, d) { //v4.01
          var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
          if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
          for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
          if(!x && d.getElementById) x=d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
          var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
           if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }

        
        $( document ).ready(function() {
           <cfif structKeyExists(session, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
            window.chkSessionStatus(function(){
                chkSessionStatus();
            }, 300*1000);
            </cfif>
                           
           <cfif structKeyExists(RC, 'getBusiness')>
           
               $('input#getBusinessReminders').on('keyup',function(){
                    var charCount = $(this).val().replace(/\s/g, '').length;
                    
                    if(charCount >= 0){
                        var searchString = $('input#getBusinessReminders').val();
                        $.ajax({
                            type: "POST",
                            url: "<cfoutput>#buildUrl("ajax.searchReminders")#</cfoutput>",
                            data: { searchString : searchString, reminderKey : '<cfoutput>#rc.getBusiness.businessId#</cfoutput>'},
                            success: function(result){
                                $('ul.list-group').html(result);
                            }
                                        
                        });

                    }

                });
            </cfif>

           $(window).bind('resize load', function() {
                if ($(this).width() < 767) {
                    $('.hideMob').remove();
                    $('#mobLogin').addClass('open');
                } 
            });

            $(document.body).on("click", "#retrievePassword", function(){
                var emailAddress = $('#FPEmail').val();

                // hide any previous error or success messages
                $('.passwordError, .passwordSuccess').hide();
                
                $.ajax({
                    type: "POST",
                    url: "<cfoutput>#buildUrl("ajax.resetPassword")#</cfoutput>",
                    dataType: "json",
                    data: { emailAddress : emailAddress }
                }).done(function( html ) {
                    if(html['errorMessage']){
                        $(".passwordError").html( html.errorMessage );
                        $(".passwordError").show();
                    }else if(html['successMessage']){
                        $( ".passwordSuccess" ).html( html.successMessage );
                        $(".passwordSuccess").show();
                    }
                });
                
                return false;   
            });
        });
        
        function chkSessionStatus(){
            $.ajax({
                type: "get",
                url: "/controllers/ajax.cfc?method=checkSessionExists",
                success: function(data, textStatus, jqXHR){
                    // all good here
                    console.log(jqXHR);
                },
                error: function(jqXHR){
                    console.log(jqXHR.status);
                    //window.location = "/";
                }
            });
        }

        </script>

        <cfif structKeyExists(URL, 'ForgottenPassword')>
            <script type="text/javascript">
                $(window).load(function(){
                    $('#passwordModal').modal('show');
                });
            </script>
        </cfif>

        <!--- facebook connect code --->
        <script src="//connect.facebook.net/en_US/all.js"></script>
        <!--- custom facebook javascript goes here --->
        <script src="/assets/js/facebook.js" type="text/javascript"></script>

        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="828595320587-dfi9kuet9kihch91jas93jd1dm1ht39l.apps.googleusercontent.com">
        <!---  cg29RqH4XCDL-9fOzefFptfa  --->
    
        <script>
            function onSuccess(googleUser) {
      
                var profile = googleUser.getBasicProfile();

                var params = "GName="+profile.getName()+"&GId="+profile.getId()+"&GEmail="+profile.getEmail()+"&rid="+getUrlParameter('rid');
                $.ajax({
                       type: "POST",
                       url: "/index.cfm?action=ajax.googleConnect",
                       data: params,
                       dataType: "html",
                       success: function(resp)
                       {
                            window.location = '/index.cfm?action=users.reminders'   

                       },
                       error: function (json, status, e)
                       {
                            alert("There was a problem login in. Please try again");                
                       }
                 });

            }
            function onFailure(error) {
              console.log(error);
            }
            function renderButton() {
              gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': 0,
                'height': 0,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
              });
            }

            $(document.body).on("click", "#googleSignup", function(){
                renderButton()
            });

        </script>

        <script>
            $(document).ready(function() {   
                    var sideslider = $('[data-toggle=collapse-side]');
                    var sel = sideslider.attr('data-target-2');
                    
                    sideslider.click(function(event){
                        $(sel).toggleClass('in');
                    });
                });
        </script>

        <!--- Password Modal --->
            <!-- Modal -->
            <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" 
                 aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" 
                               data-dismiss="modal">
                                   <span aria-hidden="true">&times;</span>
                                   <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Forgotten Password</h4>
                        </div>
                        
                        <!-- Modal Body -->
                        <div class="modal-body">
                            
                            <div style="display:none;" class='alert alert-danger passwordError'></div>
                            <div style="display:none;" class='alert alert-success passwordSuccess'></div>

                            <div id="FPContent">
                            <p>Give us you're email address and we will send you a email that will allow you to reset your password</p>
                                <form role="form">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                      <input type="email" class="form-control"
                                      id="FPEmail" placeholder="Enter email"/>
                                  </div>
                                </form>
                            </div>
                        </div>
                        
                        <!-- Modal Footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="retrievePassword" class="btn btn-primary">Retrieve</button>
                        </div>
                    </div>
                </div>
            </div>

           
    
    </body>
</html>
