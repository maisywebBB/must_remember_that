<cfsetting showdebugoutput="false">
<!--- hide debugoutput. You dont want to 
	return the debug information as well --->

<cfparam name="url.imgX" 	type="numeric" 	default="0" />
<cfparam name="url.imgY" 	type="numeric" 	default="0" />
<cfparam name="url.width" 	type="numeric" 	default="0" />
<cfparam name="url.height" 	type="numeric" 	default="0" />
<cfparam name="url.imgLoc" 	type="string" 	default="" />
<!--- set the params for the image dimensions --->

<cfif len(url.imgLoc)>

<!--- read the image and create a ColdFusion image object --->
<cfimage source="#url.imgLoc#" name="originalImage" />

<!--- crop the image using the supplied coords from the url request --->
<cfset ImageCrop(originalImage, url.imgX, url.imgY, url.width, url.height) />

<!--- write the revised/cropped image to the browser
 to display on the calling page --->
<cfimage source="#originalImage#" action="writeToBrowser" />

</cfif>