<cfsetting showdebugoutput="false">
<html>
<head>
	<title>coldfumonkeh : Jcrop and ColdFusion - image cropping</title>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.Jcrop.js"></script>
	<link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	
	<script language="Javascript">
		jQuery(document).ready(function(){			

		// obtain original image dimensions
		var originalImgHeight 	= jQuery('#cropbox').height();
		var originalImgWidth 	= jQuery('#cropbox').width();

		// set the padding for the crop-selection box
		var padding = 10;
		
		// set the x and y coords using the image dimensions
		// and the padding to leave a border
		var setX = originalImgHeight-padding;
		var setY = originalImgWidth-padding;
		
		// create variables for the form field elements
		var imgX 		= jQuery('input[name=x]');
		var imgY 		= jQuery('input[name=y]');
		var imgHeight 	= jQuery('input[name=h]');
		var imgWidth 	= jQuery('input[name=w]');
		var imgLoc 		= jQuery('input[name=imageFile]');
		
		// get the current image source in the main view
		var currentImage = jQuery("#croppedImage img").attr('src');
		
		setImageFileValue(currentImage);
		
			// instantiate the jcrop plugin
			buildJCrop();
			
			// selecting revert will create the img html tag complete with
			// image source attribute, read from the imageFile form field
			jQuery("#revert_btn").click(function() {					
				var htmlImg = '<img src="' + jQuery('input[name=imageFile]').val() 
						+ '" id="cropbox" />';
				jQuery('#croppedImage').html(htmlImg);
				// instantiate the jcrop plugin
				buildJCrop();
				
			});
			
			jQuery("#imageCrop_btn").click(function(){					
				// organise data into a readable string
				var data = 'imgX=' + imgX.val() + '&imgY=' + imgY.val() + 
						'&height=' + imgHeight.val() + '&width=' + imgWidth.val() + 
						'&imgLoc=' + encodeURIComponent(imgLoc.val());
				// 
				jQuery('#croppedImage').load('crop.cfm',data);
				
				// disable the image crop button and
				// enable the revert button
				jQuery('#imageCrop_btn').attr('disabled', 'disabled');
				jQuery('#revert_btn').removeAttr('disabled');
				
				// do not submit the form using the default behaviour
				return false;
			});
			
			// add the jQuery invocation into a separate function,
			// which we will need to call more than once
			function buildJCrop() {
				jQuery('#cropbox').Jcrop({
					aspectRatio: 0,
					onChange: showCoords,
					onSelect: showCoords,
					setSelect: [padding,padding,setY,setX]
				});
				// enable the image crop button and
				// disable the revert button
				jQuery('#imageCrop_btn').removeAttr('disabled');
				jQuery('#revert_btn').attr('disabled', 'disabled');
			}
			
			// set the imageFile form field value to match
			// the new image source
			function setImageFileValue(imageSource) {
				imgLoc.val(imageSource);
			}
			
			jQuery("ul.thumb li").hover(function() {
				// increase the z-index to ensure element stays on top
				jQuery(this).css({'z-index' : '10'});
				// add hover class and stop animation queue
				jQuery(this).find('img').addClass("hover").stop()
					.animate({
						// vertically align the image
						marginTop: '-110px', 
						marginLeft: '-110px',
						top: '50%',
						left: '50%',
						// set width
						width: '174px',
						// set height
						height: '174px',
						padding: '20px'
					}, 
						// set hover animation speed
						200);

				} , function() {
				// set z-index back to zero
				jQuery(this).css({'z-index' : '0'});
				// remove the hover class and stop animation queue
				jQuery(this).find('img').removeClass("hover").stop()
					.animate({
						// reset alignment to default
						marginTop: '0',
						marginLeft: '0',
						top: '0',
						left: '0',
						// reset width
						width: '100px',
						// reset height
						height: '100px',
						padding: '5px'
					}, 400);
			});

			// onclick action for the thumbnails
			jQuery("ul.thumb li a").click(function() {
		
				// check to see if  id="cropbox" attribute exists
				// in the img html
				if (!jQuery("#croppedImage img").attr('id')) {
					// no attribute exists. add it in
					jQuery("#croppedImage img").attr('id', 'cropbox')
				}
				// instantiate the jcrop plugin
				buildJCrop();
		
				 // Get the image name
				var mainImage = $(this).attr("href");
				jQuery("#croppedImage img").attr({ src: mainImage });
				
				setImageFileValue(mainImage);
			
				return false;		
			});
		});

		// Our simple event handler, called from onChange and onSelect
		// event handlers, as per the Jcrop invocation above
		function showCoords(c) {
			jQuery('#x').val(c.x);
			jQuery('#y').val(c.y);
			jQuery('#x2').val(c.x2);
			jQuery('#y2').val(c.y2);
			jQuery('#w').val(c.w);
			jQuery('#h').val(c.h);		
		};
	</script>
	
</head>
<body>
<cfoutput>	
	<div id="outer">
	<div class="imageContainer">
	<h2>jCrop and ColdFusion</h2>	
		<!--- The event handler from the JCrop plugin populates these
			values for us. Required to obtain the X Y coords and persist
			the image location for cropping and reverting the image. --->
		<form action="crop.cfm" method="post">
			<input type="hidden" size="4" id="x" 	name="x" />
			<input type="hidden" size="4" id="y" 	name="y" />
			<input type="hidden" size="4" id="x2" 	name="x2" />
			<input type="hidden" size="4" id="y2" 	name="y2" />
			<input type="hidden" size="4" id="w" 	name="w" />
			<input type="hidden" size="4" id="h" 	name="h" />
			
			<input type="hidden" name="imageFile" 		id="imageFile" 		value="" />		
			<input type="button" name="imageCrop_btn" 	id="imageCrop_btn" 	value="Crop the image" />
			<input type="button" name="revert_btn" 		id="revert_btn" 	value="Revert to original" />
		</form>
		<!-- This is the image we're attaching Jcrop to -->
		<div id="croppedImage"><img src="images/ZamakRobots1.jpg" id="cropbox" /></div>
		<div id="thumbs">
			<ul class="thumb">
				<li><a href="images/ZamakRobots1.jpg"><img src="images/thumbs/ZamakRobots1.jpg" alt="Image 1" /></a></li>
				<li><a href="images/ZamakRobots2.jpg"><img src="images/thumbs/ZamakRobots2.jpg" alt="Image 2" /></a></li>
				<li><a href="images/ZamakRobots3.jpg"><img src="images/thumbs/ZamakRobots3.jpg" alt="Image 3" /></a></li>
			</ul>
		</div>
		<p>This demonstration uses the awesome <a href="http://deepliquid.com/content/Jcrop.html" title="Jcrop jQuery plugin" target="_blank">Jcrop jQuery plugin</a> from Deep Liquid.</p>
		<p>Robots designed by <a href="http://www.zamak.fr/" title="Oliver Bucheron at zamak.fr" target="_blank">Olivier Bucheron</a>.</p>
		<p>Thumbnail hover effect inspired by <a href="http://www.sohtanaka.com/web-design/fancy-thumbnail-hover-effect-w-jquery/" title="sohtanaka.com" target="_blank">sohtanaka.com</a>.</p>
	</div>
	</div>
</cfoutput>
</body>
</html>