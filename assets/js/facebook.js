//  1588083784817629
FB.init({ appId: '864511866924978', 
	status: true, 
	cookie: true,
	xfbml: true,
	oauth: true
});

// Check if the current user is logged in and has authorized the app
FB.getLoginStatus(checkLoginStatus);

// Login in the current user via Facebook and ask for email permission
function authUser() {
	
	FB.login(function(response) {
		if (response.authResponse) {
			FB.api('/me', function(response) {
				var fbName = response.name;
				if(response.location){
					var location = response.location.name;
				}else{
					var location = '';
				}
				var fbId = response.id;
				var fbEmail = response.email;
				
				if (typeof fbEmail === "undefined") {
				    alert("We were unable to obtain your email address. Please register using the registration form");	
				} else {
					var params = "fbName="+response.name+"&fbId="+response.id+"&fbEmail="+fbEmail+"&fbLocation="+location+"&rid="+getUrlParameter('rid');
					$.ajax({
						   type: "POST",
						   url: "/index.cfm?action=ajax.faceBookConnect",
						   data: params,
						   dataType: "html",
						   success: function(resp)
						   {
								window.location = '/index.cfm?action=users.reminders'	

						   },
						   error: function (json, status, e)
						   {
								alert("There was a problem login in. Please try again");				
						   }
					 });
				}
			
			});	   
		} else {
			//user cancelled login or did not grant authorization
		}
	}
	, {scope:'public_profile,email'}
	);
return false;
}

function logoutUser() {
	FB.logout(function(response) {
		window.location.reload();
	});
	return false;
}	

// Check the result of the user status and display login button if necessary
function checkLoginStatus(response) {
if(response && response.status == 'connected') {
  var params = "accessToken="+response.authResponse.accessToken;
  
  // Hide the login button
  // document.getElementById('loginButton').style.display = 'none';
  // document.getElementById('logoutButton').style.display = 'block';
  
  // Now Personalize the User Experience
} else {
  
  // Display the login button
  if (document.getElementById('loginButton')) {
	  document.getElementById('loginButton').style.display = 'block';
	  document.getElementById('logoutButton').style.display = 'none';
	}
}

}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
