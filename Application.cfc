<cfcomponent extends="org.corfield.framework" output="false">

	<cfset this.name="MRT6">
    <cfset this.sessionManagement=true>
    <cfset this.sessiontimeout = createTimeSpan(0,0,2,0)>
    <cfset this.secureJSON = "false" >
    <cfset this.secureJSONPrefix = "">
    
    <cfset variables.framework = {trace = false}>  

    <!--- set up environment specific variables --->
    <cfset this.dataSource="mrt2"> 
    
    <cffunction name="setupSession">
    	<cfset controller( 'security.session' )>
    </cffunction>
    
    <cffunction name="setupRequest">
	  
        <!--- set http headers --->
        <cfheader name="X-Frame-Options" value="deny">
        <cfheader name="X-XSS-Protection" value="1">
        
        <cfset controller( 'security.authorize' )>
        <cfset controller( 'security.chkPermissions' )>

    </cffunction>
   
    
    <cffunction name="setupApplication">
    	<cfset bf = new framework.ioc( "model" )>
        <cfset setBeanFactory( bf )>
        <cfset controller( 'security.appSetup' )>
    </cffunction>
        
</cfcomponent>

