<!--- <div class="row">
    <div class="col-lg-12" id="businessHero">
		<div class="hero-unit">
		    <img class="img-responsive" src="<cfoutput>/assets/businessImages/#rc.getBusiness.heroImageURL#</cfoutput>">
		</div>
    </div>
</div> --->

<cfobject component="controllers.utils" name="application.utils" type="component">

<cfif rc.owner>
	<div class="row" style="margin-bottom:10px;">	

		<div class="col-lg-12 text-center">
	        <div class="btn-group hidden-xs">
	            <a href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>" class="btn btn-primary">Manage reminders</a>
	            <a href="/<cfoutput>#rc.getBusiness.urlslug#</cfoutput>/" class="btn btn-primary">View page</a>
	            <a href="<cfoutput>#buildUrl("business.edit")#&businessId=#rc.businessId#</cfoutput>" id="viewAdminUsers" class="btn btn-primary">Edit page</a>
	            <a href="<cfoutput>#buildUrl("business.delete")#&businessId=#rc.businessId#</cfoutput>" id="deleteAdminUsers" class="btn btn-primary" onclick="return confirm('Are you sure you want to delete this page?');">Delete page</a>
	        </div>
	    </div>

	    <div class="col-lg-12 visible-xs">
	        <a href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>" class="btn btn-primary visible-xs">Manage reminders</a>
	        <a href="/<cfoutput>#rc.getBusiness.urlslug#</cfoutput>/" class="btn btn-primary visible-xs">View page</a>
	        <a href="<cfoutput>#buildUrl("business.edit")#&businessId=#rc.businessId#</cfoutput>" id="viewAdminUsersMob" class="btn btn-primary visible-xs">Edit page</a>
	        <a href="<cfoutput>#buildUrl("business.delete")#&businessId=#rc.businessId#</cfoutput>" id="deleteAdminUsers" class="btn btn-primary visible-xs" onclick="return confirm('Are you sure you want to delete this page?');">Delete page</a>
	    </div>

	</div>
</cfif>

<div class="row">
	
	<div class="col-lg-6 col-lg-push-3 businessDivider" id="businessReminders">
		<div class="businessReminderPadding" style="display: inline-block; margin-bottom: 10px; width: 100%;">
			<span id="businessTitle"><strong><cfoutput>#rc.getBusiness.title#</cfoutput> - Reminders</strong></span><br/>
			Let us remind you as important dates approach so you can keep your life organised.
		</div>
		<div style="display:none;" class='alert alert-danger reminderError businessReminderPadding'></div>
        <div style="display:none;" class='alert alert-success reminderSuccess businessReminderPadding'></div>
		<div class="businessReminderPadding" style="display: inline-block; margin-bottom: 10px; width: 100%;">
			<input type="text" class="form-control" id="getBusinessReminders" placeholder="Search reminders">
		</div>
		<div style="display: inline-block; margin-bottom: 10px; width: 100%;">
			<div class="col-lg-1 col-xs-1 mobileNoPadding">
				<input type="checkbox" id="checkAll" style="margin-right:10px;">
				
			</div>
			<div class="col-xs-1 visible-xs">
				<button id="addAllSelectMob" type="button" class="btn btn-default-yellow"><span class="glyphicon glyphicon-check"></span> Add Selected </button>
			</div>
			<div class="col-lg-5 hidden-xs">
				<button id="addAllSelect" type="button" class="btn btn-default-yellow"><span class="glyphicon glyphicon-check"></span> Add Selected </button>
			</div>
			<cfif structKeyExists(SESSION, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn AND NOT rc.chkSubscription.recordcount>
				<div class="col-lg-6 hidden-xs">
					<button type="button" class="btn btn-default-green pull-right subscribeBtn" data-toggle="modal" data-target="#subscribeModal"> Subscribe </button>
				</div>
				<div class="col-lg-10 visible-xs">
					<button type="button" class="btn btn-default-green pull-right subscribeBtn" data-toggle="modal" data-target="#subscribeModal"> Subscribe </button>
				</div>
			<cfelseif NOT structKeyExists(SESSION, 'auth') OR NOT structKeyExists(session.auth, 'isLoggedIn') OR NOT session.auth.isLoggedIn>
				<cfset itemLink = buildUrl("main.registration") & '&subscriptionId=' & rc.getBusiness.businessId>
				<div class="col-lg-6 hidden-xs">
					<a type="button" class="btn btn-default-green pull-right subscribeBtn" href="<cfoutput>#itemLink#</cfoutput>"> Subscribe </a>
				</div>
				<div class="col-lg-10 visible-xs">
					<a type="button" class="btn btn-default-green pull-right subscribeBtn" href="<cfoutput>#itemLink#</cfoutput>"> Subscribe </a>
				</div>
			</cfif>
			<cfif structKeyExists(SESSION, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn AND rc.chkSubscription.recordcount>
				<div class="col-lg-6 hidden-xs">
					<button type="button" class="btn btn-default-green pull-right unsubscribeBtn"> Unsubscribe </button>
				</div>
				<div class="col-lg-10 visible-xs">
					<button type="button" class="btn btn-default-green pull-right unsubscribeBtn"> Unsubscribe </button>
				</div>
			</cfif>
				
		</div>
		<ul class="list-group">
            <cfoutput query="rc.getReminders">
                
                <cfsilent>
	                <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
		            	<cfset itemLink = buildUrl("users.businessreminder") & '&rid=' & rc.getReminders.reminderId>
		            <cfelse>
	                	<cfset itemLink = buildUrl("main.registration") & '&msg=login&slug=' & rc.getBusiness.urlslug  & '&rid=' & rc.getReminders.reminderId>
	                </cfif>
                </cfsilent>

                <li class="list-group-item <cfif childcount>noBorder</cfif>">
                	
                	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 mobileNoPadding">
                		<input type="checkbox" name="rid" class="cbrid" value="#rc.getReminders.reminderId#">
                	</div>
                	<div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 mobileNoPadding">
	                    <div class="titleContainer">
	                    	<div style="float:right;">
		                    	<a href="#itemLink#" style="float:right;" type="button" class="<cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>singleReminderBtn</cfif> btn btn-default-yellow" data-rid="#rc.getReminders.reminderId#"><span class="glyphicon glyphicon-pushpin"></span> Add </a><br/>
		                    </div>
		                    #title#
		                </div>

	                    <div>
	                    	
		                    <p style="margin-bottom: -4px;">
		                    	#application.utils.DateFormatExtended(eventDate, "d mmmm yyyy")# - #TimeFormat(eventTime, "HH:mm")#
		                    	<cfif len(rc.getReminders.comments)><a href="##" style="float:right;" data-toshow="moreInfoContent#rc.getReminders.currentRow#" class="showMoreInfo">More info</a></cfif>
			                </p>
		                    <cfif len(rc.getReminders.comments)>
		                    	<p class="moreInfoContent#rc.getReminders.currentRow# moreInfoContent" style="margin-top:10px">#rc.getReminders.comments#</p>
	                    	</cfif>
		                    
	                    </div>
	                </div>
	            </li>
	            <cfif childcount>
		            <li class="childcountRow">
		                <div class="row clearfix">
			                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                    	reminder set #childCOunt# time<cfif childCOunt GT 1>s</cfif>
		                    </div>
	                    </div>
                   	</li>
                </cfif>

            </cfoutput>
        </ul>
	</div>
	<div class="col-lg-3 col-lg-pull-6 businessDivider">
		<cfif len(rc.getBusiness.logo)>
			<img class="img-responsive" src="<cfoutput>/assets/businessImages/#rc.getBusiness.logo#</cfoutput>">
		</cfif>
		<cfif isValid('URL', rc.getBusiness.website)>
			<p><a href="<cfoutput>#rc.getBusiness.website#</cfoutput>" target="_blank"><cfoutput>#rc.getBusiness.website#</cfoutput></a></p>	
		</cfif>
		<p><cfoutput>#ParagraphFormat(rc.getBusiness.aboutContent)#</cfoutput></p>
	</div>
	<div class="col-lg-3 businessDivider">
		<p><strong>How To</strong></p>
		<p>Click add button to select a single reminder</P>
		<P><button type="button" class="btn btn-default-yellow"><span class="glyphicon glyphicon-pushpin"></span> Add </button></P>
		<p>To add more than one reminder tick the check box for each reminder then click the add selected button</p>
		<P><button type="button" id="addAllSelect2" class="btn btn-default-yellow"><span class="glyphicon glyphicon-pushpin"></span> Add Selected </button></P>
	</div>
</div>