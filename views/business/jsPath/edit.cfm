<script src="/assets/js/bootstrap-datetimepicker.js"></script>
<script src="/assets/clipboard.js-master/dist/clipboard.min.js"></script>

<script>
    var clipboard = new Clipboard('.copyURL');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('.share_button').click(function(e){

            var reminderTitle = $(this).data('remindertitle');

            e.preventDefault();
            FB.ui(
            {
                method: 'feed',
                name: reminderTitle,
                link: ' http://www.mustrememberthat.com/',
                picture: 'http://www.mustrememberthat.com/images/MRT-facebook-icon5.png',
                caption: 'I have just remembered this thanks to Must Remember That',
                description: 'MUST REMEMBER THAT - Never Forget Again',
                message: ''
            });
        });

        /*$(window).bind('resize load', function() {
            if ($(this).width() < 767) {
                $('#addReminderDiv').removeClass('in');
                $('#addReminderDiv').addClass('out');
            } else {
                $('#addReminderDiv').removeClass('out');
                $('#addReminderDiv').addClass('in');
            }
        });*/

        $(document.body).on("click", "#viewAdminUsersMob", function(){

            $('#displayAdminUsersMob').slideToggle();

        });

        $(document.body).on("click", "#viewAdminUsers", function(){

            $('#displayAdminUsers').slideToggle();

        });

        $(document.body).on("change", "#maincategory", function(){

            $('#subCatContainer').show();

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=' + this.value,
                success: function(data){
                    $('#category').html(data);
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });
        
    });

    // date picker
    $('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    // time picker
    $('.form_time').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
</script>