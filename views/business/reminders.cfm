<div class="row">
    
    <div class="col-lg-12 text-center">
        <div class="btn-group hidden-xs">
            <a href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>" class="btn btn-primary">Manage reminders</a>
            <a href="/<cfoutput>#rc.getBusiness.urlslug#</cfoutput>/" class="btn btn-primary">View page</a>
            <a href="<cfoutput>#buildUrl("business.edit")#&businessId=#rc.businessId#</cfoutput>" id="viewAdminUsers" class="btn btn-primary">Edit Page</a>
            <a href="<cfoutput>#buildUrl("business.delete")#&businessId=#rc.businessId#</cfoutput>" id="deleteAdminUsers" class="btn btn-primary" onclick="return confirm('Are you sure you want to delete this page?');">Delete page</a>
        </div>
    </div>

    <div id="mobReminderDiv" class="col-lg-5">
        
        <a href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>" class="visible-xs btn btn-primary">Manage reminders</a>
        <a href="/<cfoutput>#rc.getBusiness.urlslug#</cfoutput>/" class="visible-xs btn btn-primary">View page</a>
        <a href="<cfoutput>#buildUrl("business.edit")#&businessId=#rc.businessId#</cfoutput>" id="viewAdminUsers" class="visible-xs btn btn-primary">Edit Page</a>
        <a href="<cfoutput>#buildUrl("business.delete")#&businessId=#rc.businessId#</cfoutput>" id="deleteAdminUsers" class="visible-xs btn btn-primary" onclick="return confirm('Are you sure you want to delete this page?');">Delete page</a>
        <button style="width:100%; margin-bottom:10px;" class="addMobReminderDiv visible-xs btn btn-primary" data-toggle="dropdown" data-target="#addMobReminderDiv">Add a reminder</button>

        <div style="margin-top:10px;" class="well bs-component dropdown in" id="addMobReminderDiv">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="newReminder"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Add Business Reminder</legend>
                    <div class="form-group col-md-12">
                        <label for="title">Title</label>
                        <input type="text" autocomplete="off" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Category</label>
                        <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                            <option value="">Select Category</option>
                            <cfoutput query="rc.getMainCats">
                                <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                            </cfoutput>
                        </select>
                    </div>
                    <div id="subCatContainer" class="form-group col-md-12">
                        <label for="category">Sub Category</label>
                        <select id="category" name="category" class="form-control" data-role="none">   
                            <cfoutput query="rc.getCats">
                                <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                            </cfoutput>  
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Date of event</label>
                        <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.rdate#</cfoutput>" />
                        <div class='input-group date form_date'>
                            <input type="text" data-date-format="Do MMMM YYYY" data-link-field="rdate" autocomplete="off" id="inputEmail" class="form-control" <cfif len(rc.rdate)>value="<cfoutput>#day(rc.rdate)# #monthAsString(month(rc.rdate))# #year(rc.rdate)#</cfoutput>"</cfif>>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Time of event</label>
                        <div class='input-group date form_time' id='datetimepicker3'>
                            <input type='text' class="form-control" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Send me reminder</label>
                        <select id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                            <cfloop from="1" to="100" index="i"><cfoutput>
                                <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                            </cfoutput></cfloop>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">&nbsp;</label>
                        <select id="remindertime" name="remindertime" class="form-control" data-role="none">              
                            <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minute(s) before</option>
                            <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour(s) before</option>
                            <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day(s) Before</option>
                            <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week(s) Before</option>
                            <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month(s) Before</option>
                        </select>
                    </div>
                    <div id="reminderActualTimeDiv" class="form-group col-md-6">
                        <label for="inputEmail">Reminder Time</label>
                        <div class='input-group date form_time' id='datetimepicker4'>
                            <input type='text' class="form-control" name="reminderActualTime" id="reminderActualTime" value="<cfoutput>#rc.reminderActualTime#</cfoutput>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">How frequent</label>
                        <select id="interval" name="interval" class="form-control" data-role="none">              
                            <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                            <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                            <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                            <option <cfif rc.interval EQ 'weely'>selected="selected"</cfif> value="weekly">Weekly</option>
                            <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                            <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Comments</label>
                        <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Location</label>
                        <input name="location" class="form-control" id="keywordAttraction" type="text" value="" placeholder="Enter address">
                        <input type="hidden" id="latitude" name="latitude" value=""/>
                        <input type="hidden" id="longitude" name="longitude" value=""/>
                    </div>
                    <div class="form-group col-md-12">
                        <div id="mapContainer" style="height:150px"></div>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-default" type="reset">Cancel</button>
                        <button class="btn btn-primary" type="submit">Set Reminder</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div>

    <div style="margin-top:10px;" class="col-lg-7 hidden-xs">
        <div class="col-lg-12 text-center">
            <div class="btn-group">
                <cfif rc.isArchive>
                    <a style="margin-top:10px;" class="btn btn-default" href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>&">Active</a><button style="margin-top:10px;" class="btn btn-primary">Expired</button>
                <cfelse>
                    <button style="margin-top:10px;" class="btn btn-primary">Active</button><a style="margin-top:10px;" class="btn btn-default" href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>&isArchive=1">Expired</a>
                </cfif>
            </div>
        </div>
        <div class="well bs-component collapse in" style="background-color:#fff;">
            <legend><cfif NOT rc.isArchive><cfoutput>#rc.title#</cfoutput>Business Reminders<cfelse>Old Business Reminders</cfif></legend>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Reminder</th>
                            <th>Date</th>
                            <th colspan="3"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query="rc.getReminders">
                                <tr>
                                    <td width="70%"><a href="<cfoutput>#buildUrl("business.updateReminder")#&reminderId=#reminderId#&businessId=#rc.businessId#</cfoutput>">#rc.getReminders.title#</a></td>
                                    <td width="15%">#dateFormat(rc.getReminders.eventDate, 'dd/mm/yyyy')#</td>
                                    <td width="5%">
                                        <a class="share_button" data-reminderTitle="#jsStringFormat(rc.getReminders.title)#" href="##"><i class="fa fa-facebook"></i></a>
                                    </td>
                                    <td width="5%">
                                        <a href="<cfoutput>mailto:?subject=MUST REMEMBER THAT - #title#&body=Hi, thought you might want a quick reminder. #title# #DateFormat(eventDate, "dd mmmm yyyy")# http://www.mustrememberthat.com/index.cfm?action=main.singleReminder%26reminderId=#reminderId#</cfoutput>"><i class="fa fa-envelope"></i></a>
                                    </td>
                                    <td width="5%">
                                        <a onclick="return confirm('Are you sure you want to delete this reminder?');" href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#&update=deleteReminder&reminderId=#reminderId#</cfoutput>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
            <cfif rc.totalPages GT 1> 
                <ul class="pagination">
                    <li <cfif 1 EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("business.reminders")#&pageNumber=#rc.pageNumber - 1#&businessId=#rc.businessId#</cfoutput>">&laquo;</a></li>
                    <cfloop from="#rc.startpage#" to="#rc.endpage#" index="i">
                       <li <cfif rc.pageNumber EQ i> class="active"</cfif>><a href="<cfoutput>#buildUrl("business.reminders")#&pageNumber=#i#&businessId=#rc.businessId#</cfoutput>"><cfoutput>#i#</cfoutput></a></li> 
                    </cfloop>
                    <li <cfif rc.totalPages EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("business.reminders")#&pageNumber=#rc.pageNumber + 1#&businessId=#rc.businessId#</cfoutput>">&raquo;</a></li>
                </ul>
            </cfif>
        </div>
    </div>

    <div class="visible-xs col-lg-7">
        <ul class="list-group">
            <cfoutput query="rc.getReminders">
                <li class="list-group-item">
                    <a href="#buildUrl("business.updateReminder")#&reminderId=#reminderId#&businessId=#rc.businessId#" data-ajax="false">
                        <span style="float:right;" class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-angle-double-right fa-stack-1x fa-inverse"></i>
                        </span>
                        #title#
                        <p style="font-size:16px; font-weight:normal; margin-bottom: -4px;">#DateFormat(eventDate, "dd mmmm yyyy")# - #TimeFormat(eventTime, "HH:mm")#</p>
                    </a>
                </li>
            </cfoutput>
        </ul>
        <cfif rc.totalPages GT 1> 
            <ul class="pagination">
                <li <cfif 1 EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#rc.pageNumber - 1#</cfoutput>">&laquo;</a></li>
                <cfloop from="#rc.startpage#" to="#rc.endpage#" index="i">
                   <li <cfif rc.pageNumber EQ i> class="active"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#i#</cfoutput>"><cfoutput>#i#</cfoutput></a></li> 
                </cfloop>
                <li <cfif rc.totalPages EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#rc.pageNumber + 1#</cfoutput>">&raquo;</a></li>
            </ul>
        </cfif>
    </div>
    

</div>
