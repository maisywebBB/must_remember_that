<div class="row">
    <div class="col-lg-7">
        <div class="well bs-component">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="addBusiness"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Create a page</legend>
                    <div class="form-group">
                        <label for="title">Name</label>
                        <input type="text" placeholder="Name" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Description</label>
                        <textarea name="aboutContent" class="form-control"><cfoutput>#rc.aboutContent#</cfoutput></textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Web Address</label>
                        <input type="text" placeholder="http://" id="website" name="website" class="form-control" value="<cfoutput>#rc.website#</cfoutput>">
                    </div>
                    <!--- <div class="form-group">
                        <label for="title">Header Image. Height to be 300px max</label>
                        <input type="file" id="heroImageURL" name="heroImageURL">
                    </div> --->
                    <div class="form-group">
                        <label for="title">Logo (160px X 160px)</label>
                        <input type="file" id="logo" name="logo">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Make Page Live</label>
                        <select id="isLive" name="isLive" class="form-control" data-role="none">              
                            <option <cfif rc.isLive EQ 1>selected="selected"</cfif> value="1">Yes</option>
                            <option <cfif rc.isLive EQ 0>selected="selected"</cfif> value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default" type="reset">Cancel</button>
                        <button class="btn btn-primary" type="submit">Create</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div>

    <div class="col-lg-5">
        <legend>Tips</legend>
        <ol>
            <li>The title of your business will be the basis of the final URL. Make it short but descriptive.</li>
            <li>A banner image will really make you're business page stand out and draw attention to itself.</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
            <li>Veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</li>
            <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</li>
        </ol>
    </div>

</div>
        