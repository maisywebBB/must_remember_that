<div class="row">
    
    <div class="col-lg-12 text-center">
        <div class="btn-group hidden-xs">
            <a href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>" class="btn btn-primary">Manage reminders</a>
            <a href="/<cfoutput>#rc.getBusiness.urlslug#</cfoutput>/" class="btn btn-primary">View page</a>
            <a href="#" class="btn btn-primary">Edit page</a>
            <a href="<cfoutput>#buildUrl("business.delete")#&businessId=#rc.businessId#</cfoutput>" id="deleteAdminUsers" class="btn btn-primary" onclick="return confirm('Are you sure you want to delete this page?');">Delete page</a>
        </div>
    </div>

    <div class="col-lg-12 visible-xs">
        <a href="<cfoutput>#buildUrl("business.reminders")#&businessId=#rc.businessId#</cfoutput>" class="btn btn-primary visible-xs">Manage reminders</a>
        <a href="/<cfoutput>#rc.getBusiness.urlslug#</cfoutput>/" class="btn btn-primary visible-xs">View page</a>
        <a href="#" class="btn btn-primary visible-xs">Edit page</a>
        <a href="<cfoutput>#buildUrl("business.delete")#&businessId=#rc.businessId#</cfoutput>" id="deleteAdminUsers" class="btn btn-primary visible-xs" onclick="return confirm('Are you sure you want to delete this page?');">Delete page</a>
    </div>
    
    <div class="col-lg-6 visible-xs">
        <div style="display: none;" id="displayAdminUsersMob">
            <legend>Admin Users</legend>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query="rc.getBusinessAdmins">
                                <tr>
                                    <td width="95%">#rc.getBusinessAdmins.firstname# #rc.getBusinessAdmins.lastname#</td>
                                    <td width="5%">
                                        <a onclick="return confirm('Are you sure you want to remove this administrator?');" href="<cfoutput>#buildUrl("business.edit")#&businessId=#rc.businessId#&update=deleteAdmin&userId=#rc.getBusinessAdmins.userId#</cfoutput>"><i class="fa fa-trash"></i></a>
                                    </td>
                                 </tr>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        
        <div style="margin-top:10px;" class="well bs-component collapse in">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="updateBusiness"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Update <cfoutput>#rc.title#</cfoutput></legend>
                    <div class="form-group">
                        <label for="title">URL - <a href="http://<cfoutput>#cgi.http_host#/#rc.urlslug#/</cfoutput>">http://<cfoutput>#cgi.http_host#/#rc.urlslug#/</cfoutput></a>    <i data-clipboard-text="http://<cfoutput>#cgi.http_host#/#rc.urlslug#/</cfoutput>" class="copyURL fa fa-copy"></i></label>
                    </div>
                    <div class="form-group visible-xs">
                        <label for="title">Share - 
                        <a class="share_button" data-reminderTitle="<cfoutput>#rc.title#</cfoutput>" href="##"><img src="/assets/images/fb.png" height="25px"></a>&nbsp;&nbsp;&nbsp;
                        <a href="whatsapp://send?text=MUST REMEMBER THAT - <cfoutput>#rc.title#</cfoutput> Hi, thought you might want a quick look at our page. http://<cfoutput>#cgi.http_host#/#rc.urlslug#/</cfoutput>"><img src="/assets/images/wa.png" height="25px">&nbsp;&nbsp;&nbsp;</a>
                        <a href="mailto:?subject=MUST REMEMBER THAT - <cfoutput>#rc.title#</cfoutput>&body=Hi, thought you might want a quick look at our page. http://<cfoutput>#cgi.http_host#/#rc.urlslug#/</cfoutput>"><img src="/assets/images/email.png" height="25px"></a>
                        </label>
                    </div>
                    <div class="form-group hidden-xs">
                        <label for="title">Share - 
                        <a class="share_button" data-reminderTitle="<cfoutput>#rc.title#</cfoutput>" href="##"><img src="/assets/images/fb.png" height="25px"></a>&nbsp;&nbsp;&nbsp;
                        <a href="mailto:?subject=MUST REMEMBER THAT - <cfoutput>#rc.title#</cfoutput>&body=Hi, thought you might want a quick look at our page. http://<cfoutput>#cgi.http_host#/#rc.urlslug#/</cfoutput>"><img src="/assets/images/email.png" height="25px"></a>
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">About Us Content</label>
                        <textarea name="aboutContent" class="form-control"><cfoutput>#rc.aboutContent#</cfoutput></textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Web Address</label>
                        <input type="text" placeholder="http://" id="website" name="website" class="form-control" value="<cfoutput>#rc.website#</cfoutput>">
                    </div>
                    <!--- <div class="form-group">
                        <label for="title">Banner Image</label>
                        <input type="file" id="heroImageURL" name="heroImageURL">
                    </div> --->
                    <div class="form-group">
                        <label for="title">Logo (160px X 160px)</label>
                        <input type="file" id="logo" name="logo">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Make Business Page Live</label>
                        <select id="isLive" name="isLive" class="form-control" data-role="none">              
                            <option <cfif rc.isLive EQ 1>selected="selected"</cfif> value="1">Yes</option>
                            <option <cfif rc.isLive EQ 0>selected="selected"</cfif> value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div>

    <div class="col-lg-6">
        
        <div style="margin-top:10px;" class="well bs-component collapse in">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="addadminUser"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Add administrator</legend>
                    <div class="form-group">
                        <label for="emailAddress">Administrator Email Address</label>
                        <input type="email" placeholder="Email Address" id="emailAddress" name="emailAddress" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Add</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>

        <div id="displayAdminUsers" class="hidden-xs">
            <legend>Admin Users</legend>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query="rc.getBusinessAdmins">
                                <tr>
                                    <td width="95%">#rc.getBusinessAdmins.firstname# #rc.getBusinessAdmins.lastname#</td>
                                    <td width="5%">
                                        <a onclick="return confirm('Are you sure you want to remove this administrator?');" href="<cfoutput>#buildUrl("business.edit")#&businessId=#rc.businessId#&update=deleteAdmin&userId=#rc.getBusinessAdmins.userId#</cfoutput>"><i class="fa fa-trash"></i></a>
                                    </td>
                                 </tr>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>

    </div>


</div>
        