﻿<cfsilent>
	<cfparam name="rc.message" default="#arrayNew(1)#">
	<cfparam name="rc.successMessage" default="#arrayNew(1)#">
</cfsilent>

<cfoutput>

<!-- begin brand -->
<div class="login-header">
    <div class="brand">
        <span class="logo"></span> Uber Guard Admin
        <small>Sign in</small>
    </div>
    <div class="icon">
        <i class="fa fa-sign-in"></i>
    </div>
</div>
<!-- end brand -->
<div class="login-content">
    <cfif not arrayIsEmpty(rc.message)>
        <div class="alert alert-danger fade in m-b-15">
            <cfloop array="#rc.message#" index="msg">
            #msg#
            </cfloop>
            <span data-dismiss="alert" class="close">x</span>
        </div>
    </cfif>
    <cfif not arrayIsEmpty(rc.successMessage)>
        <div class="alert alert-success fade in m-b-15">
            <cfloop array="#rc.successMessage#" index="msg">
            <p>#msg#</p>
            </cfloop>
            <span data-dismiss="alert" class="close">x</span>
        </div>
    </cfif>
    <form action="#buildUrl("login.login")#" method="POST" class="margin-bottom-0">
        <div class="form-group m-b-20">
            <input name="emailAddress" type="text" class="form-control input-lg" placeholder="Email Address" />
        </div>
        <div class="form-group m-b-20">
            <input name="password" type="password" class="form-control input-lg" placeholder="Password" />
        </div>
        <div class="login-buttons">
            <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
        </div>
    </form>
    <a data-toggle="modal" href="##passwordReset" class="pull-right need-help">Forgotten Password? </a><span class="clearfix"></span>
   
</div>

</cfoutput>

