<div class="login-header">
    <div class="brand">
        <span class="logo"></span> Pay Manager
        <small>Maintenance</small>
    </div>
    <div class="icon">
        <i class="fa fa-sign-in"></i>
    </div>
</div>

<!-- end brand -->
<div class="login-content">
    <h1 style="color:#999; text-align:center;">Temporarily Down for Maintenance</h1>
    <p style="color:#999; text-align:center;">We are performing scheduled maintenance. We should be back online shortly</p>
</div>