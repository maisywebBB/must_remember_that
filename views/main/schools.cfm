<div id="reminders">
    <div class="container">
        <div class="row">
            <div class="col-md-12 hpContent">
                <h3>School Reminder Service</h3>
                <p>Keep the parents up-to- date with reminder alerts for all important school dates including
                <ul>
                    <li>Term Dates</li>
                    <li>Dinner money</li>
                    <li>School trips</li>
                    <li>Cake day</li>
                    <li>Dressing up day</li>
                    <li>Clubs</li>
                    <li>School Fete</li>
                    <li>Panto</li>
                </ul>
                <p>Our service allows parents to select your important dates so they never forget. They will receive your alerts direct to their mobile phone.
                <p>Share your reminders via email, Facebook, twitter, Whatsapp
                <p><a href="<cfoutput>#buildUrl("business.addABusiness")#</cfoutput>" class="btn btn-primary">Create a business page now</a>
            </div>
        </div>
    </div>
</div>

