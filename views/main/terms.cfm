<!--- <cfquery name="deleteUsers">
  truncate table tbl_users
</cfquery>

<cfquery name="deleteReminders">
  truncate table tbl_reminders
</cfquery>

<cfquery name="qUsers">
    SELECT *
    FROM users
</cfquery>

<cfset variables.userObject = createObject("component","controllers.users")>

<cfoutput query="qUsers">

  <!--- create a password salt --->
  <cfset rc.passwordSalt = createUUID()>
  <cfset rc.userId = createUUID()>
  
  <!--- now hash the password + the password salt that the user passed --->
  <cfset rc.inputHash = Hash(qUsers.UserPassword & rc.passwordSalt, 'SHA-512') />

  <!--- now create the DOB --->
  <cftry>
      <cfset rc.dateOfBirth = '#listFirst(qUsers.dob, '/')#/#listGetAt(qUsers.dob, 2, '/')#/#listLast(qUsers.dob, '/')#'>
      <cfcatch type="any">
        <cfset rc.dateOfBirth = '1/1/2016'>
      </cfcatch>
  </cftry>
  
  <cftry>
      <cftransaction> 
          
          <!--- add to database --->
          <cfquery name="rc.createUser">
              INSERT INTO tbl_users
              (
                  userId,
                  firstname,
                  lastname,
                  emailAddress,
                  DOB,
                  password,
                  passwordSalt,
                  timezone,
                  userVerify,
                  remoteId
              )
              VALUES
              (
                  <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">,
                  <cfqueryparam cfsqltype="cf_sql_varchar" value="#qUsers.UserFirstName#">,
                  <cfqueryparam cfsqltype="cf_sql_varchar" value="#qUsers.UserLastName#">,
                  <cfqueryparam cfsqltype="cf_sql_varchar" value="#qUsers.UserEmail#">,
                  <cfqueryparam cfsqltype="cf_sql_date" value="#rc.dateOfBirth#">,
                  <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.inputHash#">,
                  <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.passwordSalt#">,
                  <cfqueryparam cfsqltype="cf_sql_varchar" value="#GetTimeZoneInfo().utcHourOffset#">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="0">,
                  <cfqueryparam cfsqltype="cf_sql_integer" value="#qUsers.id#">
              )
          </cfquery>

          <!--- select all the reminders for this user --->
          <cfquery name="qGetReminders">
            SELECT *
            FROM reminders
            WHERE userid = <cfqueryparam cfsqltype="cf_sql_integer" value="#qUsers.id#">
          </cfquery>

          <cfloop query="qGetReminders">
            
            <!--- create a password salt --->
            <cfset rc.reminderId = createUUID()>
          
            <!--- add to database --->
            <cfquery name="rc.createReminder">
                INSERT INTO tbl_reminders
                (
                    reminderId,
                    reminderType,
                    reminderKey,
                    title,
                    comments,
                    category,
                    interval,
                    remindertime,
                    rdate,
                    rtime
                )
                VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetReminders.title#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetReminders.comments#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="33">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetReminders.interval#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetReminders.remindertime#">,
                    <cfqueryparam cfsqltype="cf_sql_date" value="#qGetReminders.date#">,
                    <cfqueryparam cfsqltype="cf_sql_time" value="#qGetReminders.time#">
                )
            </cfquery>

            <!--- create the schedule --->
            <cfset rc.rdate = qGetReminders.date>
            <cfset rc.rtime = qGetReminders.time>
            <cfset rc.remindertime = qGetReminders.remindertime>
            <cfset rc.reminderActualTime = ''>
            <cfset rc.intervalPeriod = 1>
            <cfset rc.interval = qGetReminders.interval>
            <cfset createSchedule = variables.userObject.createSchedule(rc)>

            <cfset rc.title = "">
            <cfset rc.comments = "">
            <cfset rc.category = "">
            <cfset rc.interval = "">
            <cfset rc.remindertime = "">
            <cfset rc.rdate = "">
            <cfset rc.rtime = "">

        </cfloop>

     </cftransaction>
    
    <cfcatch type="any">
        <cfdump var="#cfcatch#"><cfabort>
    </cfcatch>
  </cftry>

</cfoutput>

<cfabort> --->

<h2>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THIS SITE</h2>
<h3>Terms of website use</h3>
<p>This terms of use (together with the documents referred to in it) tells you the terms of use on which you may make use of our website www.mustrememberthat.com (our site), whether as a guest or a registered user. Use of our site includes accessing, browsing, or registering to use our site.
<p>Please read these terms of use carefully before you start to use our site, as these will apply to your use of our site. We recommend that you print a copy of this for future reference. 
<p>By using our site, you confirm that you accept these terms of use and that you agree to comply with them. 
<p>If you do not agree to these terms of use, you must not use our site.
<h3>Other applicable terms</h3>
<p>These terms of use refer to the following additional terms, which also apply to your use of our site:
<ul>
<li><a href="https://www.mustrememberthat.com/privacy.cfm">Our Privacy Policy</a> , which sets out the terms on which we process any personal data we collect from you, or that you provide to us. By using our site, you consent to such processing and you warrant that all data provided by you is accurate. </li>
<li><a href="https://www.mustrememberthat.com/acceptance.cfm">Our Acceptable Use Policy</a> , which sets out the permitted uses and prohibited uses of our site. When using our site, you must comply with this Acceptable Use Policy.</li>
<li><a href="https://www.mustrememberthat.com/cookie.cfm">Our Cookie Policy</a> , which sets out information about the cookies on our site.</li>
</ul>
<h3>Information about us</h3>
<p>www.mustrememberthat.com is a site operated by Must Remember That LTD (“We"). We are registered in England and Wales under company number 0C400216 and have our registered office and trading address at 66 Abbey Fields Close, London, NW10 7EG</p>
 
<h3>Changes to these terms</h3>
<p>We may revise these terms of use at any time by amending this page. 
<p>Please check this page from time to time to take notice of any changes we make, as they are binding on you. 
<h3>Changes to our site</h3>
<p>We may update our site from time to time, and may change the content at any time. However, please note that any of the content on our site may be out of date at any given time, and we are under no obligation to update it.
<p>We do not guarantee that our site, or any content on it, will be free from errors or omissions.
<h3>Our Services</h3>
<p>We provide a reminder service based on dates and information provided by you.  We also allow third parties to advertise their products and services that may be relevant to you based on the information you have given ("Advertisers").  We will not allow Advertisers to contact you directly unless we have your explicit consent to do so in accordance with our <a href="https://www.mustrememberthat.com/privacy.cfm">Privacy Policy</a> .  If you choose to purchase goods or services from an Advertiser, the resulting legal contract is between you and the Advertiser and is subject to any other terms and conditions of the Advertiser.  These will be notified to you directly by the Advertiser.  There will be no contract of any kind between you and Must Remember That LTD in respect of the supply of any goods or services from this site or via Advertisers (other than this contract for the supply of services in respect of use of the site and our reminder service). We do not provide financial, investment or other advice nor do we provide a recommendation or endorsement of our Advertisers.   
<p>If you input incorrect information whilst use our service then we cannot accept any responsibility for any loss or damage suffered by you as a result.  
  
<h3>Accessing our site</h3>
<p>Our site is made available free of charge.
<p>We do not guarantee that our site, or any content on it, will always be available or be uninterrupted. Access to our site is permitted on a temporary basis. We may suspend, withdraw, discontinue or change all or any part of our site without notice. We will not be liable to you if for any reason our site is unavailable at any time or for any period.
<p>You are responsible for making all arrangements necessary for you to have access to our site.
<p>You are also responsible for ensuring that all persons who access our site through your internet connection are aware of these terms of use and other applicable terms and conditions, and that they comply with them.
<p>Our site is directed to people residing in the Worldwide. We do not represent that content available on or through our site is appropriate or available in other locations. We may limit the availability of our site or any service or product described on our site to any person or geographic area at any time.
<h3>Your account and password</h3>
<p>If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party.
<p>We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these terms of use.
<p>If you know or suspect that anyone other than you knows your user identification code or password, you must promptly notify us at <a href="https://www.mustrememberthat.com/contact.cfm">https://www.mustrememberthat.com/contact.cfm</a>.
<h3>Intellectual property rights</h3>
<p>We are the owner or the licensee of all intellectual property rights in our site, and in the material published on it.  Those works are protected by copyright laws and treaties around the world. All such rights are reserved.
<p>You may print off one copy, and may download extracts, of any page(s) from our site for your personal use and you may draw the attention of others within your organisation to content posted on our site.
<p>You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.
<p>Our status (and that of any identified contributors) as the authors of content on our site must always be acknowledged.
<p>You must not use any part of the content on our site for commercial purposes without obtaining a licence to do so from us or our licensors.
<p>If you print off, copy or download any part of our site in breach of these terms of use, your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.
<h3>No reliance on information</h3>
<p>The content on our site is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our site.
<p>Although we make reasonable efforts to update the information on our site, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up-to-date.
<h3>Limitation of our liability</h3>
<p>Nothing in these terms of use excludes or limits our liability for death or personal injury arising from our negligence, or our fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by English law.
<p>To the extent permitted by law, we exclude all conditions, warranties, representations or other terms which may apply to our site or any content on it, whether express or implied. 
<p>We will not be liable to any user for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:
<ul>
<li>use of, or inability to use, our site; or</li>
<li>use of or reliance on any content displayed on our site. </li>
<li>If you are a business user, please note that in particular, we will not be liable for:</li>
<li>loss of profits, sales, business, or revenue;</li>
<li>business interruption;</li>
<li>loss of anticipated savings;</li>
<li>loss of business opportunity, goodwill or reputation; or</li>
<li>any indirect or consequential loss or damage.</li>
</ul>
<p>If you are a consumer user, please note that we only provide our site for domestic and private use. You agree not to use our site for any commercial or business purposes, and we have no liability to you for any loss of profit, loss of business, business interruption, or loss of business opportunity.
<p>We will not be liable for any loss or damage caused by a virus, distributed denial-of-service attack, or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of our site or to your downloading of any content on it, or on any website linked to it.
<p>We assume no responsibility for the content of websites linked on our site. Such links should not be interpreted as endorsement by us of those linked websites. We will not be liable for any loss or damage that may arise from your use of them.
<h3>Uploading content to our site</h3>
<p>Whenever you make use of a feature that allows you to upload content to our site, or to make contact with other users of our site, you must comply with the content standards set out in our <a href="https://www.mustrememberthat.com/acceptance.cfm">Acceptable Use Policy</a>.
<p>You warrant that any such contribution does comply with those standards, and you will be liable to us and indemnify us for any breach of that warranty. [If you are a consumer user, this means you will be responsible for any loss or damage we suffer as a result of your breach of warranty.]
<p>Any content you upload to our site will be considered non-confidential and non-proprietary. You retain all of your ownership rights in your content, but you are required to grant us [and other users of the Site] a limited licence to use, store and copy that content and to distribute and make it available to third parties. The rights you license to us are described in the next paragraph (Rights you licence).
<p>We also have the right to disclose your identity to any third party who is claiming that any content posted or uploaded by you to our site constitutes a violation of their intellectual property rights, or of their right to privacy.
<p>We will not be responsible, or liable to any third party, for the content or accuracy of any content posted by you or any other user of our site.
<p>We have the right to remove any posting you make on our site if, in our opinion, your post does not comply with the content standards set out in our <a href="https://www.mustrememberthat.com/acceptance.cfm">Acceptable Use Policy</a>.
<p>The views expressed by other users on our site do not represent our views or values.
<p>You are solely responsible for securing and backing up your content.
<h3>Viruses</h3>
<p>We do not guarantee that our site will be secure or free from bugs or viruses.
<p>You are responsible for configuring your information technology, computer programmes and platform in order to access our site. You should use your own virus protection software.
<p>You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.
<h3>Linking to our site</h3>
<p>You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.
<p>You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.
<p>We reserve the right to withdraw linking permission without notice.
<p>The website in which you are linking must comply in all respects with the content standards set out in our <a href="https://www.mustrememberthat.com/acceptance.cfm">Acceptable Use Policy</a>.
<p>If you wish to make any use of content on our site other than that set out above, please contact <a href="https://www.mustrememberthat.com/contact.cfm">https://www.mustrememberthat.com/contact.cfm</a>.
<h3>Third party links and resources in our site</h3>
<p>Where our site contains links to other sites and resources provided by third parties, these links are provided for your information only.
<p>We have no control over the contents of those sites or resources.
<h3>Applicable law</h3>
<p>If you are a consumer, please note that these terms of use, its subject matter and its formation, are governed by English law. You and we both agree to that the courts of England and Wales will have non-exclusive jurisdiction. However, if you are a resident of Northern Ireland you may also bring proceedings in Northern Ireland, and if you are resident of Scotland, you may also bring proceedings in Scotland.
<p>If you are a business, these terms of use, its subject matter and its formation (and any non-contractual disputes or claims) are governed by English law. We both agree to the exclusive jurisdiction of the courts of England and Wales.
<h3>Contact us</h3>
<p>To contact us, please click <a href="https://www.mustrememberthat.com/contact.cfm">https://www.mustrememberthat.com/contact.cfm</a>.
<p>Thank you for visiting our site.


<!--- 
<h3>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THIS SITE</h3>
  <p><strong>Terms of website use</strong><br>
    This terms of use  (together with the documents referred to in it) tells you the terms of use on  which you may make use of our website www.rememberthat.com (our site), whether as a guest or a registered user. Use of  our site includes accessing, browsing, or registering to use our site.<br><br>
    Please read these terms  of use carefully before you start to use our site, as these will apply to your  use of our site. We recommend that you print a copy of this for future  reference. <br>By using our site, you  confirm that you accept these terms of use and that you agree to comply with  them. <br>If you do not agree to  these terms of use, you must not use our site.<br><br>
    <strong>Other applicable  terms<br>
    </strong>These terms of use refer  to the following additional terms, which also apply to your use of our site:</p>
  <ul>
    <li><a href="privacy.cfm">Our Privacy Policy</a>, which sets out the terms on which we process any personal data  we collect from you, or that you provide to us. By using our site, you consent  to such processing and you warrant that all data provided by you is accurate. </li>
    <li><a href="acceptance.cfm">Our Acceptable Use Policy</a>, which sets out the permitted uses and prohibited uses  of our site. When using our site, you must comply with this Acceptable Use  Policy. </li>
    <li><a href="cookie.cfm">Our Cookie Policy</a>, which sets out information about the cookies on our site.</li>
  </ul>
  <p><strong>Information about us</strong><br>www.mustrememberthat.com is  a site operated by Must Remember That LLP ("We"). We are registered in  England and Wales under company number 0C400216 and have our  registered office and trading address at 80, Marlin Square, Abbots Langley,  Hertfordshire, WD5 0EG. Our VAT  number is [VAT NUMBER].<br><br>
    <strong>Changes to these  terms</strong><br>We may revise these terms  of use at any time by amending this page. <br>Please check this page  from time to time to take notice of any changes we made, as they are binding on  you. <br><br>
    <strong>Changes to our site</strong><br>We may update our site  from time to time, and may change the content at any time. However, please note  that any of the content on our site may be out of date at any given time, and  we are under no obligation to update it.<br>We do not guarantee that  our site, or any content on it, will be free from errors or omissions.<br><br>
    <strong>our services<br>
    </strong>We provide a reminder service  based on dates and information provided by you.  We also allow third parties to advertise their products and services  that may be relevant to you based on the information you have given ("Advertisers"). We will not allow Advertisers to contact you  directly unless we have your explicit consent to do so in accordance with our <u>Privacy  Policy</u> [include link  to privacy policy]. If you choose to purchase goods or services from an Advertiser, the  resulting legal contract is between you and the Advertiser and is subject to  any other terms and conditions of the Advertiser. These will be notified to you directly by the  Advertiser. There will be no contract of  any kind between you and Must Remember That LLP in respect of the supply of any  goods or services from this site or via Advertisers (other than this contract  for the supply of services in respect of use of the site and our reminder  service). We do not provide financial, investment or other advice nor do we  provide a recommendation or endorsement of our Advertisers.
  </p>
  <p>If you input incorrect  information whilst use our service then we cannot accept any responsibility for  any loss or damage suffered by you as a result. <br><br>
    <strong>Accessing our site</strong><br>Our site is made  available free of charge [for the first twelve months and thereafter an annual charge of  Â£xx will be made and the credit/debit card you provided on registration will be  debited automatically]. <br>We do not guarantee that  our site, or any content on it, will always be available or be uninterrupted.  Access to our site is permitted on a temporary basis. We may suspend, withdraw,  discontinue or change all or any part of our site without notice. We will not  be liable to you if for any reason our site is unavailable at any time or for  any period.<br>You are responsible for  making all arrangements necessary for you to have access to our site.<br>You are also responsible  for ensuring that all persons who access our site through your internet  connection are aware of these terms of use and other applicable terms and  conditions, and that they comply with them.<br>Our site is directed to  people residing in the United Kingdom. We do not represent that content  available on or through our site is appropriate or available in other  locations. We may limit the availability of our site or any service or product  described on our site to any person or geographic area at any time. If you  choose to access our site from outside the United Kingdom, you do so at your  own risk.<br><br>
    <strong>Your account and  password</strong><br>If you choose, or you are  provided with, a user identification code, password or any other piece of  information as part of our security procedures, you must treat such information  as confidential. You must not disclose it to any third party.<br>
    We have the right to  disable any user identification code or password, whether chosen by you or  allocated by us, at any time, if in our reasonable opinion you have failed to  comply with any of the provisions of these terms of use.<br>
    If you know or suspect that  anyone other than you knows your user identification code or password, you must  promptly notify us<a href="contact.cfm"> click here</a>.<br><br>
    <strong>Intellectual property  rights</strong><br>We are the owner or the  licensee of all intellectual property rights in our site, and in the material  published on it. Those works are  protected by copyright laws and treaties around the world. All such rights are  reserved.<br>You may print off one  copy, and may download extracts, of any page(s) from our site for your personal  use and you may draw the attention of others within your organisation to  content posted on our site.<br>You must not modify the  paper or digital copies of any materials you have printed off or downloaded in  any way, and you must not use any illustrations, photographs, video or audio  sequences or any graphics separately from any accompanying text.<br>Our status (and that of  any identified contributors) as the authors of content on our site must always  be acknowledged.<br>You must not use any part  of the content on our site for commercial purposes without obtaining a licence  to do so from us or our licensors.<br>If you print off, copy or  download any part of our site in breach of these terms of use, your right to  use our site will cease immediately and you must, at our option, return or destroy  any copies of the materials you have made.<br>No reliance on  information<br>The content on our site  is provided for general information only. It is not intended to amount to  advice on which you should rely. You must obtain professional or specialist  advice before taking, or refraining from, any action on the basis of the  content on our site.<br>Although we make  reasonable efforts to update the information on our site, we make no  representations, warranties or guarantees, whether express or implied, that the  content on our site is accurate, complete or up-to-date.<br><br>
    <strong>Limitation of our  liability</strong><br>Nothing in these terms of  use excludes or limits our liability for death or personal injury arising from  our negligence, or our fraud or fraudulent misrepresentation, or any other  liability that cannot be excluded or limited by English law.<br>To the extent permitted  by law, we exclude all conditions, warranties, representations or other terms  which may apply to our site or any content on it, whether express or implied. <br>We will not be liable to  any user for any loss or damage, whether in contract, tort (including  negligence), breach of statutory duty, or otherwise, even if foreseeable,  arising under or in connection with:</p>
  <ul>
    <li>use of, or inability to use, our site; or</li>
    <li>use of or reliance on any content displayed on  our site. </li>
  </ul>
  <p>If you are a business  user, please note that in particular, we will not be liable for:</p>
  <ul>
    <li>loss of profits, sales, business, or revenue;</li>
    <li>business interruption;</li>
    <li>loss of anticipated savings;</li>
    <li>loss of business opportunity, goodwill or  reputation; or</li>
    <li>any indirect or consequential loss or damage.</li>
  </ul>
  <p>If you are a consumer  user, please note that we only provide our site for domestic and private use.  You agree not to use our site for any commercial or business purposes, and we have  no liability to you for any loss of profit, loss of business, business  interruption, or loss of business opportunity.<br>We will not be liable for  any loss or damage caused by a virus, distributed denial-of-service attack, or  other technologically harmful material that may infect your computer equipment,  computer programs, data or other proprietary material due to your use of our  site or to your downloading of any content on it, or on any website linked to  it.<br>We assume no  responsibility for the content of websites linked on our site. Such links  should not be interpreted as endorsement by us of those linked websites. We  will not be liable for any loss or damage that may arise from your use of them.<br><br>
    <strong>Uploading content to our site</strong><br>
    Whenever you make use of  a feature that allows you to upload content to our site, or to make contact  with other users of our site, you must comply with the content standards set  out in our <a href="acceptance.cfm">Acceptable Use Policy</a>.<br>You warrant that any such  contribution does comply with those standards, and you will be liable to us and  indemnify us for any breach of that warranty. [If you are a consumer user, this  means you will be responsible for any loss or damage we suffer as a result of  your breach of warranty.]<br>Any content you upload to  our site will be considered non-confidential and non-proprietary. You retain  all of your ownership rights in your content, but you are required to grant us  [and other users of the Site] a limited licence to use, store and copy that  content and to distribute and make it available to third parties. The rights  you license to us are described in the next paragraph (Rights you licence).<br>We also have the right to  disclose your identity to any third party who is claiming that any content  posted or uploaded by you to our site constitutes a violation of their  intellectual property rights, or of their right to privacy.<br>We will not be  responsible, or liable to any third party, for the content or accuracy of any  content posted by you or any other user of our site.<br>
    We have the right to  remove any posting you make on our site if, in our opinion, your post does not  comply with the content standards set out in our <a href="acceptance.cfm">Acceptable Use Policy</a>.<br>The views expressed by  other users on our site do not represent our views or values.<br>You are solely  responsible for securing and backing up your content.<br><br>
    <strong>Viruses</strong><br>We do not guarantee that  our site will be secure or free from bugs or viruses.<br>You are responsible for  configuring your information technology, computer programmes and platform in  order to access our site. You should use your own virus protection software.<br>You must not misuse our  site by knowingly introducing viruses, trojans, worms, logic bombs or other  material which is malicious or technologically harmful. You must not attempt to  gain unauthorised access to our site, the server on which our site is stored or  any server, computer or database connected to our site. You must not attack our  site via a denial-of-service attack or a distributed denial-of service attack.  By breaching this provision, you would commit a criminal offence under the  Computer Misuse Act 1990. We will report any such breach to the relevant law  enforcement authorities and we will co-operate with those authorities by  disclosing your identity to them. In the event of such a breach, your right to  use our site will cease immediately.<br><br>
    <strong>Linking to our site</strong><br>You may link to our home  page, provided you do so in a way that is fair and legal and does not damage  our reputation or take advantage of it.<br>You must not establish a  link in such a way as to suggest any form of association, approval or  endorsement on our part where none exists.<br>You must not establish a  link to our site in any website that is not owned by you.<br>Our site must not be  framed on any other site, nor may you create a link to any part of our site  other than the home page.<br>We reserve the right to  withdraw linking permission without notice.<br>
    The website in which you  are linking must comply in all respects with the content standards set out in  our <a href="acceptance.cfm">Acceptable Use Policy</a>.<br>
    If you wish to make any  use of content on our site other than that set out above, please<a href="contact.cfm"> contact us</a>.<br><br>
    <strong>Third party links and  resources in our site</strong><br>Where our site contains  links to other sites and resources provided by third parties, these links are  provided for your information only.<br>We have no control over  the contents of those sites or resources.<br><br>
    <strong>Applicable law</strong><br>If you are a consumer,  please note that these terms of use, its subject matter and its formation, are  governed by English law. You and we both agree to that the courts of England  and Wales will have non-exclusive jurisdiction. However, if you are a resident  of Northern Ireland you may also bring proceedings in Northern Ireland, and if  you are resident of Scotland, you may also bring proceedings in Scotland.<br>If you are a business,  these terms of use, its subject matter and its formation (and any  non-contractual disputes or claims) are governed by English law. We both agree  to the exclusive jurisdiction of the courts of England and Wales.<br><br>
    <strong>Contact us</strong><br><br>
    To contact us, please  <a href="contact.cfm">click here</a>.<br><br>
    Thank you for visiting  our site.</p> --->

            