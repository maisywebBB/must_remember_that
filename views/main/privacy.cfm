<h3>Privacy Policy</h3>

  <p>Must Remember That LLP ("We") are committed to  protecting and respecting your privacy.<br>
    This policy (together with our <a href="terms.cfm">terms of use</a> and any other documents referred to on it) sets out the basis on  which any personal data we collect from you, or that you provide to us, will be  processed by us. Please read the  following carefully to understand our views and practices regarding your  personal data and how we will treat it. By visiting <a href="http://www.mustrememberthat.com">www.mustrememberthat.com</a> you are  accepting and consenting to the practices described in this policy. <br>
    For the purpose of the Data Protection Act 1998 (the Act), the data controller is Must Remember That LLP of 80,  Marlin Square, Abbots Langley, Hertfordshire, WD5 0EG. </p>
  <p><a name="main">Information we may collect from you</a><br>
    We may collect and  process the following data about you:</p>
  <ul>
    <li><strong>Information  you give us.</strong> You may give us information about you by filling in forms on  our site <a href="http://www.mustrememberthat.com">www.mustrememberthat.com </a> (our site) or by  corresponding with us by phone, e-mail or otherwise. This includes information  you provide when you register to use our site, subscribe to our service, [participate  in discussion boards or other social media functions on our site],  [enter a  competition, promotion or survey], and when you report a problem with our site. The information you give us may  include your name, address, e-mail address and phone number, financial and  credit card information, personal description and photograph, key dates  provided by you in order to use our service. </li>
    <li><strong>Information  we collect about you.</strong> With regard to each of your visits to our site we may  automatically collect the following information:</li>
    <li>technical information, including the Internet  protocol (IP) address used to connect your computer to the Internet, your login  information, browser type and version, time zone setting, browser plug-in types  and versions, operating system and platform; </li>
    <li>information about your visit, including the full  Uniform Resource Locators (URL) clickstream to, through and from our site  (including date and time); products you viewed or searched for; page response  times, download errors, length of visits to certain pages, page interaction  information (such as scrolling, clicks, and mouse-overs), and methods used to  browse away from the page and any phone number used to call our customer  service number. </li>
    <li><strong>Information  we receive from other sources.</strong> We may receive information about you if you  use any of the other websites we operate or the other services we provide. [In  this case we will have informed you when we collected that data that it may be  shared internally and combined with data collected on this site.] We are also  working closely with third parties (including, for example, business partners,  sub-contractors in technical, and payment services, advertising networks,  analytics providers, search information providers) and may receive information  about you from them.</li>
  </ul>
  <p><strong>Cookies</strong><br>
    Our website uses cookies  to distinguish you from other users of our website. This helps us to provide  you with a good experience when you browse our website and also allows us to  improve our site. For detailed information on the cookies we use and the  purposes for which we use them see our <a href="cookie.cfm">Cookie policy</a>.<br>
    Uses made of the  information<br>
    We use information held  about you in the following ways:</p>
  <p><strong>Information  you give to us.</strong> We will use this information:</p>
  <ul>
    <li>to carry out our obligations arising from any  contracts entered into between you and us and to provide you with the  information, products and services that you request from us;</li>
    <li>to provide you with information about other goods  and services we offer that are similar to those that you have already purchased  or enquired about;</li>
    <li>to provide you, or permit selected third parties  to provide you, with information about goods or services we feel may interest  you. If you are an existing customer, we will only contact you by electronic  means (e-mail or SMS) with information about goods and services similar to  those which were the subject of a previous sale or negotiations of a sale to  you. If you are a new customer, and where we permit selected third parties to  use your data, we (or they) will contact you by electronic means only if you  have consented to this. If you do not want us to use your data in this way, or  to pass your details on to third parties for marketing purposes, please tick  the relevant box situated on the form on which we collect your data (the  registration form);</li>
    <li>to notify you about changes to our service;</li>
    <li>to ensure that content from our site is  presented in the most effective manner for you and for your computer. </li>
    </ul>
  <p><strong>Information  we collect about you.</strong> We will use this information:</p>
  <ul>
    <li>to administer our site and for internal  operations, including troubleshooting, data analysis, testing, research,  statistical and survey purposes;</li>
    <li>to improve our site to ensure that content is  presented in the most effective manner for you and for your computer; </li>
    <li>to allow you to participate in interactive  features of our service, when you choose to do so;</li>
    <li>as part of our efforts to keep our site safe and  secure;</li>
    <li>to measure or understand the effectiveness of  advertising we serve to you and others, and to deliver relevant advertising to  you;</li>
    <li>to make suggestions and recommendations to you  and other users of our site about goods or services that may interest you or  them.</li>
    </ul>
  <p><strong>Information  we receive from other sources. </strong></p>
  <ul>
    <li>We may combine this information with  information you give to us and information we collect about you. We may use  this information and the combined information for the purposes set out above  (depending on the types of information we receive).</li>
  </ul>
  <p><strong>Disclosure of your  information</strong><br>
    We may share your  personal information with any member of our group,  which means our subsidiaries, our ultimate holding company and its  subsidiaries, as defined in section 1159 of the UK Companies Act 2006.<br>
    We may share your information  with selected third parties including:</p>
  <ul>
    <li>Business partners, suppliers and sub-contractors  for the performance of any contract we enter into with them or you.</li>
    <li>Advertisers and advertising networks that  require the data to select and serve relevant adverts to you and others. We do not disclose information about  identifiable individuals to our advertisers, but we may provide them with  aggregate information about our users (for example, we may inform them that 500  men aged under 30 have clicked on their advertisement on any given day). We may  also use such aggregate information to help advertisers reach the kind of  audience they want to target (for example, women in SW1). We may make use of  the personal data we have collected from you to enable us to comply with our  advertisers' wishes by displaying their advertisement to that target audience. </li>
    <li>Analytics and search engine providers that  assist us in the improvement and optimisation of our site. </li>
  </ul>
  <p><strong>We may disclose your  personal information to third parties:</strong></p>
  <ul>
    <li>In the event that we sell or buy any business or  assets, in which case we may disclose your personal data to the prospective  seller or buyer of such business or assets.</li>
    <li>If Must Remember That LLP or substantially all  of its assets are acquired by a third party, in which case personal data held  by it about its customers will be one of the transferred assets.</li>
    <li>If we are under a duty to disclose or share your  personal data in order to comply with any legal obligation, or in order to  enforce or apply our <a href="terms.cfm">terms of use</a> and other agreements;  or to protect the rights, property, or safety of Must Remember That LLP, our  customers, or others. This includes exchanging information with other companies  and organisations for the purposes of fraud protection and credit risk  reduction.</li>
  </ul>
  <p><strong>Where we store your  personal data</strong><br>
    The data that we collect  from you may be transferred to, and stored at, a destination outside the  European Economic Area ("EEA"). It may also be processed by staff  operating outside the EEA who work for us or for one of our suppliers. Such  staff maybe engaged in, among other things, the fulfilment of your order, the  processing of your payment details and the provision of support services. By  submitting your personal data, you agree to this transfer, storing or  processing. We will take all steps reasonably necessary to ensure that your  data is treated securely and in accordance with this privacy policy.<br>
    All information you  provide to us is stored on our secure servers. Any payment transactions will be  encrypted [using  SSL technology].  Where we have given you (or where you  have chosen) a password which enables you to access certain parts of our site,  you are responsible for keeping this password confidential. We ask you not to  share a password with anyone.<br>
    Unfortunately, the  transmission of information via the internet is not completely secure. Although  we will do our best to protect your personal data, we cannot guarantee the  security of your data transmitted to our site; any transmission is at your own  risk. Once we have received your information, we will use strict procedures and  security features to try to prevent unauthorised access.<br>
    <br>
    <strong>Your rights</strong><br>
    You have the right to ask  us not to process your personal data for marketing purposes. We will usually  inform you (before collecting your data) if we intend to use your data for such  purposes or if we intend to disclose your information to any third party for  such purposes. You can exercise your right to prevent such processing by checking  certain boxes on the forms we use to collect your data. You can also exercise the right at any time  by contacting us <a href="contact.cfm">click here</a>.<br>
    Our site may, from time  to time, contain links to and from the websites of our partner networks, advertisers  and affiliates. If you follow a link to  any of these websites, please note that these websites have their own privacy  policies and that we do not accept any responsibility or liability for these  policies. Please check these policies  before you submit any personal data to these websites.<br>
    Access to information<br>
    The Act gives you the  right to access information held about you. Your right of access can be  exercised in accordance with the Act. Any access request may be subject to a  fee of £10 to meet our costs in providing you with details of the information  we hold about you.<br>
    Changes to our  privacy policy<br>
    Any changes we may make  to our privacy policy in the future will be posted on this page and, where  appropriate, notified to you by e-mail. Please check back frequently to see any  updates or changes to our privacy policy.<br>
    Contact<br>
    Questions, comments and  requests regarding this privacy policy are welcomed please<a href="contact.cfm"> click here</a> to submit.</p>

            