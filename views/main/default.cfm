<cfsilent>
    
    <!--- if already logged in redirect to reminders page --->
    <cfif structKeyExists(SESSION, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
        <cflocation url="#buildUrl("users.reminders")#" addtoken="false">
    </cfif>
        
    <cfset rc.MyDateTime=Now()> 
    <cfset rc.Date13 = '#DateFormat(DateAdd('yyyy', -13, rc.MyDateTime),'yyyy')#'>
    <cfset rc.Date30 = '#DateFormat(DateAdd('yyyy', -30, rc.MyDateTime),'yyyy')#'>
    <cfset rc.Date100 = '#DateFormat(DateAdd('yyyy', -100, rc.MyDateTime),'yyyy')#'>

</cfsilent>

    <div class="section vertical-center" id="regForm">
        <div class="container mobNoPadding hpFormWidth">
            <div class="col-md-6 mobNoPadding">
                
                <cfif not arrayIsEmpty(rc.errors)>
                    <div class="alert alert-danger fade in m-b-15">
                        <a href="##" data-dismiss="alert" class="close">x</a>
                        <cfloop array="#rc.errors#" index="msg">
                            <cfoutput>#msg#</cfoutput></br>
                        </cfloop>
                    </div>
                </cfif>

                <cfif not arrayIsEmpty(rc.successMessage)>
                    <div class="alert alert-success fade in m-b-15">
                        <strong>Success!</strong>
                        <a href="##" data-dismiss="alert" class="close">x</a><br/>
                        <cfloop array="#rc.successMessage#" index="msg">
                            <cfoutput>#msg#</cfoutput></br>
                        </cfloop>
                    </div>
                </cfif>

                <form method="post" autocomplete="false" action="" class="form" id="register-form" name="myForm">
                    <input type="hidden" name="update" value="addUser"/>
                    <div id="regStageOne" class="regStage">
                        <fieldset>
                            <h1 class="text-left">Set a reminder</h1>
                            <!--- <p class="text-center">Keep your life organised with reminders sent direct to your phone.</p> --->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger fade in m-b-15" id="alert1" style="display:none;">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Remind me to...</label>
                                    <input type="text" id="hpTitle" required="" placeholder="e.g. Car insurnace renewal" autocomplete="false" class="form-control" name="title" value="<cfoutput>#rc.title#</cfoutput>">
                                </div>
                            </div>    

                            <div class="row">
                                <!-- first name -->
                                <div class="form-group col-md-6">
                                    <input type="hidden" name="rdate" id="rdate" value="" />
                                    <div class='input-group date form_date'>
                                        <input placeholder="Date of event" type="text" data-date-format="Do MMMM YYYY" data-link-field="rdate" autocomplete="off" id="inputEmail" class="form-control" value="">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <!-- last name -->
                                <div class="form-group col-md-6">
                                    <div class='input-group date form_time' id='datetimepicker3'>
                                        <input placeholder="Time of event" type='text' class="form-control" name="rtime" id="rtime" value="" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="footer col-md-12 text-center">
                                    <button class="btn btn-primary" id="regButtonOne" type="button">Set Reminder</button><!--- <br/>or<br/><button class="btn btn-fb" onclick="authUser();" id="fblogin" type="button">Sign Up with Facebook</button> --->
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    <div id="regStageTwo" class="formHide regStage">
                        
                        <fieldset>
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger fade in m-b-15" id="alert2" style="display:none;">
                                    </div>
                                </div>
                            </div>

                            <p class="text-center">Set your reminder options</p>
                            <div class="form-group col-md-12">
                                <label for="category">Category</label>
                                <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                                    <option value="">Select Category</option>
                                    <cfoutput query="rc.getMainCats">
                                        <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                                    </cfoutput>
                                </select>
                            </div>
                            <div id="subCatContainer" class="form-group col-md-12">
                                <label for="category">Sub Category</label>
                                <select id="category" name="category" class="form-control" data-role="none">   
                                    <cfoutput query="rc.getCats">
                                        <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                                    </cfoutput>  
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail">Send me reminder</label>
                                <select id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                                    <cfloop from="1" to="100" index="i"><cfoutput>
                                        <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                                    </cfoutput></cfloop>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail" class="hidden-xs">&nbsp;</label>
                                <select id="remindertime" name="remindertime" class="form-control" data-role="none">              
                                    <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                                    <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour before</option>
                                    <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day Before</option>
                                    <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week Before</option>
                                    <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month Before</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputEmail">How frequent</label>
                                <select id="interval" name="interval" class="form-control" data-role="none">              
                                    <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                                    <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                                    <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                                    <option <cfif rc.interval EQ 'weekly'>selected="selected"</cfif> value="weekly">Weekly</option>
                                    <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                                    <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="inputEmail">Comments</label>
                                <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                            </div>
                            <div class="form-group col-md-12 text-center">
                                <button class="btn btn-primary" id="regButtonTwo" type="button">Save</button>
                            </div>
                        </fieldset>
                    </div>    
                    <div id="regStageThree" class="formHide regStage">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger fade in m-b-15" id="alert3" style="display:none;">
                                </div>
                            </div>
                        </div>

                        <fieldset>
                            <div class="row">
                               <div class="col-md-12 text-center" style="font-size:16px;">
                                    <p>Just a few details to create your account</p>
                                    <!---<p><button class="btn btn-fb" onclick="authUser();" id="fblogin" type="button"><i class="fa fa-facebook"></i> Sign Up with Facebook</button></p>--->
                                    <!---<p><button class="btn btn-fb" type="button" id="googleSignup"><i class="fa fa-google"></i> Sign Up with Google</button>
                                    <div style="opacity:0; height:10px;" id="my-signin2"></div></p>--->
                                    <p>Or signup with your email address</p>
                                </div>
                            </div>
                            <div class="row">
                                <!-- first name -->
                                <div class="form-group col-md-6">
                                    <input type="text" autocomplete="false" placeholder="First name" class="form-control" name="firstname" id="firstname">
                                </div>
                                <!-- last name -->
                                <div class="form-group col-md-6">
                                    <input type="text" autocomplete="false" placeholder="Last name" class="form-control" name="lastname" id="lastname">
                                </div>
                            </div>
                            <div class="row">
                                <!-- username -->
                                <div class="form-group col-md-6">
                                    <!-- email -->
                                    <input id="email" type="email" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.confEmailAddress.pattern = this.value;" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,3}$" title="Email must be a valid email address" autocomplete="false" placeholder="E-mail" class="form-control" name="emailAddress">
                                </div>
                                <div class="form-group col-md-6">
                                    <!-- password -->
                                    <input id="password" type="password" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.confirmPassword.pattern = this.value;" pattern=".{6,}" placeholder="Password... 6 characters or more" class="form-control" title="Password must contain at least 6 characters" name="password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <select required placeholder="Gender" name="Gender" class="form-control" data-role="none">
                                        <option selected="selected" value="F">Female</option>
                                        <option value="M">Male</option>
                                    </select>
                                </div>
                                <div class="form-group" style="margin-bottom:10px;">
                                    <label for="DOB" id="dob" class="control-label col-md-2 col-xs-12">Date Of Birth</label>
                                    <div class="col-xs-4 col-md-2">
                                        <select required name="day" class="form-control" data-role="none">
                                            <cfloop from="1" to="31" index="i">
                                                <option value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                                            </cfloop>
                                        </select>
                                    </div>
                                    <div class="col-xs-4 col-md-2 monthPadding">
                                        <select required name="month" class="form-control" data-role="none">
                                            <option value="01">Jan</option>
                                            <option value="02">Feb</option>
                                            <option value="03">Mar</option>
                                            <option value="04">Apr</option>
                                            <option value="05">May</option>
                                            <option value="06">Jun</option>
                                            <option value="07">Jul</option>
                                            <option value="08">Aug</option>
                                            <option value="09">Sep</option>
                                            <option value="10">Oct</option>
                                            <option value="11">Nov</option>
                                            <option value="12">Dec</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-4 col-md-2 monthYear">
                                        <select required name="year" class="form-control" data-role="none">    
                                            <cfloop from="#rc.Date100#" to="#rc.Date13#" index="i">
                                                <option <cfif i eq #rc.Date30#>selected</cfif> value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                                            </cfloop>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 visible-xs text-center" style="line-height:10px; margin-top:10px;">
                                    <span style="font-size:8px; color:#3f3f3f;">By clicking the Sign Up button you agree to the Must Remember That <a style="color:white;" href="terms.cfm">Terms and Conditions</a> and confirm you have read the <a style="color:white;" href="cookie.cfm">Cookie Policy</a>, <a style="color:white;" href="privacy.cfm">Privacy Policy</a> and <a style="color:white;" href="acceptance.cfm">Acceptable Use Policy</a>.</span><br><br>
                                </div>
                                <div class="col-md-12 hidden-xs" style="line-height:10px;">
                                    <span style="font-size:8px; color:#3f3f3f;">By clicking the Sign Up button you agree to the Must Remember That <a style="color:#3f3f3f;" href="terms.cfm">Terms and Conditions</a> and confirm you have read the <a style="color:#3f3f3f;" href="cookie.cfm">Cookie Policy</a>, <a style="color:#3f3f3f;" href="privacy.cfm">Privacy Policy</a> and <a style="color:#3f3f3f;" href="acceptance.cfm">Acceptable Use Policy</a>.</span><br><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="footer col-md-12 visible-xs text-center">
                                    <button class="btn btn-success regButtonThree" type="submit">Sign Up And Create Account</button><!--- <br/>or<br/><button class="btn btn-fb" onclick="authUser();" id="fblogin" type="button">Sign Up with Facebook</button> --->
                                </div>
                                <div class="footer col-md-12 hidden-xs text-center">
                                    <button class="btn btn-success regButtonThree" type="submit">Sign Up And Create Account</button><!---  or <button class="btn btn-fb" onclick="authUser();" id="fblogin" type="button">Sign Up with Facebook</button> --->
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="howitworks" class="howitworks-section vertical-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 hpContent">
                    <h3>How It Works</h3>
                    <p>Create an account by using our simple sign up form
                    <p>Download our App to receive reminder notifications direct to your phone
                    <p>Use our simple reminder service to set those important dates from your car insurance to Grandma's birthday
                    <p>Share your reminders with your friends and family so they never miss those important as well
                    <p><a href="#regForm" class="btn btn-primary">Sign up</a>
                </div>
                <div class="col-md-6 hidden-xs vcenter" style="float:right;">
                </div>
            </div>
        </div>
    </div>

    <div id="business" class="business-section vertical-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 hpContent">
                    <h3>Business Reminder Service</h3>
                    <p>Keep your customers up-to-date with reminder alerts for all your promotions, events and appointments whether you are
                    <ul>
                        <li>A company selling a product or service</li>
                        <li>A hairdressers or dentist who create appointments</li>
                        <li>A band or school that hold events</li>
                        <li>Run a club</li>
                        <li>Fundraise for a charity</li>
                    </ul>
                    <p>Our service allows your customers to select your important dates so they never forget you. They will receive your business alerts direct to their mobile phone.
                    <p>Share your reminders via email, Facebook, twitter or Whatsapp
                    <p><a href="<cfoutput>#buildUrl('business.addABusiness')#</cfoutput>" class="btn btn-primary">Create a business page now</a>
                </div>
                <div class="col-md-4 hidden-xs" style="float:right;">
                    <!--- <img src="/assets/business.png" class="hpImage center-block img-responsive"> --->
                </div>
            </div>
        </div>
    </div>

    <div id="ourapp" class="ourapp-section vertical-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 hpContent">
                    <h3>Our App</h3>
                    <p>Download our app to get Reminder alerts sent direct to your phone</p>
                    <p><a href="https://itunes.apple.com/us/app/must-remember-that/id1121692955?ls=1&mt=8"><img src="/assets/app-download.jpg"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.mustrememberthat.app"><img src="/assets/android.png"></a></p>
                </div>
            </div>
        </div>
    </div>

    <div id="reminders" class="reminders-section vertical-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12 hpContent">
                    <h3>Popular Reminders</h3>
                    <p>Select your reminders from the list below</p>
                    <cfoutput query="rc.getPriorityCats" startrow="1" maxrows="6">
                        <a class="btn reminderTypeBtn" href="?defaultReminder=#urlSafe#">#title#</a>
                    </cfoutput>
                    <cfoutput query="rc.getPriorityCats" startrow="7" maxrows="6">
                        <a class="btn reminderTypeBtn" href="?defaultReminder=#urlSafe#">#title#</a>
                    </cfoutput>
                    <cfoutput query="rc.getPriorityCats" startrow="13" maxrows="6">
                        <a class="btn reminderTypeBtn" href="?defaultReminder=#urlSafe#">#title#</a>
                    </cfoutput>
                 </div>
            </div>
        </div>
    </div>

    <div id="aboutus" class="aboutus-section vertical-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 hpContent">
                    <h3>About Us</h3>
                    <p>Must Remember That... is a Free reminder service for everything in your life!
                    <p>You can chose whether you want the reminder alerts by email and text or both straight to your PC and Mobile phone.
                    <p>Whether it's a utility bill that about to expire, an MOT, or your home TV package or a 0% credit card. You never need to go out of contract again, incur a fine or hidden charges; this will also give you the opportunity to shop around for better deals.
                    <p>Here are just a few of the most common reminders people forget about because of their busy lives.
                    <div class="row">
                        <div class="col-md-4">
                            <ul>
                                <li>Vehicle MOT</li>
                                <li>Vehicle Tax</li>
                                <li>Credit &amp; Store cards</li>
                                <li>Birthdays</li>
                                <li>Car Insurance</li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li>Driving license renewal</li>
                                <li>Smoke Detector battery</li>
                                <li>Anniversary / Wedding</li>
                                <li>TV Package</li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li>School activity</li>
                                <li>Night out</li>
                                <li>E111 Card</li>
                                <li>Passports</li>
                            </ul>
                        </div>
                    </div>
                    <p>Or anything else in your life that you need to reminded about...
                </div>
                <div class="col-md-4 hidden-xs imgContainer">
                    <!--- <img src="/assets/aboutus.png" class="hpImage center-block img-responsive"> --->
                </div>
            </div>
        </div>
    </div>
