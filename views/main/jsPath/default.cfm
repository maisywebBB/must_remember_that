<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<style>
    .bootstrap-datetimepicker-widget.dropdown-menu {
        width: auto !important;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('#regButtonOne').click(function(){
            
            var proceed = true

            $('#alert1').hide();

            if(!$('#hpTitle').val()){
                $('#hpTitle').parent().addClass("has-warning");
                $('#alert1').append('Please supply a reminder<br/>');
                proceed = false
            }else{
                $('hpTitle').parent().removeClass("has-warning")
            }

            if(!$('#rdate').val()){
                $('#rdate').parent().addClass("has-warning");
                $('#alert1').append('Please supply a date<br/>');
                proceed = false
            }else{
                $('#rdate').parent().removeClass("has-warning")
            }

            if(!$('#rtime').val()){
                $('#rtime').parent().addClass("has-warning");
                $('#alert1').append('Please supply a time<br/>');
                proceed = false
            }else{
                $('#rtime').parent().removeClass("has-warning")
            }

            if(proceed){
                $('.regStage').hide();
                $('#regStageTwo').show();
            }else{
                $('#alert1').show();
            }
        });

        $('#regButtonTwo').click(function(){
            
            var proceed = true

            $('#alert2').hide();

            if(!$('#maincategory').val()){
                $('#maincategory').parent().addClass("has-warning");
                $('#alert2').append('Please supply a category<br/>');
                proceed = false
            }else{
                $('#maincategory').parent().removeClass("has-warning")
            }

            if(!$('#category').val()){
                $('#category').parent().addClass("has-warning");
                $('#alert2').append('Please supply a sub category<br/>');
                proceed = false
            }else{
                $('#category').parent().removeClass("has-warning")
            }

            if(proceed){
                $('.regStage').hide();
                $('#regStageThree').show();
            }else{
                $('#alert2').show();
            }
        });

        $('.regButtonThree').click(function(){
            
            var proceed = true

            $('#alert3').hide();

            if(!$('#firstname').val()){
                $('#firstname').parent().addClass("has-warning");
                $('#alert3').append('Please supply a first name<br/>');
                proceed = false
            }else{
                $('##firstname').parent().removeClass("has-warning")
            }

            if(!$('#lastname').val()){
                $('#lastname').parent().addClass("has-warning");
                $('#alert3').append('Please supply a last name<br/>');
                proceed = false
            }else{
                $('##lastname').parent().removeClass("has-warning")
            }

            if(!$('#email').val()){
                $('#email').parent().addClass("has-warning");
                $('#alert3').append('Please supply a email address<br/>');
                proceed = false
            }else{
                $('##email').parent().removeClass("has-warning")
            }

            if(!$('#password').val()){
                $('#password').parent().addClass("has-warning");
                $('#alert3').append('Please supply a password<br/>');
                proceed = false
            }else{
                $('##password').parent().removeClass("has-warning")
            }

            if(proceed){}else{
                $('#alert3').show();
                $('#howitworks').addClass('hiwMob');
                return false;
            }
        });

    });
    

    $('.form_time').datetimepicker({
        format: 'HH:mm',
        useCurrent: true
    }).on('dp.change', function(e) {
        if (e.oldDate === null) {
            $(this).data('DateTimePicker').date(new Date(e.date._d.setHours(09, 00, 00)));
        }
    });

    // date picker
    $('.form_date').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: true
    }).on('dp.change', function(ev) {
        var selectedDate = new Date(ev.date);
        
        var dd = selectedDate.getDate();
        var mm = selectedDate.getMonth()+1; //January is 0!

        var yyyy = selectedDate.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
        var selectedDate = mm+'/'+dd+'/'+yyyy;
        document.getElementById("rdate").value = selectedDate;


    });

</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(document.body).on("change", "#maincategory", function(){

            $('#subCatContainer').show();

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=' + this.value,
                success: function(data){
                    $('#category').html(data);
                    $("#category").val('<cfoutput>#rc.category#</cfoutput>');
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });
    });

</script>

<cfif structKeyExists(RC, 'mainCategory') AND isNumeric(rc.mainCategory) AND  structKeyExists(RC, 'category') AND isNumeric(rc.category) >
    <script type="text/javascript">
        $(document).ready(function(){
            $('#subCatContainer').show();

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=<cfoutput>#rc.mainCategory#</cfoutput>',
                success: function(data){
                    $('#category').html(data);
                    $("#category").val('<cfoutput>#rc.category#</cfoutput>');
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });
        });

    </script>
</cfif>
