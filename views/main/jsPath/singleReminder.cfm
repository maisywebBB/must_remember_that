<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<style>
    .bootstrap-datetimepicker-widget.dropdown-menu {
        width: auto !important;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('#reminderActualTimeDiv').hide();
        $('#reminderActualTimeMobDiv').hide();

        $('.share_button').click(function(e){

            var reminderTitle = $(this).data('remindertitle');

            e.preventDefault();
            FB.ui(
            {
                method: 'feed',
                name: reminderTitle,
                link: ' http://www.mustrememberthat.com/',
                picture: 'http://www.mustrememberthat.com/images/MRT-facebook-icon5.png',
                caption: 'I have just remembered this thanks to Must Remember That',
                description: 'MUST REMEMBER THAT - Never Forget Again',
                message: ''
            });
        });

        $('.addReminderDiv').click(function(e){
            $('#addReminderDiv').slideToggle();
        });

        /*$(window).bind('resize load', function() {
            if ($(this).width() < 767) {
                $('#addReminderDiv').removeClass('in');
                $('#addReminderDiv').addClass('out');
            } else {
                $('#addReminderDiv').removeClass('out');
                $('#addReminderDiv').addClass('in');
            }
        });*/
    
        if($("#getPersonalReminders").length == 0) {
        }else{
            $('input#getPersonalReminders').on('keyup',function(){
                var charCount = $(this).val().replace(/\s/g, '').length;
                if(charCount >= 0){
                    var searchString = $('input#getPersonalReminders').val();
                    $.ajax({
                        type: "POST",
                        url: "<cfoutput>#buildUrl("ajax.searchPersonalReminders")#</cfoutput>",
                        data: { searchString : searchString, reminderKey : '<cfoutput>#session.auth.userId#</cfoutput>'},
                        success: function(result){
                            $('ul.list-group').html(result);
                            $('.pagination').hide();
                        }
                                    
                    });

                }

            });
        }

        

        $(document.body).on("change", "#maincategory", function(){

            $('#subCatContainer').show();

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=' + this.value,
                success: function(data){
                    $('#category').html(data);
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });

        $(document.body).on("change", "#remindertime", function(){

            //get the selected value
            var reminderType = $( "#remindertime option:selected" ).val();

            if(reminderType == 'd' || reminderType == 'ww' || reminderType == 'm' ){
                $('#reminderActualTimeDiv').show();
            }else{
                $('#reminderActualTimeDiv').hide();
            }

        });

        $(document.body).on("change", "#remindertimeMob", function(){

            //get the selected value
            var reminderType = $( "#remindertimeMob option:selected" ).val();

            if(reminderType == 'd' || reminderType == 'ww' || reminderType == 'm' ){
                $('#reminderActualTimeMobDiv').show();
            }else{
                $('#reminderActualTimeMobDiv').hide();
            }

        });
        
    });

    $('.form_time').datetimepicker({
        format: 'HH:mm',
        useCurrent: true
    }).on('dp.change', function(e) {
        if (e.oldDate === null) {
            $(this).data('DateTimePicker').date(new Date(e.date._d.setHours(09, 00, 00)));
        }
    });

    // date picker
    $('.form_date').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: true
    }).on('dp.change', function(ev) {
        var selectedDate = new Date(ev.date);
        
        var dd = selectedDate.getDate();
        var mm = selectedDate.getMonth()+1; //January is 0!

        var yyyy = selectedDate.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
        var selectedDate = mm+'/'+dd+'/'+yyyy;
        document.getElementById("rdate").value = selectedDate;


    });
</script>

<cfif structKeyExists(RC, 'mainCategory') AND isNumeric(rc.mainCategory) AND  structKeyExists(RC, 'category') AND isNumeric(rc.category) >
    <script type="text/javascript">
        $(document).ready(function(){
            $('#subCatContainer').show();

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=<cfoutput>#rc.mainCategory#</cfoutput>',
                success: function(data){
                    $('#category').html(data);
                    $("#category").val('<cfoutput>#rc.category#</cfoutput>');
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });
        });

    </script>
</cfif>