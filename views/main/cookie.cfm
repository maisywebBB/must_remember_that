
<h3>Cookie Settings</h3>

  <p>Our website uses  cookies to give you the best experience.<br>
    <br>
    <strong>What are cookies</strong><br>
    We may store information about you using cookies (files  which are sent by us to your computer or other access device) which we can  access when you visit our site in future. Below is a list of the cookies we use  and the function they perform</p>
  <p><strong>How to manage cookies</strong><br>
    Information on deleting or controlling cookies is available  at www.AboutCookies.org. Please note that by deleting our cookies or disabling  future cookies you may not be able to access certain areas or features of our  site.</p>
  <p>The cookies we use</p>
  <table border="1" cellpadding="3" cellspacing="1" width="80%">
    <tbody>
      <tr>
        <td width="111"><strong>Cookie name</strong></td>
        <td width="467"><strong>Cookie purpose</strong></td>
      </tr>
      <tr>
        <td>CFID</td>
        <td> This cookie stores encrypted data that is used to securely log you in to or register you with our website. </td>
      </tr>
      <tr>
        <td>CFTOKEN</td>
        <td>This cookie providing session security</td>
      </tr>
      <tr>
        <td> CFGLOBALS </td>
        <td>This cookie providing session security</td>
      </tr>
      <tr>
        <td> D4COOKIE_PREFERENCE </td>
        <td>This cookie remenbers your cookie preference </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Google Cookies</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>__utma</td>
        <td>A persistent cookie - remains on a computer,   unless it expires or the cookie cache is cleared. It tracks visitors.   Metrics associated with the Google __utma cookie include: first visit   (unique visit), last visit (returning visit). This also includes Days   and Visits to purchase calculations which afford ecommerce websites with   data intelligence around purchasing sales funnels.<br></td>
      </tr>
      <tr>
        <td>__utmb __utmc</td>
        <td><p>These cookies work in tandem to calculate   visit length. Google __utmb cookie demarks the exact arrival time, then   Google __utmc registers the precise exit time of the user.<br>
          <br>
          Because __utmb counts entrance visits, it is a session cookie, and   expires at the end of the session, e.g. when the user leaves the page. A   timestamp of 30 minutes must pass before Google cookie __utmc expires.   Given__utmc cannot tell if a browser or website session ends. Therefore,   if no new page view is recorded in 30 minutes the cookie is expired.</p>
          <p>This is a standard 'grace period' in web analytics. Ominture and WebTrends among many others follow the same procedure.<br>
          </p>
          <p> <br>
          </p></td>
      </tr>
      <tr>
        <td>__utmz</td>
        <td><p>Cookie __utmz monitors the HTTP Referrer   and notes where a visitor arrived from, with the referrer siloed into   type (Search engine (organic or cpc), direct, social and unaccounted).   From the HTTP Referrer the   __utmz Cookie also registers, what keyword   generated the visit plus geolocation data.</p>
          <p>This cookie lasts six months. In tracking terms this Cookie is   perhaps the most important as it will tell you about your traffic and   help with conversion information such as what source / medium / keyword   to attribute for a Goal Conversion.<br>
          </p></td>
      </tr>
      <tr>
        <td>__utmv</td>
        <td>Google __utmv Cookie lasts "forever". It is a   persistant cookie. It is used for segmentation, data experimentation and    the __utmv works hand in hand with the   __utmz cookie to improve   cookie targeting capabilities.</td>
      </tr>
    </tbody>
  </table>
  <p>&nbsp;</p>

            