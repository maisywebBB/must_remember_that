<div class="row hidden-xs">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <div class="well bs-component">
            <form action="<cfoutput>#buildUrl("users.reminders")#</cfoutput>" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="newReminder"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Add Reminder</legend>
                    <div class="form-group col-md-12">
                        <label for="title">Title</label>
                        <input  type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Category</label>
                        <select  id="maincategory" name="maincategory" class="form-control" data-role="none">   
                            <option value="">Select Category</option>
                            <cfoutput query="rc.getMainCats">
                                <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                            </cfoutput>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Sub Category</label>
                        <select  id="category" name="category" class="form-control" data-role="none">   
                            <cfoutput query="rc.getCats">
                                <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                            </cfoutput>  
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail"><span class="visible-xs">Date</span><span class="hidden-xs">Date of event</span></label>
                        <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.rdate#</cfoutput>" />
                        <div class='input-group date form_date'>
                            <input type="text" data-date-format="Do MMMM YYYY" data-link-field="rdate" autocomplete="off" id="inputEmail" class="form-control" <cfif len(rc.rdate)>value="<cfoutput>#day(rc.rdate)# #monthAsString(month(rc.rdate))# #year(rc.rdate)#</cfoutput>"</cfif>>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail"><span class="visible-xs">Time</span><span class="hidden-xs">Time of event</span></label>
                        <!--- <input type="hidden" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" /> --->
                        <div class='input-group date form_time' id='datetimepicker3'>
                            <input type='text' class="form-control" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Send me reminder</label>
                        <select  id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                            <cfloop from="1" to="100" index="i"><cfoutput>
                                <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                            </cfoutput></cfloop>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">&nbsp;</label>
                        <select  id="remindertime" name="remindertime" class="form-control" data-role="none">              
                            <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                            <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour(s) before</option>
                            <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day(s) Before</option>
                            <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week(s) Before</option>
                            <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month(s) Before</option>
                        </select>
                    </div>
                    <div id="reminderActualTimeDiv" class="form-group col-md-6">
                        <label for="inputEmail">Reminder Time</label>
                        <div class='input-group date form_time' id='datetimepicker4'>
                            <input type='text' class="form-control" name="reminderActualTime" id="reminderActualTime" value="<cfoutput>#rc.reminderActualTime#</cfoutput>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="inputEmail">How frequent</label>
                        <select  id="interval" name="interval" class="form-control" data-role="none">              
                            <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                            <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                            <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                            <option <cfif rc.interval EQ 'weely'>selected="selected"</cfif> value="weekly">Weekly</option>
                            <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                            <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Comments</label>
                        <textarea  name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                    </div>
                    <div class="form-group col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Set Reminder</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>

<div class="row visible-xs">
    <div class="col-lg-3"></div>
    <div class="col-lg-6 text-center">
        <legend><span><cfoutput>#rc.title#</cfoutput></span><br>
        <span style="font-size:16px;"><cfoutput>#DateFormat(rc.eventDate, "dd mmmm yy")#</cfoutput></span></legend>

        <!--- share on facebook --->
        <cfoutput>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('##share_button#rc.reminderId#').click(function(e){
                    e.preventDefault();
                    FB.ui(
                        {
                            method: 'feed',
                            name: '#rc.title#',
                            link: ' http://www.mustrememberthat.com/',
                            picture: 'http://www.mustrememberthat.com/images/MRT-facebook-icon5.png',
                            caption: 'I have just remembered this thanks to Must Remember That',
                            description: 'MUST REMEMBER THAT - Never Forget Again',
                            message: ''
                        });
                    });
                });
            </script>
        </cfoutput>
        
        <div>
            <div class="well bs-component">
                <form action="<cfoutput>#buildUrl("users.reminders")#</cfoutput>" method="POST" enctype="multipart/form-data">
            
                    <input type="hidden" name="update" value="newReminder"/>
                    <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                    <!--- <input type="hidden" name="title" value="<cfoutput>#rc.title#</cfoutput>"/>
                    <input type="hidden" name="maincategory" value="<cfoutput>#rc.maincategory#</cfoutput>"/>
                    <input type="hidden" name="category" value="<cfoutput>#rc.category#</cfoutput>"/>
                    <input type="hidden" name="rdate" value="<cfoutput>#rc.rdate#</cfoutput>"/>
                    <input type="hidden" name="rtime" value="<cfoutput>#rc.rtime#</cfoutput>"/>
                    <input type="hidden" name="interval" value="<cfoutput>#rc.interval#</cfoutput>"/>
                    <input type="hidden" name="remindertime" value="<cfoutput>#rc.remindertime#</cfoutput>"/> --->
                    
                    <fieldset>
                        <legend>Add Reminder</legend>
                        <div class="form-group col-md-12">
                            <label for="title">Title</label>
                            <input type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="category">Category</label>
                            <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                                <option value="">Select Category</option>
                                <cfoutput query="rc.getMainCats">
                                    <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                                </cfoutput>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="category">Sub Category</label>
                            <select id="category" name="category" class="form-control" data-role="none">   
                                <cfoutput query="rc.getCats">
                                    <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                                </cfoutput>  
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputEmail"><span class="visible-xs">Date</span><span class="hidden-xs">Date of event</span></label>
                            <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.rdate#</cfoutput>" />
                            <div class='input-group date form_date'>
                                <input type="text" data-date-format="Do MMMM YYYY" data-link-field="rdate" autocomplete="off" id="inputEmail" class="form-control" <cfif len(rc.eventdate)>value="<cfoutput>#day(rc.eventdate)# #monthAsString(month(rc.eventdate))# #year(rc.eventdate)#</cfoutput>"</cfif>>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail"><span class="visible-xs">Time</span><span class="hidden-xs">Time of event</span></label>
                            <!--- <input type="hidden" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" /> --->
                            <div class='input-group date form_time' id='datetimepicker3'>
                                <input type='text' class="form-control" name="rtime" id="rtime" value="<cfoutput>#rc.eventtime#</cfoutput>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputEmail">Send me reminder</label>
                            <select  id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                                <cfloop from="1" to="100" index="i"><cfoutput>
                                    <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                                </cfoutput></cfloop>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail">&nbsp;</label>
                            <select  id="remindertimeMob" name="remindertime" class="form-control" data-role="none">              
                                <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                                <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour(s) before</option>
                                <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day(s) Before</option>
                                <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week(s) Before</option>
                                <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month(s) Before</option>
                            </select>
                        </div>
                        <div id="reminderActualTimeMobDiv" class="form-group col-md-6">
                            <label for="inputEmail">Reminder Time</label>
                            <div class='input-group date form_time' id='datetimepicker4'>
                                <input type='text' class="form-control" name="reminderActualTime" id="reminderActualTime" value="<cfoutput>#rc.reminderActualTime#</cfoutput>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputEmail">How frequent</label>
                            <select  id="interval" name="interval" class="form-control" data-role="none">              
                                <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                                <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                                <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                                <option <cfif rc.interval EQ 'weely'>selected="selected"</cfif> value="weekly">Weekly</option>
                                <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                                <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="inputEmail">Comments</label>
                            <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button class="btn btn-primary" type="submit">Add Reminder</button>
                        </div>
                    </fieldset>
                </form>
                <!---<div class="btn btn-primary btn-xs" id="source-button">&lt; &gt;</div>--->
            </div>
        </div>

    </div>
    <div class="col-lg-3"></div>
</div>
        