<cfparam name="rc.successMessage" default="#arrayNew(1)#">
<cfparam name="rc.message" default="#arrayNew(1)#">

<cfoutput>
    <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="well bs-component">
                <cfif not arrayIsEmpty(rc.message)>
                    <div class="alert alert-warning fade in m-b-15">
                        <cfloop array="#rc.message#" index="msg">
                        <p>#msg#</p>
                        </cfloop>
                        <span data-dismiss="alert" class="close">x</span>
                    </div>
                </cfif>
                <cfif not arrayIsEmpty(rc.successMessage)>
                    <div class="alert alert-success fade in m-b-15">
                        <cfloop array="#rc.successMessage#" index="msg">
                        <p>#msg#</p>
                        </cfloop>
                        <span data-dismiss="alert" class="close">x</span>
                    </div>
                </cfif>
                <legend>Reset Password</legend>
                <form action="#buildUrl("main.newPassword")#" method="POST" class="margin-bottom-0">
                    <input type="hidden" name="token" value="#rc.token#"/>
                    <!--- <div class="form-group m-b-20">
                        <input name="emailAddress" type="text" class="form-control input-lg" placeholder="Email Address" />
                    </div> --->
                    <div class="form-group m-b-20">
                        <input name="password" type="password" class="form-control input-lg" placeholder="New Password" />
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Reset Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</cfoutput>
