<!---<cfobject type="JAVA" action="Create" name="factory" class="coldfusion.server.ServiceFactory">
<cfset allTasks = factory.CronService.listAll()/>
<cfloop index="i" from="1" to="#ArrayLen(allTasks)#">
    <cfif allTasks[i].task EQ 'MRT-SendAllRemindersLive'>
    <cfdump var="#allTasks[i]#" />
    </cfif>
</cfloop>--->
 
<div class="col-lg-3"></div>
<div class="col-lg-6">
       
    <form method="post" autocomplete="false" action="" class="form" id="register-form" name="myForm">
    
    <cfif structKeyExists(rc, 'subscriptionId') AND isValid('uuid', rc.subscriptionId)>
        <cfquery name="getTitle">
            SELECT title
            FROM tbl_businesses
            WHERE businessId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.subscriptionId#">
        </cfquery>
        <input type="hidden" type="hidden" value="#rc.subscriptionId#">
        <div class="row">
            <div class="form-group" style="margin-bottom:10px;">
                <!-- password -->
                <label class="control-label col-md-10 col-xs-12 text-center">Would you like to add all current reminders from <cfoutput>#getTitle.title#</cfoutput> to your list?</label>
                <div class="col-xs-2 col-md-2">
                    <select name="allreminders" class="form-control">    
                        <option <cfif structKeyExists(rc, 'allreminders') AND rc.allreminders EQ 1>selected</cfif> value="1">Yes</option>
                        <option <cfif structKeyExists(rc, 'allreminders') AND rc.allreminders EQ 0>selected</cfif> value="0">No</option>
                    </select>
                </div>
            </div>
        </div>
    </cfif>

    <div class="row">   
        <div class="col-lg-12 text-center" style="font-size:16px; margin-bottom:10px;">
            Just a few details to create your account
        </div>
    </div>

    <!---
        <div class="row">   
        	<div class="col-lg-6 text-center" style="font-size:16px; margin-bottom:10px;">
        	    <button class="btn btn-fb" onclick="authUser();" id="fblogin" type="button"><i class="fa fa-facebook"></i> Sign Up with Facebook</button>
            </div>
            <div class="col-lg-6 text-center" style="font-size:16px;">
                <button class="btn btn-google" type="button" id="googleSignup"><i class="fa fa-google"></i> Sign Up with Google</button>
                <div style="opacity:0; height:10px;" id="my-signin2"></div>
            </div>
        </div>
    
	
    <div class="row">   
        <div class="col-lg-12 text-center" style="font-size:16px; margin-bottom:10px;">
            Or sign up with your email address
        </div>
    </div>
    --->
    
	<input type="hidden" name="update" value="addUser"/>
        <input type="hidden" name="slug" value="<cfoutput>#rc.slug#</cfoutput>"/>

        <fieldset>
            <div class="row">
                <!-- first name -->
                <div class="form-group col-md-6">
                    <input type="text" required="" autocomplete="false" placeholder="First name" class="form-control" name="firstname">
                </div>
                <!-- last name -->
                <div class="form-group col-md-6">
                    <input type="text" required="" autocomplete="false" placeholder="Last name" class="form-control" name="lastname">
                </div>
            </div>
            <div class="row">
                <!-- username -->
                <div class="form-group col-md-6">
                    <!-- email -->
                    <input type="email" required="" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.confEmailAddress.pattern = this.value;" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,3}$" title="Email must be a valid email address" autocomplete="false" placeholder="E-mail" class="form-control" name="emailAddress">
                </div>
                <div class="form-group col-md-6">
                    <!-- email -->
                    <input type="email" required="" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,3}$" autocomplete="false" placeholder="Retype E-mail" title="Please enter the same Email" class="form-control" name="confEmailAddress">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <!-- password -->
                    <input type="password" required="" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.confirmPassword.pattern = this.value;" pattern=".{6,}" placeholder="Password... 6 characters or more" class="form-control" title="Password must contain at least 6 characters" name="password">
                </div>
                <div class="form-group col-md-6">
                    <!-- password -->
                    <input type="password" required="" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" pattern=".{6,}" placeholder="Retype password" class="form-control" title="Please enter the same password" name="confirmPassword">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <select required="" placeholder="Gender" name="Gender" class="form-control" data-role="none">
                        <option selected="selected" value="F">Female</option>
                        <option value="M">Male</option>
                    </select>
                </div>
                <div class="form-group" style="margin-bottom:10px;">
                    <label for="DOB" id="dob" class="control-label col-md-2 col-xs-12">Date Of Birth</label>
                    <div class="col-xs-4 col-md-2">
                        <select required="" name="day" class="form-control" data-role="none">
                            <cfloop from="1" to="31" index="i">
                                <option value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                            </cfloop>
                        </select>
                    </div>
                    <div class="col-xs-4 col-md-2 monthPadding">
                        <select required="" name="month" class="form-control" data-role="none">
                            <option value="01">Jan</option>
                            <option value="02">Feb</option>
                            <option value="03">Mar</option>
                            <option value="04">Apr</option>
                            <option value="05">May</option>
                            <option value="06">Jun</option>
                            <option value="07">Jul</option>
                            <option value="08">Aug</option>
                            <option value="09">Sep</option>
                            <option value="10">Oct</option>
                            <option value="11">Nov</option>
                            <option value="12">Dec</option>
                        </select>
                    </div>
                    <div class="col-xs-4 col-md-2 monthYear">
                        <select required="required" name="year" class="form-control" data-role="none">    
                            <cfloop from="#rc.Date100#" to="#rc.Date13#" index="i">
                                <option <cfif i eq #rc.Date30#>selected</cfif> value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                            </cfloop>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 visible-xs text-center" style="line-height:10px; margin-top:10px;">
                    <span style="font-size:8px; color:#FFFFFF;">By clicking the Sign Up button you agree to the Must Remember That <a style="color:black;" href="terms.cfm">Terms and Conditions</a> and confirm you have read the <a style="color:black;" href="cookie.cfm">Cookie Policy</a>, <a style="color:black;" href="privacy.cfm">Privacy Policy</a> and <a style="color:black;" href="acceptance.cfm">Acceptable Use Policy</a>.</span><br><br>
                </div>
                <div class="col-md-12 hidden-xs" style="line-height:10px;">
                    <span style="font-size:8px; color:#000;">By clicking the Sign Up button you agree to the Must Remember That <a style="color:black;" href="terms.cfm">Terms and Conditions</a> and confirm you have read the <a style="color:black;" href="cookie.cfm">Cookie Policy</a>, <a style="color:black;" href="privacy.cfm">Privacy Policy</a> and <a style="color:black;" href="acceptance.cfm">Acceptable Use Policy</a>.</span><br><br>
                </div>
            </div>
            <div class="row">
                <div class="footer col-md-12 visible-xs text-center">
                    <button class="btn btn-success" type="submit">Sign Up And Create Account</button><!--- <br/>or<br/><button id="fblogin" onclick="authUser();" class="btn btn-fb" type="button">Sign Up with Facebook</button> --->
                </div>
                <div class="footer col-md-12 hidden-xs">
                    <button class="btn btn-success" type="submit">Sign Up And Create Account</button><!---  or <button id="fblogin" onclick="authUser();" class="btn btn-fb" type="button">Sign Up with Facebook</button> --->
                </div>
            </div>
        </fieldset>
    </form>
 
</div>
