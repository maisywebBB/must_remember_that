<!--- <div class="row">
    <div class="col-lg-12" id="businessHero">
		<div class="hero-unit">
		    <img class="img-responsive" src="<cfoutput>/assets/businessImages/#rc.getBusiness.heroImageURL#</cfoutput>">
		</div>
    </div>
</div> --->

<cfobject component="controllers.utils" name="application.utils" type="component">

<div class="row">
	<div class="col-lg-6 col-lg-push-3 businessDivider" id="businessReminders">
		<div class="businessReminderPadding" style="display: inline-block; margin-bottom: 10px; width: 100%;">
			<span id="businessTitle"><strong><cfoutput>#rc.getGroups.title#</cfoutput> - Reminders</strong></span><br/>
			Let us remind you as important dates approach so you can keep your life organised.
		</div>
		<div style="display:none;" class='alert alert-danger reminderError businessReminderPadding'></div>
        <div style="display:none;" class='alert alert-success reminderSuccess businessReminderPadding'></div>
		<div class="businessReminderPadding" style="display: inline-block; margin-bottom: 10px; width: 100%;">
			<input type="text" class="form-control" id="getBusinessReminders" placeholder="Search reminders">
		</div>
		<div style="display: inline-block; margin-bottom: 10px; width: 100%;">
			<div class="col-lg-1 col-xs-1 mobileNoPadding">
				<input type="checkbox" id="checkAll" style="margin-right:10px;">
				
			</div>
			<div class="col-xs-1 visible-xs">
				<button id="addAllSelectMob" type="button" class="btn btn-default-yellow"><span class="glyphicon glyphicon-check"></span> Add Selected </button>
			</div>
			<div class="col-lg-5 hidden-xs">
				<button id="addAllSelect" type="button" class="btn btn-default-yellow"><span class="glyphicon glyphicon-check"></span> Add Selected </button>
			</div>
		</div>
		<ul class="list-group">
            <cfoutput query="rc.getReminders">
                
                <cfsilent>
	                <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
		            	<cfset itemLink = buildUrl("users.groupsreminder") & '&rid=' & rc.getReminders.reminderId>
		            <cfelse>
	                	<cfset itemLink = buildUrl("main.registration") & '&msg=login&slug=' & rc.getGroups.urlslug  & '&rid=' & rc.getReminders.reminderId>
	                </cfif>
                </cfsilent>

                <li class="list-group-item">
                	
                	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 mobileNoPadding">
                		<input type="checkbox" name="rid" class="cbrid" value="#rc.getReminders.reminderId#">
                	</div>
                	<div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 mobileNoPadding">
	                    <div class="titleContainer">
	                    	<div style="float:right;">
		                    	<a href="#itemLink#" style="float:right;" type="button" class="<cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>singleReminderBtn</cfif> btn btn-default-yellow" data-rid="#rc.getReminders.reminderId#"><span class="glyphicon glyphicon-pushpin"></span> Add </a><br/>
		                    </div>
		                    #title#
		                </div>

	                    <div>
	                    	
		                    <p style="margin-bottom: -4px;">
		                    	#application.utils.DateFormatExtended(eventDate, "d mmmm yyyy")# - #TimeFormat(rtime, "HH:mm")#
		                    	<cfif len(rc.getReminders.comments)><a href="##" style="float:right;" data-toshow="moreInfoContent#rc.getReminders.currentRow#" class="showMoreInfo">More info</a></cfif>
			                </p>
		                    <cfif len(rc.getReminders.comments)>
		                    	<p class="moreInfoContent#rc.getReminders.currentRow# moreInfoContent" style="margin-top:10px">#rc.getReminders.comments#</p>
	                    	</cfif>
		                    
	                    </div>
	                </div>
	            </li>
	            <cfif childcount>
		            <li class="childcountRow">
		                <div class="row clearfix">
			                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                    	reminder set #childCOunt# time<cfif childCOunt GT 1>s</cfif>
		                    </div>
	                    </div>
                   	</li>
                </cfif>

            </cfoutput>
        </ul>
	</div>
	<div class="col-lg-3 col-lg-pull-6 businessDivider">
		<cfif len(rc.getGroups.logo)>
			<img class="img-responsive" src="<cfoutput>/assets/groupImages/#rc.getGroups.logo#</cfoutput>">
		</cfif>
		<cfif isValid('URL', rc.getGroups.website)>
			<p><a href="<cfoutput>#rc.getGroups.website#</cfoutput>" target="_blank"><cfoutput>#rc.getGroups.website#</cfoutput></a></p>	
		</cfif>
		<p><cfoutput>#ParagraphFormat(rc.getGroups.aboutContent)#</cfoutput></p>
	</div>
	<div class="col-lg-3 businessDivider">
		<p><strong>How To</strong></p>
		<p>Click add button to select a single reminder</P>
		<P><button type="button" class="btn btn-default-yellow"><span class="glyphicon glyphicon-pushpin"></span> Add </button></P>
		<p>To add more than one reminder tick the check box for each reminder then click the add selected button</p>
		<P><button type="button" id="addAllSelect2" class="btn btn-default-yellow"><span class="glyphicon glyphicon-pushpin"></span> Add Selected </button></P>
	</div>
</div>
