<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&v=3.7"></script>
<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<style>
    .bootstrap-datetimepicker-widget.dropdown-menu {
        width: auto !important;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('.share_button').click(function(e){

            var reminderTitle = $(this).data('remindertitle');

            e.preventDefault();
            FB.ui(
            {
                method: 'feed',
                name: reminderTitle,
                link: ' http://www.mustrememberthat.com/',
                picture: 'http://www.mustrememberthat.com/images/MRT-facebook-icon5.png',
                caption: 'I have just remembered this thanks to Must Remember That',
                description: 'MUST REMEMBER THAT - Never Forget Again',
                message: ''
            });
        });

         $(document.body).on("change", "#maincategory", function(){

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=' + this.value,
                success: function(data){
                    $('#category').html(data);
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });
        
    });

    // date picker
    $('.form_time').datetimepicker({
        format: 'HH:mm',
        useCurrent: true
    }).on('dp.change', function(e) {
        if (e.oldDate === null) {
            $(this).data('DateTimePicker').date(new Date(e.date._d.setHours(09, 00, 00)));
        }
    });

    // date picker
    $('.form_date').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: true
    }).on('dp.change', function(ev) {
        var selectedDate = new Date(ev.date);
        
        var dd = selectedDate.getDate();
        var mm = selectedDate.getMonth()+1; //January is 0!

        var yyyy = selectedDate.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
        var selectedDate = mm+'/'+dd+'/'+yyyy;
        $('[name="rdate"]').val(selectedDate);


    });

    var geocoder = new google.maps.Geocoder();
    var marker = new google.maps.Marker();

    function displayMap() {
        setTimeout(function() {
  
        var geocoder = new google.maps.Geocoder();
        var marker = new google.maps.Marker();

        var swAttractionMob = new google.maps.LatLng(51.29885215199866, -0.828094482421875);
        var neAttractionMob = new google.maps.LatLng(51.76529023435832, 0.611114501953125);
        var defaultBoundsAttractionMob = new google.maps.LatLngBounds(swAttractionMob, neAttractionMob);
        var defaultOptionsAttractionMob = {
            center: new google.maps.LatLng(51.508515, -0.12548719999995228),
            zoom: 7,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    
        var gLat = isNaN($('#gestLat').val()) ? 0 : Number($('#gestLat').val());
        var gLng = isNaN($('#gestLng').val()) ? 0 : Number($('#gestLng').val());
        var gLatLng = new google.maps.LatLng(gLat, gLng);
        if (defaultBoundsAttractionMob.contains(gLatLng)) {
            defaultOptionsAttractionMob.zoom = 15;
            defaultOptionsAttractionMob.center = gLatLng;
        }
    
        var mapAttractionMob = new google.maps.Map(document.getElementById("mapContainerMob"), defaultOptionsAttractionMob);
        google.maps.event.trigger(mapAttractionMob, "resize");
        var input = document.getElementById("keywordAttractionMob");
        
        var autocomplete = new google.maps.places.Autocomplete(input);
        
        autocomplete.bindTo('bounds', mapAttractionMob);
        marker.setMap(mapAttractionMob);
    
        if (defaultBoundsAttractionMob.contains(gLatLng)) {
            marker.setPosition(gLatLng);
        }
        
        /* If Google Map Co-Ordinates already Exist, show marker on the map */
        if ($('#latitude').val().length > 1  && $('#longitude').val().length >1)
        {
            var existingLocationAttraction = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
            
            marker.setPosition(existingLocationAttraction); 
            marker.setTitle($('#name').val());
            $('#keywordAttractionMob').val($('#name').val());
        }
    
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
    
            if (place.geometry.viewport) {
                mapAttractionMob.fitBounds(place.geometry.viewport);
            } else {
                mapAttractionMob.setCenter(place.geometry.location);
                mapAttractionMob.setZoom(15);
            }
    
            marker.setPosition(place.geometry.location);
            $('#latitudeMob').val(marker.getPosition().lat());
            $('#longitudeMob').val(marker.getPosition().lng());
        });
    
        $('#saveLatLngAttraction').click(function() {
        $('#latitudeMob').val(marker.getPosition().lat());
        $('#longitudeMob').val(marker.getPosition().lng());
        
        var latlng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
    
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
    
                var result = results[0];
                var locationName = $('#keywordAttractionMob').val()
                console.log(results);
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountryCode').val(ac.short_name);
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountry').val(ac.long_name);
                    if(ac.types.indexOf("postal_code") >= 0) state = $('#sharesparestepAddressPostalcode').val(ac.long_name);
                }
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("postal_town") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);
                        break;
                    }else if(ac.types.indexOf("administrative_area_level_2") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);   
                    }
                }

                $('#sharesparestepAddressCity').prop("disabled", false);
                $('#sharesparestepAddressPostalcode').prop("disabled", false);
                $('#sharesparestepAddressCountry').prop("disabled", false);
                $('#sharesparestepAddressCountryCode').prop("disabled", false);

            } else {
                console.log("Geocoder failed due to: " + status);
            }
        });
    
        return false;
    
            });
    
        }, 500);

    }

    $(function() {
    
        var swAttraction = new google.maps.LatLng(51.29885215199866, -0.828094482421875);
        var neAttraction = new google.maps.LatLng(51.76529023435832, 0.611114501953125);
        var defaultBoundsAttraction = new google.maps.LatLngBounds(swAttraction, neAttraction);
        var defaultOptionsAttraction = {
            center: new google.maps.LatLng(51.508515, -0.12548719999995228),
            zoom: 7,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    
        var gLat = isNaN($('#gestLat').val()) ? 0 : Number($('#gestLat').val());
        var gLng = isNaN($('#gestLng').val()) ? 0 : Number($('#gestLng').val());
        var gLatLng = new google.maps.LatLng(gLat, gLng);
        if (defaultBoundsAttraction.contains(gLatLng)) {
            defaultOptionsAttraction.zoom = 15;
            defaultOptionsAttraction.center = gLatLng;
        }
    
        var mapAttraction = new google.maps.Map(document.getElementById("mapContainer"), defaultOptionsAttraction);
        google.maps.event.trigger(mapAttraction, "resize");
        var input = document.getElementById("keywordAttraction");
        
        var autocomplete = new google.maps.places.Autocomplete(input);
        
        autocomplete.bindTo('bounds', mapAttraction);
        marker.setMap(mapAttraction);
    
        if (defaultBoundsAttraction.contains(gLatLng)) {
            marker.setPosition(gLatLng);
        }
        
        /* If Google Map Co-Ordinates already Exist, show marker on the map */
        if ($('#latitude').val().length > 1  && $('#longitude').val().length >1)
        {
            var existingLocationAttraction = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
            
            marker.setPosition(existingLocationAttraction); 
            marker.setTitle($('#name').val());
            $('#keywordAttraction').val($('#name').val());
        }
    
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
    
            if (place.geometry.viewport) {
                mapAttraction.fitBounds(place.geometry.viewport);
            } else {
                mapAttraction.setCenter(place.geometry.location);
                mapAttraction.setZoom(15);
            }
    
            marker.setPosition(place.geometry.location);
            $('#latitude').val(marker.getPosition().lat());
            $('#longitude').val(marker.getPosition().lng());
        });
    
        $('#saveLatLngAttraction').click(function() {
        $('#latitude').val(marker.getPosition().lat());
        $('#longitude').val(marker.getPosition().lng());
        
        var latlng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
    
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
    
                var result = results[0];
                var locationName = $('#keywordAttraction').val()
                console.log(results);
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountryCode').val(ac.short_name);
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountry').val(ac.long_name);
                    if(ac.types.indexOf("postal_code") >= 0) state = $('#sharesparestepAddressPostalcode').val(ac.long_name);
                }
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("postal_town") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);
                        break;
                    }else if(ac.types.indexOf("administrative_area_level_2") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);   
                    }
                }

                $('#sharesparestepAddressCity').prop("disabled", false);
                $('#sharesparestepAddressPostalcode').prop("disabled", false);
                $('#sharesparestepAddressCountry').prop("disabled", false);
                $('#sharesparestepAddressCountryCode').prop("disabled", false);

            } else {
                console.log("Geocoder failed due to: " + status);
            }
        });
    
        return false;
    
            });
    
        });
</script>