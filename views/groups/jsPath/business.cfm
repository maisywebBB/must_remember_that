<script src="/assets/js/bootbox.js"></script>

<script type="text/javascript">
    
    $(document).ready(function(){
        $(document.body).on("click", ".showMoreInfo", function(){

            var theClass = $(this).attr("data-toshow")

            $('.' + theClass).slideToggle();
            //$(this).hide();

            return false;

        });

        $(document.body).on("click", "#checkAll", function(){
            var checkedStatus = this.checked;
            $(document.body).find('.cbrid').each(function() {
                $(this).prop('checked', checkedStatus);
            });

        });
        
        $(document.body).on("click", "#addAllSelect, #addAllSelect2, #addAllSelectMob", function(){

        	var checkedValues = $('.cbrid:checked').map(function() {
			    return this.value;
			}).get();

            console.log(checkedValues);

           <cfif structKeyExists(session.auth, 'userId')>
               
               $.ajax({
                    type: "POST",
                    url: "<cfoutput>#buildUrl("ajax.batchReminder")#</cfoutput>",
                    dataType: "json",
                    data: { reminders : JSON.stringify(checkedValues), userId : '<cfoutput>#session.auth.userId#</cfoutput>' }
                }).done(function( html ) {
                    if(html['errorMessage']){
                        //$(".reminderError").html( html.errorMessage );
                        //$(".reminderError").show();
                        bootbox.alert(html.successMessage, function() {
                            console.log("Alert Callback");
                        });
                    }else if(html['successMessage']){
                        //$( ".reminderSuccess" ).html( html.successMessage );
                        //$(".reminderSuccess").show();

                        bootbox.alert(html.successMessage, function() {
                            console.log("Alert Callback");
                        });

                    }
                });

            <cfelse>

                $.ajax({
                    type: "POST",
                    url: "<cfoutput>#buildUrl("ajax.setReminderSessions")#</cfoutput>",
                    dataType: "json",
                    data: { reminders : JSON.stringify(checkedValues) }
                }).done(function( html ) {
                    <cfset itemLink = buildUrl("main.registration") & '&msg=login&slug=' & rc.getBusiness.urlslug>
                    var redirectLink = '<cfoutput>#itemLink#</cfoutput>'
                    console.log(redirectLink);
                    window.location.href = redirectLink;
                });

                <cfset itemLink = buildUrl("main.registration") & '&msg=login&slug=' & rc.getBusiness.urlslug>
                //var redirectLink = '<cfoutput>#itemLink#</cfoutput>'
                //window.location.href = redirectLink;
            </cfif>

           return false;

        });
        
        $(document.body).on("click", ".unsubscribeBtn", function(){

            $.ajax({
                    type: "POST",
                    url: "<cfoutput>#buildUrl("ajax.unsubscribe")#</cfoutput>",
                    dataType: "json",
                    data: { businessId : '<cfoutput>#rc.getBusiness.businessId#</cfoutput>', userId : '<cfoutput>#session.auth.userId#</cfoutput>' }
                }).done(function( html ) {
                    if(html['errorMessage']){
                        //$(".reminderError").html( html.errorMessage );
                        //$(".reminderError").show();
                        bootbox.alert(html.successMessage, function() {
                            console.log("Alert Callback");
                            location.reload();
                        });
                    }else if(html['successMessage']){
                        //$( ".reminderSuccess" ).html( html.successMessage );
                        //$(".reminderSuccess").show();

                        bootbox.alert(html.successMessage, function() {
                            console.log("Alert Callback");
                            location.reload();
                        });

                    }
                });

        });

        $(document.body).on("click", ".subscribeAct", function(){

            var radioValue = $("input[name='subscriptionType']:checked"). val();

           $.ajax({
                type: "POST",
                url: "<cfoutput>#buildUrl("ajax.subscribe")#</cfoutput>",
                dataType: "json",
                data: { businessId : '<cfoutput>#rc.getBusiness.businessId#</cfoutput>', userId : '<cfoutput>#session.auth.userId#</cfoutput>', subscriptionType : radioValue }
            }).done(function( html ) {
                if(html['errorMessage']){
                    //$(".reminderError").html( html.errorMessage );
                    //$(".reminderError").show();
                    $('#modalContent').html(html.successMessage, function() {
                        console.log("Alert Callback");
                        location.reload();
                    });
                }else if(html['successMessage']){
                    //$( ".reminderSuccess" ).html( html.successMessage );
                    //$(".reminderSuccess").show();

                    $('#modalContent').html(html.successMessage, function() {
                        console.log("Alert Callback");
                        location.reload();
                    });

                }
            });

           return false;

        });

        $(document.body).on("click", ".singleReminderBtn", function(){

            var checkedValues = [$(this).attr("data-rid")];
            
            <cfif structKeyExists(session.auth, 'userId')>
                $.ajax({
                    type: "POST",
                    url: "<cfoutput>#buildUrl("ajax.batchReminder")#</cfoutput>",
                    dataType: "json",
                    data: { reminders : JSON.stringify(checkedValues), userId : '<cfoutput>#session.auth.userId#</cfoutput>' }
                }).done(function( html ) {
                    if(html['errorMessage']){
                        //$(".reminderError").html( html.errorMessage );
                        //$(".reminderError").show();
                        bootbox.alert(html.successMessage, function() {
                            console.log("Alert Callback");
                        });
                    }else if(html['successMessage']){
                        //$( ".reminderSuccess" ).html(  );
                        //$(".reminderSuccess").show();
                        bootbox.alert(html.successMessage, function() {
                            console.log("Alert Callback");
                        });
                    }
                });
            
           </cfif>
                
           return false;

        });
        
    });
</script>