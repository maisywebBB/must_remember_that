<div class="row">
    
    <div class="col-lg-12 text-center">
        <div class="btn-group hidden-xs">
            <a href="/groups/<cfoutput>#rc.getGroups.urlslug#</cfoutput>/" class="btn btn-primary">View group</a>
            <a href="#" id="viewAdminUsers" class="btn btn-primary">View group users</a>
            <a href="<cfoutput>#buildUrl("groups.delete")#&groupid=#rc.groupid#</cfoutput>" id="deleteAdminUsers" class="btn btn-primary" onclick="return confirm('Are you sure you want to delete this group?');">Delete group</a>
        </div>
    </div>

    <div class="col-lg-12 visible-xs">
        <a href="/groups/<cfoutput>#rc.getGroups.urlslug#</cfoutput>/" class="btn btn-primary visible-xs">View group</a>
        <a href="#" id="viewAdminUsersMob" class="btn btn-primary visible-xs">View group users</a>
        <a href="<cfoutput>#buildUrl("groups.delete")#&groupid=#rc.groupid#</cfoutput>" id="deleteAdminUsers" class="btn btn-primary visible-xs" onclick="return confirm('Are you sure you want to delete this group?');">Delete group</a>
    </div>
    
    <div class="col-lg-6 visible-xs">
        <div style="display: none;" id="displayAdminUsersMob">
            <legend>Admin Users</legend>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query="rc.getGroupUsers">
                                <tr>
                                    <td width="95%">#rc.getGroupUsers.firstname# #rc.getGroupUsers.lastname#</td>
                                    <td width="5%">
                                        <a onclick="return confirm('Are you sure you want to remove this administrator?');" href="<cfoutput>#buildUrl("groups.edit")#&groupid=#rc.groupid#&update=deleteAdmin&userId=#rc.getGroupuSers.userId#</cfoutput>"><i class="fa fa-trash"></i></a>
                                    </td>
                                 </tr>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        
        <div style="margin-top:10px;" class="well bs-component collapse in">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="updateGroups"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Update <cfoutput>#rc.title#</cfoutput></legend>
                    <!--- <div class="form-group">
                        <label for="title">URL - <a href="http://<cfoutput>#cgi.http_host#/groups/#rc.urlslug#/</cfoutput>">http://<cfoutput>#cgi.http_host#/groups/#rc.urlslug#/</cfoutput></a>    <i data-clipboard-text="http://<cfoutput>#cgi.http_host#/groups/#rc.urlslug#/</cfoutput>" class="copyURL fa fa-copy"></i></label>
                    </div> --->
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">About Us Content</label>
                        <textarea name="aboutContent" class="form-control"><cfoutput>#rc.aboutContent#</cfoutput></textarea>
                    </div>
                    <!--- 
                    <div class="form-group">
                        <label for="title">Web Address</label>
                        <input type="text" placeholder="http://" id="website" name="website" class="form-control" value="<cfoutput>#rc.website#</cfoutput>">
                    </div>
                    <div class="form-group">
                        <label for="title">Banner Image</label>
                        <input type="file" id="heroImageURL" name="heroImageURL">
                    </div>
                    <div class="form-group">
                        <label for="title">Logo (160px X 160px)</label>
                        <input type="file" id="logo" name="logo">
                    </div>
                     --->
                    <div class="form-group">
                        <label for="inputEmail">Make Group Page Live</label>
                        <select id="isLive" name="isLive" class="form-control" data-role="none">              
                            <option <cfif rc.isLive EQ 1>selected="selected"</cfif> value="1">Yes</option>
                            <option <cfif rc.isLive EQ 0>selected="selected"</cfif> value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div>

    <div class="col-lg-6">
        
        <div style="margin-top:10px;" class="well bs-component collapse in">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="searchadminUser"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Add member</legend>
                    <div class="form-group">
                        <label for="emailAddress">Users Name</label>
                        <input type="text" placeholder="Users Name" id="usersname" name="usersname" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Add</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>

        <cfif structKeyExists(RC, 'searchUserResults')>
            <div id="displayFoundUsers">
                <legend>Found Members</legend>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <cfoutput>
                                <cfloop query="rc.searchUserResults">
                                    <tr>
                                        <td width="80%">
                                            #application.utilsCFC.createUserCroppedImage(rc.searchUserResults.userId)# #rc.searchUserResults.firstname# #rc.searchUserResults.lastname#
                                            <a style="float: right; margin-top:20px" class="btn btn-primary" href="<cfoutput>#buildUrl("groups.edit")#&update=addadminUser&userId=#rc.searchUserResults.userId#&groupId=#rc.groupId#</cfoutput>">Add to group</a>
                                        </td>
                                     </tr>
                                </cfloop>
                            </cfoutput>
                        </tbody>
                    </table>
                </div>
            </div>
        </cfif>

        <div style="display: none;" id="displayAdminUsers" class="hidden-xs">
            <legend>Group Members</legend>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query="rc.getGroupuSers">
                                <tr>
                                    <td width="95%">
                                        #rc.getGroupuSers.firstname# #rc.getGroupuSers.lastname#<br/>
                                        <strong>Allow Adding Reminders:</strong> <label class="radio-inline"><input type="radio" name="optradio">Yes</label>
                                        <label class="radio-inline"><input type="radio" name="optradio">No</label>
                                    </td>
                                    <td width="5%">
                                        <a onclick="return confirm('Are you sure you want to remove this administrator?');" href="<cfoutput>#buildUrl("groups.edit")#&groupid=#rc.groupid#&update=deleteAdmin&userId=#rc.getGroupuSers.userId#</cfoutput>"><i class="fa fa-trash"></i></a>
                                    </td>
                                 </tr>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>

    </div>


</div>
        