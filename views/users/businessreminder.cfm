<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        
        <div class="well bs-component collapse in">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="newReminder"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Add Reminder</legend>
                    <div class="form-group col-md-12">
                        <label for="title">Title</label>
                        <input type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Category</label>
                        <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                            <option value="">Select Category</option>
                            <cfoutput query="rc.getMainCats">
                                <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                            </cfoutput>
                        </select>
                    </div>
                    <div id="subCatContainer" class="form-group col-md-12">
                        <label for="category">Sub Category</label>
                        <select id="category" name="category" class="form-control" data-role="none">   
                            <cfoutput query="rc.getCats">
                                <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                            </cfoutput>  
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Date</label>
                        <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.rdate#</cfoutput>" />
                        <input type="text" id="inputEmail" class="form-control form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="rdate" data-link-format="yyyy-mm-dd" data-picker-position="bottom-left" <cfif len(rc.rdate)>value="<cfoutput>#day(rc.rdate)# #monthAsString(month(rc.rdate))# #year(rc.rdate)#</cfoutput>"</cfif>>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Time</label>
                        <input type="hidden" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" />
                        <input type="text" id="inputEmail" class="form-control form_time" data-date="" data-date-format="hh:ii" data-link-field="rtime" data-link-format="hh:ii" data-picker-position="bottom-left" value="<cfoutput>#rc.rtime#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">How frequent</label>
                        <select id="interval" name="interval" class="form-control" data-role="none">              
                            <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                            <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                            <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                            <option <cfif rc.interval EQ 'weekly'>selected="selected"</cfif> value="weekly">Weekly</option>
                            <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                            <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Send me reminder</label>
                        <select id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                            <cfloop from="1" to="100" index="i"><cfoutput>
                                <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                            </cfoutput></cfloop>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">&nbsp;</label>
                        <select id="remindertime" name="remindertime" class="form-control" data-role="none">              
                            <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                            <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour before</option>
                            <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day Before</option>
                            <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week Before</option>
                            <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month Before</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Comments</label>
                        <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-default" type="reset">Cancel</button>
                        <button class="btn btn-primary" type="submit">Set Reminder</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div>

</div>
        