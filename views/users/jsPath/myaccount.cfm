<!--- <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script src="/assets/pictureCut/src/jquery.picture.cut.js"></script>

<script>
    $(document).ready(function(){

       $("#container_photo").PictureCut({                                
            InputOfImageDirectory       : "image",
            PluginFolderOnServer        : "/assets/pictureCut/",
            FolderOnServer              : "/assets/businessImages/",                
            EnableCrop                  : true,
            CropWindowStyle             : "Bootstrap"                        
        });

    })
</script> --->

<script src="/assets/jcrop/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="/assets/jcrop/css/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="/assets/jcrop/css/style.css" type="text/css" />

<script language="Javascript">
	
	$(document).ready(function(){			

		// obtain original image dimensions
		var originalImgHeight 	= $('#cropbox').height();
		var originalImgWidth 	= $('#cropbox').width();

		// set the padding for the crop-selection box
		var padding = 10;
		
		// set the x and y coords using the image dimensions
		// and the padding to leave a border
		var setX = originalImgHeight-padding;
		var setY = originalImgWidth-padding;
		
		// create variables for the form field elements
		var imgX 		= $('input[name=x]');
		var imgY 		= $('input[name=y]');
		var imgHeight 	= $('input[name=h]');
		var imgWidth 	= $('input[name=w]');
		var imgLoc 		= $('input[name=imageFile]');
		
		// get the current image source in the main view
		var currentImage = $("#croppedImage img").attr('src');
		
		setImageFileValue(currentImage);
		
			// instantiate the jcrop plugin
			buildJCrop();
			
			// selecting revert will create the img html tag complete with
			// image source attribute, read from the imageFile form field
			$("#revert_btn").click(function() {					
				var htmlImg = '<img src="' + $('input[name=imageFile]').val() 
						+ '" id="cropbox" />';
				$('#croppedImage').html(htmlImg);
				// instantiate the jcrop plugin
				buildJCrop();
				
			});
			
			$("#imageCrop_btn").click(function(){					
				// organise data into a readable string
				var data = 'imgX=' + imgX.val() + '&imgY=' + imgY.val() + 
						'&height=' + imgHeight.val() + '&width=' + imgWidth.val() + 
						'&imgLoc=' + encodeURIComponent(imgLoc.val());
				// 
				$('#cropped').load('/controllers/ajax.cfc?method=imgCrop',data);
				$('#myModal').modal('toggle');
				
				// do not submit the form using the default behaviour
				return false;
			});
			
			// add the jQuery invocation into a separate function,
			// which we will need to call more than once
			function buildJCrop() {
				jcrop_api = $('#cropbox').Jcrop({
					aspectRatio: 1,
					onChange: showCoords,
					onSelect: showCoords,
					setSelect: [padding,padding,setY,setX]
				});
				// enable the image crop button and
				// disable the revert button
				$('#imageCrop_btn').removeAttr('disabled');
				$('#revert_btn').attr('disabled', 'disabled');
			}
			
			// set the imageFile form field value to match
			// the new image source
			function setImageFileValue(imageSource) {
				imgLoc.val(imageSource);
			}
			
		});

		// Our simple event handler, called from onChange and onSelect
		// event handlers, as per the Jcrop invocation above
		function showCoords(c) {
			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#x2').val(c.x2);
			$('#y2').val(c.y2);
			$('#w').val(c.w);
			$('#h').val(c.h);		
		};


</script>