<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&v=3.7"></script>
<link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<style>
    .bootstrap-datetimepicker-widget.dropdown-menu {
        width: auto !important;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('#reminderActualTimeDiv').hide();
        $('#reminderActualTimeMobDiv').hide();

        $('.share_button').click(function(e){

            var reminderTitle = $(this).data('remindertitle');

            e.preventDefault();
            FB.ui(
            {
                method: 'feed',
                name: reminderTitle,
                link: ' http://www.mustrememberthat.com/',
                picture: 'http://www.mustrememberthat.com/images/MRT-facebook-icon5.png',
                caption: 'I have just remembered this thanks to Must Remember That',
                description: 'MUST REMEMBER THAT - Never Forget Again',
                message: ''
            });
        });

        $('.addReminderDiv').click(function(e){
            $('#addReminderDiv').slideToggle();
            var swAttraction = new google.maps.LatLng(51.29885215199866, -0.828094482421875);
        var neAttraction = new google.maps.LatLng(51.76529023435832, 0.611114501953125);
        var defaultBoundsAttraction = new google.maps.LatLngBounds(swAttraction, neAttraction);
        var defaultOptionsAttraction = {
            center: new google.maps.LatLng(51.508515, -0.12548719999995228),
            zoom: 7,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    
        var gLat = isNaN($('#gestLat').val()) ? 0 : Number($('#gestLat').val());
        var gLng = isNaN($('#gestLng').val()) ? 0 : Number($('#gestLng').val());
        var gLatLng = new google.maps.LatLng(gLat, gLng);
        if (defaultBoundsAttraction.contains(gLatLng)) {
            defaultOptionsAttraction.zoom = 15;
            defaultOptionsAttraction.center = gLatLng;
        }
    
        var mapAttraction = new google.maps.Map(document.getElementById("mapContainer"), defaultOptionsAttraction);
        google.maps.event.trigger(mapAttraction, "resize");
        var input = document.getElementById("keywordAttraction");
        
        var autocomplete = new google.maps.places.Autocomplete(input);
        
        autocomplete.bindTo('bounds', mapAttraction);
        marker.setMap(mapAttraction);
    
        if (defaultBoundsAttraction.contains(gLatLng)) {
            marker.setPosition(gLatLng);
        }
        
        /* If Google Map Co-Ordinates already Exist, show marker on the map */
        if ($('#latitude').val().length > 1  && $('#longitude').val().length >1)
        {
            var existingLocationAttraction = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
            
            marker.setPosition(existingLocationAttraction); 
            marker.setTitle($('#name').val());
            $('#keywordAttraction').val($('#name').val());
        }
    
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
    
            if (place.geometry.viewport) {
                mapAttraction.fitBounds(place.geometry.viewport);
            } else {
                mapAttraction.setCenter(place.geometry.location);
                mapAttraction.setZoom(15);
            }
    
            marker.setPosition(place.geometry.location);
            $('#latitude').val(marker.getPosition().lat());
            $('#longitude').val(marker.getPosition().lng());
        });
    
        $('#saveLatLngAttraction').click(function() {
        $('#latitude').val(marker.getPosition().lat());
        $('#longitude').val(marker.getPosition().lng());
        
        var latlng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
    
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
    
                var result = results[0];
                var locationName = $('#keywordAttraction').val()
                console.log(results);
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountryCode').val(ac.short_name);
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountry').val(ac.long_name);
                    if(ac.types.indexOf("postal_code") >= 0) state = $('#sharesparestepAddressPostalcode').val(ac.long_name);
                }
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("postal_town") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);
                        break;
                    }else if(ac.types.indexOf("administrative_area_level_2") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);   
                    }
                }

                $('#sharesparestepAddressCity').prop("disabled", false);
                $('#sharesparestepAddressPostalcode').prop("disabled", false);
                $('#sharesparestepAddressCountry').prop("disabled", false);
                $('#sharesparestepAddressCountryCode').prop("disabled", false);

            } else {
                console.log("Geocoder failed due to: " + status);
            }
        });
    
        return false;
    
            });
        });

        /*$(window).bind('resize load', function() {
            if ($(this).width() < 767) {
                $('#addReminderDiv').removeClass('in');
                $('#addReminderDiv').addClass('out');
            } else {
                $('#addReminderDiv').removeClass('out');
                $('#addReminderDiv').addClass('in');
            }
        });*/
    
        if($("#getPersonalReminders").length == 0) {
        }else{
            $('input#getPersonalReminders').on('keyup',function(){
                var charCount = $(this).val().replace(/\s/g, '').length;
                if(charCount >= 0){
                    var searchString = $('input#getPersonalReminders').val();
                    $.ajax({
                        type: "POST",
                        url: "<cfoutput>#buildUrl("ajax.searchPersonalReminders")#</cfoutput>",
                        data: { searchString : searchString, reminderKey : '<cfoutput>#session.auth.userId#</cfoutput>'},
                        success: function(result){
                            $('ul.list-group').html(result);
                            $('.pagination').hide();
                        }
                                    
                    });

                }

            });
        }

        $(document.body).on("click", ".suggestedReminderLink", function(){

            clearform();
            var safeURL = $(this).data('safeurl');
            
            $('#reminderModalTitle').html('New Reminder');

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=safeURL',
                dataType: "JSON",
                data: 'safeURL=' + safeURL,
                success: function(data){
                    $('#modalmaincategory').val(data.MAINCATEGORY);
                    
                    $.ajax({    
                        method: 'get',
                        url: 'controllers/ajax.cfc?method=getCategories',
                        dataType: "html",
                        data: 'mainCategoryId=' + data.MAINCATEGORY,
                        success: function(data){
                            $('#modalcategory').html(data);
                        },
                        error: function(data){
                            alert('There has been an error. Please try again');
                        }
                    });

                    setTimeout( function() 
                      {
                        $('#modalcategory').val(data.CATEGORY);
                      }, 500);

                    $('.form_date').datetimepicker({
                        date: new Date(data.EVENTDATE),
                    });
                    $('#modalrtime').val(data.EVENTTIME);
                    $('#modalrtime').datetimepicker({
                        showClear: true,
                        format: 'HH:mm'
                    });
                    $('#modalremindertime').val(data.REMINDERTIME);
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });

        $(document.body).on("click", ".updateReminderLink", function(){

            clearform();
            var reminderid = $(this).data('reminderid');
            
            $('#reminderModalTitle').html('Update Reminder');

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getReminder',
                dataType: "JSON",
                data: 'reminderid=' + reminderid,
                success: function(data){
                    $('#modalReminderId').val(reminderid);
                    $('#modaltitle').val(data.TITLE);
                    $('#modalmaincategory').val(data.MAINCATEGORY);
                    
                    $.ajax({    
                        method: 'get',
                        url: 'controllers/ajax.cfc?method=getCategories',
                        dataType: "html",
                        data: 'mainCategoryId=' + data.MAINCATEGORY,
                        success: function(data){
                            $('#modalcategory').html(data);
                        },
                        error: function(data){
                            alert('There has been an error. Please try again');
                        }
                    });

                    setTimeout( function() 
                      {
                        $('#modalcategory').val(data.CATEGORY);
                      }, 500);
                    
                    $('#modalrdate').val(data.EVENTDATE);
                    $('#modalinputdate').val(data.EVENTDISPLAYDATE);
                    $('#modalrtime').val(data.EVENTTIME);
                    
                    $('#modalintervalPeriod').val(data.INTERVALPERIOD);
                    $('#modalremindertime').val(data.REMINDERTIME);
                    $('#modalinterval').val(data.INTERVAL);
                    $('#modalcomments').val(data.COMMENTS);
                    
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });

        $(document.body).on("change", "#maincategory", function(){

            $('#subCatContainer').show();

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=' + this.value,
                success: function(data){
                    $('#category').html(data);
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });

        $(document.body).on("change", "#modalmaincategory", function(){

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=' + this.value,
                success: function(data){
                    $('#modalcategory').html(data);
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });

        });

        $(document.body).on("change", "#modalremindertime", function(){

            //get the selected value
            var reminderType = $( "#modalremindertime option:selected" ).val();

            if(reminderType == 'd' || reminderType == 'ww' || reminderType == 'm' ){
                $('#reminderActualTimeModal').show();
            }else{
                $('#reminderActualTimeModal').hide();
            }

        });

        $(document.body).on("change", "#remindertime", function(){

            //get the selected value
            var reminderType = $( "#remindertime option:selected" ).val();

            if(reminderType == 'd' || reminderType == 'ww' || reminderType == 'm' ){
                $('#reminderActualTimeDiv').show();
            }else{
                $('#reminderActualTimeDiv').hide();
            }

        });

        $(document.body).on("change", "#remindertimeMob", function(){

            //get the selected value
            var reminderType = $( "#remindertimeMob option:selected" ).val();

            if(reminderType == 'd' || reminderType == 'ww' || reminderType == 'm' ){
                $('#reminderActualTimeMobDiv').show();
            }else{
                $('#reminderActualTimeMobDiv').hide();
            }

        });
        
    });
    
    $('.form_time').datetimepicker({
        format: 'HH:mm',
        useCurrent: true
    }).on('dp.change', function(e) {
        if (e.oldDate === null) {
            $(this).data('DateTimePicker').date(new Date(e.date._d.setHours(09, 00, 00)));
        }
    });

    // date picker
    $('.form_date').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: true
    }).on('dp.change', function(ev) {
        var selectedDate = new Date(ev.date);
        
        var dd = selectedDate.getDate();
        var mm = selectedDate.getMonth()+1; //January is 0!

        var yyyy = selectedDate.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
        var selectedDate = mm+'/'+dd+'/'+yyyy;
        document.getElementById("rdate").value = selectedDate;
    });

    $('.form_date2').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: true
    }).on('dp.change', function(ev) {
        var selectedDate = new Date(ev.date);
        
        var dd = selectedDate.getDate();
        var mm = selectedDate.getMonth()+1; //January is 0!

        var yyyy = selectedDate.getFullYear();
        if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 
        var selectedDate = mm+'/'+dd+'/'+yyyy;
        document.getElementById("modalrdate").value = selectedDate;
    });

    $( "#modalForm" ).submit(function( event ) {
        
        $.ajax({
            type: "POST",
            url: "<cfoutput>#buildUrl("ajax.modalReminder")#</cfoutput>",
            data: $( this ).serialize(),
            dataType: "html",
            success: function(result){
                console.log(result)
                location.reload();
            },
            error: function(err){
                $('#modalError').show();
                $('.modalContainer').html($.trim(err.responseText));
            }
        });

        event.preventDefault();

    });

    function clearform() {
        $('#modalError').hide();
        $('#modaltitle').val("");
        $('#modalmaincategory').val("");
        $('#modalcategory').val("");
        $('#modalrdate').val("");
        $('#modalinputdate').val("");
        $('#modalrtime').val("");
        $('#modalintervalPeriod').val("");
        $('#modalremindertime').val("");
        $('#modalinterval').val("");
        $('#modalcomments').val("");
        $('#modalReminderId').val("");
    }


    var geocoder = new google.maps.Geocoder();
    var marker = new google.maps.Marker();
    
    $(function() {
    
        var swAttraction = new google.maps.LatLng(51.29885215199866, -0.828094482421875);
        var neAttraction = new google.maps.LatLng(51.76529023435832, 0.611114501953125);
        var defaultBoundsAttraction = new google.maps.LatLngBounds(swAttraction, neAttraction);
        var defaultOptionsAttraction = {
            center: new google.maps.LatLng(51.508515, -0.12548719999995228),
            zoom: 7,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
    
        var gLat = isNaN($('#gestLat').val()) ? 0 : Number($('#gestLat').val());
        var gLng = isNaN($('#gestLng').val()) ? 0 : Number($('#gestLng').val());
        var gLatLng = new google.maps.LatLng(gLat, gLng);
        if (defaultBoundsAttraction.contains(gLatLng)) {
            defaultOptionsAttraction.zoom = 15;
            defaultOptionsAttraction.center = gLatLng;
        }
    
        var mapAttraction = new google.maps.Map(document.getElementById("mapContainer"), defaultOptionsAttraction);
        google.maps.event.trigger(mapAttraction, "resize");
        var input = document.getElementById("keywordAttraction");
        
        var autocomplete = new google.maps.places.Autocomplete(input);
        
        autocomplete.bindTo('bounds', mapAttraction);
        marker.setMap(mapAttraction);
    
        if (defaultBoundsAttraction.contains(gLatLng)) {
            marker.setPosition(gLatLng);
        }
        
        /* If Google Map Co-Ordinates already Exist, show marker on the map */
        if ($('#latitude').val().length > 1  && $('#longitude').val().length >1)
        {
            var existingLocationAttraction = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
            
            marker.setPosition(existingLocationAttraction); 
            marker.setTitle($('#name').val());
            $('#keywordAttraction').val($('#name').val());
        }
    
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
    
            if (place.geometry.viewport) {
                mapAttraction.fitBounds(place.geometry.viewport);
            } else {
                mapAttraction.setCenter(place.geometry.location);
                mapAttraction.setZoom(15);
            }
    
            marker.setPosition(place.geometry.location);
            $('#latitude').val(marker.getPosition().lat());
            $('#longitude').val(marker.getPosition().lng());
        });
    
        $('#saveLatLngAttraction').click(function() {
        $('#latitude').val(marker.getPosition().lat());
        $('#longitude').val(marker.getPosition().lng());
        
        var latlng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
    
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
    
                var result = results[0];
                var locationName = $('#keywordAttraction').val()
                console.log(results);
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountryCode').val(ac.short_name);
                    if(ac.types.indexOf("country") >= 0) state = $('#sharesparestepAddressCountry').val(ac.long_name);
                    if(ac.types.indexOf("postal_code") >= 0) state = $('#sharesparestepAddressPostalcode').val(ac.long_name);
                }
                for(var i=0, len=result.address_components.length; i<len; i++) {
                    var ac = result.address_components[i];
                    if(ac.types.indexOf("postal_town") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);
                        break;
                    }else if(ac.types.indexOf("administrative_area_level_2") >= 0){
                        state = $('#sharesparestepAddressCity').val(ac.long_name);   
                    }
                }

                $('#sharesparestepAddressCity').prop("disabled", false);
                $('#sharesparestepAddressPostalcode').prop("disabled", false);
                $('#sharesparestepAddressCountry').prop("disabled", false);
                $('#sharesparestepAddressCountryCode').prop("disabled", false);

            } else {
                console.log("Geocoder failed due to: " + status);
            }
        });
    
        return false;
    
            });
    
        });

</script>

<cfif structKeyExists(RC, 'mainCategory') AND isNumeric(rc.mainCategory) AND  structKeyExists(RC, 'category') AND isNumeric(rc.category) >
    <script type="text/javascript">
        $(document).ready(function(){
            $('#subCatContainer').show();

            $.ajax({    
                method: 'get',
                url: 'controllers/ajax.cfc?method=getCategories',
                dataType: "html",
                data: 'mainCategoryId=<cfoutput>#rc.mainCategory#</cfoutput>',
                success: function(data){
                    $('#category').html(data);
                    $("#category").val('<cfoutput>#rc.category#</cfoutput>');
                },
                error: function(data){
                    alert('There has been an error. Please try again');
                }
            });
        });

    </script>
</cfif>