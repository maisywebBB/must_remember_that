<div class="col-lg-6">
    
    <cfif len(rc.getUser.profileImage)>
        <div class="well bs-component dropdown in" style="background-color:#fff">     
            <h3>Profile Image</h3>
            
            <div class="row profileImages">
                <div class="col-md-6">
                    <label>Current Image</label>
                    <div id="cropped">
                        <cfif NOT len(rc.getUser.imgX) OR (rc.getUser.imgX EQ 0 AND rc.getUser.imgY EQ 0)>
                            <img src="/assets/userImages/<cfoutput>#rc.getUser.profileImage#</cfoutput>" id="cropbox" style="max-width:100%" />
                        <cfelse>
                            <cfoutput>#application.utilsCFC.createCroppedImage()#</cfoutput>
                        </cfif>
                    </div>
                </div>
                <!-- Button trigger modal -->
                <div class="col-md-6">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                      Crop the image
                    </button>
                    <form method="post" action="">
                        <input type="hidden" name="formAction" value="rotate">
                        <input type="submit" value="Rotate the image" class="btn btn-primary">
                    </form>
                </div>
            </div>

        </div>
    </cfif>

    <div class="well bs-component dropdown in" style="background-color:#fff"> 
        <h3>Update Personal Details</h3>
        <form method="post" autocomplete="false" action="" class="form" id="register-form1" name="myForm" enctype="multipart/form-data">
            <input type="hidden" name="update" value="userDetails"/>

            <fieldset>
                <div class="row">
                    <!-- first name -->
                    <div class="form-group col-md-6">
                        <input type="text" required="" autocomplete="false" placeholder="First name" class="form-control" name="firstname" value="<cfoutput>#rc.getUser.firstname#</cfoutput>">
                    </div>
                    <!-- last name -->
                    <div class="form-group col-md-6">
                        <input type="text" required="" autocomplete="false" placeholder="Last name" class="form-control" name="lastname" value="<cfoutput>#rc.getUser.lastname#</cfoutput>">
                    </div>
                </div>
                <div class="row">
                    <!-- username -->
                    <div class="form-group col-md-6">
                        <!-- email -->
                        <input type="email" required="" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.confEmailAddress.pattern = this.value;" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,3}$" title="Email must be a valid email address" autocomplete="false" placeholder="E-mail" class="form-control" name="emailAddress" value="<cfoutput>#rc.getUser.emailAddress#</cfoutput>">
                    </div>
                    <div class="form-group col-md-6">
                        <!-- email -->
                        <select required="" placeholder="gender" name="gender" class="form-control" data-role="none">
                            <option <cfif rc.getUser.Gender EQ 'F'>selected="selected"</cfif> value="F">Female</option>
                            <option <cfif rc.getUser.Gender EQ 'M'>selected="selected"</cfif> value="M">Male</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <!-- username -->
                    <div class="form-group col-md-6">
                        <!-- email -->
                        <input type="text" required="" autocomplete="false" placeholder="Home Town" class="form-control" name="homeTown" value="<cfoutput>#rc.getUser.homeTown#</cfoutput>">
                    </div>
                    <div class="form-group col-md-6">
                        <!-- email -->
                        <select placeholder="country" name="country" class="form-control" data-role="none">
                            <option value="">Select a country</option>
                            <option <cfif rc.getUser.country EQ 'AF'>selected="selected"</cfif> value="AF">Afghanistan</option>
                            <option <cfif rc.getUser.country EQ 'AX'>selected="selected"</cfif> value="AX">Aland Islands</option>
                            <option <cfif rc.getUser.country EQ 'AL'>selected="selected"</cfif> value="AL">Albania</option>
                            <option <cfif rc.getUser.country EQ 'DZ'>selected="selected"</cfif> value="DZ">Algeria</option>
                            <option <cfif rc.getUser.country EQ 'AS'>selected="selected"</cfif> value="AS">American Samoa</option>
                            <option <cfif rc.getUser.country EQ 'AD'>selected="selected"</cfif> value="AD">Andorra</option>
                            <option <cfif rc.getUser.country EQ 'AO'>selected="selected"</cfif> value="AO">Angola</option>
                            <option <cfif rc.getUser.country EQ 'AI'>selected="selected"</cfif> value="AI">Anguilla</option>
                            <option <cfif rc.getUser.country EQ 'AQ'>selected="selected"</cfif> value="AQ">Antarctica</option>
                            <option <cfif rc.getUser.country EQ 'AG'>selected="selected"</cfif> value="AG">Antigua and Barbuda</option>
                            <option <cfif rc.getUser.country EQ 'AR'>selected="selected"</cfif> value="AR">Argentina</option>
                            <option <cfif rc.getUser.country EQ 'AM'>selected="selected"</cfif> value="AM">Armenia</option>
                            <option <cfif rc.getUser.country EQ 'AW'>selected="selected"</cfif> value="AW">Aruba</option>
                            <option <cfif rc.getUser.country EQ 'AU'>selected="selected"</cfif> value="AU">Australia</option>
                            <option <cfif rc.getUser.country EQ 'AT'>selected="selected"</cfif> value="AT">Austria</option>
                            <option <cfif rc.getUser.country EQ 'AZ'>selected="selected"</cfif> value="AZ">Azerbaijan</option>
                            <option <cfif rc.getUser.country EQ 'BS'>selected="selected"</cfif> value="BS">Bahamas</option>
                            <option <cfif rc.getUser.country EQ 'BH'>selected="selected"</cfif> value="BH">Bahrain</option>
                            <option <cfif rc.getUser.country EQ 'BD'>selected="selected"</cfif> value="BD">Bangladesh</option>
                            <option <cfif rc.getUser.country EQ 'BB'>selected="selected"</cfif> value="BB">Barbados</option>
                            <option <cfif rc.getUser.country EQ 'BY'>selected="selected"</cfif> value="BY">Belarus</option>
                            <option <cfif rc.getUser.country EQ 'BE'>selected="selected"</cfif> value="BE">Belgium</option>
                            <option <cfif rc.getUser.country EQ 'BZ'>selected="selected"</cfif> value="BZ">Belize</option>
                            <option <cfif rc.getUser.country EQ 'BJ'>selected="selected"</cfif> value="BJ">Benin</option>
                            <option <cfif rc.getUser.country EQ 'BM'>selected="selected"</cfif> value="BM">Bermuda</option>
                            <option <cfif rc.getUser.country EQ 'BT'>selected="selected"</cfif> value="BT">Bhutan</option>
                            <option <cfif rc.getUser.country EQ 'BO'>selected="selected"</cfif> value="BO">Bolivia, Plurinational State of</option>
                            <option <cfif rc.getUser.country EQ 'BQ'>selected="selected"</cfif> value="BQ">Bonaire, Sint Eustatius and Saba</option>
                            <option <cfif rc.getUser.country EQ 'BA'>selected="selected"</cfif> value="BA">Bosnia and Herzegovina</option>
                            <option <cfif rc.getUser.country EQ 'BW'>selected="selected"</cfif> value="BW">Botswana</option>
                            <option <cfif rc.getUser.country EQ 'BV'>selected="selected"</cfif> value="BV">Bouvet Island</option>
                            <option <cfif rc.getUser.country EQ 'BR'>selected="selected"</cfif> value="BR">Brazil</option>
                            <option <cfif rc.getUser.country EQ 'IO'>selected="selected"</cfif> value="IO">British Indian Ocean Territory</option>
                            <option <cfif rc.getUser.country EQ 'BN'>selected="selected"</cfif> value="BN">Brunei Darussalam</option>
                            <option <cfif rc.getUser.country EQ 'BG'>selected="selected"</cfif> value="BG">Bulgaria</option>
                            <option <cfif rc.getUser.country EQ 'BF'>selected="selected"</cfif> value="BF">Burkina Faso</option>
                            <option <cfif rc.getUser.country EQ 'BI'>selected="selected"</cfif> value="BI">Burundi</option>
                            <option <cfif rc.getUser.country EQ 'KH'>selected="selected"</cfif> value="KH">Cambodia</option>
                            <option <cfif rc.getUser.country EQ 'CM'>selected="selected"</cfif> value="CM">Cameroon</option>
                            <option <cfif rc.getUser.country EQ 'CA'>selected="selected"</cfif> value="CA">Canada</option>
                            <option <cfif rc.getUser.country EQ 'CV'>selected="selected"</cfif> value="CV">Cape Verde</option>
                            <option <cfif rc.getUser.country EQ 'KY'>selected="selected"</cfif> value="KY">Cayman Islands</option>
                            <option <cfif rc.getUser.country EQ 'CF'>selected="selected"</cfif> value="CF">Central African Republic</option>
                            <option <cfif rc.getUser.country EQ 'TD'>selected="selected"</cfif> value="TD">Chad</option>
                            <option <cfif rc.getUser.country EQ 'CL'>selected="selected"</cfif> value="CL">Chile</option>
                            <option <cfif rc.getUser.country EQ 'CN'>selected="selected"</cfif> value="CN">China</option>
                            <option <cfif rc.getUser.country EQ 'CX'>selected="selected"</cfif> value="CX">Christmas Island</option>
                            <option <cfif rc.getUser.country EQ 'CC'>selected="selected"</cfif> value="CC">Cocos (Keeling) Islands</option>
                            <option <cfif rc.getUser.country EQ 'CO'>selected="selected"</cfif> value="CO">Colombia</option>
                            <option <cfif rc.getUser.country EQ 'KM'>selected="selected"</cfif> value="KM">Comoros</option>
                            <option <cfif rc.getUser.country EQ 'CG'>selected="selected"</cfif> value="CG">Congo</option>
                            <option <cfif rc.getUser.country EQ 'CD'>selected="selected"</cfif> value="CD">Congo, the Democratic Republic of the</option>
                            <option <cfif rc.getUser.country EQ 'CK'>selected="selected"</cfif> value="CK">Cook Islands</option>
                            <option <cfif rc.getUser.country EQ 'CR'>selected="selected"</cfif> value="CR">Costa Rica</option>
                            <option <cfif rc.getUser.country EQ 'CI'>selected="selected"</cfif> value="CI">Côte d'Ivoire</option>
                            <option <cfif rc.getUser.country EQ 'HR'>selected="selected"</cfif> value="HR">Croatia</option>
                            <option <cfif rc.getUser.country EQ 'Cu'>selected="selected"</cfif> value="CU">Cuba</option>
                            <option <cfif rc.getUser.country EQ 'CW'>selected="selected"</cfif> value="CW">Curaçao</option>
                            <option <cfif rc.getUser.country EQ 'CY'>selected="selected"</cfif> value="CY">Cyprus</option>
                            <option <cfif rc.getUser.country EQ 'CZ'>selected="selected"</cfif> value="CZ">Czech Republic</option>
                            <option <cfif rc.getUser.country EQ 'DK'>selected="selected"</cfif> value="DK">Denmark</option>
                            <option <cfif rc.getUser.country EQ 'DJ'>selected="selected"</cfif> value="DJ">Djibouti</option>
                            <option <cfif rc.getUser.country EQ 'DM'>selected="selected"</cfif> value="DM">Dominica</option>
                            <option <cfif rc.getUser.country EQ 'DO'>selected="selected"</cfif> value="DO">Dominican Republic</option>
                            <option <cfif rc.getUser.country EQ 'EC'>selected="selected"</cfif> value="EC">Ecuador</option>
                            <option <cfif rc.getUser.country EQ 'EG'>selected="selected"</cfif> value="EG">Egypt</option>
                            <option <cfif rc.getUser.country EQ 'SV'>selected="selected"</cfif> value="SV">El Salvador</option>
                            <option <cfif rc.getUser.country EQ 'GQ'>selected="selected"</cfif> value="GQ">Equatorial Guinea</option>
                            <option <cfif rc.getUser.country EQ 'ER'>selected="selected"</cfif> value="ER">Eritrea</option>
                            <option <cfif rc.getUser.country EQ 'EE'>selected="selected"</cfif> value="EE">Estonia</option>
                            <option <cfif rc.getUser.country EQ 'ET'>selected="selected"</cfif> value="ET">Ethiopia</option>
                            <option <cfif rc.getUser.country EQ 'FK'>selected="selected"</cfif> value="FK">Falkland Islands (Malvinas)</option>
                            <option <cfif rc.getUser.country EQ 'FO'>selected="selected"</cfif> value="FO">Faroe Islands</option>
                            <option <cfif rc.getUser.country EQ 'FJ'>selected="selected"</cfif> value="FJ">Fiji</option>
                            <option <cfif rc.getUser.country EQ 'FI'>selected="selected"</cfif> value="FI">Finland</option>
                            <option <cfif rc.getUser.country EQ 'FR'>selected="selected"</cfif> value="FR">France</option>
                            <option <cfif rc.getUser.country EQ 'GF'>selected="selected"</cfif> value="GF">French Guiana</option>
                            <option <cfif rc.getUser.country EQ 'PF'>selected="selected"</cfif> value="PF">French Polynesia</option>
                            <option <cfif rc.getUser.country EQ 'TF'>selected="selected"</cfif> value="TF">French Southern Territories</option>
                            <option <cfif rc.getUser.country EQ 'GA'>selected="selected"</cfif> value="GA">Gabon</option>
                            <option <cfif rc.getUser.country EQ 'GM'>selected="selected"</cfif> value="GM">Gambia</option>
                            <option <cfif rc.getUser.country EQ 'GE'>selected="selected"</cfif> value="GE">Georgia</option>
                            <option <cfif rc.getUser.country EQ 'DE'>selected="selected"</cfif> value="DE">Germany</option>
                            <option <cfif rc.getUser.country EQ 'GH'>selected="selected"</cfif> value="GH">Ghana</option>
                            <option <cfif rc.getUser.country EQ 'GI'>selected="selected"</cfif> value="GI">Gibraltar</option>
                            <option <cfif rc.getUser.country EQ 'GR'>selected="selected"</cfif> value="GR">Greece</option>
                            <option <cfif rc.getUser.country EQ 'GL'>selected="selected"</cfif> value="GL">Greenland</option>
                            <option <cfif rc.getUser.country EQ 'GD'>selected="selected"</cfif> value="GD">Grenada</option>
                            <option <cfif rc.getUser.country EQ 'GP'>selected="selected"</cfif> value="GP">Guadeloupe</option>
                            <option <cfif rc.getUser.country EQ 'GU'>selected="selected"</cfif> value="GU">Guam</option>
                            <option <cfif rc.getUser.country EQ 'GT'>selected="selected"</cfif> value="GT">Guatemala</option>
                            <option <cfif rc.getUser.country EQ 'GG'>selected="selected"</cfif> value="GG">Guernsey</option>
                            <option <cfif rc.getUser.country EQ 'GN'>selected="selected"</cfif> value="GN">Guinea</option>
                            <option <cfif rc.getUser.country EQ 'GW'>selected="selected"</cfif> value="GW">Guinea-Bissau</option>
                            <option <cfif rc.getUser.country EQ 'GY'>selected="selected"</cfif> value="GY">Guyana</option>
                            <option <cfif rc.getUser.country EQ 'HT'>selected="selected"</cfif> value="HT">Haiti</option>
                            <option <cfif rc.getUser.country EQ 'HM'>selected="selected"</cfif> value="HM">Heard Island and McDonald Islands</option>
                            <option <cfif rc.getUser.country EQ 'VA'>selected="selected"</cfif> value="VA">Holy See (Vatican City State)</option>
                            <option <cfif rc.getUser.country EQ 'HN'>selected="selected"</cfif> value="HN">Honduras</option>
                            <option <cfif rc.getUser.country EQ 'HK'>selected="selected"</cfif> value="HK">Hong Kong</option>
                            <option <cfif rc.getUser.country EQ 'HU'>selected="selected"</cfif> value="HU">Hungary</option>
                            <option <cfif rc.getUser.country EQ 'IS'>selected="selected"</cfif> value="IS">Iceland</option>
                            <option <cfif rc.getUser.country EQ 'ID'>selected="selected"</cfif> value="ID">Indonesia</option>
                            <option <cfif rc.getUser.country EQ 'IR'>selected="selected"</cfif> value="IR">Iran, Islamic Republic of</option>
                            <option <cfif rc.getUser.country EQ 'IQ'>selected="selected"</cfif> value="IQ">Iraq</option>
                            <option <cfif rc.getUser.country EQ 'IE'>selected="selected"</cfif> value="IE">Ireland</option>
                            <option <cfif rc.getUser.country EQ 'IM'>selected="selected"</cfif> value="IM">Isle of Man</option>
                            <option <cfif rc.getUser.country EQ 'IL'>selected="selected"</cfif> value="IL">Israel</option>
                            <option <cfif rc.getUser.country EQ 'IT'>selected="selected"</cfif> value="IT">Italy</option>
                            <option <cfif rc.getUser.country EQ 'JM'>selected="selected"</cfif> value="JM">Jamaica</option>
                            <option <cfif rc.getUser.country EQ 'JP'>selected="selected"</cfif> value="JP">Japan</option>
                            <option <cfif rc.getUser.country EQ 'JE'>selected="selected"</cfif> value="JE">Jersey</option>
                            <option <cfif rc.getUser.country EQ 'JO'>selected="selected"</cfif> value="JO">Jordan</option>
                            <option <cfif rc.getUser.country EQ 'KZ'>selected="selected"</cfif> value="KZ">Kazakhstan</option>
                            <option <cfif rc.getUser.country EQ 'KE'>selected="selected"</cfif> value="KE">Kenya</option>
                            <option <cfif rc.getUser.country EQ 'KI'>selected="selected"</cfif> value="KI">Kiribati</option>
                            <option <cfif rc.getUser.country EQ 'KP'>selected="selected"</cfif> value="KP">Korea, Democratic People's Republic of</option>
                            <option <cfif rc.getUser.country EQ 'KR'>selected="selected"</cfif> value="KR">Korea, Republic of</option>
                            <option <cfif rc.getUser.country EQ 'KW'>selected="selected"</cfif> value="KW">Kuwait</option>
                            <option <cfif rc.getUser.country EQ 'KG'>selected="selected"</cfif> value="KG">Kyrgyzstan</option>
                            <option <cfif rc.getUser.country EQ 'LA'>selected="selected"</cfif> value="LA">Lao People's Democratic Republic</option>
                            <option <cfif rc.getUser.country EQ 'LV'>selected="selected"</cfif> value="LV">Latvia</option>
                            <option <cfif rc.getUser.country EQ 'LB'>selected="selected"</cfif> value="LB">Lebanon</option>
                            <option <cfif rc.getUser.country EQ 'LS'>selected="selected"</cfif> value="LS">Lesotho</option>
                            <option <cfif rc.getUser.country EQ 'LR'>selected="selected"</cfif> value="LR">Liberia</option>
                            <option <cfif rc.getUser.country EQ 'LY'>selected="selected"</cfif> value="LY">Libya</option>
                            <option <cfif rc.getUser.country EQ 'LI'>selected="selected"</cfif> value="LI">Liechtenstein</option>
                            <option <cfif rc.getUser.country EQ 'LT'>selected="selected"</cfif> value="LT">Lithuania</option>
                            <option <cfif rc.getUser.country EQ 'LU'>selected="selected"</cfif> value="LU">Luxembourg</option>
                            <option <cfif rc.getUser.country EQ 'MO'>selected="selected"</cfif> value="MO">Macao</option>
                            <option <cfif rc.getUser.country EQ 'MK'>selected="selected"</cfif> value="MK">Macedonia, the former Yugoslav Republic of</option>
                            <option <cfif rc.getUser.country EQ 'MG'>selected="selected"</cfif> value="MG">Madagascar</option>
                            <option <cfif rc.getUser.country EQ 'MW'>selected="selected"</cfif> value="MW">Malawi</option>
                            <option <cfif rc.getUser.country EQ 'MY'>selected="selected"</cfif> value="MY">Malaysia</option>
                            <option <cfif rc.getUser.country EQ 'MV'>selected="selected"</cfif> value="MV">Maldives</option>
                            <option <cfif rc.getUser.country EQ 'ML'>selected="selected"</cfif> value="ML">Mali</option>
                            <option <cfif rc.getUser.country EQ 'MT'>selected="selected"</cfif> value="MT">Malta</option>
                            <option <cfif rc.getUser.country EQ 'MH'>selected="selected"</cfif> value="MH">Marshall Islands</option>
                            <option <cfif rc.getUser.country EQ 'MQ'>selected="selected"</cfif> value="MQ">Martinique</option>
                            <option <cfif rc.getUser.country EQ 'MR'>selected="selected"</cfif> value="MR">Mauritania</option>
                            <option <cfif rc.getUser.country EQ 'MU'>selected="selected"</cfif> value="MU">Mauritius</option>
                            <option <cfif rc.getUser.country EQ 'YT'>selected="selected"</cfif> value="YT">Mayotte</option>
                            <option <cfif rc.getUser.country EQ 'MX'>selected="selected"</cfif> value="MX">Mexico</option>
                            <option <cfif rc.getUser.country EQ 'FM'>selected="selected"</cfif> value="FM">Micronesia, Federated States of</option>
                            <option <cfif rc.getUser.country EQ 'MD'>selected="selected"</cfif> value="MD">Moldova, Republic of</option>
                            <option <cfif rc.getUser.country EQ 'MC'>selected="selected"</cfif> value="MC">Monaco</option>
                            <option <cfif rc.getUser.country EQ 'MN'>selected="selected"</cfif> value="MN">Mongolia</option>
                            <option <cfif rc.getUser.country EQ 'ME'>selected="selected"</cfif> value="ME">Montenegro</option>
                            <option <cfif rc.getUser.country EQ 'MS'>selected="selected"</cfif> value="MS">Montserrat</option>
                            <option <cfif rc.getUser.country EQ 'MA'>selected="selected"</cfif> value="MA">Morocco</option>
                            <option <cfif rc.getUser.country EQ 'MZ'>selected="selected"</cfif> value="MZ">Mozambique</option>
                            <option <cfif rc.getUser.country EQ 'MM'>selected="selected"</cfif> value="MM">Myanmar</option>
                            <option <cfif rc.getUser.country EQ 'NA'>selected="selected"</cfif> value="NA">Namibia</option>
                            <option <cfif rc.getUser.country EQ 'NR'>selected="selected"</cfif> value="NR">Nauru</option>
                            <option <cfif rc.getUser.country EQ 'NP'>selected="selected"</cfif> value="NP">Nepal</option>
                            <option <cfif rc.getUser.country EQ 'NL'>selected="selected"</cfif> value="NL">Netherlands</option>
                            <option <cfif rc.getUser.country EQ 'NC'>selected="selected"</cfif> value="NC">New Caledonia</option>
                            <option <cfif rc.getUser.country EQ 'NZ'>selected="selected"</cfif> value="NZ">New Zealand</option>
                            <option <cfif rc.getUser.country EQ 'NI'>selected="selected"</cfif> value="NI">Nicaragua</option>
                            <option <cfif rc.getUser.country EQ 'NE'>selected="selected"</cfif> value="NE">Niger</option>
                            <option <cfif rc.getUser.country EQ 'NG'>selected="selected"</cfif> value="NG">Nigeria</option>
                            <option <cfif rc.getUser.country EQ 'NU'>selected="selected"</cfif> value="NU">Niue</option>
                            <option <cfif rc.getUser.country EQ 'NF'>selected="selected"</cfif> value="NF">Norfolk Island</option>
                            <option <cfif rc.getUser.country EQ 'MP'>selected="selected"</cfif> value="MP">Northern Mariana Islands</option>
                            <option <cfif rc.getUser.country EQ 'NO'>selected="selected"</cfif> value="NO">Norway</option>
                            <option <cfif rc.getUser.country EQ 'OM'>selected="selected"</cfif> value="OM">Oman</option>
                            <option <cfif rc.getUser.country EQ 'PK'>selected="selected"</cfif> value="PK">Pakistan</option>
                            <option <cfif rc.getUser.country EQ 'PW'>selected="selected"</cfif> value="PW">Palau</option>
                            <option <cfif rc.getUser.country EQ 'PS'>selected="selected"</cfif> value="PS">Palestinian Territory, Occupied</option>
                            <option <cfif rc.getUser.country EQ 'PA'>selected="selected"</cfif> value="PA">Panama</option>
                            <option <cfif rc.getUser.country EQ 'PG'>selected="selected"</cfif> value="PG">Papua New Guinea</option>
                            <option <cfif rc.getUser.country EQ 'PY'>selected="selected"</cfif> value="PY">Paraguay</option>
                            <option <cfif rc.getUser.country EQ 'PE'>selected="selected"</cfif> value="PE">Peru</option>
                            <option <cfif rc.getUser.country EQ 'PH'>selected="selected"</cfif> value="PH">Philippines</option>
                            <option <cfif rc.getUser.country EQ 'PN'>selected="selected"</cfif> value="PN">Pitcairn</option>
                            <option <cfif rc.getUser.country EQ 'PL'>selected="selected"</cfif> value="PL">Poland</option>
                            <option <cfif rc.getUser.country EQ 'PT'>selected="selected"</cfif> value="PT">Portugal</option>
                            <option <cfif rc.getUser.country EQ 'PR'>selected="selected"</cfif> value="PR">Puerto Rico</option>
                            <option <cfif rc.getUser.country EQ 'QA'>selected="selected"</cfif> value="QA">Qatar</option>
                            <option <cfif rc.getUser.country EQ 'RE'>selected="selected"</cfif> value="RE">Réunion</option>
                            <option <cfif rc.getUser.country EQ 'RO'>selected="selected"</cfif> value="RO">Romania</option>
                            <option <cfif rc.getUser.country EQ 'RU'>selected="selected"</cfif> value="RU">Russian Federation</option>
                            <option <cfif rc.getUser.country EQ 'RW'>selected="selected"</cfif> value="RW">Rwanda</option>
                            <option <cfif rc.getUser.country EQ 'BL'>selected="selected"</cfif> value="BL">Saint Barthélemy</option>
                            <option <cfif rc.getUser.country EQ 'SH'>selected="selected"</cfif> value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                            <option <cfif rc.getUser.country EQ 'KN'>selected="selected"</cfif> value="KN">Saint Kitts and Nevis</option>
                            <option <cfif rc.getUser.country EQ 'LC'>selected="selected"</cfif> value="LC">Saint Lucia</option>
                            <option <cfif rc.getUser.country EQ 'MF'>selected="selected"</cfif> value="MF">Saint Martin (French part)</option>
                            <option <cfif rc.getUser.country EQ 'PM'>selected="selected"</cfif> value="PM">Saint Pierre and Miquelon</option>
                            <option <cfif rc.getUser.country EQ 'VC'>selected="selected"</cfif> value="VC">Saint Vincent and the Grenadines</option>
                            <option <cfif rc.getUser.country EQ 'WS'>selected="selected"</cfif> value="WS">Samoa</option>
                            <option <cfif rc.getUser.country EQ 'SM'>selected="selected"</cfif> value="SM">San Marino</option>
                            <option <cfif rc.getUser.country EQ 'ST'>selected="selected"</cfif> value="ST">Sao Tome and Principe</option>
                            <option <cfif rc.getUser.country EQ 'SA'>selected="selected"</cfif> value="SA">Saudi Arabia</option>
                            <option <cfif rc.getUser.country EQ 'SN'>selected="selected"</cfif> value="SN">Senegal</option>
                            <option <cfif rc.getUser.country EQ 'RS'>selected="selected"</cfif> value="RS">Serbia</option>
                            <option <cfif rc.getUser.country EQ 'SC'>selected="selected"</cfif> value="SC">Seychelles</option>
                            <option <cfif rc.getUser.country EQ 'SL'>selected="selected"</cfif> value="SL">Sierra Leone</option>
                            <option <cfif rc.getUser.country EQ 'SG'>selected="selected"</cfif> value="SG">Singapore</option>
                            <option <cfif rc.getUser.country EQ 'SX'>selected="selected"</cfif> value="SX">Sint Maarten (Dutch part)</option>
                            <option <cfif rc.getUser.country EQ 'SK'>selected="selected"</cfif> value="SK">Slovakia</option>
                            <option <cfif rc.getUser.country EQ 'SI'>selected="selected"</cfif> value="SI">Slovenia</option>
                            <option <cfif rc.getUser.country EQ 'SB'>selected="selected"</cfif> value="SB">Solomon Islands</option>
                            <option <cfif rc.getUser.country EQ 'SO'>selected="selected"</cfif> value="SO">Somalia</option>
                            <option <cfif rc.getUser.country EQ 'ZA'>selected="selected"</cfif> value="ZA">South Africa</option>
                            <option <cfif rc.getUser.country EQ 'GS'>selected="selected"</cfif> value="GS">South Georgia and the South Sandwich Islands</option>
                            <option <cfif rc.getUser.country EQ 'SS'>selected="selected"</cfif> value="SS">South Sudan</option>
                            <option <cfif rc.getUser.country EQ 'ES'>selected="selected"</cfif> value="ES">Spain</option>
                            <option <cfif rc.getUser.country EQ 'LK'>selected="selected"</cfif> value="LK">Sri Lanka</option>
                            <option <cfif rc.getUser.country EQ 'SD'>selected="selected"</cfif> value="SD">Sudan</option>
                            <option <cfif rc.getUser.country EQ 'SR'>selected="selected"</cfif> value="SR">Suriname</option>
                            <option <cfif rc.getUser.country EQ 'SJ'>selected="selected"</cfif> value="SJ">Svalbard and Jan Mayen</option>
                            <option <cfif rc.getUser.country EQ 'SZ'>selected="selected"</cfif> value="SZ">Swaziland</option>
                            <option <cfif rc.getUser.country EQ 'SE'>selected="selected"</cfif> value="SE">Sweden</option>
                            <option <cfif rc.getUser.country EQ 'CH'>selected="selected"</cfif> value="CH">Switzerland</option>
                            <option <cfif rc.getUser.country EQ 'SY'>selected="selected"</cfif> value="SY">Syrian Arab Republic</option>
                            <option <cfif rc.getUser.country EQ 'TW'>selected="selected"</cfif> value="TW">Taiwan, Province of China</option>
                            <option <cfif rc.getUser.country EQ 'TJ'>selected="selected"</cfif> value="TJ">Tajikistan</option>
                            <option <cfif rc.getUser.country EQ 'TZ'>selected="selected"</cfif> value="TZ">Tanzania, United Republic of</option>
                            <option <cfif rc.getUser.country EQ 'TH'>selected="selected"</cfif> value="TH">Thailand</option>
                            <option <cfif rc.getUser.country EQ 'TL'>selected="selected"</cfif> value="TL">Timor-Leste</option>
                            <option <cfif rc.getUser.country EQ 'TG'>selected="selected"</cfif> value="TG">Togo</option>
                            <option <cfif rc.getUser.country EQ 'TK'>selected="selected"</cfif> value="TK">Tokelau</option>
                            <option <cfif rc.getUser.country EQ 'TO'>selected="selected"</cfif> value="TO">Tonga</option>
                            <option <cfif rc.getUser.country EQ 'TT'>selected="selected"</cfif> value="TT">Trinidad and Tobago</option>
                            <option <cfif rc.getUser.country EQ 'TN'>selected="selected"</cfif> value="TN">Tunisia</option>
                            <option <cfif rc.getUser.country EQ 'TR'>selected="selected"</cfif> value="TR">Turkey</option>
                            <option <cfif rc.getUser.country EQ 'TM'>selected="selected"</cfif> value="TM">Turkmenistan</option>
                            <option <cfif rc.getUser.country EQ 'TC'>selected="selected"</cfif> value="TC">Turks and Caicos Islands</option>
                            <option <cfif rc.getUser.country EQ 'TV'>selected="selected"</cfif> value="TV">Tuvalu</option>
                            <option <cfif rc.getUser.country EQ 'UG'>selected="selected"</cfif> value="UG">Uganda</option>
                            <option <cfif rc.getUser.country EQ 'UA'>selected="selected"</cfif> value="UA">Ukraine</option>
                            <option <cfif rc.getUser.country EQ 'AE'>selected="selected"</cfif> value="AE">United Arab Emirates</option>
                            <option <cfif rc.getUser.country EQ 'GB'>selected="selected"</cfif> value="GB">United Kingdom</option>
                            <option <cfif rc.getUser.country EQ 'US'>selected="selected"</cfif> value="US">United States</option>
                            <option <cfif rc.getUser.country EQ 'UM'>selected="selected"</cfif> value="UM">United States Minor Outlying Islands</option>
                            <option <cfif rc.getUser.country EQ 'UY'>selected="selected"</cfif> value="UY">Uruguay</option>
                            <option <cfif rc.getUser.country EQ 'UZ'>selected="selected"</cfif> value="UZ">Uzbekistan</option>
                            <option <cfif rc.getUser.country EQ 'VU'>selected="selected"</cfif> value="VU">Vanuatu</option>
                            <option <cfif rc.getUser.country EQ 'VE'>selected="selected"</cfif> value="VE">Venezuela, Bolivarian Republic of</option>
                            <option <cfif rc.getUser.country EQ 'VN'>selected="selected"</cfif> value="VN">Viet Nam</option>
                            <option <cfif rc.getUser.country EQ 'VG'>selected="selected"</cfif> value="VG">Virgin Islands, British</option>
                            <option <cfif rc.getUser.country EQ 'VI'>selected="selected"</cfif> value="VI">Virgin Islands, U.S.</option>
                            <option <cfif rc.getUser.country EQ 'WF'>selected="selected"</cfif> value="WF">Wallis and Futuna</option>
                            <option <cfif rc.getUser.country EQ 'EH'>selected="selected"</cfif> value="EH">Western Sahara</option>
                            <option <cfif rc.getUser.country EQ 'YE'>selected="selected"</cfif> value="YE">Yemen</option>
                            <option <cfif rc.getUser.country EQ 'ZM'>selected="selected"</cfif> value="ZM">Zambia</option>
                            <option <cfif rc.getUser.country EQ 'ZW'>selected="selected"</cfif> value="ZW">Zimbabwe</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <!-- username -->
                    <div class="form-group col-md-6">
                        <!-- email -->
                        <input type="text" required="" autocomplete="false" placeholder="Mobile Number" class="form-control" name="mobile" value="<cfoutput>#rc.getUser.mobile#</cfoutput>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12" style="margin-bottom:10px;">
                        <label for="DOB" id="dob" class="control-label col-md-2 col-xs-12">Date Of Birth</label>
                        <div class="col-xs-4 col-md-2">
                            <select required="" name="day" class="form-control" data-role="none">
                                <cfloop from="1" to="31" index="i">
                                    <option <cfif day(rc.getUser.dob) EQ i>selected="selected"</cfif> value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                                </cfloop>
                            </select>
                        </div>
                        <div class="col-xs-4 col-md-2 monthPadding">
                            <select required="" name="month" class="form-control" data-role="none">
                                <option  <cfif month(rc.getUser.dob) EQ 1>selected="selected"</cfif> value="01">Jan</option>
                                <option  <cfif month(rc.getUser.dob) EQ 2>selected="selected"</cfif> value="02">Feb</option>
                                <option  <cfif month(rc.getUser.dob) EQ 3>selected="selected"</cfif> value="03">Mar</option>
                                <option  <cfif month(rc.getUser.dob) EQ 4>selected="selected"</cfif> value="04">Apr</option>
                                <option  <cfif month(rc.getUser.dob) EQ 5>selected="selected"</cfif> value="05">May</option>
                                <option  <cfif month(rc.getUser.dob) EQ 6>selected="selected"</cfif> value="06">Jun</option>
                                <option  <cfif month(rc.getUser.dob) EQ 7>selected="selected"</cfif> value="07">Jul</option>
                                <option  <cfif month(rc.getUser.dob) EQ 8>selected="selected"</cfif> value="08">Aug</option>
                                <option  <cfif month(rc.getUser.dob) EQ 9>selected="selected"</cfif> value="09">Sep</option>
                                <option  <cfif month(rc.getUser.dob) EQ 10>selected="selected"</cfif> value="10">Oct</option>
                                <option  <cfif month(rc.getUser.dob) EQ 11>selected="selected"</cfif> value="11">Nov</option>
                                <option  <cfif month(rc.getUser.dob) EQ 12>selected="selected"</cfif> value="12">Dec</option>
                            </select>
                        </div>
                        <div class="col-xs-4 col-md-3 monthYear">
                            <select required="required" name="year" class="form-control" data-role="none">    
                                <cfloop from="#rc.Date100#" to="#rc.Date13#" index="i">
                                    <option <cfif i eq year(rc.getUser.dob)>selected</cfif> value="<cfoutput>#i#</cfoutput>"><cfoutput>#i#</cfoutput></option>
                                </cfloop>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="title">Profile Image</label>
                        <input type="file" id="profileImage" name="profileImage">
                    </div>
                </div>
                <div class="row">
                    <div class="footer col-md-12 visible-xs text-center">
                        <button class="btn btn-success" type="submit">Update Details</button><!--- <br/>or<br/><button id="fblogin" onclick="authUser();" class="btn btn-fb" type="button">Sign Up with Facebook</button> --->
                    </div>
                    <div class="footer col-md-12 hidden-xs pull-right">
                        <button class="btn btn-success pull-right" type="submit">Update Details</button><!---  or <button id="fblogin" onclick="authUser();" class="btn btn-fb" type="button">Sign Up with Facebook</button> --->
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="col-lg-6">
    <div class="well bs-component dropdown in" style="background-color:#fff">     
        <h3>Update Password</h3>
        <form method="post" autocomplete="false" action="" class="form" id="register-form2" name="myForm">
            <input type="hidden" name="update" value="password"/>

            <fieldset>
                <div class="row">
                    <div class="form-group col-md-6">
                        <!-- password -->
                        <input type="password" required="" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : ''); if(this.checkValidity()) form.confirmPassword.pattern = this.value;" pattern=".{6,}" placeholder="Password... 6 characters or more" class="form-control" title="Password must contain at least 6 characters" name="password">
                    </div>
                    <div class="form-group col-md-6">
                        <!-- password -->
                        <input type="password" required="" onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" pattern=".{6,}" placeholder="Retype password" class="form-control" title="Please enter the same password" name="confirmPassword">
                    </div>
                </div>
                <div class="row">
                    <div class="footer col-md-12 visible-xs text-center">
                        <button class="btn btn-success" type="submit">Update Password</button><!--- <br/>or<br/><button id="fblogin" onclick="authUser();" class="btn btn-fb" type="button">Sign Up with Facebook</button> --->
                    </div>
                    <div class="footer col-md-12 hidden-xs">
                        <button class="btn btn-success pull-right" type="submit">Update Password</button><!---  or <button id="fblogin" onclick="authUser();" class="btn btn-fb" type="button">Sign Up with Facebook</button> --->
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>


<!-- Modal -->
<cfif len(rc.getUser.profileImage)>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Crop Your Image</h4>
          </div>
          <div class="modal-body">
            <!-- This is the image we're attaching Jcrop to -->
            <div class="row profileImages">
                <div class="col-md-12">
                    <label>Origional Image</label>
                    <div id="croppedImage">
                        <img src="/assets/userImages/<cfoutput>#rc.getUser.profileImage#</cfoutput>" id="cropbox" style="max-width:100%" />
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            
            <!-- The event handler from the JCrop plugin populates these
            values for us. Required to obtain the X Y coords and persist
            the image location for cropping and reverting the image. -->
            <form action="crop.cfm" method="post" class="form">
                <input type="hidden" size="4" id="x" name="x" />
                <input type="hidden" size="4" id="y" name="y" />
                <input type="hidden" size="4" id="x2" name="x2" />
                <input type="hidden" size="4" id="y2" name="y2" />
                <input type="hidden" size="4" id="w" name="w" />
                <input type="hidden" size="4" id="h" name="h" />
                <input type="hidden" name="imageFile" id="imageFile" value="" />
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="button" class="btn btn-primary pull-right" name="imageCrop_btn" id="imageCrop_btn" value="Crop" />
            </form>
          </div>
        </div>
      </div>
    </div>
</cfif>
