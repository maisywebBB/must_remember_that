<div class="row wellReminders">
    
    <div class="col-xs-12 addReminderTitle" style="margin-bottom:10px;">
        <strong class="mrTitle">Suggested Reminders</strong>
    </div>

    <div class="visible-xs col-lg-12 reminderContainerMobile">
        <ul class="list-group">
            <cfoutput query="rc.getPriorityCats">
                <li class="list-group-item">
                    <a href="#buildUrl("users.reminders")#&defaultReminder=#urlSafe#" data-ajax="false">
                        <span style="float:right;" class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
                        </span>
                        #title#
                    </a>
                </li>
            </cfoutput>
        </ul>
    </div>

    <div class="hidden-xs col-md-12 sListGroup">
       <ul class="list-group row">
            <cfoutput query="rc.getPriorityCats">
                <li class="list-group-item col-md-4">
                    <a href="#buildUrl("users.reminders")#&defaultReminder=#urlSafe#" data-ajax="false">
                        <span style="float:right;" class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
                        </span>
                        #title#
                    </a>
                </li>
            </cfoutput>
        </ul>
    </div>

</div>
        