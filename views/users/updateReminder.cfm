<div class="row hidden-xs">
    <div class="col-lg-7">
        <div class="well bs-component collapse in" style="background-color:#fff;">
            <div class="col-lg-12 text-center">
                <div class="btn-group">
                    <cfif rc.isArchive>
                        <a style="margin-top:10px;" class="btn btn-default" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>">Active</a><button style="margin-top:10px;" class="btn btn-primary">Expired</button>
                    <cfelse>
                        <a style="margin-top:10px;" class="btn btn-primary" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>">Active</a><a style="margin-top:10px;" class="btn btn-default" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&isArchive=1">Expired</a>
                    </cfif>
                </div>
            </div>
            <legend><cfif NOT rc.isArchive>Active<cfelse>Expired</cfif></legend>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Reminder</th>
                            <th>Date</th>
                            <th colspan="3"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput>
                            <cfloop query="rc.getReminders">
                                <tr>
                                    <td width="70%"><a href="<cfoutput>#buildUrl("users.updateReminder")#&reminderId=#reminderId#</cfoutput>">#rc.getReminders.title#</a></td>
                                    <td width="15%">#dateFormat(rc.getReminders.eventDate, 'dd/mm/yyyy')#</td>
                                    <td width="5%">
                                        <a class="share_button" data-reminderTitle="#jsStringFormat(rc.getReminders.title)#" href="##"><i class="fa fa-facebook"></i></a>
                                    </td>
                                    <td width="5%">
                                        <a href="<cfoutput>mailto:?subject=MUST REMEMBER THAT - #title#&body=Hi, thought you might want a quick reminder. #title# #DateFormat(eventDate, "dd mmmm yyyy")# http://www.mustrememberthat.com/index.cfm?action=main.singleReminder%26reminderId=#reminderId#</cfoutput>"><i class="fa fa-envelope"></i></a>
                                    </td>
                                    <td width="5%">
                                        <a onclick="return confirm('Are you sure you want to delete this reminder?');" href="<cfoutput>#buildUrl("users.reminders")#&update=deleteReminder&reminderId=#reminderId#</cfoutput>"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </cfloop>
                        </cfoutput>
                    </tbody>
                </table>
            </div>
            <cfif rc.totalPages GT 1> 
                <ul class="pagination">
                    <li <cfif 1 EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.updateReminder")#&reminderId=#rc.reminderId#&pageNumber=#rc.pageNumber - 1##rc.isArchiveURL#</cfoutput>">&laquo;</a></li>
                    <cfloop from="#rc.startpage#" to="#rc.endpage#" index="i">
                       <li <cfif rc.pageNumber EQ i> class="active"</cfif>><a href="<cfoutput>#buildUrl("users.updateReminder")#&reminderId=#rc.reminderId#&pageNumber=#i##rc.isArchiveURL#</cfoutput>"><cfoutput>#i#</cfoutput></a></li> 
                    </cfloop>
                    <li <cfif rc.totalPages EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.updateReminder")#&reminderId=#rc.reminderId#&pageNumber=#rc.pageNumber + 1##rc.isArchiveURL#</cfoutput>">&raquo;</a></li>
                </ul>
            </cfif>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="well bs-component updateCol">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="updateReminder"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Update Reminder</legend>
                    <div class="form-group col-md-12">
                        <label for="title">Title</label>
                        <input type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Category</label>
                        <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                            <option value="">Select Category</option>
                            <cfoutput query="rc.getMainCats">
                                <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                            </cfoutput>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Sub Category</label>
                        <select id="category" name="category" class="form-control" data-role="none">   
                            <cfoutput query="rc.getCats">
                                <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                            </cfoutput>  
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail">Date of event</label>
                        <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.eventDate#</cfoutput>" />
                        <div class='input-group date form_date'>
                            <input type="text" data-date-format="Do MMMM YYYY" data-link-field="rdate" autocomplete="off" id="inputEmail" class="form-control" <cfif len(rc.eventDate)>value="<cfoutput>#day(rc.eventDate)# #monthAsString(month(rc.eventDate))# #year(rc.eventDate)#</cfoutput>"</cfif>>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Time of event</label>
                        <div class='input-group date form_time' id='datetimepicker3'>
                            <input type='text' class="form-control" name="rtime" id="rtime" value="<cfoutput>#rc.eventTime#</cfoutput>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>    
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Send me reminder</label>
                        <select id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                            <cfloop from="1" to="100" index="i"><cfoutput>
                                <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                            </cfoutput></cfloop>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">&nbsp;</label>
                        <select id="remindertime" name="remindertime" class="form-control" data-role="none">              
                            <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                            <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour(s) before</option>
                            <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day(s) Before</option>
                            <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week(s) Before</option>
                            <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month(s) Before</option>
                        </select>
                    </div>
                    <div id="reminderActualTimeDiv" class="form-group col-md-6">
                        <label for="inputEmail">Reminder Time</label>
                        <input type="text" id="inputEmail" class="form-control form_time"  name="reminderActualTime" value="<cfoutput>#rc.reminderActualTime#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">How frequent</label>
                        <select id="interval" name="interval" class="form-control" data-role="none">              
                            <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                            <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                            <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                            <option <cfif rc.interval EQ 'weely'>selected="selected"</cfif> value="weekly">Weekly</option>
                            <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                            <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Comments</label>
                        <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-default" type="reset">Cancel</button>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>

<div class="row visible-xs">
    <div class="col-xs-12" style="margin-bottom:20px;">
        <button onclick="history.go(-1);" class="btn btn-primary pull-right" type="submit">Back to reminders</button>
    </div>
    
    <div class="col-xs-12 text-center">

        <!--- share on facebook --->
        <cfoutput>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('##share_button#rc.reminderId#').click(function(e){
                    e.preventDefault();
                    FB.ui(
                        {
                            method: 'feed',
                            name: '#rc.title#',
                            link: ' http://www.mustrememberthat.com/',
                            picture: 'http://www.mustrememberthat.com/images/MRT-facebook-icon5.png',
                            caption: 'I have just remembered this thanks to Must Remember That',
                            description: 'MUST REMEMBER THAT - Never Forget Again',
                            message: ''
                        });
                    });
                });
            </script>
        </cfoutput>
        
        <!--- update reminder --->
        <!--- <a href="#showForm" class="btn btn-success btn-updateREminder" data-toggle="collapse" style="width:100%;"><strong>Update Reminder</strong></a><br/><br/> --->

        <form id="mobForm" action="" method="POST" enctype="multipart/form-data">
            
            <input type="hidden" name="update" value="updateReminder"/>
            <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
            
            <fieldset>
                <div class="form-group col-xs-12">
                    <label for="title">Title</label>
                    <input type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                </div>
                <div class="form-group col-xs-6">
                    <label for="inputEmail">Date of event</label>
                    <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.eventDate#</cfoutput>" />
                    <div class='input-group date form_date'>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <input type="text" data-date-format="Do MMM YYYY" data-link-field="rdate" autocomplete="off" id="inputEmail" class="form-control" <cfif len(rc.eventDate)>value="<cfoutput>#day(rc.eventDate)# #monthAsString(month(rc.eventDate))# #year(rc.eventDate)#</cfoutput>"</cfif>>
                    </div>
                </div>
                <div class="form-group col-xs-6">
                    <label for="inputEmail">Time of event </label>
                    <div class='input-group date form_time' id='datetimepicker4'>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                        <input type='text' class="form-control" name="rtime" id="rtime" value="<cfoutput>#rc.eventTime#</cfoutput>" />
                    </div>
                </div>

                <div class="form-group col-xs-12">
                    <div class="col-xs-3"><strong>Share:</strong> </div>
                    <div class="col-xs-9">
                        <a data-reminderTitle="<cfoutput>#jsStringFormat(rc.getReminders.title)#</cfoutput>" class="share_button">
                            <img src="/assets/images/fb.png" height="25px">
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <!--- Whats App Reminder --->
                        <a href="whatsapp://send?text=REMINDER:%20<cfoutput>#rc.title#</cfoutput>%20-%20<cfoutput>#DateFormat(rc.eventDate, "dd mmmm yyyy")#</cfoutput>%0A%0ASet%20reminder%20now%20-%20http%3A%2F%2Fwww.mustrememberthat.com%2Findex.cfm%3Faction%3Dmain.singleReminder%26reminderId%3D<cfoutput>#rc.reminderId#</cfoutput>"><img src="/assets/images/wa.png" height="25px"></a>
                        &nbsp;&nbsp;&nbsp;
                        <!--- SMS Link --->
                        <cfset smslink = 'http://www.mustrememberthat.com/index.cfm?action=main.singleReminder&reminderId=#reminderId#'>
                        <cfif findNoCase('iPhone OS 8', CGI.HTTP_USER_AGENT) OR findNoCase ('iPhone OS 9', CGI.HTTP_USER_AGENT) OR findNoCase ('iPhone OS 10', CGI.HTTP_USER_AGENT) >
                            <a href="sms:&body=&#128204; REMINDER: <cfoutput>#rc.title#</cfoutput> - <cfoutput>#DateFormat(rc.eventDate, "dd mmmm yyyy")#</cfoutput> %0A %0ASet reminder now <CFOUTPUT>#URLEncodedFormat(smslink)#</CFOUTPUT>%0A %0AMust Remember That - the reminder service"><img src="/assets/images/sms.jpg" height="25px"></a>
                            &nbsp;&nbsp;&nbsp;
                        <cfelseif findNoCase('iPhone', CGI.HTTP_USER_AGENT)>
                            <a href="sms:;body=&#128204; REMINDER: <cfoutput>#rc.title#</cfoutput> %0A %0ASet reminder now <CFOUTPUT>#URLEncodedFormat(smslink)#</CFOUTPUT>%0A %0AMust Remember That - the reminder service"><img src="/assets/images/sms.jpg" height="25px"></a>
                            &nbsp;&nbsp;&nbsp;
                        <cfelseif findNoCase('Android', CGI.HTTP_USER_AGENT) OR findNoCase('Android 3', CGI.HTTP_USER_AGENT) OR findNoCase('Android 2', CGI.HTTP_USER_AGENT) OR findNoCase('Android 4', CGI.HTTP_USER_AGENT) OR findNoCase('Android 5', CGI.HTTP_USER_AGENT) >
                            <a href="sms:?body=&#128204; REMINDER: <cfoutput>#rc.title#</cfoutput> - set reminder now <CFOUTPUT>#URLEncodedFormat(smslink)#</CFOUTPUT>%0A %0AMust Remember That - the reminder service"><img src="/assets/images/sms.jpg" height="25px"></a>
                            &nbsp;&nbsp;&nbsp;
                        <cfelse>
                        </cfif>

                        <!--- Email Reminder --->
                        <a href="mailto:?subject= <cfoutput>#rc.title#</cfoutput>&body=Hi, thought you might want a quick reminder. <cfoutput>#rc.title#</cfoutput> <cfoutput>#DateFormat(rc.eventDate, "dd mmmm yyyy")#</cfoutput> http://www.mustrememberthat.com//index.cfm?action=main.singleReminder&reminderId=<cfoutput>#rc.reminderId#</cfoutput>  Must Remember That - the reminder service"><img src="/assets/images/email.png" height="25px"></a>
                    </div>   
                </div>
                
                <div class="form-group col-xs-12">
                    <label for="category">Category</label>
                    <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                        <option value="">Select Category</option>
                        <cfoutput query="rc.getMainCats">
                            <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                        </cfoutput>
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <label for="category">Sub Category</label>
                    <select id="category" name="category" class="form-control" data-role="none">   
                        <cfoutput query="rc.getCats">
                            <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                        </cfoutput>  
                    </select>
                </div>

                <div class="form-group col-xs-12 push-left mobReminderText">
                    Send me reminder
                </div>

                <div class="form-group col-xs-3">
                    <label for="inputEmail">Send me reminder</label>
                    <select id="intervalPeriodMob" name="intervalPeriod" class="form-control" data-role="none">              
                        <cfloop from="1" to="100" index="i"><cfoutput>
                            <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                        </cfoutput></cfloop>
                    </select>
                </div>
                <div class="form-group col-xs-5">
                    <label for="inputEmail">&nbsp;</label>
                    <select id="remindertimeMob" name="remindertime" class="form-control" data-role="none">              
                        <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                        <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour(s) before</option>
                        <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day(s) Before</option>
                        <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week(s) Before</option>
                        <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month(s) Before</option>
                    </select>
                </div>
                <div class="form-group col-xs-4">
                    <label for="inputEmail">How frequent</label>
                    <select id="interval" name="interval" class="form-control" data-role="none">              
                        <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                        <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                        <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                        <option <cfif rc.interval EQ 'weely'>selected="selected"</cfif> value="weely">Weekly</option>
                        <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                        <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                    </select>
                </div>
                
                <cfif rc.remindertime EQ 'n' OR rc.remindertime EQ 'h'>
                    <cfset rc.reminderStyle = "display: none;">
                <cfelse>
                    <cfset rc.reminderStyle = "">
                    <div id="reminderActualTimeMobDiv" class="form-group col-xs-12" style="<cfoutput>#rc.reminderStyle#</cfoutput>">
                        <label for="inputEmail">Reminder Time</label>
                        <input type="text" id="inputEmail" class="form-control form_time"  name="reminderActualTime" value="<cfoutput>#rc.reminderActualTime#</cfoutput>">
                    </div>
                </cfif>
                

                <div class="form-group col-xs-12">
                    <label for="inputEmail">Comments</label>
                    <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                </div>
                <div class="form-group col-xs-12">
                    <!--- <button class="btn btn-default" type="reset">Cancel</button> --->
                    <button class="btn btn-primary btn-block" type="submit">Update</button>
                </div>
            </fieldset>
        </form>
        <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>

        <!--- Delete Reminder --->
        <a class="btn btn-default btn-orig" style="width:50%;" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&update=deleteReminder&reminderId=<cfoutput>#rc.reminderId#</cfoutput>"><strong>Delete Reminder </strong><i class="fa fa-trash"></i></a>

    </div>
</div>
        