<cfif structKeyExists(URL, 'addReminder') OR structKeyExists(URL, 'defaultReminder')>
    <style>
        #addReminderDiv{
          display: block;
        }
    </style>
</cfif>

<div class="row visible-xs">
    <div class="col-xs-12 addReminderTitle">
        <strong class="mrTitle">My Reminders</strong>
        <button class="visible-xs btn btn-primary btn-sm addReminderDiv" data-toggle="dropdown" data-target="#addReminderDiv">Add a reminder</button>
    </div>
</div>

<div class="row">
    
    <div id="mobReminderDiv" class="col-lg-4">
        
        <div class="well bs-component dropdown in" id="addReminderDiv">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="newReminder"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Add Reminder</legend>
                    <div class="form-group col-md-12">
                        <label for="title">Title</label>
                        <input type="text" autocomplete="off" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Category</label>
                        <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                            <option value="">Select Category</option>
                            <cfoutput query="rc.getMainCats">
                                <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                            </cfoutput>
                        </select>
                    </div>
                    <div id="subCatContainer" class="form-group col-md-12">
                        <label for="category">Sub Category</label>
                        <select id="category" name="category" class="form-control" data-role="none">   
                            <cfoutput query="rc.getCats">
                                <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                            </cfoutput>  
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail"><span class="visible-xs">Date</span><span class="hidden-xs">Date of event</span></label>
                        <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.rdate#</cfoutput>" />
                        <div class='input-group date form_date'>
                            <input type="text" data-date-format="Do MMMM YYYY" data-link-field="rdate" autocomplete="off" id="inputEmail" class="form-control" <cfif len(rc.rdate)>value="<cfoutput>#day(rc.rdate)# #monthAsString(month(rc.rdate))# #year(rc.rdate)#</cfoutput>"</cfif>>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail"><span class="visible-xs">Time</span><span class="hidden-xs">Time of event</span></label>
                        <!--- <input type="hidden" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" /> --->
                        <div class='input-group date form_time' id='datetimepicker3'>
                            <input type='text' class="form-control" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Send me reminder</label>
                        <select id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                            <cfloop from="1" to="100" index="i"><cfoutput>
                                <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                            </cfoutput></cfloop>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">&nbsp;</label>
                        <select id="remindertime" name="remindertime" class="form-control" data-role="none">              
                            <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                            <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour before</option>
                            <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day Before</option>
                            <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week Before</option>
                            <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month Before</option>
                        </select>
                    </div>
                    <div id="reminderActualTimeDiv" class="form-group col-md-6">
                        <label for="inputEmail">Reminder Time</label>
                        <div class='input-group date form_time' id='datetimepicker4'>
                            <input type='text' class="form-control" name="reminderActualTime" id="reminderActualTime" value="<cfoutput>#rc.reminderActualTime#</cfoutput>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">How frequent</label>
                        <select id="interval" name="interval" class="form-control" data-role="none">              
                            <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                            <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                            <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                            <option <cfif rc.interval EQ 'weekly'>selected="selected"</cfif> value="weekly">Weekly</option>
                            <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                            <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Comments</label>
                        <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Location</label>
                        <input name="location" class="form-control" id="keywordAttraction" type="text" value="" placeholder="Enter address">
                        <input type="hidden" id="latitude" name="latitude" value=""/>
                        <input type="hidden" id="longitude" name="longitude" value=""/>
                    </div>
                    <div class="form-group col-md-12">
                        <div id="mapContainer" style="height:150px"></div>
                    </div>
                    <div class="form-group col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Set Reminder</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div>

    <!--- <div class="col-lg-5 visible-xs">
        
        <button class="visible-xs btn btn-primary addReminderDiv" data-toggle="collapse" data-target="#addReminderDiv">Add a reminder</button>

        <div class="well bs-component collapse in" id="addReminderDiv">
            <form action="" method="POST" enctype="multipart/form-data">
        
                <input type="hidden" name="update" value="newReminder"/>
                <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                
                <fieldset>
                    <legend>Add Reminder</legend>
                    <div class="form-group col-md-12">
                        <label for="title">Title</label>
                        <input type="text" placeholder="Reminder Title" id="title" name="title" class="form-control" value="<cfoutput>#rc.title#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="category">Category</label>
                        <select id="maincategory" name="maincategory" class="form-control" data-role="none">   
                            <option value="">Select Category</option>
                            <cfoutput query="rc.getMainCats">
                                <option <cfif rc.maincategory EQ rc.getMainCats.mainCategoryId>selected="selected"</cfif> value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                            </cfoutput>
                        </select>
                    </div>
                    <div id="subCatContainer" class="form-group col-md-12">
                        <label for="category">Sub Category</label>
                        <select id="category" name="category" class="form-control" data-role="none">   
                            <cfoutput query="rc.getCats">
                                <option <cfif rc.category EQ rc.getCats.id>selected="selected"</cfif> value="#rc.getCats.id#">#rc.getCats.title#</option>
                            </cfoutput>  
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Date</label>
                        <input type="hidden" name="rdate" id="rdate" value="<cfoutput>#rc.rdate#</cfoutput>" />
                        <input type="text" id="inputEmail" class="form-control form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="rdate" data-link-format="yyyy-mm-dd" data-picker-position="bottom-left" <cfif len(rc.rdate)>value="<cfoutput>#day(rc.rdate)# #monthAsString(month(rc.rdate))# #year(rc.rdate)#</cfoutput>"</cfif>>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Time</label>
                        <input type="hidden" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" />
                        <input type="text" id="inputEmail" class="form-control form_time" data-date="" data-date-format="hh:ii" data-link-field="rtime" data-link-format="hh:ii" data-picker-position="bottom-left" value="<cfoutput>#rc.rtime#</cfoutput>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">How frequent</label>
                        <select id="interval" name="interval" class="form-control" data-role="none">              
                            <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                            <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily</option>
                            <option <cfif rc.interval EQ 'weekly'>selected="selected"</cfif> value="weekly">Weekly</option>
                            <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                            <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">Send me reminder</label>
                        <select id="intervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                            <cfloop from="1" to="100" index="i"><cfoutput>
                                <option <cfif rc.intervalPeriod EQ #i#>selected="selected"</cfif> value="#i#">#i#</option>
                            </cfoutput></cfloop>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail">&nbsp;</label>
                        <select id="remindertime" name="remindertime" class="form-control" data-role="none">              
                            <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                            <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour before</option>
                            <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day Before</option>
                            <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week Before</option>
                            <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month Before</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputEmail">Comments</label>
                        <textarea name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-default" type="reset">Cancel</button>
                        <button class="btn btn-primary" type="submit">Add Reminder</button>
                    </div>
                </fieldset>
            </form>
            <div class="btn btn-primary btn-xs" id="source-button" style="display: none;">&lt; &gt;</div>
        </div>
    </div> --->
    <div class="col-lg-2 hidden-xs">
        <div class="well bs-component dropdown in" style="background-color:#fff;">
            <h4>Suggested Reminders</h4>
            <ul class="wellList">
                <cfoutput query="rc.getPriorityCats" startrow="1" maxrows="5">
                    <li><a data-toggle="modal" data-target="##reminderModal" class="suggestedReminderLink" data-safeurl="#urlSafe#" href="##">#title#</a></li>
                </cfoutput>
            </ul>
            <a href="<cfoutput>#buildUrl("users.suggestedReminders")#</cfoutput>">Show more</a>
        </div>

        <div class="well bs-component dropdown in" style="background-color:#fff;">
            <h4>My Subscriptions</h4>
            <ul class="wellList">
                <cfoutput query="rc.getSubscription" startrow="1" maxrows="5">
                    <li><a href="#urlSlug#/">#title#</a></li>
                </cfoutput>
            </ul>
            <cfif rc.getSubscription.recordcount GT 5>
                <a href="<cfoutput>#buildUrl("users.subscriptions")#</cfoutput>">Show more</a>
            </cfif>
            
        </div>
        <div class="well bs-component dropdown in" style="background-color:#fff;">
            <h4>My Groups</h4>
            <ul class="wellList">
                <cfoutput query="rc.getGroups">
                    <li><a href="/groups/#urlSlug#/">#title#</a></li>
                </cfoutput>
            </ul>
        </div>
    </div>

    <div class="col-lg-6 hidden-xs">
        <div class="col-lg-12 text-center">
	        <div class="btn-group">
                <cfif rc.isArchive>
    	            <a style="margin-top:10px;" class="btn btn-default" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&">Active</a><button style="margin-top:10px;" class="btn btn-primary">Expired</button>
    	        <cfelse>
    	            <button style="margin-top:10px;" class="btn btn-primary">Active</button><a style="margin-top:10px;" class="btn btn-default" href="<cfoutput>#buildUrl("users.reminders")#</cfoutput>&isArchive=1">Expired</a>
    	        </cfif>
            </div>
	    </div>
	   <cfif NOT rc.getReminders.recordcount AND NOT rc.isArchive>
            <div class="well bs-component collapse in" style="background-color:#fff;">
                <cfif NOT rc.totalReminders.counter>
                    <legend style=" margin-top:40px;">Time to set your reminders</legend>
                    <p>Just fill in the Add Reminder form which can be found at the right hand side of this page. </p>
                    <p>Set your reminders in time so you don't miss an important deadline such as an MOT, driving licence or that special birthday. We've all done it! Get reminded so you can shop around for better deals on insurance, credit cards, mobile phones and utility bills. We recommend setting practical reminders to check your gas boiler, and smoke detectors as advised by the fire brigade.</p>
                    <p>Set one reminder or as many as you like, don't worry you can set them at any time. Set the reminders to alert you every year, every month or just for a specific day. Yearly and monthly reminders will continue for a lifetime.</p>
                <cfelse>
                    <legend style=" margin-top:40px;">No reminders currently set</legend>
                    <p>Time to set your reminders.</p>
                </cfif>

                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>More Suggested Reminders</th>
                            </tr>
                        </thead>
                        <tbody>
                            <cfoutput query="rc.getPriorityCats" startrow="6">
                                <tr>
                                    <td><a href="#buildUrl("users.reminders")#&defaultReminder=#urlSafe#">#title#</a></td>
                                </tr>
                            </cfoutput>
                        </tbody>
                    </table>
                </div>

            </div>
        <cfelse>
            
            <div class="well bs-component collapse in" style="background-color:#fff;">
                <legend><cfif NOT rc.isArchive>Active<cfelse>Expired</cfif></legend>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Reminder</th>
                                <th>Date</th>
                                <th colspan="3"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <cfoutput>
                                <cfloop query="rc.getReminders">
                                    <tr>
                                        <td width="70%"><a data-toggle="modal" data-target="##reminderModal" class="updateReminderLink" data-reminderid="<cfoutput>#rc.getReminders.reminderId#</cfoutput>" href="##">#rc.getReminders.title#</a></td>
                                        <td width="15%">#dateFormat(rc.getReminders.eventDate, 'dd/mm/yyyy')#</td>
                                        <td width="5%">
                                            <a class="share_button" data-reminderTitle="#jsStringFormat(rc.getReminders.title)#" href="##"><i class="fa fa-facebook"></i></a>
                                        </td>
                                        <td width="5%">
                                            <a href="<cfoutput>mailto:?subject=MUST REMEMBER THAT - #title#&body=Hi, thought you might want a quick reminder. #title# #DateFormat(rdate, "dd mmmm yyyy")# http://www.mustrememberthat.com/index.cfm?action=main.singleReminder%26reminderId=#reminderId#</cfoutput>"><i class="fa fa-envelope"></i></a>
                                        </td>
                                        <td width="5%">
                                            <a onclick="return confirm('Are you sure you want to delete this reminder?');" href="<cfoutput>#buildUrl("users.reminders")#&update=deleteReminder&reminderId=#reminderId#</cfoutput>"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                </cfloop>
                            </cfoutput>
                        </tbody>
                    </table>
                </div>
                <cfif rc.totalPages GT 1> 
                    <ul class="pagination">
                        <li <cfif 1 EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#rc.pageNumber - 1##rc.isArchiveURL#</cfoutput>">&laquo;</a></li>
                        <cfloop from="#rc.startpage#" to="#rc.endpage#" index="i">
                           <li <cfif rc.pageNumber EQ i> class="active"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#i##rc.isArchiveURL#</cfoutput>"><cfoutput>#i#</cfoutput></a></li> 
                        </cfloop>
                        <li <cfif rc.totalPages EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#rc.pageNumber + 1##rc.isArchiveURL#</cfoutput>">&raquo;</a></li>
                    </ul>
                </cfif>
            </div>
        </cfif>
    </div>

    <div class="visible-xs col-lg-7 reminderContainerMobile">
        <div style="display: inline-block; margin-bottom: 10px; width: 100%;">
            <input type="text" class="form-control" id="getPersonalReminders" placeholder="Search reminders">
        </div>
        <ul class="list-group">
            <cfoutput query="rc.getReminders">
                <li class="list-group-item">
                    <a href="#buildUrl("users.updateReminder")#&reminderId=#reminderId#" data-ajax="false">
                        <span style="float:right;" class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
                        </span>
                        #title#
                        <p style="font-size:16px; font-weight:normal; margin-bottom: -4px;">#DateFormat(eventDate, "dd mmmm yyyy")# - #TimeFormat(eventTime, "HH:mm")#</p>
                    </a>
                </li>
            </cfoutput>
        </ul>
        <cfif rc.totalPages GT 1> 
            <ul class="pagination">
                <li <cfif 1 EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#rc.pageNumber - 1##rc.isArchiveURL#</cfoutput>">&laquo;</a></li>
                <cfloop from="#rc.startpage#" to="#rc.endpage#" index="i">
                   <li <cfif rc.pageNumber EQ i> class="active"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#i##rc.isArchiveURL#</cfoutput>"><cfoutput>#i#</cfoutput></a></li> 
                </cfloop>
                <li <cfif rc.totalPages EQ rc.pageNumber> class="disabled"</cfif>><a href="<cfoutput>#buildUrl("users.reminders")#&pageNumber=#rc.pageNumber + 1##rc.isArchiveURL#</cfoutput>">&raquo;</a></li>
            </ul>
        </cfif>
    </div>
    
</div>

<div class="modal fade" id="reminderModal" tabindex="-1" role="dialog" aria-labelledby="reminderModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="reminderModalTitle">Update Reminder</h4>
            </div>
            <div class="modal-body">
                <!-- This is the image we're attaching Jcrop to -->
                <div class="row profileImages">
                    <div class="col-md-12">
                        
                        <div class="alert alert-danger fade in m-b-15" id="modalError" style="display:none;">
                            <strong>Error!</strong>
                            <a href="#"  data-dismiss="alert" class="close">x</a>
                            <div class="modalContainer"></div>
                        </div>
                        
                        <form id="modalForm" action="" method="POST" enctype="multipart/form-data">
        
                            <input type="hidden" name="reminderId" id="modalReminderId" value=""/>
                            <input name="token" type="hidden" value="<cfoutput>#CSRFGenerateToken()#</cfoutput>" />
                            
                            <fieldset>
                                <div class="form-group col-md-12">
                                    <label for="title">Title</label>
                                    <input type="text" autocomplete="off" placeholder="Reminder Title" id="modaltitle" name="title" class="form-control" value="">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="category">Category</label>
                                    <select id="modalmaincategory" name="maincategory" class="form-control" data-role="none">   
                                        <option value="">Select Category</option>
                                        <cfoutput query="rc.getMainCats">
                                            <option value="#rc.getMainCats.mainCategoryId#">#rc.getMainCats.mainCategoryName#</option>
                                        </cfoutput>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="category">Sub Category</label>
                                    <select id="modalcategory" name="category" class="form-control" data-role="none">   
                                        <cfoutput query="rc.getCats">
                                            <option value="#rc.getCats.id#">#rc.getCats.title#</option>
                                        </cfoutput>  
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail"><span class="visible-xs">Date</span><span class="hidden-xs">Date of event</span></label>
                                    <input type="hidden" name="rdate" id="modalrdate" value="" />
                                    <div class='input-group date form_date2'>
                                        <input type="text" data-date-format="Do MMMM YYYY" data-link-field="rdate" autocomplete="off" id="modalinputdate" class="form-control" <cfif len(rc.rdate)>value="<cfoutput>#day(rc.rdate)# #monthAsString(month(rc.rdate))# #year(rc.rdate)#</cfoutput>"</cfif>>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail"><span class="visible-xs">Time</span><span class="hidden-xs">Time of event</span></label>
                                    <!--- <input type="hidden" name="rtime" id="rtime" value="<cfoutput>#rc.rtime#</cfoutput>" /> --->
                                    <div class='input-group date form_time'>
                                        <input type='text' class="form-control" name="rtime" id="modalrtime" value="<cfoutput>#rc.rtime#</cfoutput>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail">Send me reminder</label>
                                    <select id="modalintervalPeriod" name="intervalPeriod" class="form-control" data-role="none">              
                                        <cfloop from="1" to="100" index="i"><cfoutput>
                                            <option  value="#i#">#i#</option>
                                        </cfoutput></cfloop>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail">&nbsp;</label>
                                    <select id="modalremindertime" name="remindertime" class="form-control" data-role="none">              
                                        <option <cfif rc.remindertime EQ 'n'>selected="selected"</cfif> value="n">Minutes before</option>
                                        <option <cfif rc.remindertime EQ 'h'>selected="selected"</cfif> value="h">Hour before</option>
                                        <option <cfif rc.remindertime EQ 'd'>selected="selected"</cfif> value="d">Day Before</option>
                                        <option <cfif rc.remindertime EQ 'ww'>selected="selected"</cfif> value="ww">Week Before</option>
                                        <option <cfif rc.interval EQ 'm'>selected="selected"</cfif> value="m">Month Before</option>
                                    </select>
                                </div>
                                <div id="reminderActualTimeModal" class="form-group col-md-6" style="display:none;">
                                    <label for="inputEmail">Reminder Time</label>
                                    <div class='input-group date form_time' id='datetimepicker4'>
                                        <input type='text' class="form-control" name="reminderActualTime" id="reminderActualTime" value="<cfoutput>#rc.reminderActualTime#</cfoutput>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="inputEmail">How frequent</label>
                                    <select id="modalinterval" name="interval" class="form-control" data-role="none">              
                                        <option <cfif rc.interval EQ 'once'>selected="selected"</cfif> value="once">Once</option>
                                        <option <cfif rc.interval EQ 'daily'>selected="selected"</cfif> value="daily">Daily (Until Event)</option>
                                        <option <cfif rc.interval EQ 'daily2'>selected="selected"</cfif> value="daily2">Daily (Forever)</option>
                                        <option <cfif rc.interval EQ 'weekly'>selected="selected"</cfif> value="weekly">Weekly</option>
                                        <option <cfif rc.interval EQ 'monthly'>selected="selected"</cfif> value="monthly">Monthly</option>
                                        <option <cfif rc.interval EQ 'yearly'>selected="selected"</cfif> value="yearly">Yearly</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="inputEmail">Comments</label>
                                    <textarea id="modalcomments" name="comments" class="form-control"><cfoutput>#rc.comments#</cfoutput></textarea>
                                </div>
                                <div class="form-group col-md-12 text-center">
                                    <button class="btn btn-primary" type="submit" id="modalSubmit">Set Reminder</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        