<div class="row wellReminders">
    
    <div class="col-xs-12 addReminderTitle" style="margin-bottom:10px;">
        <strong class="mrTitle">My Groups</strong>
    </div>

    <div class="col-lg-12 reminderContainerMobile">
        <ul class="list-group">
            <cfoutput query="rc.getGroups">
                <li class="list-group-item">
                    <a href="/groups/#rc.getGroups.urlSlug#/" data-ajax="false">
                        <span style="float:right;" class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
                        </span>
                        #title#
                    </a>
                </li>
            </cfoutput>
        </ul>
    </div>

</div>
        