<cfsilent>
<cfparam name="form.fbname" default="">
<cfparam name="form.fbemail" default="">
<cfparam name="form.fbid" default="">
<cfparam name="form.fbLocation" default="">
<cfparam name="form.rid" default="">

<cfif isValid("email", form.fbemail)>

	<!--- Check to see if the user has previous login into the system --->
	<cfquery name="rc.getuser">
        SELECT usr.*
        FROM tbl_users usr
        WHERE usr.emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.fbemail#">
    </cfquery>
	
	<!--- is this a new user?:--->
	<cfif NOT rc.getuser.recordcount>
		
		<cftry>
			
			<cfset rc.randomPassword = randRange(100000000, 999999999)>
			<cfset rc.newRemoteId = 'fb_' & form.fbid>
			<cfset rc.firstname = listFirst(form.fbname, ' ')>
			<cfset rc.lastname = listLast(form.fbname, ' ')>

			<!--- create a password salt --->
            <cfset rc.passwordSalt = createUUID()>
            <cfset rc.userId = createUUID()>
            
            <!--- now hash the password + the password salt that the user passed --->
            <cfset rc.inputHash = Hash(rc.randomPassword & rc.passwordSalt, 'SHA-512') />

            <!--- now create the DOB --->
            <cfset rc.dateOfBirth = '1/1/1970'>
			
			<cftransaction> 
                        
                <!--- add to database --->
                <cfquery name="rc.createUser">
                    INSERT INTO tbl_users
                    (
                        userId,
                        firstname,
                        lastname,
                        emailAddress,
                        DOB,
                        password,
                        passwordSalt,
                        timezone,
                        userVerify
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.firstname#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.lastname#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.fbemail#">,
                        <cfqueryparam cfsqltype="cf_sql_date" value="#rc.dateOfBirth#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.inputHash#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.passwordSalt#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#GetTimeZoneInfo().utcHourOffset#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                    )
                </cfquery>

           </cftransaction> 
            
            <!--- Now login the user and set the session variables --->
            <cfset session.auth.isLoggedIn = true>
            <cfset session.auth.firstName = rc.firstName> 
            <cfset session.auth.lastname = rc.lastname> 
            <cfset session.auth.emailAddress = form.fbemail>
            <cfset session.auth.userId = rc.userId>

            <cfif structKeyExists(session, 'reminderArray') AND isArray(session.reminderArray)>
                        
                <cfloop from="1" to="#arrayLen(session.reminderArray)#" index="i">
                    
                    <!--- get the reminder --->
                    <cfquery name="rc.getReminder">
                        SELECT * 
                        FROM tbl_reminders
                        WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.reminderArray[i]#">
                    </cfquery>

                    <cfset rc.rdate = rc.getReminder.rdate>
                    <cfset rc.rtime = rc.getReminder.rtime>
                    <cfset rc.interval = rc.getReminder.interval>
                    <cfset rc.forever = rc.getReminder.forever>
                    <cfset rc.reminderId = createUUID()>
                    <cfset rc.reminderTime = rc.getReminder.reminderTime>
                    <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                    <cfset rc.title = rc.getReminder.title>
                    <cfset rc.category = rc.getReminder.category>
                    <cfset rc.reminderActualTime = ''>

                    <cfquery name="rc.createReminder">
                        INSERT INTO tbl_reminders
                        (
                            reminderId,
                            reminderType,
                            reminderKey,
                            title,
                            category,
                            interval,
                            remindertime,
                            rdate,
                            rtime,
                            intervalPeriod,
                            isLive,
                            forever
                        )
                        VALUES
                        (
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                            <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                            <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                        )
                    </cfquery>

                    <!--- create the schedule --->
                    <cfset createSchedule = variables.userObject.createSchedule(rc)>

                </cfloop>

                <cfset session.reminderArray = ''>

            </cfif>
            
            <cfif len(form.rid)>
                
                <!--- get the reminder --->
                <cfquery name="rc.getReminder">
                    SELECT * 
                    FROM tbl_reminders
                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.rid#">
                </cfquery>

                <cfset rc.rdate = rc.getReminder.rdate>
                <cfset rc.rtime = rc.getReminder.rtime>
                <cfset rc.interval = rc.getReminder.interval>
                <cfset rc.forever = rc.getReminder.forever>
                <cfset rc.reminderId = createUUID()>
                <cfset rc.reminderTime = rc.getReminder.reminderTime>
                <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                <cfset rc.title = rc.getReminder.title>
                <cfset rc.category = rc.getReminder.category>

                <cfquery name="rc.createReminder">
                    INSERT INTO tbl_reminders
                    (
                        reminderId,
                        reminderType,
                        reminderKey,
                        title,
                        category,
                        interval,
                        remindertime,
                        rdate,
                        rtime,
                        intervalPeriod,
                        forever
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                        <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                        <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                    )
                </cfquery>

                <!--- create the schedule --->
                <cfset createSchedule = variables.userObject.createSchedule(rc)>
            </cfif>
                   
            <cfcatch type="any">
				
				<cfmail server="mail.flintmedia.co.uk" username="noreply@mustrememberthat.com" password="Popesabbots1!" from="support@mustrememberthat.com" subject="MRT Facbook Login Error" to="james@maisyweb.co.uk">
					<cfdump var="#cfcatch#">
				</cfmail>

                <cfheader statuscode="400" statustext="Bad request.">
				
			</cfcatch>
		


		</cftry>
		
	<cfelse>

		<cfset session.auth.isLoggedIn = true>
        <cfset session.auth.firstName = rc.getuser.firstName> 
        <cfset session.auth.lastname = rc.getuser.lastname> 
        <cfset session.auth.emailAddress = rc.getuser.emailAddress>
        <cfset session.auth.userId = rc.getuser.userId>

        <cfif structKeyExists(session, 'reminderArray') AND isArray(session.reminderArray)>
                        
                <cfloop from="1" to="#arrayLen(session.reminderArray)#" index="i">
                    
                    <!--- get the reminder --->
                    <cfquery name="rc.getReminder">
                        SELECT * 
                        FROM tbl_reminders
                        WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.reminderArray[i]#">
                    </cfquery>

                    <cfset rc.rdate = rc.getReminder.rdate>
                    <cfset rc.rtime = rc.getReminder.rtime>
                    <cfset rc.interval = rc.getReminder.interval>
                    <cfset rc.forever = rc.getReminder.forever>
                    <cfset rc.reminderId = createUUID()>
                    <cfset rc.reminderTime = rc.getReminder.reminderTime>
                    <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                    <cfset rc.title = rc.getReminder.title>
                    <cfset rc.category = rc.getReminder.category>
                    <cfset rc.reminderActualTime = ''>

                    <cfquery name="rc.createReminder">
                        INSERT INTO tbl_reminders
                        (
                            reminderId,
                            reminderType,
                            reminderKey,
                            title,
                            category,
                            interval,
                            remindertime,
                            rdate,
                            rtime,
                            intervalPeriod,
                            isLive,
                            forever
                        )
                        VALUES
                        (
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                            <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                            <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                        )
                    </cfquery>

                    <!--- create the schedule --->
                    <cfset createSchedule = variables.userObject.createSchedule(rc)>

                </cfloop>

                <cfset session.reminderArray = ''>

            </cfif>
            
            <cfif len(form.rid)>
                
                <!--- get the reminder --->
                <cfquery name="rc.getReminder">
                    SELECT * 
                    FROM tbl_reminders
                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.rid#">
                </cfquery>

                <cfset rc.rdate = rc.getReminder.rdate>
                <cfset rc.rtime = rc.getReminder.rtime>
                <cfset rc.interval = rc.getReminder.interval>
                <cfset rc.forever = rc.getReminder.forever>
                <cfset rc.reminderId = createUUID()>
                <cfset rc.reminderTime = rc.getReminder.reminderTime>
                <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                <cfset rc.title = rc.getReminder.title>
                <cfset rc.category = rc.getReminder.category>

                <cfquery name="rc.createReminder">
                    INSERT INTO tbl_reminders
                    (
                        reminderId,
                        reminderType,
                        reminderKey,
                        title,
                        category,
                        interval,
                        remindertime,
                        rdate,
                        rtime,
                        intervalPeriod,
                        forever
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                        <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                        <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                    )
                </cfquery>

                <!--- create the schedule --->
                <cfset createSchedule = variables.userObject.createSchedule(rc)>
            </cfif>
           
    </cfif>
	
	
</cfif>
</cfsilent>