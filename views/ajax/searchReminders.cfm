<cfset request.layout = false />

<cftry>

    <cfoutput query="rc.getReminders">
        <cfsilent>
            <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
                <cfset itemLink = buildUrl("users.businessreminder") & '&rid=' & rc.getReminders.reminderId>
            <cfelse>
                <cfset itemLink = buildUrl("main.registration") & '&msg=login&slug=' & rc.getBusiness.urlslug>
            </cfif>
        </cfsilent>

        <li class="list-group-item">
            
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 mobileNoPadding">
                <input type="checkbox" name="rid" class="cbrid" value="#rc.getReminders.reminderId#">
            </div>
            <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 mobileNoPadding">
                <div class="titleContainer">
                    <div style="float:right;">
                        <a href="#itemLink#" style="float:right;" type="button" class="<cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>singleReminderBtn</cfif> btn btn-default-yellow" data-rid="#rc.getReminders.reminderId#"><span class="glyphicon glyphicon-pushpin"></span> Add </a><br/>
                    </div>
                    #title#
                </div>

                <div>
                    
                    <p style="margin-bottom: -4px;">
                        #application.utils.DateFormatExtended(eventDate, "dd mmmm yyyy")# - #TimeFormat(eventTime, "HH:mm")#
                        <cfif len(rc.getReminders.comments)><a href="##" style="float:right;" data-toshow="moreInfoContent#rc.getReminders.currentRow#" class="showMoreInfo">More info</a></cfif>
                    </p>
                    <cfif len(rc.getReminders.comments)>
                        <p class="moreInfoContent#rc.getReminders.currentRow# moreInfoContent" style="margin-top:10px">#rc.getReminders.comments#</p>
                    </cfif>
                    
                </div>
            </div>

            
        </li>
    </cfoutput>

    <cfcatch type="any"></cfcatch>
        
</cftry>