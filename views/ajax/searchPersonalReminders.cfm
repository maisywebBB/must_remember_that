<cfset request.layout = false />
<cfoutput query="rc.getReminders">
    <li class="list-group-item">
        <a href="#buildUrl("users.updateReminder")#&reminderId=#reminderId#" data-ajax="false">
            <span style="float:right;" class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-angle-right fa-stack-1x fa-inverse"></i>
            </span>
            #title#
            <p style="font-size:16px; font-weight:normal; margin-bottom: -4px;">#DateFormat(eventDate, "dd mmmm yyyy")# - #TimeFormat(eventTime, "HH:mm")#</p>
        </a>
    </li>
</cfoutput>