<cfcomponent output="false">
	
    <cffunction name="init">
        <cfargument name="fw" type="any" />
        <cfset variables.fw = arguments.fw />
	</cffunction>
    
    <cffunction name="session" output="false">  
        <cfargument name="rc">  
        
        <!--- set up the user's session --->
		<cfset session.auth = {}>
        <cfset session.auth.isLoggedIn = false>

        <!--- if the user has a cookie let try to log them in --->
        <cfif IsDefined("cookie.MRTuser")>

            <!--- check to see if the cookie is valid --->
            <cfquery name="rc.getCookieDetails">    
                SELECT * 
                FROM tbl_users 
                WHERE userid = <cfqueryparam cfsqltype="cf_sql_varchar" value="#cookie.MRTuser#">
            </cfquery>

            <!--- if no cookie is found then expire the cookies --->
            <cfif NOT rc.getCookieDetails.recordcount>
                <cfcookie name="MRTuser" expires="NOW">
                <cfcookie name="MRTid" expires="NOW"> 
            <cfelse>
                <!--- if a record is found log in the user and take them to the reminders page --->
                <cfset session.auth.isLoggedIn = true>
                <cfset session.auth.firstName = rc.getCookieDetails.firstName> 
                <cfset session.auth.lastname = rc.getCookieDetails.lastname> 
                <cfset session.auth.emailAddress = rc.getCookieDetails.emailAddress>
                <cfset session.auth.userId = rc.getCookieDetails.userId>
                
                <cfif request.action EQ 'main.default'>
                    <cfset variables.fw.redirect( "users.reminders" )>
                </cfif>        
                    
            </cfif>

        </cfif>
            
    </cffunction>
    
    <cffunction name="authorize" output="true">  
        <cfargument name="rc">  
        
        <!--- check to make sure the user is logged on --->
		<cfif ( not ( structKeyExists( session, "auth" ) AND session.auth.isLoggedIn ) AND !listfindnocase( 'login', variables.fw.getSection() ) AND !listfindnocase( 'support', variables.fw.getSection() ) AND !listfindnocase( 'ajax', variables.fw.getSection() ) AND !listfindnocase( 'main', variables.fw.getSection() ) AND request.action NEQ 'business.business' AND request.action NEQ 'business.addabusiness' ) >
            <cfset variables.fw.redirect('main')>
        </cfif>
	   
    </cffunction>

    <cffunction name="chkPermissions" output="true">  
        <cfargument name="rc">  
        
        <cfset rc.permissionStruct = 
        {
            'users.adminusers' = true,
            'users.clientmanagers' = true
        }>
        
        <!--- Check the user has the correct permissions for this page --->
        <cfif structKeyExists(rc.permissionStruct, request.action) AND session.auth.userType EQ 'locationManager'>
            <cfset variables.fw.redirect('main')>
        </cfif>
        
    </cffunction>
    
    <cffunction name="appSetup" output="true">  
        <cfargument name="rc">  
        
        <!--- set up cfcs --->
        <cfobject component="controllers.email" name="application.emailCFC" type="component">
        <cfobject component="controllers.utils" name="application.utilsCFC" type="component">
        
        <!--- app specific variables --->
        <cfif cgi.http_host CONTAINS 'local.'>
            <cfset application.sendEmails = 0>
       <cfelse>
            <cfset application.sendEmails = 1>
        </cfif>
    </cffunction>
    
</cfcomponent>
