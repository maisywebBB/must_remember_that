<cfcomponent output="false">
	
    <cffunction name="init">
        <cfargument name="fw" type="any" />
        <cfset variables.fw = arguments.fw />
        <cfset variables.userObject = createObject("component","controllers.users")>
	</cffunction>
    
    <cffunction name="default">
        <cfargument name="rc"> 
        
        <cfparam name="rc.firstname" default="">
        <cfparam name="rc.lastname" default="">
        <cfparam name="rc.emailAddress" default="">
        <cfparam name="rc.slug" default="">
        <cfparam name="rc.rid" default="">
        <cfparam name="rc.maincategory" default="0">
        <cfparam name="rc.rdate" default="">
        <cfparam name="rc.rtime" default="">
        <cfparam name="rc.reminderActualTime" default="">
        <cfparam name="rc.comments" default="">
        <cfparam name="rc.intervalPeriod" default="1">
        <cfparam name="rc.reminderTime" default="">
        <cfparam name="rc.reminderType" default="1">
        <cfparam name="rc.title" default="">
        <cfparam name="rc.category" default="">
        <cfparam name="rc.interval" default="">

        <!--- page params --->
        <cfset rc.layoutName = 'homepage'>
        <cfset rc.nofooter = true>
        <cfset rc.bodyID = 'homepage'>
        <cfset rc.errors = []>
        <cfparam name="rc.successMessage" default="#arrayNew(1)#">
        <cfif NOT reFindNoCase("android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) GT 0>
            <cfset rc.footerBottom = 'true'>
        </cfif>
            
        <!--- update the employee's password --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'addUser'>
            
            <!--- check for errors --->
            <cfset rc.reminderErrors = variables.userObject.validateReminder(rc)>
            <cfif not len(trim(rc.firstname))>
                <cfset arrayAppend(rc.errors, "Please supply a first name")>
            </cfif>
            <cfif not len(trim(rc.lastname))>
                <cfset arrayAppend(rc.errors, "Please supply a last name")>
            </cfif>
            
            <cfif NOT isValid('email', rc.emailAddress)>
                <cfset arrayAppend(rc.errors, "Please supply a valid email address")>
            </cfif>
            
            <!--- make sure the email address has not been used before --->
            <cfquery name="rc.chkEmail" result="rc.insertEmployee">
                SELECT emailAddress
                FROM tbl_users
                WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#"/>
            </cfquery>
            
            <cfif rc.chkEmail.recordcount>
                <cfset arrayAppend(rc.errors, "A uesr with that email address already exists")>
            </cfif>

            <cfif not len(trim(rc.password)) GTE 6>
                <cfset arrayAppend(rc.errors, "Your password must contain at least 6 characters")>
            </cfif>

            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.passwordSalt = createUUID()>
                    <cfset rc.userId = createUUID()>
                    <cfset rc.reminderId = createUUID()>
                    
                    <!--- now hash the password + the password salt that the user passed --->
                    <cfset rc.inputHash = Hash(rc.password & rc.passwordSalt, 'SHA-512') />

                    <!--- now create the DOB --->
                    <cfset rc.dateOfBirth = '#rc.day#/#rc.month#/#rc.year#'>
                    
                    <cfset rc.sec = randrange(1,10000000)>

                    <cftransaction> 
                        
                        <!--- add to database --->
                        <cfquery name="rc.createUser">
                            INSERT INTO tbl_users
                            (
                                userId,
                                firstname,
                                lastname,
                                emailAddress,
                                DOB,
                                password,
                                passwordSalt,
                                timezone,
                                userVerify
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.firstname#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.lastname#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#rc.dateOfBirth#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.inputHash#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.passwordSalt#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#GetTimeZoneInfo().utcHourOffset-1#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.sec#">
                            )
                        </cfquery>

                        <!--- Now send the registration email --->
                        <cfsavecontent variable="emailContent"><cfoutput>
                            Thank you for joining Must Remember That!<br><br>
                            Please click the following link to activate your account. <a href="http://www.mustrememberthat.com/index.cfm?action=main.activate&u=#rc.userId#&amp;sec=#rc.sec#">Click To Activate</a><br><br>
                            If you have a problem with the link above please copy the following link into your browser http://www.mustrememberthat.com/index.cfm?action=main.activate&u=#rc.userId#&amp;sec=#rc.sec#<br><br>
                            NEVER FORGET AGAIN
                        </cfoutput></cfsavecontent>
                        
                        <cfset application.emailCFC.sendEmail(rc.emailAddress, 'Activate your Must Remember That account',emailContent)>
                    
                        <cfset arrayAppend(rc.successMessage, "You will shortly receive an activation email, please click the link within the email to activate your account.<br><br>If you don't receive it in the next 10 minutes, check other places it might be, like your junk, spam, social, or other folders.<br>")>

                   </cftransaction> 
                    
                    <!--- Now login the user and set the session variables
                    <cfset session.auth.isLoggedIn = true>
                    <cfset session.auth.firstName = rc.firstName> 
                    <cfset session.auth.lastname = rc.lastname> 
                    <cfset session.auth.emailAddress = rc.emailAddress>
                    <cfset session.auth.userId = rc.userId>
                           
                    <cfset variables.fw.redirect( "users.reminders" )>

                     --->
                
                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

                <cfif rc.interval EQ 'daily2'>
                    <cfset rc.interval = 'daily'>
                    <cfset rc.forever = 1>
                <cfelse>
                    <cfset rc.forever = 0>
                </cfif>

                <!--- Now try to add the reminder --->
                <cftry>
                    
                    <cfset createSchedule = variables.userObject.createSchedule(rc)>

                    <cfquery name="rc.createReminder">
                        INSERT INTO tbl_reminders
                        (
                            reminderId,
                            reminderType,
                            reminderKey,
                            title,
                            comments,
                            category,
                            interval,
                            remindertime,
                            rdate,
                            rtime,
                            intervalPeriod,
                            eventDate,
                            eventTime,
                            forever
                        )
                        VALUES
                        (
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.reminderType#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.comments#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                            <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                            <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                            <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                            <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                            <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                            <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                        )
                    </cfquery>

                    <!--- create the schedule --->
                    <cfset session.auth.userId = rc.userId>
                    
                    <cfset structdelete( session.auth, "USERID" )>

                    <cfcatch type="any">
                        <cfdump var="#cfcatch#"><cfabort>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'loginUser'>
            
            <!--- get the users details from the DB --->
            <cfquery name="rc.getuser">
                SELECT usr.*
                FROM tbl_users usr
                WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">
            </cfquery>
            
            <cfif NOT rc.getuser.recordCount>
                <cfset rc.errors = ['Either your username or password is incorrect please double check and try again<br><br><a href="" data-toggle="modal" data-target="##passwordModal">Forgotten password</a>']>
            <cfelse>
                <cfif NOT rc.getuser.userAccept>
                    <cfset rc.errors = ['Your account has not yet been verified. Please check your email in order to verify your account']>
                <cfelse>
                    <!--- now hash the password + the password salt that the user passed --->
                    <cfset rc.inputHash = Hash(rc.password & rc.getuser.passwordSalt, 'SHA-512') />
                    
                    <!--- if the input hash matches the hash in the db then the user is valid --->
                    <cfif NOT rc.inputHash EQ rc.getuser.password>
                        <cfset rc.errors = ["Invalid login credentials. Please try again"]>
                    <cfelse>
                        
                        <!--- if the user wants to be reembered set a cookie --->
                        <cfif structKeyExists(RC, 'rememberMe')>
                            
                            <!--- create a new cookie pass --->
                            <cfset rc.cookiepass = createUUID()>

                            <!--- now update the cookiepass in the DB --->
                             <cfquery name="rc.setCookie">
                                UPDATE tbl_users
                                SET cookiepass = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.cookiepass#">
                                WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">
                            </cfquery>

                            <cfcookie name="MRTuser" value="#rc.getuser.userid#" expires="NEVER">
                            <cfcookie name="MRTid" value="#rc.cookiepass#" expires="NEVER">
                        
                        </cfif>

                        <cfset session.auth.isLoggedIn = true>
                        <cfset session.auth.firstName = rc.getuser.firstName> 
                        <cfset session.auth.lastname = rc.getuser.lastname> 
                        <cfset session.auth.emailAddress = rc.getuser.emailAddress>
                        <cfset session.auth.userId = rc.getuser.userId>
                        
                        <cfif structKeyExists(session, 'reminderArray') AND isArray(session.reminderArray)>
                            
                            <cfloop from="1" to="#arrayLen(session.reminderArray)#" index="i">
                                
                                <!--- get the reminder --->
                                <cfquery name="rc.getReminder">
                                    SELECT * 
                                    FROM tbl_reminders
                                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.reminderArray[i]#">
                                </cfquery>

                                <cfset rc.rdate = rc.getReminder.rdate>
                                <cfset rc.rtime = rc.getReminder.rtime>
                                <cfset rc.interval = rc.getReminder.interval>
                                <cfset rc.reminderId = createUUID()>
                                <cfset rc.reminderTime = rc.getReminder.reminderTime>
                                <cfset rc.forever = rc.getReminder.forever>
                                <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                                <cfset rc.title = rc.getReminder.title>
                                <cfset rc.category = rc.getReminder.category>
                                <cfset rc.reminderActualTime = ''>

                                <cfset createSchedule = variables.userObject.createSchedule(rc)>

                                <cfquery name="rc.createReminder">
                                    INSERT INTO tbl_reminders
                                    (
                                        reminderId,
                                        reminderType,
                                        reminderKey,
                                        title,
                                        category,
                                        interval,
                                        remindertime,
                                        rdate,
                                        rtime,
                                        intervalPeriod,
                                        isLive,
                                        eventDate,
                                        eventTime,
                                        forever
                                    )
                                    VALUES
                                    (
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                        <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                        <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                        <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                        <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                                    )
                                </cfquery>

                            </cfloop>

                            <cfset session.reminderArray = ''>

                        </cfif>
                            
                        <cfif len(RC.rid)>
                            
                            <!--- get the reminder --->
                            <cfquery name="rc.getReminder">
                                SELECT * 
                                FROM tbl_reminders
                                WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
                            </cfquery>

                            <cfset rc.rdate = rc.getReminder.rdate>
                            <cfset rc.rtime = rc.getReminder.rtime>
                            <cfset rc.interval = rc.getReminder.interval>
                            <cfset rc.reminderId = createUUID()>
                            <cfset rc.reminderTime = rc.getReminder.reminderTime>
                            <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                            <cfset rc.title = rc.getReminder.title>
                            <cfset rc.category = rc.getReminder.category>
                            <cfset rc.forever = rc.getReminder.forever>
                            <cfset rc.reminderActualTime = ''>
			    
			    <cfif NOT isNumeric(rc.intervalPeriod)>
			    	<cfset rc.intervalPeriod = 1>
			    </cfif>
			                 
                            <cfset createSchedule = variables.userObject.createSchedule(rc)>

                            <cfquery name="rc.createReminder">
                                INSERT INTO tbl_reminders
                                (
                                    reminderId,
                                    reminderType,
                                    reminderKey,
                                    title,
                                    category,
                                    interval,
                                    remindertime,
                                    rdate,
                                    rtime,
                                    intervalPeriod,
                                    isLive,
                                    eventDate,
                                    eventTime,
                                    forever
                                )
                                VALUES
                                (
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                    <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                    <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                    <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                    <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                                )
                            </cfquery>

                        </cfif>

                        <cfif len(RC.slug)>
                            <!--- <cflocation addtoken="false" url="/#rc.slug#/"> --->
                            <cfset variables.fw.redirect( "users.reminders" )>
                        <cfelse>
                            <cfset variables.fw.redirect( "users.reminders" )>
                        </cfif>

                        <cfif structKeyExists(RC, 'forwardAddress')>
                            <cflocation addtoken="false" url="/#rc.forwardAddress#/">
                        <cfelse>
                            <cfset variables.fw.redirect( "users.reminders" )>
                        </cfif>
                    
                    </cfif>
                </cfif>
            </cfif>
                
        </cfif>

        <cfif NOT isNumeric(rc.maincategory)>
            <cfset rc.maincategory = 0>
        </cfif>

         <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>

        <cfquery name="rc.getPriorityCats">
            SELECT *
            FROM tbl_categories
            WHERE isPriority = 1
        </cfquery>

        <!--- if we need to populate a default reminder --->
        <cfset rc.title = ''>
        <cfset rc.maincategory = ''>
        <cfset rc.category = ''>
        <cfset rc.remindertime = 'd'>

        <cfif structKeyExists(URL, 'defaultReminder')>

            <cfquery name="rc.getDefaultReminder">
                SELECT *
                FROM tbl_categories
                WHERE urlSafe = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.defaultReminder#">
            </cfquery>

            <cfset rc.title = rc.getDefaultReminder.defaultReminderTitle>
            <cfset rc.maincategory = rc.getDefaultReminder.mainCategoryId>
            <cfset rc.category = rc.getDefaultReminder.id>
            <cfset rc.remindertime = 'd'>

        </cfif>
                
    </cffunction>

    <cffunction name="registration">
        <cfargument name="rc"> 
        
        <cfparam name="rc.firstname" default="">
        <cfparam name="rc.lastname" default="">
        <cfparam name="rc.slug" default="">
        <cfparam name="rc.rid" default="">
        <cfparam name="rc.emailAddress" default="">

        <!--- page params --->
        <cfset rc.errors = []>
        <cfset rc.successMessage = []>

        <cfif NOT reFindNoCase("android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) GT 0>
            <cfset rc.footerBottom = 'true'>
        </cfif>
              
        <!--- update the employee's password --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'addUser'>
            
            <!--- check for errors --->
            <cfif not len(trim(rc.firstname))>
                <cfset arrayAppend(rc.errors, "Please supply a first name")>
            </cfif>
            <cfif not len(trim(rc.lastname))>
                <cfset arrayAppend(rc.errors, "Please supply a last name")>
            </cfif>
            
            <cfif NOT isValid('email', rc.emailAddress)>
                <cfset arrayAppend(rc.errors, "Please supply a valid email address")>
            </cfif>
            <cfif trim(rc.emailAddress) NEQ trim(rc.confEmailAddress)>
                <cfset arrayAppend(rc.errors, "Your email addresses must match")>
            </cfif>
            
            <!--- make sure the email address has not been used before --->
            <cfquery name="rc.chkEmail" result="rc.insertEmployee">
                SELECT emailAddress
                FROM tbl_users
                WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#"/>
            </cfquery>
            
            <cfif rc.chkEmail.recordcount>
                <cfset arrayAppend(rc.errors, "A uesr with that email address already exists")>
            </cfif>

            <cfif not len(trim(rc.password)) GTE 6>
                <cfset arrayAppend(rc.errors, "Your password must contain at least 6 characters")>
            </cfif>
            <cfif trim(rc.password) NEQ trim(rc.confirmPassword)>
                <cfset arrayAppend(rc.errors, "Your passwords must match")>
            </cfif>
            
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.passwordSalt = createUUID()>
                    <cfset rc.userId = createUUID()>
                    
                    <!--- now hash the password + the password salt that the user passed --->
                    <cfset rc.inputHash = Hash(rc.password & rc.passwordSalt, 'SHA-512') />

                    <!--- now create the DOB --->
                    <cfset rc.dateOfBirth = '#rc.day#/#rc.month#/#rc.year#'>
                    
                    <cfset rc.sec = randrange(1,10000000)>

                    <cftransaction> 
                        
                        <!--- add to database --->
                        <cfquery name="rc.createUser">
                            INSERT INTO tbl_users
                            (
                                userId,
                                firstname,
                                lastname,
                                emailAddress,
                                DOB,
                                password,
                                passwordSalt,
                                timezone,
                                userVerify
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.firstname#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.lastname#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#rc.dateOfBirth#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.inputHash#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.passwordSalt#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#GetTimeZoneInfo().utcHourOffset-1#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.sec#">
                            )
                        </cfquery>

                   </cftransaction> 
                    
                    <!--- Now login the user and set the session variables --->
                    <cfset session.auth.isLoggedIn = true>
                    <cfset session.auth.firstName = rc.firstName> 
                    <cfset session.auth.lastname = rc.lastname> 
                    <cfset session.auth.emailAddress = rc.emailAddress>
                    <cfset session.auth.userId = rc.userId>
                    
                    <cfif structKeyExists(session, 'reminderArray') AND isArray(session.reminderArray)>
                        
                        <cfloop from="1" to="#arrayLen(session.reminderArray)#" index="i">
                            
                            <!--- get the reminder --->
                            <cfquery name="rc.getReminder">
                                SELECT * 
                                FROM tbl_reminders
                                WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.reminderArray[i]#">
                            </cfquery>

                            <cfset rc.rdate = rc.getReminder.rdate>
                            <cfset rc.rtime = rc.getReminder.rtime>
                            <cfset rc.interval = rc.getReminder.interval>
                            <cfset rc.reminderId = createUUID()>
                            <cfset rc.reminderTime = rc.getReminder.reminderTime>
                            <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                            <cfset rc.title = rc.getReminder.title>
                            <cfset rc.category = rc.getReminder.category>
                            <cfset rc.forever = rc.getReminder.forever>
                            <cfset rc.reminderActualTime = ''>

                            <cfset createSchedule = variables.userObject.createSchedule(rc)>

                            <cfquery name="rc.createReminder">
                                INSERT INTO tbl_reminders
                                (
                                    reminderId,
                                    reminderType,
                                    reminderKey,
                                    title,
                                    category,
                                    interval,
                                    remindertime,
                                    rdate,
                                    rtime,
                                    intervalPeriod,
                                    isLive,
                                    eventDate,
                                    eventTime,
                                    forever
                                )
                                VALUES
                                (
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                    <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                    <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                    <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                    <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                    <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                                )
                            </cfquery>

                        </cfloop>

                        <cfset session.reminderArray = ''>

                    </cfif>

                    <cfif structKeyExists(rc, 'subscriptionId') AND isValid('uuid', rc.subscriptionId)>
                        
                        <!--- first subscribe the user to the business --->
                        <cfquery name="setReminders">
                            INSERT INTO tbl_subscriptions(userId, businessId, subscriptionType)
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.subscriptionId#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                            )
                        </cfquery>

                        <!--- now check to see if we have to add all the current reminders --->
                        <cfif structKeyExists(RC, 'allreminders') AND rc.allreminders EQ 1>
                            
                            <!--- get a list of all future reminders for this business --->
                            <cfquery name="rc.getReminderList">
                                SELECT rem.*, tbl_categories.title AS catTitle, overall_count = COUNT(*) OVER(),
                                (select count(*) from tbl_reminders where parentid= rem.reminderid ) as ChildCount
                                FROM tbl_reminders rem
                                INNER JOIN tbl_categories ON tbl_categories.id = rem.category
                                WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="2">
                                AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.subscriptionId#">
                                AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                                AND rdate >= getdate()
                                ORDER BY rdate
                            </cfquery>

                            <cfset rc.reminderList = valueList(rc.getReminderList.reminderId)>

                            <cfloop list="#rc.reminderList#" index="i">
                            
                                <!--- get the reminder --->
                                <cfquery name="rc.getReminder">
                                    SELECT * 
                                    FROM tbl_reminders
                                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#i#">
                                </cfquery>

                                <cfset rc.rdate = rc.getReminder.rdate>
                                <cfset rc.rtime = rc.getReminder.rtime>
                                <cfset rc.interval = rc.getReminder.interval>
                                <cfset rc.reminderId = createUUID()>
                                <cfset rc.reminderTime = rc.getReminder.reminderTime>
                                <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                                <cfset rc.title = rc.getReminder.title>
                                <cfset rc.category = rc.getReminder.category>
                                <cfset rc.forever = rc.getReminder.forever>
                                <cfset rc.reminderActualTime = ''>

                                <cfset createSchedule = variables.userObject.createSchedule(rc)>

                                <cfquery name="rc.createReminder">
                                    INSERT INTO tbl_reminders
                                    (
                                        reminderId,
                                        reminderType,
                                        reminderKey,
                                        title,
                                        category,
                                        interval,
                                        remindertime,
                                        rdate,
                                        rtime,
                                        intervalPeriod,
                                        isLive,
                                        eventDate,
                                        eventTime,
                                        forever
                                    )
                                    VALUES
                                    (
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                        <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                        <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                        <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                        <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                                    )
                                </cfquery>

                            </cfloop>

                        </cfif>
                        
                    </cfif>

                    <cfif structKeyExists(session, 'businessId')>
                        
                        <cfquery name="rc.getReminder">
                            UPDATE tbl_businesses
                            SET ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
                            WHERE businessId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.businessId#">
                        </cfquery>
                            
                        <cfset session.businessId = ''>

                    </cfif>
                    
                    <cfif len(RC.rid)>
                        
                        <!--- get the reminder --->
                        <cfquery name="rc.getReminder">
                            SELECT * 
                            FROM tbl_reminders
                            WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
                        </cfquery>

                        <cfset rc.rdate = rc.getReminder.rdate>
                        <cfset rc.rtime = rc.getReminder.rtime>
                        <cfset rc.forever = rc.getReminder.forever>
                        <cfset rc.interval = rc.getReminder.interval>
                        <cfset rc.reminderId = createUUID()>
                        <cfset rc.reminderTime = rc.getReminder.reminderTime>
                        <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                        <cfset rc.title = rc.getReminder.title>
                        <cfset rc.category = rc.getReminder.category>

                        <cfset createSchedule = variables.userObject.createSchedule(rc)>

                        <cfquery name="rc.createReminder">
                            INSERT INTO tbl_reminders
                            (
                                reminderId,
                                reminderType,
                                reminderKey,
                                title,
                                category,
                                interval,
                                remindertime,
                                rdate,
                                rtime,
                                intervalPeriod,
                                eventDate,
                                eventTime,
                                forever
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                            )
                        </cfquery>

                    </cfif>

                    <!--- Now send the registration email --->
                    <cfsavecontent variable="emailContent"><cfoutput>
                        Thank you for joining Must Remember That!<br><br>
                        Please click the following link to activate your account. <a href="http://www.mustrememberthat.com/index.cfm?action=main.activate&u=#rc.userId#&amp;sec=#rc.sec#">Click To Activate</a><br><br>
                        If you have a problem with the link above please copy the following link into your browser http://www.mustrememberthat.com/index.cfm?action=main.activate&u=#rc.userId#&amp;sec=#rc.sec#<br><br>
                        NEVER FORGET AGAIN
                    </cfoutput></cfsavecontent>
                    
                    <cfset application.emailCFC.sendEmail(rc.emailAddress, 'Activate your Must Remember That account',emailContent)>
                
                    <cfset arrayAppend(rc.successMessage, "You will shortly receive an activation email, please click the link within the email to activate your account.<br><br>If you don't receive it in the next 10 minutes, check other places it might be, like your junk, spam, social, or other folders.<br>")>

                    <!--- reset session variables --->
                    <cfset session.auth.isLoggedIn = false>
                    <cfset session.auth.fullname = "Guest">
                    <cfset structdelete( session.auth, "user" )>
                    <cfset structdelete( session.auth, "EMAILADDRESS" )>
                    <cfset structdelete( session.auth, "FIRSTNAME" )>
                    <cfset structdelete( session.auth, "FULLNAME" )>
                    <cfset structdelete( session.auth, "LASTNAME" )>
                    <cfset structdelete( session.auth, "USERID" )>

                    <!--- <cfif len(RC.slug)>
                        <!--- <cflocation addtoken="false" url="/#rc.slug#/"> --->
                        <cfset variables.fw.redirect( "users.reminders" )>
                    <cfelse>
                        <cfset variables.fw.redirect( "users.reminders" )>
                    </cfif> --->
                
                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

        <cfset rc.MyDateTime=Now()> 
        <cfset rc.Date13 = '#DateFormat(DateAdd('yyyy', -13, rc.MyDateTime),'yyyy')#'>
        <cfset rc.Date30 = '#DateFormat(DateAdd('yyyy', -30, rc.MyDateTime),'yyyy')#'>
        <cfset rc.Date100 = '#DateFormat(DateAdd('yyyy', -100, rc.MyDateTime),'yyyy')#'>
        
    </cffunction>

    <cffunction name="logout" output="true">  
        <cfargument name="rc">
        <!--- reset session variables --->
        <cfset session.auth.isLoggedIn = false>
        <cfset session.auth.fullname = "Guest">
        <cfset structdelete( session.auth, "user" )>
        <cfset structdelete( session.auth, "EMAILADDRESS" )>
        <cfset structdelete( session.auth, "FIRSTNAME" )>
        <cfset structdelete( session.auth, "FULLNAME" )>
        <cfset structdelete( session.auth, "LASTNAME" )>
        <cfset structdelete( session.auth, "USERID" )>

        <cftry>
            <cfcookie name="MRTuser" expires="NOW">
            <cfcookie name="MRTid" expires="NOW">

            <cfcatch type="any"></cfcatch>
        </cftry>
        
        <cfset rc.message = ["You have safely logged out"]>
        <cfset variables.fw.redirect( "main", "message" )>
    </cffunction>

    <cffunction name="passwordreset" output="true"> 
        <cfargument name="rc">
    </cffunction>

    <cffunction name="newPassword">
        <cfargument name="rc">
        
        <!--- make sure a valid UUID was passed to this page --->
        <cfif ( NOT structKeyExists( rc, "password" )
                OR NOT isValid('UUID', rc.token)) >
            <cfset variables.fw.redirect( "main" )>
        </cfif>
        
        <!--- get the users details from the DB --->
        <cfquery name="rc.getuser">
            SELECT usr.*
            FROM tbl_users usr
            WHERE usr.passwordResetToken = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.token#">
        </cfquery>
        
        <!--- validate process --->
        <cfif NOT rc.getuser.recordcount>
            <cfset rc.message = ["You're reset password attempt has failed. Please try again"]>
            <cfset variables.fw.redirect( "main.passwordReset", "token,message")>
        </cfif>
        
        <!--- make sure the reset is still in date --->
        <cfif NOT dateDiff('s',now(),rc.getuser.passwordResetDate) GTE 1>
            <cfset rc.message = ["You had 24 hours to complete this task. This time has passed. Please send a new password reset request."]>
            <cfset variables.fw.redirect( "main.passwordReset", "token,message")>
        </cfif>

        <!--- make sure the token matches --->
        <cfif rc.getuser.passwordResetToken NEQ rc.token>
            <cfset rc.message = ["This link is now invalid. You may have already updated your password previousy. Please <a href='' style='margin-top:4px;'' class='text-right' data-toggle='modal' data-target='##passwordModal'>Click Here</a> to request a new one."]>
            <cfset variables.fw.redirect( "main.passwordReset", "token,message")>
        </cfif>
        
        <!--- Looks like we passed the validation. Create a new password --->
        <cftry>
            
            <cfset rc.newPassword = Hash(rc.password & rc.getuser.passwordSalt, 'SHA-512') />
        
            <!--- save to the database --->
            <cfquery name="rc.saveUser">
                UPDATE tbl_users
                SET password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.newPassword#">,
                passwordResetToken = <cfqueryparam cfsqltype="cf_sql_varchar" value="">
                WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.getuser.userId#">
            </cfquery>
            
            <cfcatch type="any">
                <cfdump var="#cfcatch#"><cfabort>
                <cfset rc.message = ["A error occurred whislt updating your password. Please try again"]>
                <cfset variables.fw.redirect( "main.passwordReset", "token,message")>
            </cfcatch>
        </cftry>
        
        <!--- redirect the user to the login page with a nice message --->
        <cfset rc.successMessage = ["Your password has been reset. Please login"]>
        <cfset variables.fw.redirect( "main", "token,successMessage")>
        
     </cffunction>

     <cffunction name="contact">
        
        <cfargument name="rc">
        
        <cfif structKeyExists(RC, 'submit') AND rc.submit EQ 'submit'>

            <cfset rc.errors = []>
                        
            <cfif not len(trim(rc.name))>
                <cfset arrayAppend(rc.errors, "Please supply a name")>
            </cfif>

            <cfif not isValid('email', rc.email)>
                <cfset arrayAppend(rc.errors, "Please supply a valid email address")>
            </cfif>

            <cfif not len(trim(rc.message))>
                <cfset arrayAppend(rc.errors, "Please supply a message")>
            </cfif>

            <cfif NOT arrayLen(rc.errors)>
                
                <cfsavecontent variable="emailContent"><cfoutput>
                <p>Email From #rc.email#</p>

                <p>#rc.message#</p>
            </cfoutput></cfsavecontent>

            <cfsavecontent variable="UserContent">
            <p>Thank you for contacting Must Remember That</p>
            <p>We will be in touch shortly</p>
            <p>Must Remember That Team</p>
            </cfsavecontent>

            <cfset application.emailCFC.sendEmail('simon@mustrememberthat.com, steve@mustrememberthat.com', 'Contact Us Enquiery from #rc.name#',emailContent)>
            <cfset application.emailCFC.sendEmail(rc.email, 'Must Remember That Confirmation',UserContent)>

                <cfset rc.successMessage = ["Thank you for contacting Must Remember That<br><br>Must Remember That Team"]>
                <cfset variables.fw.redirect( "main.contact", "successMessage")>
            </cfif>

        </cfif>
                    
    </cffunction>

    <cffunction name="sendReminders">
        <cfabort>
        <cfquery name="rc.getReminder">
            SELECT * 
            FROM tbl_reminders
            WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.uid#">
            AND isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
        </cfquery>

        <cfquery name="rc.getUser">
            SELECT * 
            FROM tbl_users
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.uid#">
        </cfquery>
        
        <cfif rc.getReminder.recordcount AND rc.getUser.recordcount>
            
            <!--- send the email --->
            <cfmail from="MUST REMEMBER THAT <noreply@mustrememberthat.com>" to="#rc.getUser.emailAddress#,simon@mustrememberthat.com,duncan@tyrell.mobi" subject="#rc.getReminder.title# - #DateFormat(rc.getReminder.rdate, "ddd d mmm yyyy")#" username="noreply@mustrememberthat.com" password="Popesabbots1!" priority="1"  type="html" bcc="james@maisyweb.co.uk">
                <table width="90%">
                    <tr>
                        <td style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                            <strong>REMINDER:</strong><br>
                            <strong>#rc.getReminder.title# - #DateFormat(rc.getReminder.rdate, "ddd d mmm yyyy")#</strong><br>
                            <br>
                            <a href="http://www.mustrememberthat.com/">Add New Reminder</a><br>
                            <br>
                            <a href="http://www.mustrememberthat.com/">Update Your Account</a>
                        </td>
                    </tr>
                </table>
            </cfmail>

            <cfhttp url="https://api.parse.com/1/classes/Messages" method="post" resolveurl="true" result="httpResp1" timeout="60">
                <cfhttpparam type="header" name="X-Parse-Application-Id" value="4N3zuvICV8jLeaAQwNgn0dRVd5SeHjyJ98R8MiMx" />
                <cfhttpparam type="header" name="X-Parse-REST-API-Key" value="EuXqEOVMXCCrBid3QCRkUIhAmOf4Zyt9hxmINEPN" />
                <cfhttpparam type="header" name="Content-Type" value="application/json" />
                <cfhttpparam type="body" value='{"userId":"#rc.getUser.remoteId#", "Date":"#DateFormat(rc.getReminder.rdate, "dd mmm yyyy")#", "time":"#TimeFormat(rc.getReminder.rtime, "HH:mm")#", "Message": "#rc.getReminder.title#"}'/ >
                <!---<cfhttpparam type="body" value="#serializeJSON(PUSHMESSAGE)#">--->
            </cfhttp>

            <cfif #httpResp1.statusCode# CONTAINS '201 Created'>

                <!--- Push the message --->
                <cfhttp url="https://api.parse.com/1/push" method="post" resolveurl="true" result="httpResp" timeout="60">
                    <cfhttpparam type="header" name="X-Parse-Application-Id" value="4N3zuvICV8jLeaAQwNgn0dRVd5SeHjyJ98R8MiMx" />
                    <cfhttpparam type="header" name="X-Parse-REST-API-Key" value="EuXqEOVMXCCrBid3QCRkUIhAmOf4Zyt9hxmINEPN" />
                    <cfhttpparam type="header" name="Content-Type" value="application/json" />
                    <cfhttpparam type="body" value='{"where": {"userId": "#rc.getUser.remoteId#"},"data": {"date":"#DateFormat(rc.getReminder.rdate, "dd mmm yyyy")#", "alert": "#rc.getReminder.title#", "content-available": "1", "sound": "default", "badge": "increment"}}'/ >
                    <!--- <cfhttpparam type="body" value="#serializeJSON(PUSHMESSAGE)#"> --->
                </cfhttp>

            <cfelse>
                
                <cfmail from="MUST REMEMBER THAT <noreply@mustrememberthat.com>" to="simon@mustrememberthat.com" subject="Notification issue" username="noreply@mustrememberthat.com" password="Popesabbots1!" priority="1"  type="html" bcc="james@maisyweb.co.uk">
                    <table width="90%">
                        <tr>
                            <td style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                            User - #rc.uid#<br><br>
                            Date - #DateFormat(rc.getReminder.rdate, "dd mmm yyyy")#<br><br>
                            Alert - #rc.getReminder.title#
                            </td>
                        </tr>
                    </table>
                </cfmail>
            
            </cfif>

            <!--- If a reminder is re-occurring it should not go into old reminders. On trigger of reminder it should be reset to the next day, month or Year. --->
            <cfif rc.getReminder.interval NEQ 'once'>
                
                <cfif rc.getReminder.interval EQ 'yearly'>
                    
                    <cfset rc.newRdate = dateAdd('yyyy', 1, rc.getReminder.rdate)>
                    <cfset rc.rdate = rc.getReminder.rdate>
                    <cfset rc.rtime = rc.getReminder.rtime>
                    <cfset rc.interval = rc.getReminder.interval>
                    <cfset rc.reminderId = createUUID()>
                    <cfset rc.reminderTime = rc.getReminder.reminderTime>
                    <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                    <cfset rc.title = rc.getReminder.title>
                    <cfset rc.category = rc.getReminder.category>
                    <cfset rc.reminderActualTime = ''>
                    <!--- create the schedule --->
                    <!--- <cfset createSchedule = variables.userObject.createSchedule(rc)> --->

                </cfif>
                <cfif rc.getReminder.interval EQ 'weekly'>
                    <cfset rc.newRdate = dateAdd('ww', 1, rc.getReminder.rdate)>
                </cfif>
                <cfif rc.getReminder.interval EQ 'monthly'>
                    <cfset rc.newRdate = dateAdd('m', 1, rc.getReminder.rdate)>
                </cfif>
                <cfif rc.getReminder.interval EQ 'daily'>
                    <cfset rc.newRdate = dateAdd('d', 1, rc.getReminder.rdate)>
                </cfif>

                <cfquery name="rc.updateReminder">
                    UPDATE tbl_reminders
                    SET rdate = <cfqueryparam cfsqltype="cf_sql_date" value="#rc.newRdate#">
                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
                    AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.uid#">
                </cfquery>

            </cfif>

        </cfif>
        <cfdump var="#rc.getUser.emailAddress#"><cfdump var="#rc#"><cfabort>

    </cffunction>


    <cffunction name="singleReminder">

        <cfparam name="rc.reminderActualTime" default="">
        <cfif structKeyExists(SESSION, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
        <cfelse>
            <cflocation addtoken="false" url="/index.cfm?action=main.registration&msg=login&rid=#url.reminderId#">
        </cfif>

        <!--- get the current reminder --->
        <cfquery name="rc.getReminder">
            SELECT *
            FROM tbl_reminders
            WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">
            ORDER BY rdate
        </cfquery>

        <cfset rc.title = rc.getReminder.title>
        <cfset rc.category = rc.getReminder.category>
        <cfset rc.interval = rc.getReminder.interval>
        <cfset rc.reminderTime = rc.getReminder.reminderTime>
        <cfset rc.rdate = dateFormat(rc.getReminder.rdate, 'yyyy-mm-dd')>
        <cfset rc.rtime = timeFormat(rc.getReminder.rtime, 'HH:MM')>
        <cfset rc.eventdate = dateFormat(rc.getReminder.eventdate, 'yyyy-mm-dd')>
        <cfset rc.eventtime = timeFormat(rc.getReminder.eventtime, 'HH:MM')>
        <cfset rc.comments = rc.getReminder.comments>
        <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>


        <cfquery name="rc.getMainCategory">
            SELECT *
            FROM tbl_categories
            WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.category#">
        </cfquery>

        <cfset rc.maincategory = rc.getMainCategory.maincategoryId>
        
        <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>

    </cffunction>

    <cffunction name="activate">

        <cfargument name="rc"> 

        <cfquery name="rc.getuser">
            SELECT usr.*
            FROM tbl_users usr
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.u#">
            AND userVerify = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.sec#">
        </cfquery>

        <cfif rc.getuser.recordcount>
            
            <cfquery name="rc.setUserAccept">
                UPDATE tbl_users
                SET userAccept = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.getuser.userId#">
            </cfquery>

            <cfset session.auth.isLoggedIn = true>
            <cfset session.auth.firstName = rc.getuser.firstName> 
            <cfset session.auth.lastname = rc.getuser.lastname> 
            <cfset session.auth.emailAddress = rc.getuser.emailAddress>
            <cfset session.auth.userId = rc.getuser.userId>

            <!--- Now send the registration email --->
            <cfsavecontent variable="emailContent"><cfoutput>
                Download our free APP now to receive your push notification alerts, so you Never Forget Again.<br><br>
                <a style="padding-right:10px" href="https://itunes.apple.com/us/app/must-remember-that/id1121692955?ls=1&mt=8"><img src="https://www.mustrememberthat.com/assets/app-download.jpg"></a>
                <a href="https://play.google.com/store/apps/details?id=com.mustrememberthat.app"><img src="https://www.mustrememberthat.com/assets/android.png"></a><br/><br/>
                NEVER FORGET AGAIN
            </cfoutput></cfsavecontent>
            
            <cfset application.emailCFC.sendEmail(rc.getuser.emailAddress, 'Registration Complete - Must Remember That',emailContent)>

            <cfset variables.fw.redirect( "users.reminders" )>
        
        <cfelse>

            <cfset variables.fw.redirect( "main.default" )>

        </cfif>

     </cffunction>
                
    <cffunction name="joinGroup"> 

        <cfargument name="rc"> 

        <!--- add to database --->
        <cfquery name="rc.qGetGroup">
            SELECT *
            FROM tbl_groups
            WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
        </cfquery>

        <cftry>
            <cfquery name="rc.createGroup">
                INSERT INTO tbl_groupUsers
                       (groupId
                       ,userId)
                VALUES
                       (<cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
                       ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">)
             </cfquery>

             <cfcatch type="any"></cfcatch>
                 
        </cftry>
        

    </cffunction>

    <cffunction name="sendAllReminders"> 

        <!--- <cfschedule action = "update"
            task = "MRT-SendAllRemindersLive" 
            operation = "HTTPRequest"
            url = "https://www.mustrememberthat.com/index.cfm?action=main.sendAllReminders"
            startDate = "#DateFormat(now(), "dd/mmm/yyyy")#"
            startTime = "#TimeFormat(now(), "HH:mm:ss")#"
            interval = "60"
            resolveurl="yes"
            requestTimeOut = "60"> --->

        <cfquery name="rc.getAllReminder">
            SELECT      tbl_reminders.reminderId, tbl_reminders.reminderType, tbl_reminders.reminderKey, tbl_reminders.title, tbl_reminders.category, tbl_reminders.rdate, tbl_reminders.rtime, tbl_reminders.interval, 
                        tbl_reminders.remindertime, tbl_reminders.comments, tbl_reminders.parentId, tbl_reminders.dateCreated, tbl_reminders.isLive, tbl_reminders.intervalPeriod, tbl_users.userId, tbl_users.firstname, 
                        tbl_users.lastname, tbl_users.emailAddress, tbl_users.userVerify, tbl_users.userAccept, tbl_users.DOB, tbl_users.createdAt, tbl_users.password, tbl_users.passwordSalt, tbl_users.passwordResetToken, 
                        tbl_users.passwordResetDate, tbl_users.timezone, tbl_users.cookiePass, tbl_users.remoteId, tbl_users.profileImage, tbl_users.gender, tbl_users.imgX, tbl_users.imgY, 
                        tbl_users.imageWidth, tbl_users.imageHeight, tbl_users.homeTown, tbl_users.mobile, tbl_users.country, tbl_users.isLive AS Expr1, tbl_reminders.eventDate, tbl_reminders.eventTime, tbl_reminders.forever
            FROM        tbl_reminders
            INNER JOIN  tbl_users ON tbl_reminders.reminderKey = tbl_users.userId
            WHERE       (rdate = CAST(CURRENT_TIMESTAMP AS DATE)) 
            AND         ({ fn MINUTE(GETDATE()) } = { fn MINUTE(rtime) }) 
            AND         ({ fn HOUR(GETDATE()) } = { fn HOUR(rtime) }) 
            AND         (tbl_reminders.isLive = 1)
            AND         (reminderType = 1)
        </cfquery>
        
        <cfif rc.getAllReminder.recordcount>
            <cfloop query="rc.getAllReminder">
                
                <!--- send the email --->
                <cfmail from="MUST REMEMBER THAT <noreply@mustrememberthat.com>" to="#rc.getAllReminder.emailAddress#,simon@mustrememberthat.com" subject="#rc.getAllReminder.title# - #DateFormat(rc.getAllReminder.eventDate, "ddd d mmm yyyy")#" username="noreply@mustrememberthat.com" password="Popesabbots1!" priority="1"  type="html">
                    <table width="90%">
                        <tr>
                            <td style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                                <strong>REMINDER:</strong><br>
                                <strong>#rc.getAllReminder.title# - #DateFormat(rc.getAllReminder.eventDate, "ddd d mmm yyyy")#</strong><br>
                                <br>
                                <a href="http://www.mustrememberthat.com/">Add New Reminder</a><br>
                                <br>
                                <a href="http://www.mustrememberthat.com/">Update Your Account</a>
                            </td>
                        </tr>
                    </table>
                </cfmail>

                <cfhttp url="https://api.parse.com/1/classes/Messages" method="post" resolveurl="true" result="httpResp1" timeout="60">
                    <cfhttpparam type="header" name="X-Parse-Application-Id" value="4N3zuvICV8jLeaAQwNgn0dRVd5SeHjyJ98R8MiMx" />
                    <cfhttpparam type="header" name="X-Parse-REST-API-Key" value="EuXqEOVMXCCrBid3QCRkUIhAmOf4Zyt9hxmINEPN" />
                    <cfhttpparam type="header" name="Content-Type" value="application/json" />
                    <cfhttpparam type="body" value='{"userId":"#rc.getAllReminder.remoteId#", "Date":"#DateFormat(rc.getAllReminder.eventDate, "dd mmm yyyy")#", "time":"#TimeFormat(rc.getAllReminder.eventTime, "HH:mm")#", "Message": "#rc.getAllReminder.title#"}'/ >
                    <!---<cfhttpparam type="body" value="#serializeJSON(PUSHMESSAGE)#">--->
                </cfhttp>
		
		<!--- NEW: Push the message using AWS. TODO make this call locally rather than via HTTP --->
                <cfhttp url="https://mustrememberthat.com/authapi/index-new.php/notifyUser" method="post" resolveurl="true" result="httpResp" timeout="60">
                    <cfhttpparam type="header" name="Content-Type" value="application/x-www-form-urlencoded" />
                    <cfhttpparam type="Formfield" name="userId" value="#rc.getAllReminder.userId#"/ >
                    <cfhttpparam type="Formfield" name="reminderId" value="#rc.getAllReminder.reminderId#"/ >
                    <cfhttpparam type="Formfield" name="subject" value="#rc.getAllReminder.title#"/ >
                    <cfhttpparam type="Formfield" name="message" value="#rc.getAllReminder.comments#"/ >
                    <cfhttpparam type="Formfield" name="date" value="#DateFormat(rc.getAllReminder.eventDate, "dd mmm yyyy")#"/ >
                </cfhttp>	
		
		<cfmail from="MUST REMEMBER THAT <noreply@mustrememberthat.com>" to="simon@mustrememberthat.com" subject="Notification Result" username="noreply@mustrememberthat.com" password="Popesabbots1!" priority="1"  type="html" bcc="duncan@tyrell.digital ">
                    <cfdump var="#httpResp#">
                </cfmail>
		
                <cfif #httpResp1.statusCode# CONTAINS '201 Created'>

                    <!--- Push the message --->
                    <cfhttp url="https://api.parse.com/1/push" method="post" resolveurl="true" result="httpResp" timeout="60">
                        <cfhttpparam type="header" name="X-Parse-Application-Id" value="4N3zuvICV8jLeaAQwNgn0dRVd5SeHjyJ98R8MiMx" />
                        <cfhttpparam type="header" name="X-Parse-REST-API-Key" value="EuXqEOVMXCCrBid3QCRkUIhAmOf4Zyt9hxmINEPN" />
                        <cfhttpparam type="header" name="Content-Type" value="application/json" />
                        <cfhttpparam type="body" value='{"where": {"userId": "#rc.getAllReminder.remoteId#"},"data": {"date":"#DateFormat(rc.getAllReminder.eventDate, "dd mmm yyyy")#", "alert": "#rc.getAllReminder.title#", "content-available": "1", "sound": "default", "badge": "increment"}}'/ >
                        <!--- <cfhttpparam type="body" value="#serializeJSON(PUSHMESSAGE)#"> --->
                    </cfhttp>

                <cfelse>
                    
                    <cfmail from="MUST REMEMBER THAT <noreply@mustrememberthat.com>" to="simon@mustrememberthat.com" subject="Notification issue" username="noreply@mustrememberthat.com" password="Popesabbots1!" priority="1"  type="html" bcc="james@maisyweb.co.uk">
                        <table width="90%">
                            <tr>
                                <td style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px;">
                                User - #rc.getAllReminder.userId#<br><br>
                                Date - #DateFormat(rc.getAllReminder.eventDate, "dd mmm yyyy")#<br><br>
                                Alert - #rc.getAllReminder.title#
                                </td>
                            </tr>
                        </table>
                    </cfmail>
                
                </cfif>

                <!--- If a reminder is re-occurring it should not go into old reminders. On trigger of reminder it should be reset to the next day, month or Year. --->
                <cfif rc.getAllReminder.interval NEQ 'once'>
                    
                    <cfif rc.getAllReminder.interval EQ 'yearly'>
                        <cfset rc.newRdate = dateAdd('yyyy', 1, rc.getAllReminder.rdate)>
                    </cfif>
                    <cfif rc.getAllReminder.interval EQ 'weekly'>
                        <cfset rc.newRdate = dateAdd('ww', 1, rc.getAllReminder.rdate)>
                    </cfif>
                    <cfif rc.getAllReminder.interval EQ 'monthly'>
                        <cfset rc.newRdate = dateAdd('m', 1, rc.getAllReminder.rdate)>
                    </cfif>
                    <cfif rc.getAllReminder.interval EQ 'daily'>
                        <cfset rc.newRdate = dateAdd('d', 1, rc.getAllReminder.rdate)>
                    </cfif>
                   
                    <cfif (rc.getAllReminder.forever EQ 0) AND (DateCompare(rc.newRdate, rc.getAllReminder.rdate, "d"))>
                        <cfset test = 1>
                    <cfelse> 
                        <cfquery name="rc.updateReminder">
                            UPDATE tbl_reminders
                            SET rdate = <cfqueryparam cfsqltype="cf_sql_date" value="#rc.newRdate#">
                            WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.getAllReminder.reminderId#">
                            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.getAllReminder.userId#">
                        </cfquery>   
                    </cfif>

                 </cfif>

            </cfloop> 

        </cfif>

        <cfdump var="#rc.getAllReminder#"><cfabort>

    </cffunction>

</cfcomponent>

