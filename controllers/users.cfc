<cfcomponent output="false">
	
    <cffunction name="init">
        <cfargument name="fw" type="any" />
        <cfset variables.fw = arguments.fw />
	</cffunction>
    
    <cffunction name="reminders">
        
        <cfargument name="rc"> 
        
        <cfparam name="rc.reminderId" default="">
        <cfparam name="rc.reminderType" default="1">
        <cfparam name="rc.reminderKey" default="">
        <cfparam name="rc.title" default="">
        <cfparam name="rc.category" default="">
        <cfparam name="rc.interval" default="">
        <cfparam name="rc.reminderTime" default="">
        <cfparam name="rc.rdate" default="">
        <cfparam name="rc.rtime" default="">
        <cfparam name="rc.reminderActualTime" default="">
        <cfparam name="rc.comments" default="">
        <cfparam name="rc.pageNumber" default="1">
        <cfparam name="rc.maincategory" default="0">
        <cfparam name="rc.isArchive" default="0">
        <cfparam name="rc.intervalPeriod" default="1">
        <cfparam name="rc.location" default="">
        <cfparam name="rc.latitude" default="">
        <cfparam name="rc.longitude" default="">

        <!--- set the reminder layout --->
        <cfset rc.layoutName = 'reminder'>
        <cfset rc.bodyID = 'myReminders'>

        <cfif NOT isNumeric(rc.maincategory)>
            <cfset rc.maincategory = 0>
        </cfif>
        
        <!--- delete a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'deleteReminder'>

            <cfquery name="rc.createReminder">
                UPDATE tbl_reminders
                SET isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">
                AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            </cfquery>
            
            <cfset rc.successMessage = []>
            <cfset arrayAppend(rc.successMessage, "Reminder has been removed")>

        </cfif>

        <!--- if we need to populate a default reminder --->
        <cfif structKeyExists(URL, 'defaultReminder')>

            <cfquery name="rc.getDefaultReminder">
                SELECT *
                FROM tbl_categories
                WHERE urlSafe = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.defaultReminder#">
            </cfquery>

            <cfset rc.title = rc.getDefaultReminder.defaultReminderTitle>
            <cfset rc.maincategory = rc.getDefaultReminder.mainCategoryId>
            <cfset rc.category = rc.getDefaultReminder.id>
            <cfset rc.remindertime = 'd'>

        </cfif>
            
        <!--- add a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'newReminder'>
            
            <!--- check for errors --->
            <cfset rc.bypass = ''> 
            <cfset rc.errors = validateReminder(rc)>
            
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.reminderId = createUUID()>
                    
                    <cfif rc.interval EQ 'daily2'>
                        <cfset rc.interval = 'daily'>
                        <cfset rc.forever = 1>
                    <cfelse>
                        <cfset rc.forever = 0>
                    </cfif>

                    <cftransaction> 
                        
                        <!--- create the schedule --->
                        <cfset createSchedule = createSchedule(rc)>

                        <!--- add to database --->
                        <cfquery name="rc.createReminder">
                            INSERT INTO tbl_reminders
                            (
                                reminderId,
                                reminderType,
                                reminderKey,
                                title,
                                comments,
                                category,
                                interval,
                                remindertime,
                                rdate,
                                rtime,
                                intervalPeriod,
                                eventDate,
                                eventTime,
                                forever,
                                location,
                                latitude,
                                longitude
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.reminderType#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.comments#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.location#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.latitude#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.longitude#">
                            )
                        </cfquery>

                        <cfset rc.title = "">
                        <cfset rc.comments = "">
                        <cfset rc.category = "">
                        <cfset rc.interval = "">
                        <cfset rc.remindertime = "">
                        <cfset rc.rdate = "">
                        <cfset rc.rtime = "">

                    </cftransaction> 
                    
                    <cfset rc.successMessage = []>
                    <cfset arrayAppend(rc.successMessage, "A new reminder has been added.")>
                
                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

        <cfif structKeyExists(RC, 'rid') AND NOT structKeyExists(RC, 'bypass')>
            <cfquery name="rc.getReminder">
                SELECT *
                FROM tbl_reminders
                WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
            </cfquery>
            
            <cfset rc.title = rc.getReminder.title>
            <cfset rc.comments = rc.getReminder.comments>
            <cfset rc.category = rc.getReminder.category>
            <cfset rc.interval = rc.getReminder.interval>
            <cfset rc.remindertime = rc.getReminder.remindertime>
            <cfset rc.rdate = rc.getReminder.rdate>
            <cfset rc.rtime = timeFormat(rc.getReminder.rtime, 'HH:mm')>

            <!--- get the category --->
            <cfquery name="rc.getMainCat">
                SELECT *
                FROM tbl_categories
                WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.getReminder.category#">
            </cfquery>

            <cfset rc.mainCategory = rc.getMainCat.mainCategoryId>
            
            <cfset rc.infoMessage = []>
            <cfset arrayAppend(rc.infoMessage, "Please review the reminder, and then add it to you personal list")>

        </cfif>

        <!--- get all the reminders for this user --->
        <cfquery name="rc.getReminders">
            DECLARE
            @PageSize INT = 10,
            @PageNumber  INT = #rc.pageNumber#;    

            SELECT *, overall_count = COUNT(*) OVER()
            FROM tbl_reminders
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            <cfif rc.isArchive>
                AND CONVERT(DATETIME, CONVERT(CHAR(8), eventDate, 112)  + ' ' + CONVERT(CHAR(8), eventTime, 108)) <= getdate()  
            <cfelse>
                AND CONVERT(DATETIME, CONVERT(CHAR(8), eventDate, 112)  + ' ' + CONVERT(CHAR(8), eventTime, 108)) >= getdate()   
            </cfif>
            ORDER BY eventdate  <cfif rc.isArchive>desc</cfif>, eventtime <cfif rc.isArchive>desc</cfif>
            OFFSET (@PageNumber-1)*@PageSize ROWS
            FETCH NEXT @PageSize ROWS ONLY
        </cfquery>

        <cfif rc.isArchive>
            <cfset rc.isArchiveURL = '&isArchive=1'>
        <cfelse>
            <cfset rc.isArchiveURL = ''>
        </cfif>

        <!--- get the pagination numbers --->
        <cfif rc.getReminders.recordcount>
            <cfset rc.totalPages = ceiling(rc.getReminders.overall_count / 10)>
            <cfif rc.pageNumber GTE 3>
                <cfset rc.startPage = rc.pageNumber - 2>
            <cfelse>
                <cfset rc.startPage = 1>   
            </cfif>

            <cfif rc.pageNumber LTE 5>
                <cfif rc.totalPages GT rc.pageNumber>
                    <cfset rc.endpage = rc.totalPages>
                <cfelse>
                    <cfset rc.endpage = rc.pageNumber>
                </cfif>
                 
            <cfelse>
                <cfset rc.endpage = rc.totalPages>
            </cfif>
        <cfelse>
            <cfset rc.totalPages = 0>
            <cfset rc.startPage = 1>
            <cfset rc.endpage = 1>
        </cfif>

        <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>

        <cfquery name="rc.totalReminders">
            SELECT count(reminderId) AS Counter
            FROM tbl_reminders
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

        <cfquery name="rc.getPriorityCats">
            SELECT *
            FROM tbl_categories
            WHERE isPriority = 1
            order by newid()
        </cfquery>

        <cfquery name="rc.getSubscription">
            SELECT *
            FROM tbl_subscriptions
            INNER JOIN tbl_businesses ON tbl_businesses.businessId = tbl_subscriptions.businessId
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

        <cfquery name="rc.getGroups">
           SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            WHERE tbl_groups.ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            UNION
            SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            INNER JOIN tbl_groupUsers ON tbl_groupUsers.groupId = tbl_groups.groupId
            WHERE tbl_groupUsers.userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>
        
    </cffunction>

    <cffunction name="businessreminder">
        
        <cfargument name="rc"> 
        
        <cfparam name="rc.reminderId" default="">
        <cfparam name="rc.reminderType" default="1">
        <cfparam name="rc.reminderKey" default="">
        <cfparam name="rc.title" default="">
        <cfparam name="rc.category" default="">
        <cfparam name="rc.interval" default="">
        <cfparam name="rc.reminderTime" default="">
        <cfparam name="rc.rdate" default="">
        <cfparam name="rc.rtime" default="">
        <cfparam name="rc.comments" default="">
        <cfparam name="rc.pageNumber" default="1">
        <cfparam name="rc.maincategory" default="0">
        <cfparam name="rc.isArchive" default="0">
        <cfparam name="rc.intervalPeriod" default="1">
        <cfparam name="rc.reminderActualTime" default="">
        <cfparam name="rc.location" default="">
        <cfparam name="rc.latitude" default="">
        <cfparam name="rc.longitude" default="">

        <!--- add a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'newReminder'>
            
            <!--- check for errors --->
            <cfset rc.bypass = ''> 
            <cfset rc.errors = validateReminder(rc)>
            
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.reminderId = createUUID()>
                    
                    <cfif rc.interval EQ 'daily2'>
                        <cfset rc.interval = 'daily'>
                        <cfset rc.forever = 1>
                    <cfelse>
                        <cfset rc.forever = 0>
                    </cfif>

                    <cftransaction> 
                        
                        <cfset createSchedule = createSchedule(rc)>

                        <!--- add to database --->
                        <cfquery name="rc.createReminder">
                            INSERT INTO tbl_reminders
                            (
                                reminderId,
                                reminderType,
                                reminderKey,
                                title,
                                comments,
                                category,
                                interval,
                                remindertime,
                                rdate,
                                rtime,
                                intervalPeriod,
                                eventDate,
                                eventTime,
                                forever,
                                location,
                                latitude,
                                longitude
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.reminderType#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.comments#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.location#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.latitude#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.longitude#">
                            )
                        </cfquery>

                        <!--- create the schedule --->
                        <cfset rc.title = "">
                        <cfset rc.comments = "">
                        <cfset rc.category = "">
                        <cfset rc.interval = "">
                        <cfset rc.remindertime = "">
                        <cfset rc.rdate = "">
                        <cfset rc.rtime = "">
                        <cfset rc.location = "">
                        <cfset rc.latitude = "">
                        <cfset rc.longitude = "">

                    </cftransaction> 
                    
                    <cfset rc.successMessage = []>
                    <cfset arrayAppend(rc.successMessage, "A new reminder has been added.")>
                
                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

        <cfif structKeyExists(RC, 'rid') AND NOT structKeyExists(RC, 'bypass')>
            <cfquery name="rc.getReminder">
                SELECT *
                FROM tbl_reminders
                WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
            </cfquery>
            
            <cfset rc.title = rc.getReminder.title>
            <cfset rc.comments = rc.getReminder.comments>
            <cfset rc.category = rc.getReminder.category>
            <cfset rc.interval = rc.getReminder.interval>
            <cfset rc.remindertime = rc.getReminder.remindertime>
            <cfset rc.rdate = rc.getReminder.rdate>
            <cfset rc.rtime = timeFormat(rc.getReminder.rtime, 'HH:mm')>

            <!--- get the category --->
            <cfquery name="rc.getMainCat">
                SELECT *
                FROM tbl_categories
                WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.getReminder.category#">
            </cfquery>

            <cfset rc.mainCategory = rc.getMainCat.mainCategoryId>
            
            <cfset rc.infoMessage = []>
            <cfset arrayAppend(rc.infoMessage, "Please review the reminder, and then add it to you personal list")>

        </cfif>

        <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>
            
    </cffunction>

    <cffunction name="groupsreminder">
        
        <cfargument name="rc"> 
        
        <cfparam name="rc.reminderId" default="">
        <cfparam name="rc.reminderType" default="1">
        <cfparam name="rc.reminderKey" default="">
        <cfparam name="rc.title" default="">
        <cfparam name="rc.category" default="">
        <cfparam name="rc.interval" default="">
        <cfparam name="rc.reminderTime" default="">
        <cfparam name="rc.rdate" default="">
        <cfparam name="rc.rtime" default="">
        <cfparam name="rc.comments" default="">
        <cfparam name="rc.pageNumber" default="1">
        <cfparam name="rc.maincategory" default="0">
        <cfparam name="rc.isArchive" default="0">
        <cfparam name="rc.intervalPeriod" default="1">
        <cfparam name="rc.reminderActualTime" default="">
        <cfparam name="rc.location" default="">
        <cfparam name="rc.latitude" default="">
        <cfparam name="rc.longitude" default="">

        <!--- add a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'newReminder'>
            
            <!--- check for errors --->
            <cfset rc.bypass = ''> 
            <cfset rc.errors = validateReminder(rc)>
            
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.reminderId = createUUID()>
                    
                    <cfif rc.interval EQ 'daily2'>
                        <cfset rc.interval = 'daily'>
                        <cfset rc.forever = 1>
                    <cfelse>
                        <cfset rc.forever = 0>
                    </cfif>
                    
                    <cftransaction> 
                        
                        <cfset createSchedule = createSchedule(rc)>

                        <!--- add to database --->
                        <cfquery name="rc.createReminder">
                            INSERT INTO tbl_reminders
                            (
                                reminderId,
                                reminderType,
                                reminderKey,
                                title,
                                comments,
                                category,
                                interval,
                                remindertime,
                                rdate,
                                rtime,
                                intervalPeriod,
                                eventDate,
                                eventTime,
                                forever,
                                location,
                                latitude,
                                longitude
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.reminderType#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.comments#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.location#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.latitude#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.longitude#">
                            )
                        </cfquery>

                        <cfset rc.title = "">
                        <cfset rc.comments = "">
                        <cfset rc.category = "">
                        <cfset rc.interval = "">
                        <cfset rc.remindertime = "">
                        <cfset rc.rdate = "">
                        <cfset rc.rtime = "">
                        <cfset rc.location = "">
                        <cfset rc.latitude = "">
                        <cfset rc.longitude = "">

                    </cftransaction> 
                    
                    <cfset rc.successMessage = []>
                    <cfset arrayAppend(rc.successMessage, "A new reminder has been added.")>
                
                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

        <cfif structKeyExists(RC, 'rid') AND NOT structKeyExists(RC, 'bypass')>
            <cfquery name="rc.getReminder">
                SELECT *
                FROM tbl_reminders
                WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.rid#">
            </cfquery>
            
            <cfset rc.title = rc.getReminder.title>
            <cfset rc.comments = rc.getReminder.comments>
            <cfset rc.category = rc.getReminder.category>
            <cfset rc.interval = rc.getReminder.interval>
            <cfset rc.remindertime = rc.getReminder.remindertime>
            <cfset rc.rdate = rc.getReminder.rdate>
            <cfset rc.rtime = timeFormat(rc.getReminder.rtime, 'HH:mm')>

            <!--- get the category --->
            <cfquery name="rc.getMainCat">
                SELECT *
                FROM tbl_categories
                WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.getReminder.category#">
            </cfquery>

            <cfset rc.mainCategory = rc.getMainCat.mainCategoryId>
            
            <cfset rc.infoMessage = []>
            <cfset arrayAppend(rc.infoMessage, "Please review the reminder, and then add it to you personal list")>

        </cfif>

        <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>
            
    </cffunction>

    <cffunction name="updateReminder">
        
        <cfargument name="rc"> 
        
        <cfparam name="rc.reminderId" default="">
        <cfparam name="rc.reminderType" default="1">
        <cfparam name="rc.reminderKey" default="">
        <cfparam name="rc.title" default="">
        <cfparam name="rc.category" default="">
        <cfparam name="rc.interval" default="">
        <cfparam name="rc.reminderTime" default="">
        <cfparam name="rc.rdate" default="">
        <cfparam name="rc.rtime" default="">
        <cfparam name="rc.eventdate" default="">
        <cfparam name="rc.eventtime" default="">
        <cfparam name="rc.comments" default="">
        <cfparam name="rc.isArchive" default="0">
        <cfparam name="rc.maincategory" default="0">
        <cfparam name="rc.intervalPeriod" default="1">
        <cfparam name="rc.pageNumber" default="1">
        <cfparam name="rc.reminderActualTime" default="">

        <!--- set the reminder layout --->
        <cfset rc.layoutName = 'reminder'>

        <!--- get the current reminder --->
        <cfquery name="rc.getReminder">
            SELECT *
            FROM tbl_reminders
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">
            ORDER BY rdate
        </cfquery>

        <!--- add a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'updateReminder'>
            
            <!--- check for errors --->
            <cfset rc.errors = validateReminder(rc)>
            
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <cftransaction> 
                        
                        <cfset createSchedule = createSchedule(rc)> 

                        <!--- update database --->
                        <cfquery name="rc.createReminder">
                            UPDATE tbl_reminders
                            SET
                            title = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                            category = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                            interval = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                            reminderTime = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                            rdate = <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                            rtime = <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                            eventDate = <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                            eventTime = <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                            comments = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.comments#">,
                            intervalPeriod = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">
                            WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">
                        </cfquery>

                    </cftransaction> 
                    
                    <cfset rc.successMessage = []>
                    <cfset arrayAppend(rc.successMessage, "This reminder has been updated.")>
                
                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        <cfelse>

            <cfset rc.title = rc.getReminder.title>
            <cfset rc.category = rc.getReminder.category>
            <cfset rc.interval = rc.getReminder.interval>
            <cfset rc.reminderTime = rc.getReminder.reminderTime>
            <cfset rc.rdate = dateFormat(rc.getReminder.rdate, 'yyyy-mm-dd')>
            <cfset rc.rtime = timeFormat(rc.getReminder.rtime, 'HH:MM')>
            <cfset rc.eventdate = dateFormat(rc.getReminder.eventdate, 'yyyy-mm-dd')>
            <cfset rc.eventtime = timeFormat(rc.getReminder.eventtime, 'HH:MM')>
            <cfset rc.comments = rc.getReminder.comments>
            <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
            <cfset rc.reminderActualTime = timeFormat(rc.getReminder.rtime, 'HH:MM')>

        </cfif>

        <!--- get all the reminders for this user --->
        <cfquery name="rc.getReminders">
            DECLARE
            @PageSize INT = 10,
            @PageNumber  INT = #rc.pageNumber#;    

            SELECT *, overall_count = COUNT(*) OVER()
            FROM tbl_reminders
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            <cfif rc.isArchive>
                AND CONVERT(DATETIME, CONVERT(CHAR(8), eventDate, 112)  + ' ' + CONVERT(CHAR(8), eventTime, 108)) <= getdate()  
            <cfelse>
                AND CONVERT(DATETIME, CONVERT(CHAR(8), eventDate, 112)  + ' ' + CONVERT(CHAR(8), eventTime, 108)) >= getdate()   
            </cfif>
            ORDER BY eventdate  <cfif rc.isArchive>desc</cfif>, eventtime <cfif rc.isArchive>desc</cfif>
            OFFSET (@PageNumber-1)*@PageSize ROWS
            FETCH NEXT @PageSize ROWS ONLY
        </cfquery>

        <cfif rc.isArchive>
            <cfset rc.isArchiveURL = '&isArchive=1'>
        <cfelse>
            <cfset rc.isArchiveURL = ''>
        </cfif>

        <!--- get the pagination numbers --->
        <cfif rc.getReminders.recordcount>
            <cfset rc.totalPages = ceiling(rc.getReminders.overall_count / 10)>
            <cfif rc.pageNumber GTE 3>
                <cfset rc.startPage = rc.pageNumber - 2>
            <cfelse>
                <cfset rc.startPage = 1>   
            </cfif>

            <cfif rc.pageNumber LTE 5>
                <cfif rc.totalPages GT rc.pageNumber>
                    <cfset rc.endpage = rc.totalPages>
                <cfelse>
                    <cfset rc.endpage = rc.pageNumber>
                </cfif>
                 
            <cfelse>
                <cfset rc.endpage = rc.totalPages>
            </cfif>
        <cfelse>
            <cfset rc.totalPages = 0>
            <cfset rc.startPage = 1>
            <cfset rc.endpage = 1>
        </cfif>

        <cfquery name="rc.getMainCategory">
            SELECT *
            FROM tbl_categories
            WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.category#">
        </cfquery>

        <cfset rc.maincategory = rc.getMainCategory.maincategoryId>

        <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>
            
    </cffunction>

    <cffunction name="validateReminder" access="public" returntype="array">
        
        <cfargument name="rc"> 
        <cfset rc.errors = []>
                        
        <cfif structKeyExists(RC, 'token') AND NOT CSRFverifyToken(rc.token)>
            <cfset arrayAppend(rc.errors, "A new reminder could not be added")>
        </cfif>
        <cfif not len(trim(rc.title))>
            <cfset arrayAppend(rc.errors, "Please supply a title")>
        </cfif>
        <cfif not len(trim(rc.category))>
            <cfset arrayAppend(rc.errors, "Please supply a category")>
        </cfif>
        <cfif NOT isValid('date', rc.rdate)>
            <cfset arrayAppend(rc.errors, "Please supply a date")>
            <cfset rc.rdate = ''>
        </cfif> 
        <cfif not len(trim(rc.rtime))>
            <cfset arrayAppend(rc.errors, "Please supply a time")>
        </cfif>
            
        <cfreturn rc.errors>

    </cffunction>

    <cffunction name="createSchedule" access="public">
        
        <cfargument name="rc"> 
        
        <!--- set thetime --->
        <cfset rc.theyear = '#DateFormat(rc.rdate, "yyyy")#'>
        <cfset rc.themonth = '#DateFormat(rc.rdate, "mm")#'>
        <cfset rc.theday = '#DateFormat(rc.rdate, "dd")#'>
        
        <cfif (rc.reminderTime EQ 'd' OR rc.reminderTime EQ 'ww' OR rc.reminderTime EQ 'm') AND isvalid('time', rc.reminderActualTime)>
            <cfset rc.thehour = '#TimeFormat(rc.reminderActualTime, "HH")#'>
            <cfset rc.themin = '#TimeFormat(rc.reminderActualTime, "mm")#'>
        <cfelse>
            <cfset rc.thehour = '#TimeFormat(rc.rtime, "HH")#'>
            <cfset rc.themin = '#TimeFormat(rc.rtime, "mm")#'> 
        </cfif>
                
        <!--- database Date Time Conversion --->
        <cfset rc.TheReminder = '#createDateTime(rc.theyear, rc.themonth, rc.theday, rc.thehour, rc.themin, 0)#'>
        <!--- How long before --->
        <cfset rc.TheReminder2 = dateAdd('#rc.remindertime#', - #rc.intervalPeriod#, #rc.TheReminder#)>
        <!--- test date time --->
        <!---<cfoutput>#DateFormat(TheReminder2, "dd/mmm/yyyy")#</cfoutput><br>
        <cfoutput>#TimeFormat(TheReminder2, "HH:mm:ss")#</cfoutput><br>--->
        <!--- set schedule --->
        
        <cfif rc.interval EQ 'yearly'>
            <cfset rc.interval = 'once'>
        </cfif>

        <!--- http://www.mustrememberthat.com --->
        <!--- <cfschedule action = "update"
            task = "MRT-#rc.reminderId#" 
            operation = "HTTPRequest"
            url = "http://www.mustrememberthat.com/index.cfm?action=main.sendReminders&rid=#rc.reminderId#&uid=#session.auth.userId#"
            startDate = "#DateFormat(rc.TheReminder2, "dd/mmm/yyyy")#"
            startTime = "#TimeFormat(rc.TheReminder2, "HH:mm:ss")#"
            interval = "#rc.interval#"
            resolveurl="yes"
            requestTimeOut = "60"> --->

        <cfset returnStruct = {}>
        <cfset returnStruct.reminderDate = DateFormat(rc.TheReminder2, "yyyy/mm/dd")> 
        <cfset returnStruct.reminderTime = TimeFormat(rc.TheReminder2, "HH:mm:ss")> 

        <cfreturn returnStruct>
        
    </cffunction>

    <cffunction name="myaccount">
        <cfargument name="rc">

        <cfset rc.nofooter = true> 
        
        <!--- page params --->
        <cfset rc.errors = []>
        <cfset rc.successMessage = []>

        <!--- update the users details --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'password'>

            <cfif not len(trim(rc.password)) GTE 6>
                <cfset arrayAppend(rc.errors, "Your password must contain at least 6 characters")>
            </cfif>
            <cfif trim(rc.password) NEQ trim(rc.confirmPassword)>
                <cfset arrayAppend(rc.errors, "Your passwords must match")>
            </cfif>

            <cfif NOT arrayLen(rc.errors)>
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.passwordSalt = createUUID()>
                    
                    <!--- now hash the password + the password salt that the user passed --->
                    <cfset rc.inputHash = Hash(rc.password & rc.passwordSalt, 'SHA-512') />
                    
                    <cftransaction> 
                        
                        <!--- add to database --->
                        <cfquery name="rc.createUser">
                            UPDATE  tbl_users
                               SET  password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.inputHash#">
                                    ,passwordSalt = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.passwordSalt#">
                             WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#"/>
                        </cfquery>

                   </cftransaction> 
                    
                    <cfset arrayAppend(rc.successMessage, "Your password has been updated")>

                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>
            </cfif>
                
        </cfif>
            
        <!--- update the users details --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'userDetails'>
            
            <!--- check for errors --->
            <cfif not len(trim(rc.firstname))>
                <cfset arrayAppend(rc.errors, "Please supply a first name")>
            </cfif>
            <cfif not len(trim(rc.lastname))>
                <cfset arrayAppend(rc.errors, "Please supply a last name")>
            </cfif>
            
            <cfif NOT isValid('email', rc.emailAddress)>
                <cfset arrayAppend(rc.errors, "Please supply a valid email address")>
            </cfif>
            
            <!--- make sure the email address has not been used before --->
            <cfquery name="rc.chkEmail" result="rc.insertEmployee">
                SELECT emailAddress
                FROM tbl_users
                WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#"/>
                AND userId <> <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#"/>
            </cfquery>
            
            <cfif rc.chkEmail.recordcount>
                <cfset arrayAppend(rc.errors, "A uesr with that email address already exists")>
            </cfif>

            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- If a logo image was passed in then resize it --->
                    <cfif structKeyExists(rc,'profileImage') AND len(rc.profileImage)>
                        <cfset rc.profileImageURL = createProfileImage(form)>
                    </cfif>

                    <!--- now create the DOB --->
                    <cfset rc.dateOfBirth = '#rc.day#/#rc.month#/#rc.year#'>
                    
                    <cftransaction> 
                        
                        <!--- add to database --->
                        <cfquery name="rc.createUser">
                            UPDATE tbl_users
                               SET firstname = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.firstname#">
                                  ,lastname = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.lastname#">
                                  ,emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">
                                  ,DOB = <cfqueryparam cfsqltype="cf_sql_date" value="#rc.dateOfBirth#">
                                  ,hometown = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.hometown#">
                                  ,mobile = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.mobile#">
                                  ,country = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.country#">
                                  ,gender = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.gender#">
                                  <cfif structKeyExists(RC, 'profileImageURL') AND len(rc.profileImageURL)>
                                    ,profileImage = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.profileImageURL#">
                                    ,imgX = <cfqueryparam cfsqltype="cf_sql_varchar" value="1">
                                    ,imgY = <cfqueryparam cfsqltype="cf_sql_varchar" value="1">
                                    ,imageWidth = <cfqueryparam cfsqltype="cf_sql_varchar" value="300">
                                    ,imageHeight = <cfqueryparam cfsqltype="cf_sql_varchar" value="300">
                                </cfif>
                             WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#"/>
                        </cfquery>

                   </cftransaction> 
                    
                    <!--- Now login the user and set the session variables --->
                    <cfset session.auth.firstName = rc.firstName> 
                    <cfset session.auth.lastname = rc.lastname> 
                    <cfset session.auth.emailAddress = rc.emailAddress>
                    
                    <cfset arrayAppend(rc.successMessage, "Your personal details have been updated")>

                    <cfcatch type="any">
                        <cfdump var="#cfcatch#"><cfabort>
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

        <cfset rc.MyDateTime=Now()> 
        <cfset rc.Date13 = '#DateFormat(DateAdd('yyyy', -13, rc.MyDateTime),'yyyy')#'>
        <cfset rc.Date30 = '#DateFormat(DateAdd('yyyy', -30, rc.MyDateTime),'yyyy')#'>
        <cfset rc.Date100 = '#DateFormat(DateAdd('yyyy', -100, rc.MyDateTime),'yyyy')#'>
        
        <cfquery name="rc.getUser">
            SELECT *
            FROM tbl_users
            WHERE USERID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

         <!--- rotate the image --->
        <cfif structKeyExists(RC, 'formAction') AND rc.formAction EQ 'rotate'>
            
            <cfset imagePath = expandpath('\assets\userImages') & '\' & rc.getUser.profileImage>
            
            <cfset myImage = ImageNew(imagePath)> 
            
            <cfset ImageSetAntialiasing(myImage,"on")> 
            
            <!--- Rotate the image by 10 degrees. ---> 
            <cfset ImageRotate(myImage,1,1,90,"bicubic")>
            
            <cfimage source="#myImage#" action="write" destination="#imagePath#" overwrite="yes"> 

            <cfset myNewImage = ImageNew(imagePath)> 

            <cfquery name="rc.createUser">
                UPDATE tbl_users
                   SET imgX = <cfqueryparam cfsqltype="cf_sql_varchar" value="1">
                                    ,imgY = <cfqueryparam cfsqltype="cf_sql_varchar" value="1">
                                    ,imageWidth = <cfqueryparam cfsqltype="cf_sql_varchar" value="#myNewImage.width#">
                    ,imageHeight = <cfqueryparam cfsqltype="cf_sql_varchar" value="#myNewImage.height#">
                 WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#"/>
            </cfquery>

            <cfquery name="rc.getUser">
                SELECT *
                FROM tbl_users
                WHERE USERID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            </cfquery>

        </cfif>

    </cffunction>

    <cffunction name="createProfileImage" access="public">
        
        <cfargument name="rc">
        <cfargument name="imageManipulation" default="ImageScale">
        <cfargument name="imageWidth" default="300">
        <cfargument name="imageHeight" default="300">

        <cfset var local = {}>
        
        <!--- upload file --->
        <cffile action="upload" fileField="profileImage" destination="#expandpath('\assets\userImages')#" result="fileUpload" nameconflict="makeunique"> 
        
        <!--- Determine whether the image file is saved. ---> 
        <cfif fileUpload.fileWasSaved>
        
            <!--- Determine whether the saved file is a valid image file. ---> 
            <cfset local.pathwheresaved = "#expandpath('\assets\userImages')#\#fileUpload.serverfile#">
            
            <cfif listContainsNoCase('jpg,jpeg,gif,png',fileUpload.serverfileext)> 
                
                <!--- Read the image file into a variable called myImage. ---> 
                <cfimage action="read" source="#local.pathwheresaved#" name="myImage">

                <!--- Manipulate image --->
                <cfif arguments.imageManipulation eq 'ImageResize'>
                    <cfset ImageResize(myImage,arguments.imageWidth,arguments.imageHeight)>
                <cfelseif arguments.imageManipulation eq 'ImageScale'>
                    <cfset ImageScaleToFit(myImage,arguments.imageWidth,arguments.imageHeight)>
                </cfif> 
                    
                <!--- delete the temp image --->
                <cffile action="delete" file="#local.pathwheresaved#">

                <!--- save the image --->
                <cfimage source="#myImage#" action="write" destination="#expandpath('\assets\userImages')#\#fileUpload.serverfile#"> 
                
                <cfreturn fileUpload.serverfile>

            <cfelse> 
                
                <!--- If it is not a valid image file, delete it from the server. ---> 
                <cffile action="delete" file="#local.pathwheresaved#">
                <cfreturn ''>
            
            </cfif>
        
        </cfif>

    </cffunction>

    <cffunction name="suggestedReminders" access="public">
        
        <cfargument name="rc">
        <cfset rc.bodyID = 'mobwell'>

        <cfquery name="rc.getPriorityCats">
            SELECT DISTINCT urlSafe, title
            FROM    tbl_categories
            ORDER BY title
        </cfquery>

    </cffunction>
        
    <cffunction name="subscriptions" access="public">

        <cfargument name="rc">
        <cfset rc.bodyID = 'mobwell'>

        <cfquery name="rc.getSubscription">
            SELECT *
            FROM tbl_subscriptions
            INNER JOIN tbl_businesses ON tbl_businesses.businessId = tbl_subscriptions.businessId
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

    </cffunction>

    <cffunction name="groups" access="public">

        <cfargument name="rc">
        <cfset rc.bodyID = 'mobwell'>

        <cfquery name="rc.getGroups">
           SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            WHERE tbl_groups.ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            UNION
            SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            INNER JOIN tbl_groupUsers ON tbl_groupUsers.groupId = tbl_groups.groupId
            WHERE tbl_groupUsers.userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

    </cffunction>


</cfcomponent>



