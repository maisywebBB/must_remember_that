<cfcomponent output="false">
	
    <cffunction name="init">
        <cfargument name="fw" type="any" />
        <cfset variables.fw = arguments.fw />
	</cffunction>
	
    <cffunction name="before" output="false">  
        <cfargument name="rc"> 
        <cfif ( structKeyExists( session, "auth" ) AND session.auth.isLoggedIn AND variables.fw.getItem() NEQ "logout")>
            <cfset variables.fw.redirect( "main" )>
        </cfif>
    </cffunction>
    
    <cffunction name="default" output="false">  
        <cfargument name="rc">  
    </cffunction>

    <cffunction name="passwordreset" output="false">  
        <cfargument name="rc">  
    </cffunction>
    
    <cffunction name="newPassword">
        <cfargument name="rc">
        
        <!--- make sure a valid UUID was passed to this page --->
        <cfif ( NOT structKeyExists( rc, "emailAddress" ) 
                OR NOT structKeyExists( rc, "password" )
                OR NOT isValid('UUID', rc.token)) >
            <cfset variables.fw.redirect( "login" )>
        </cfif>
        
        <!--- get the users details from the DB --->
        <cfquery name="rc.getuser">
            SELECT usr.*
            FROM tbl_users usr
            WHERE usr.emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">
            AND usr.passwordResetToken = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.token#">
        </cfquery>

        <!--- validate process --->
        <cfif NOT rc.getuser.recordcount>
            <cfset rc.message = ["You're reset password attempt has failed. Please try again"]>
            <cfset variables.fw.redirect( "login.passwordReset", "token,message")>
        </cfif>
        
        <!--- make sure the reset is still in date --->
        <cfif NOT dateDiff('s',now(),rc.getuser.passwordResetDate) GTE 1>
            <cfset rc.message = ["You had 24 hours to complete this task. This time has passed. Please send a new password reset request."]>
            <cfset variables.fw.redirect( "login.passwordReset", "token,message")>
        </cfif>
        
        <!--- Looks like we passed the validation. Create a new password --->
        <cftry>
            
            <cfset rc.newPassword = Hash(rc.password & rc.getuser.passwordSalt, 'SHA-512') />
	    
            <!--- save to the database --->
            <cfquery name="rc.saveUser">
                UPDATE tbl_users
                SET password = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.newPassword#">,
                passwordResetToken = <cfqueryparam cfsqltype="cf_sql_varchar" value="">
                WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.getuser.userId#">
            </cfquery>
            
            <cfcatch type="any">
                <cfset rc.message = ["A error occurred whislt updating your password. Please try again"]>
                <cfset variables.fw.redirect( "login.passwordReset", "token,message")>
            </cfcatch>
            
        </cftry>
        
        <!--- redirect the user to the login page with a nice message --->
        <cfset rc.successMessage = ["Your password has been reset. Please login"]>
        <cfset variables.fw.redirect( "main.default", "token,successMessage")>
        
     </cffunction>

    <cffunction name="login" output="true">  
        
        <cfargument name="rc">  
        
		<!--- if the form variables do not exist, redirect to the login form --->
        <cfif ( NOT structKeyExists( rc, "emailAddress" ) 
				OR NOT structKeyExists( rc, "password" ))>
            <cfset variables.fw.redirect( "login" )>
        </cfif>
        
        <!--- get the users details from the DB --->
        <cfquery name="rc.getuser">
 			SELECT usr.*
            FROM tblAdminUsers usr
            WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">
 		</cfquery>
		
		<cfif NOT rc.getuser.recordCount>
        	<cfset rc.message = ["Invalid login credentials. Please try again"]>
            <cfset variables.fw.redirect( "login", "message" )>
        </cfif>
        
        <!--- now hash the password + the password salt that the user passed --->
		<cfset rc.inputHash = Hash(rc.password & rc.getuser.passwordSalt, 'SHA-512') />
        
        <!--- if the input hash matches the hash in the db then the user is valid --->
        <cfif NOT rc.inputHash EQ rc.getuser.password>
        	<cfset rc.message = ["Invalid login credentials. Please try again"]>
            <cfset variables.fw.redirect( "login", "message" )>
        </cfif>

        <!--- set the session variables --->
        <cfset session.auth.isLoggedIn = true>
        <cfset session.auth.firstName = rc.getuser.firstName> 
        <cfset session.auth.lastname = rc.getuser.lastname> 
        <cfset session.auth.emailAddress = rc.getuser.emailAddress>
        <cfset session.auth.adminUserId = rc.getuser.adminUserId>
        <cfset session.auth.userType = rc.getuser.userType>
        <cfset session.auth.photodata = rc.getuser.photodata>
        <cfset session.auth.encryptionKey = generateSecretKey( "AES" ) />
                
        <cfset variables.fw.redirect( "main.default" )>
	
    </cffunction>
        
    <cffunction name="logout" output="true">  
        <cfargument name="rc">
       	<!--- reset session variables --->
        <cfset session.auth.isLoggedIn = false>
        <cfset session.auth.fullname = "Guest">
        <cfset structdelete( session.auth, "user" )>
        <cfset structdelete( session.auth, "EMAILADDRESS" )>
        <cfset structdelete( session.auth, "FIRSTNAME" )>
        <cfset structdelete( session.auth, "FULLNAME" )>
        <cfset structdelete( session.auth, "LASTNAME" )>
        <cfset structdelete( session.auth, "USERID" )>

        <!--- if a user logs out expire any cookies that may exist --->
        <cftry>
            <cfcookie name="MRTuser" expires="NOW">
            <cfcookie name="MRTid" expires="NOW">

            <cfcatch type="any"></cfcatch>
        </cftry>

        <cfset rc.message = ["You have safely logged out"]>
        <cfset variables.fw.redirect( "login", "message" )>
    </cffunction>
    
</cfcomponent>
