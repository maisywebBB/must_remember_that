<cfcomponent output="false">
	
    <cffunction name="init">
        <cfargument name="fw" type="any" />
        <cfset variables.fw = arguments.fw />
	</cffunction>

    <cffunction name="delete" output="true">  
        <cfargument name="rc">
        
        <cfquery name="rc.getGroups">
            SELECT *
            FROM tbl_groups
            WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
            AND ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

        <cfif NOT rc.getGroups.recordcount>
            <cfset rc.errors = []>
            <cfset arrayAppend(rc.errors, "Only the owner of a page can delete it")>
            <cfset variables.fw.redirect( action="groups.edit",preserve="all" )> 
        <cfelse>
            <cfquery name="rc.getGroups">
                DELETE FROM tbl_groups
                WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
                AND ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            </cfquery>
            <cfset rc.successMessage = []>
            <cfset arrayAppend(rc.successMessage, "The group has been deleted")>
            <cfset variables.fw.redirect( action="users.reminders",preserve="all" )>
        </cfif>
        
    </cffunction>

    <cffunction name="group" output="true">  
        <cfargument name="rc">
	
	<!--- <cfset rc.layoutName = 'businessPage'> --->
        <cfset rc.bodyID = 'businessPage'>
        <cfquery name="rc.getGroups">
            SELECT *
            FROM tbl_groups
            WHERE urlSlug = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.safeURL#">
        </cfquery>

        <!--- get all the reminders for this user --->
        <cfquery name="rc.getReminders">
            SELECT rem.*, tbl_categories.title AS catTitle, overall_count = COUNT(*) OVER(),
            (select count(*) from tbl_reminders where parentid= rem.reminderid ) as ChildCount
            FROM tbl_reminders rem
            INNER JOIN tbl_categories ON tbl_categories.id = rem.category
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="3">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.getGroups.groupId#">
            AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND rdate >= getdate()
            ORDER BY rdate
        </cfquery>

        <cfif NOT rc.getGroups.recordcount>
            <cfset variables.fw.redirect( "main.default" )> 
        </cfif>

    </cffunction>
    
    <cffunction name="addAGroup">
        <cfargument name="rc">

        <cfparam name="rc.title" default="">
        <cfparam name="rc.aboutContent" default="">
        <cfparam name="rc.website" default="http://">
        <cfparam name="rc.isLive" default="">
        <cfparam name="rc.imageURL" default="">
        <cfparam name="rc.logoURL" default="">
        <cfparam name="rc.logo" default="">

        <!--- add a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'addGroup'>
            
            <!--- check for errors --->
            <cfset rc.errors = validateGroup(rc)>

            <!--- create a safe URL --->
            <cfset rc.urlSlug = formatFilename(rc.title)>

            <!--- now make sure a group with the same slug dose not exist --->
            <cfquery name="rc.getSlugGroup">
                SELECT *
                FROM tbl_groups
                WHERE urlSlug = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.urlSlug#">
            </cfquery>

            <cfif rc.getSlugGroup.recordcount>
                <cfset arrayAppend(rc.errors, "A group with the same name already exists")>
            </cfif>

            <!--- <cfif NOT len(rc.logo)>
                <cfset arrayAppend(rc.errors, "Please supply an image")>
            </cfif> --->
                
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.groupId = createUUID()>

                    <!--- If a hero image was passed in then resize it --->
                    <cfif structKeyExists(rc,'heroImageURL') AND len(rc.heroImageURL)>
                        <cfset rc.imageURL = createHeroImage(form)>
                    </cfif>

                    <!--- If a logo image was passed in then resize it --->
                    <cfif structKeyExists(rc,'logo') AND len(rc.logo)>
                        <cfset rc.logoURL = createLogoImage(form)>
                    </cfif>

                    <cftransaction> 
                    
                        <!--- add to database --->
                        <cfquery name="rc.createGroup">
                            INSERT INTO tbl_groups 
                            (
                                groupId,
                                ownerId,
                                title,
                                urlSlug,
                                aboutContent,
                                website,
                                heroImageURL,
                                logo,
                                isLive
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.urlSlug#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.aboutContent#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.website#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.imageURL#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.logoURL#">,
                                <cfqueryparam cfsqltype="cf_sql_bit" value="#rc.isLive#">
                            )
                        </cfquery>

                    </cftransaction> 
                    
                    <cflocation addtoken="false" url="/index.cfm?action=groups.edit&groupId=#rc.groupId#">

                    <cfcatch type="any">
                        <cfdump var="#cfcatch#"><cfabort>
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

    </cffunction>

    <cffunction name="edit">
        
        <cfargument name="rc"> 
        
        <cfparam name="rc.title" default="">
        <cfparam name="rc.aboutContent" default="">
        <cfparam name="rc.website" default="">
        <cfparam name="rc.isLive" default="">
        <cfparam name="rc.imageURL" default="">
        <cfparam name="rc.logoURL" default="">
        <cfparam name="rc.urlslug" default="">

        <!--- make sure user has permission to view/edit this page --->
        <cfset groupValidation(rc)>

        <cfquery name="rc.getGroups">
            SELECT *
            FROM tbl_groups
            INNER JOIN tbl_users ON tbl_users.userId = tbl_groups.ownerId
            WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
        </cfquery>

        <!--- delete an admin users --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'deleteAdmin'>
            
            <cfquery name="rc.deleteAdmin">
                DELETE FROM tbl_groupUsers
                WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
                AND userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">
            </cfquery>

            <cfset rc.successMessage = []>
            <cfset arrayAppend(rc.successMessage, "The user has been removed")>

        </cfif>
        
        <!--- search for an admin user --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'searchadminUser'>

            <cfquery name="rc.searchUserResults">
                SELECT *
                FROM tbl_users
                WHERE firstname + ' ' + lastname  = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.usersname#">
            </cfquery>

        </cfif>
            
        <!--- add an admin user --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'addadminUser'>

            <cfset rc.errors = []>
            
            <!--- now check in the DB to see if this user exists --->
            <cfquery name="rc.chkUser">
                SELECT *
                FROM tbl_users
                WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">
            </cfquery>

            <cfif NOT rc.chkUser.recordcount>
                <cfset arrayAppend(rc.errors, "The users name supplied is not a Must Remember That member - If you would like this person be an administrator please advise them to sign up.")>
            <cfelse>
                <!--- now make sure the user is not already an administrator --->
                <cfquery name="rc.chkAdminUser">
                    SELECT *
                    FROM tbl_groupUsers
                    WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
                    AND userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.chkUser.userId#">
                </cfquery>

                <cfif rc.chkAdminUser.recordcount>
                    <cfset arrayAppend(rc.errors, "This user is already a group member")>
                </cfif>
                                    
            </cfif>

            <cfif NOT arrayLen(rc.errors)>

                <cfset textlink = 'http://www.mustrememberthat.com/index.cfm?action=main.joinGroup&userid=#rc.chkUser.userId#&groupId=#rc.groupId#'>

                <cfsavecontent variable="emailContent"><cfoutput>
                    Hi<br><br>
                    You have been invited by #rc.getGroups.firstname# #rc.getGroups.lastname# to join the #rc.getGroups.title# group on Must Remember That. <a href="#textlink#">Click To join group</a><br><br>
                    If you have a problem with the link above please copy the following link into your browser #textlink#<br><br>
                    NEVER FORGET AGAIN
                </cfoutput></cfsavecontent>
                
                <cfset application.emailCFC.sendEmail(rc.chkUser.emailAddress, 'Must Remember That – Group Invitation' ,emailContent)>

                <!--- <cftransaction> 
                    
                    <!--- add to database --->
                    <cfquery name="rc.createGroup">
                        INSERT INTO tbl_groupUsers
                               (groupId
                               ,userId)
                        VALUES
                               (<cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
                               ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.chkUser.userId#">)
                     </cfquery>
                
                </cftransaction>  --->
                
                <cfset rc.successMessage = []>
                <cfset arrayAppend(rc.successMessage, "An email has been sent to this user asking them to join the group.")>

            </cfif>
                
        </cfif>
            
        <!--- update business --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'updateGroups'>

            <!--- check for errors --->
            <cfset rc.errors = validateGroup(rc)>

            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- If a hero image was passed in then resize it --->
                    <cfif structKeyExists(rc,'heroImageURL') AND len(rc.heroImageURL)>
                        <cfset rc.imageURL = createHeroImage(form)>
                    </cfif>

                    <!--- If a logo image was passed in then resize it --->
                    <cfif structKeyExists(rc,'logo') AND len(rc.logo)>
                        <cfset rc.logoURL = createLogoImage(form)>
                    </cfif>

                    <cftransaction> 
                    
                        <!--- add to database --->
                        <cfquery name="rc.createGroup">
                            UPDATE tbl_groups
                            SET
                            title = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                            aboutContent = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.aboutContent#">,
                            website = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.website#">,
                            <cfif len(rc.imageURL)>
                                heroImageURL = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.imageURL#">,
                            </cfif>
                            <cfif len(rc.logoURL)>
                                logo = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.logoURL#">,
                            </cfif>
                            isLive = <cfqueryparam cfsqltype="cf_sql_bit" value="#rc.isLive#">
                            WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
                         </cfquery>
                    
                    </cftransaction> 
                    
                    <cfset rc.successMessage = []>
                    <cfset arrayAppend(rc.successMessage, "Group details have been updated")>

                    <cfcatch type="any">
                        <cfdump var="#cfcatch#"><cfabort>
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>

        <cfelse>

            <cfset rc.title = rc.getGroups.title >
            <cfset rc.urlslug = rc.getGroups.urlslug >
            <cfset rc.aboutContent = rc.getGroups.aboutContent >
            <cfset rc.website = rc.getGroups.website >
            <cfset rc.isLive = rc.getGroups.isLive >   

        </cfif>

        <cfquery name="rc.getGroupUsers">
            SELECT *
            FROM tbl_groupUsers
            INNER JOIN tbl_users ON tbl_users.userId = tbl_groupUsers.userId
            WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
        </cfquery>

    </cffunction>

    <cffunction name="reminders">
        
        <cfargument name="rc"> 

        <cfset rc.bodyID = 'myReminders'>
        
        <cfif structKeyExists(RC, 'safeURL')>
            <cfquery name="rc.getGroups">
                SELECT *
                FROM tbl_groups
                WHERE urlSlug = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.safeURL#">
            </cfquery>

            <cfif rc.getGroups.recordcount>
                <cfset rc.groupid = rc.getGroups.groupId>
            </cfif>

        </cfif>

        <!--- make sure user has permission to view/edit this page --->
        <cfset groupValidation(rc)>

        <cfparam name="rc.reminderId" default="">
        <cfparam name="rc.reminderType" default="3">
        <cfparam name="rc.reminderKey" default="">
        <cfparam name="rc.title" default="">
        <cfparam name="rc.category" default="">
        <cfparam name="rc.interval" default="">
        <cfparam name="rc.reminderTime" default="">
        <cfparam name="rc.rdate" default="">
        <cfparam name="rc.rtime" default="">
        <cfparam name="rc.comments" default="">
        <cfparam name="rc.pageNumber" default="1">
        <cfparam name="rc.maincategory" default="0">
        <cfparam name="rc.isArchive" default="0">
        <cfparam name="rc.intervalPeriod" default="1">
        <cfparam name="rc.location" default="">
        <cfparam name="rc.latitude" default="">
        <cfparam name="rc.longitude" default="">

        <!--- delete a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'deleteReminder'>

            <cfquery name="rc.createReminder">
                UPDATE tbl_reminders
                SET isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="0">
                WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">
                AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
            </cfquery>
            
            <cfset rc.successMessage = []>
            <cfset arrayAppend(rc.successMessage, "Reminder has been removed")>

        </cfif>
            
        <!--- add a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'newReminder'>
            
            <!--- check for errors --->
            <cfset rc.errors = validateReminder(rc)>
            
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <!--- create a password salt --->
                    <cfset rc.reminderId = createUUID()>
                    
                    <cfif rc.interval EQ 'daily2'>
                        <cfset rc.interval = 'daily'>
                        <cfset rc.forever = 1>
                    <cfelse>
                        <cfset rc.forever = 0>
                    </cfif>
                    
                    <cftransaction> 
                        
                        <!--- create the schedule --->
                        <cfset createSchedule = createSchedule(rc)>

                        <!--- add to database --->
                        <cfquery name="rc.createReminder">
                            INSERT INTO tbl_reminders
                            (
                                reminderId,
                                reminderType,
                                reminderKey,
                                title,
                                comments,
                                category,
                                interval,
                                remindertime,
                                rdate,
                                rtime,
                                intervalPeriod,
                                eventDate,
                                eventTime,
                                forever,
                                location,
                                latitude,
                                longitude
                            )
                            VALUES
                            (
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.reminderType#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.comments#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                                <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                                <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                                <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.location#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.latitude#">,
                                <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.longitude#">
                            )
                        </cfquery>

                    </cftransaction> 
                    
                    <cfset rc.successMessage = []>
                    <cfset arrayAppend(rc.successMessage, "A new reminder has been added.")>
                    
                    <cfset rc.title = "">
                    <cfset rc.comments = "">
                    <cfset rc.category = "">
                    <cfset rc.interval = "">
                    <cfset rc.remindertime = "">
                    <cfset rc.rdate = "">
                    <cfset rc.rtime = "">
                    <cfset rc.intervalPeriod = "">

                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        </cfif>

        <!--- get all the reminders for this user --->
        <cfquery name="rc.getReminders">
            DECLARE
            @PageSize INT = 10,
            @PageNumber  INT = #rc.pageNumber#;    

            SELECT *, overall_count = COUNT(*) OVER()
            FROM tbl_reminders
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="3">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
            AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            <cfif rc.isArchive>
                AND CONVERT(DATETIME, CONVERT(CHAR(8), rdate, 112)  + ' ' + CONVERT(CHAR(8), rtime, 108)) <= getdate()  
            <cfelse>
                AND CONVERT(DATETIME, CONVERT(CHAR(8), rdate, 112)  + ' ' + CONVERT(CHAR(8), rtime, 108)) >= getdate()   
            </cfif>
            ORDER BY rdate <cfif rc.isArchive>desc</cfif>
            OFFSET (@PageNumber-1)*@PageSize ROWS
            FETCH NEXT @PageSize ROWS ONLY
        </cfquery>

        <!--- get the pagination numbers --->
        <cfif rc.getReminders.recordcount>
            <cfset rc.totalPages = ceiling(rc.getReminders.overall_count / 10)>
            <cfif rc.pageNumber GTE 3>
                <cfset rc.startPage = rc.pageNumber - 2>
            <cfelse>
                <cfset rc.startPage = 1>   
            </cfif>

            <cfif rc.pageNumber LTE 5>
                <cfif rc.totalPages GT rc.pageNumber>
                    <cfset rc.endpage = rc.totalPages>
                <cfelse>
                    <cfset rc.endpage = rc.pageNumber>
                </cfif>
                 
            <cfelse>
                <cfset rc.endpage = rc.totalPages>
            </cfif>
        <cfelse>
            <cfset rc.totalPages = 0>
            <cfset rc.startPage = 1>
            <cfset rc.endpage = 1>
        </cfif>

        <cfquery name="rc.getGroups">
            SELECT *
            FROM tbl_groups
            WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
        </cfquery>

        <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>

        <cfquery name="rc.getPriorityCats">
            SELECT *
            FROM tbl_categories
            WHERE isPriority = 1
            order by newid()
        </cfquery>

        <cfquery name="rc.getSubscription">
            SELECT *
            FROM tbl_subscriptions
            INNER JOIN tbl_businesses ON tbl_businesses.businessId = tbl_subscriptions.businessId
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

        <cfquery name="rc.getGroups">
           SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            WHERE tbl_groups.ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            UNION
            SELECT tbl_groups.groupId, tbl_groups.title, tbl_groups.urlSlug
            FROM tbl_groups
            INNER JOIN tbl_groupUsers ON tbl_groupUsers.groupId = tbl_groups.groupId
            WHERE tbl_groupUsers.userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>
            
    </cffunction>

    <cffunction name="updateReminder">
        
        <cfargument name="rc"> 
        
        <cfparam name="rc.reminderId" default="">
        <cfparam name="rc.reminderType" default="3">
        <cfparam name="rc.reminderKey" default="">
        <cfparam name="rc.title" default="">
        <cfparam name="rc.category" default="">
        <cfparam name="rc.interval" default="">
        <cfparam name="rc.reminderTime" default="">
        <cfparam name="rc.rdate" default="">
        <cfparam name="rc.rtime" default="">
        <cfparam name="rc.comments" default="">
        <cfparam name="rc.maincategory" default="0">
        <cfparam name="rc.intervalPeriod" default="1">
        <cfparam name="rc.pageNumber" default="1">
        <cfparam name="rc.isArchive" default="0">
        <cfparam name="rc.eventdate" default="">
        <cfparam name="rc.eventtime" default="">
        <cfparam name="rc.location" default="">
        <cfparam name="rc.latitude" default="">
        <cfparam name="rc.longitude" default="">

        <cfif reFindNoCase("android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino",CGI.HTTP_USER_AGENT) GT 0 OR reFindNoCase("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-",Left(CGI.HTTP_USER_AGENT,4)) GT 0>
            <cfset rc.noFooter = 'true'>
        </cfif>

        <!--- get the current reminder --->
        <cfquery name="rc.getReminder">
            SELECT *
            FROM tbl_reminders
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="3">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
            AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">
            ORDER BY rdate
        </cfquery>

        <cfquery name="rc.getGroups">
            SELECT *
            FROM tbl_groups
            WHERE groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
            AND ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

        <!--- add a reminder --->
        <cfif structKeyExists(RC, 'update') AND rc.update EQ 'updateReminder'>
            
            <!--- check for errors --->
            <cfset rc.errors = validateReminder(rc)>
            
            <cfif NOT arrayLen(rc.errors)>
                
                <!--- catch any errors --->
                <cftry>
                    
                    <cftransaction> 
                        
                        <!--- create the schedule --->
                        <cfset createSchedule = createSchedule(rc)>
                        
                        <!--- update database --->
                        <cfquery name="rc.createReminder">
                            UPDATE tbl_reminders
                            SET
                            title = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                            category = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                            interval = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                            reminderTime = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                            rdate = <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                            rtime = <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                            comments = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.comments#">,
                            location = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.location#">,
                            latitude = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.latitude#">,
                            longitude = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.longitude#">,
                            intervalPeriod = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">
                            WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">
                        </cfquery>

                    </cftransaction> 
                    
                    <cfset rc.successMessage = []>
                    <cfset arrayAppend(rc.successMessage, "This reminder has been updated.")>
                
                    <cfcatch type="any">
                        <cfif len(cfcatch.Detail)>
                            <cfset arrayAppend(rc.errors, cfcatch.Detail)>
                        <cfelse>
                            <cfset arrayAppend(rc.errors, cfcatch.message)>
                        </cfif>
                    </cfcatch>
                
                </cftry>

            </cfif>
                
        <cfelse>

            <cfset rc.title = rc.getReminder.title>
            <cfset rc.category = rc.getReminder.category>
            <cfset rc.interval = rc.getReminder.interval>
            <cfset rc.reminderTime = rc.getReminder.reminderTime>
            <cfset rc.rdate = dateFormat(rc.getReminder.rdate, 'yyyy-mm-dd')>
            <cfset rc.rtime = timeFormat(rc.getReminder.rtime, 'HH:MM')>
            <cfset rc.comments = rc.getReminder.comments>
            <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
            <cfset rc.location = rc.getReminder.location>
            <cfset rc.latitude = rc.getReminder.latitude>
            <cfset rc.longitude = rc.getReminder.longitude>
            <cfset rc.eventdate = dateFormat(rc.getReminder.eventdate, 'yyyy-mm-dd')>
            <cfset rc.eventtime = timeFormat(rc.getReminder.eventtime, 'HH:MM')>

        </cfif>

        <!--- get all the reminders for this user --->
        <cfquery name="rc.getReminders">
            DECLARE
            @PageSize INT = 10,
            @PageNumber  INT = #rc.pageNumber#;    

            SELECT *, overall_count = COUNT(*) OVER()
            FROM tbl_reminders
            WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="3">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
            AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            <cfif rc.isArchive>
                AND rdate <= getdate()  
            <cfelse>
                AND rdate >= getdate()   
            </cfif>
            ORDER BY dateCreated desc
            OFFSET (@PageNumber-1)*@PageSize ROWS
            FETCH NEXT @PageSize ROWS ONLY
        </cfquery>

        <!--- get the pagination numbers --->
        <cfif rc.getReminders.recordcount>
            <cfset rc.totalPages = ceiling(rc.getReminders.overall_count / 10)>
            <cfif rc.pageNumber GTE 3>
                <cfset rc.startPage = rc.pageNumber - 2>
            <cfelse>
                <cfset rc.startPage = 1>   
            </cfif>

            <cfif rc.pageNumber LTE 5>
                <cfif rc.totalPages GT rc.pageNumber>
                    <cfset rc.endpage = rc.totalPages>
                <cfelse>
                    <cfset rc.endpage = rc.pageNumber>
                </cfif>
                 
            <cfelse>
                <cfset rc.endpage = rc.totalPages>
            </cfif>
        <cfelse>
            <cfset rc.totalPages = 0>
            <cfset rc.startPage = 1>
            <cfset rc.endpage = 1>
        </cfif>

        <cfquery name="rc.getMainCategory">
            SELECT *
            FROM tbl_categories
            WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.category#">
        </cfquery>

        <cfset rc.maincategory = rc.getMainCategory.maincategoryId>

        <!--- get all the categories --->
        <cfquery name="rc.getMainCats">
            SELECT *
            FROM tbl_mainCategories
            ORDER BY mainCategoryName
        </cfquery>

        <cfquery name="rc.getCats">
            SELECT *
            FROM tbl_categories
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.maincategory#">
            ORDER BY title
        </cfquery>
            
    </cffunction>

    <cffunction name="formatFilename" returntype="any" output="false" access="private">
        <cfargument name="filename" type="any" />
        
        <cfset var wordDelim = '-'>

        <!--- Remove HTML --->
        <cfset arguments.filename=ReReplace(arguments.filename, "<[^>]*>","","all") />
        
        <!--- temporarily escape " " used for word separation --->
        <cfset arguments.filename=rereplace(arguments.filename," ","svphsv","ALL") />
        
        <!--- temporarily escape "-" used for word separation --->
        <cfset arguments.filename=rereplace(arguments.filename,"\#wordDelim#","svphsv","ALL") />
        
        <!--- remove all punctuation --->
        <cfset arguments.filename=rereplace(arguments.filename,"[[:punct:]]","","ALL") />
            
        <!---  put word separators " "  and "-" back in --->
        <cfset arguments.filename=rereplace(arguments.filename,"svphsv",wordDelim,"ALL") />
            
        <cfset arguments.filename=lcase(rereplace(arguments.filename,"\#wordDelim#+",wordDelim,"ALL")) />

        <cfreturn arguments.filename>

    </cffunction>

    <cffunction name="validateGroup" access="private" returntype="array">
        
        <cfargument name="rc"> 
        <cfset rc.errors = []>
                        
        <cfif NOT CSRFverifyToken(rc.token)>
            <cfset arrayAppend(rc.errors, "A new group could not be added")>
        </cfif>
        <cfif not len(trim(rc.title))>
            <cfset arrayAppend(rc.errors, "Please supply a title")>
        </cfif>
            
        <cfreturn rc.errors>

    </cffunction>

    <cffunction name="createLogoImage" access="public">
        
        <cfargument name="rc">
        <cfargument name="imageManipulation" default="ImageResize">
        <cfargument name="imageWidth" default="160">
        <cfargument name="imageHeight" default="160">

        <cfset var local = {}>
        
        <!--- upload file --->
        <cffile action="upload" fileField="logo" destination="#expandpath('\assets\groupImages')#" result="fileUpload" nameconflict="makeunique"> 
        
        <!--- Determine whether the image file is saved. ---> 
        <cfif fileUpload.fileWasSaved>
        
            <!--- Determine whether the saved file is a valid image file. ---> 
            <cfset local.pathwheresaved = "#expandpath('\assets\groupImages')#\#fileUpload.serverfile#">
            
            <cfif listContainsNoCase('jpg,jpeg,gif,png',fileUpload.serverfileext)> 
                
                <!--- Read the image file into a variable called myImage. ---> 
                <cfimage action="read" source="#local.pathwheresaved#" name="myImage">

                <!--- Manipulate image --->
                <cfif arguments.imageManipulation eq 'ImageResize'>
                    <cfset ImageResize(myImage,arguments.imageWidth,arguments.imageHeight)>
                <cfelseif arguments.imageManipulation eq 'ImageScale'>
                    <cfset ImageScaleToFit(myImage,arguments.imageWidth,arguments.imageHeight)>
                </cfif> 
                    
                <!--- delete the temp image --->
                <cffile action="delete" file="#local.pathwheresaved#">

                <!--- save the image --->
                <cfimage source="#myImage#" action="write" destination="#expandpath('\assets\groupImages')#\#fileUpload.serverfile#"> 
                
                <cfreturn fileUpload.serverfile>

            <cfelse> 
                
                <!--- If it is not a valid image file, delete it from the server. ---> 
                <cffile action="delete" file="#local.pathwheresaved#">
                <cfreturn ''>
            
            </cfif>
        
        </cfif>

    </cffunction>

    <cffunction name="createHeroImage" access="public">
        
        <cfargument name="rc">
        <cfargument name="imageManipulation" default="ImageResize">
        <cfargument name="imageWidth" default="1500">
        <cfargument name="imageHeight" default="300">

        <cfset var local = {}>
        
        <!--- upload file --->
        <cffile action="upload" fileField="heroImageURL" destination="#expandpath('\assets\groupImages')#" result="fileUpload" nameconflict="makeunique"> 
        
        <!--- Determine whether the image file is saved. ---> 
        <cfif fileUpload.fileWasSaved>
        
            <!--- Determine whether the saved file is a valid image file. ---> 
            <cfset local.pathwheresaved = "#expandpath('\assets\groupImages')#\#fileUpload.serverfile#">
            
            <cfif listContainsNoCase('jpg,jpeg,gif,png',fileUpload.serverfileext)> 
                
                <!--- Read the image file into a variable called myImage. ---> 
                <cfimage action="read" source="#local.pathwheresaved#" name="myImage">

                <!--- Manipulate image --->
                <cfif arguments.imageManipulation eq 'ImageResize'>
                    <cfset ImageResize(myImage,arguments.imageWidth,arguments.imageHeight)>
                <cfelseif arguments.imageManipulation eq 'ImageResize'>
                    <cfset ImageScaleToFit(myImage,arguments.imageWidth,arguments.imageHeight)>
                </cfif> 
                    
                <!--- delete the temp image --->
                <cffile action="delete" file="#local.pathwheresaved#">

                <!--- save the image --->
                <cfimage source="#myImage#" action="write" destination="#expandpath('\assets\groupImages')#\#fileUpload.serverfile#"> 
                
                <cfreturn fileUpload.serverfile>

            <cfelse> 
                
                <!--- If it is not a valid image file, delete it from the server. ---> 
                <cffile action="delete" file="#local.pathwheresaved#">
                <cfreturn ''>
            
            </cfif>
        
        </cfif>

    </cffunction>

    <cffunction name="validateReminder" access="private" returntype="array">
        
        <cfargument name="rc"> 
        <cfset rc.errors = []>
                        
        <cfif NOT CSRFverifyToken(rc.token)>
            <cfset arrayAppend(rc.errors, "A new reminder could not be added")>
        </cfif>
        <cfif not len(trim(rc.title))>
            <cfset arrayAppend(rc.errors, "Please supply a title")>
        </cfif>
        <cfif not len(trim(rc.category))>
            <cfset arrayAppend(rc.errors, "Please supply a category")>
        </cfif>
        <cfif NOT isValid('date', rc.rdate)>
            <cfset arrayAppend(rc.errors, "Please supply a date")>
            <cfset rc.rdate = ''>
        </cfif> 
        <cfif not len(trim(rc.rtime))>
            <cfset arrayAppend(rc.errors, "Please supply a time")>
        </cfif>
            
        <cfreturn rc.errors>

    </cffunction>

    <cffunction name="createSchedule" access="private">
        
        <cfargument name="rc"> 
        
        <!--- set thetime --->
        <cfset rc.theyear = '#DateFormat(rc.rdate, "yyyy")#'>
        <cfset rc.themonth = '#DateFormat(rc.rdate, "mm")#'>
        <cfset rc.theday = '#DateFormat(rc.rdate, "dd")#'>
        <cfset rc.thehour = '#TimeFormat(rc.rtime, "HH")#'>
        <cfset rc.themin = '#TimeFormat(rc.rtime, "mm")#'>
        
        
        <!--- database Date Time Conversion --->
        <cfset rc.TheReminder = '#createDateTime(rc.theyear, rc.themonth, rc.theday, rc.thehour, rc.themin, 0)#'>
        <!--- How long before --->
        <cfset rc.TheReminder2 = dateAdd('#rc.remindertime#', - #rc.intervalPeriod#, #rc.TheReminder#)>
        <!--- test date time --->
        <!---<cfoutput>#DateFormat(TheReminder2, "dd/mmm/yyyy")#</cfoutput><br>
        <cfoutput>#TimeFormat(TheReminder2, "HH:mm:ss")#</cfoutput><br>--->
        <!--- set schedule --->
        
        <cfif rc.interval EQ 'yearly'>
            <cfset rc.interval = 'once'>
        </cfif>

        <!--- http://www.mustrememberthat.com --->
        <!--- <cfschedule action = "update"
            task = "MRT-#rc.reminderId#" 
            operation = "HTTPRequest"
            url = "http://www.mustrememberthat.com/index.cfm?action=main.sendReminders&rid=#rc.reminderId#&uid=#session.auth.userId#"
            startDate = "#DateFormat(rc.TheReminder2, "dd/mmm/yyyy")#"
            startTime = "#TimeFormat(rc.TheReminder2, "HH:mm:ss")#"
            interval = "#rc.interval#"
            resolveurl="yes"
            requestTimeOut = "60"> --->

        <cfset returnStruct = {}>
        <cfset returnStruct.reminderDate = DateFormat(rc.TheReminder2, "yyyy/mm/dd")> 
        <cfset returnStruct.reminderTime = TimeFormat(rc.TheReminder2, "HH:mm:ss")> 

        <cfreturn returnStruct>

    </cffunction>

    <cffunction name="groupValidation" access="private">
        
        <cfargument name="rc">
        <!--- check that the user is logged in and a valid admin for this group (if not send them to the home page) --->

        <cfif structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>

            <cfquery name="rc.vhkValidation">
                SELECT tbl_groups.*
                FROM tbl_groups
                WHERE ownerId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
                AND tbl_groups.groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
                UNION
                SELECT tbl_groups.*
                FROM tbl_groups
                INNER JOIN tbl_groupUsers ON tbl_groupUsers.groupId = tbl_groups.groupId
                WHERE tbl_groupUsers.userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
                AND tbl_groups.groupId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.groupId#">
            </cfquery>

            <cfif NOT rc.vhkValidation.recordcount>
                <cfset variables.fw.redirect( "main.default" )>  
            </cfif>
        
        <cfelse>
            <cfset variables.fw.redirect( "main.default" )>  
        </cfif>
            
    </cffunction>
        
    <cfscript>
        public string function urlSafeFormat(str, delim='-') {
            arguments.str=approximate(arguments.str);
            arguments.str=deaccent(arguments.str);
            arguments.str=reReplace(arguments.str,'<[^>]*>','','all');
            arguments.str=rereplace(arguments.str,'[^a-zA-Z0-9\#arguments.delim#]',arguments.delim,'all');
            arguments.str=rereplace(arguments.str,'\#arguments.delim#+',arguments.delim,'all');
            arguments.str=rereplace(arguments.str,'^#arguments.delim#','','all');
            arguments.str=rereplace(arguments.str,'#arguments.delim#$','','all');
            return (arguments.str=='')?createUUID():lcase(arguments.str);
        }
    </cfscript>

</cfcomponent>