<cfcomponent output="false">

	<cffunction name="DateFormatExtended" output="false" returntype="string">
	  <!---
	  * Returns a formatted date with the correct day suffix.
	  * 
	  * @example DateFormatExtended(Now(), "dd mmm yy")   returns   15th Apr 08
	  * @example DateFormatExtended(Now(), "dddd, dd mmmm yyyy")   returns   Tuesday, 15th April 2008
	  * @example DateFormatExtended(Now(), "ddd mmm dd, yyyy")   returns   Tue Apr 15th, 2008  
	  *
	  * @param date    A CF date object. (Required)
	  * @param mask    The mask to apply to the date. (Optional)
	  * 
	  * @return Returns a string. 
	  * @author John Whish (www.aliaspooryorik.com)    
	  * @version 1, April 15, 2008    
	  --->
	  
	  <cfargument name="date" type="date" required="true" />
	  <cfargument name="mask" type="string" required="false" default="mmmm dd yyyy" />
	  
	  <cfset var daysuffix = "" />
	  <cfset var datemask = LCase(Arguments.mask) />
	  <cfset var dateformatted = "" />
	  <cfset var asearchpattern = ListToArray("(^(d){1,2}[^d])|([^d](d){1,2}[^d])|([^d](d){1,2}$)", "|") />
	  
	  <cfif ReFind(asearchpattern[1], datemask) gt 0>
	    <cfset datemask = Reverse(Insert('}1{', Reverse(datemask), ReFind("(d){1,2}([^d]){0,1}", Reverse(datemask))-1)) />
	  <cfelseif ReFind(asearchpattern[2], datemask) gt 0>
	    <cfset datemask = Reverse(Insert('}2{', Reverse(datemask), ReFind(asearchpattern[2], Reverse(datemask)))) />
	  <cfelseif ReFind(asearchpattern[3], datemask) gt 0>
	    <cfset datemask = Reverse(Insert('}3{', Reverse(datemask), ReFind(asearchpattern[3], Reverse(datemask)))) />
	  </cfif>
	  
	  <cfset dateformatted = DateFormat(Arguments.date, datemask) />
	  
	  <cfif (IsDate(Arguments.date))>
	    <cfswitch expression="#Day(Arguments.date)#">
	      <cfcase value="2,22">
	        <cfset daysuffix = "nd" />
	      </cfcase>
	      <cfcase value="3,23"> 
	        <cfset daysuffix = "rd" />
	      </cfcase>
	      <cfcase value="1,21,31">
	        <cfset daysuffix = "st" />
	      </cfcase>
	      <cfdefaultcase>
	        <cfset daysuffix = "th" />
	      </cfdefaultcase>
	    </cfswitch>
	    
	    <cfreturn ReReplace(dateformatted, "{[1-3]}", daysuffix, "all") />
	  </cfif>
	</cffunction>

	<cffunction name="createCroppedImage" access="remote" returntype="string" returnformat="plain">
        
        <cfsetting showdebugoutput="false">
        <!--- hide debugoutput. You dont want to 
            return the debug information as well --->

        <cfquery name="updateUserImage">
            SELECT *
            FROM    tbl_users
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
        </cfquery>

        <!--- read the image and create a ColdFusion image object --->
        <cfimage source="#expandPath('/')#/assets/userImages/#updateUserImage.profileImage#" name="originalImage" />

        <!--- crop the image using the supplied coords from the url request --->
        <cfset ImageCrop(originalImage, updateUserImage.imgX, updateUserImage.imgY, updateUserImage.imageWidth, updateUserImage.imageHeight) />

        <!--- write the revised/cropped image to the browser
         to display on the calling page --->
        <cfimage source="#originalImage#" action="writeToBrowser" />
        
     </cffunction>

     <cffunction name="createUserCroppedImage" access="remote" returntype="string" returnformat="plain">
        
     	<cfargument name="userId" required="true" type="string">

        <cfsetting showdebugoutput="false">
        <!--- hide debugoutput. You dont want to 
            return the debug information as well --->

        <cfquery name="updateUserImage">
            SELECT *
            FROM    tbl_users
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.userId#">
        </cfquery>

        
        <cftry>
	        
	        <!--- read the image and create a ColdFusion image object --->
	        <cfimage source="#expandPath('/')#/assets/userImages/#updateUserImage.profileImage#" name="originalImage" />

	        <!--- crop the image using the supplied coords from the url request --->
	        <cfset ImageCrop(originalImage, updateUserImage.imgX, updateUserImage.imgY, updateUserImage.imageWidth, updateUserImage.imageHeight) />
	        <cfset ImageScaleToFit(newImage, 75, 75)>
	        <!--- write the revised/cropped image to the browser
	         to display on the calling page --->
	        <cfimage source="#originalImage#" action="writeToBrowser" />

	    	<cfcatch type="any">
				<cfimage source="#expandPath('/')#/assets/userImages/default.jpg" name="newImage" />
				<cfset ImageScaleToFit(newImage, 75, 75)>
	    		<cfimage source="#newImage#" action="writeToBrowser" />
	    	</cfcatch>
		</cftry>
        
     </cffunction>
	
</cfcomponent>