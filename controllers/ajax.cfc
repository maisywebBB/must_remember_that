<cfcomponent output="false">
	
    <cffunction name="init">
        <cfargument name="fw" type="any" />
        <cfset variables.fw = arguments.fw />
	</cffunction>

	<cffunction name="resetPassword" access="remote" output="false">
    	<cfargument name="rc"> 
        
		<!--- prevent displaying the layout --->
		<cfset request.layout = false>
        
        <!--- return response --->
        <cfset rc.response["status"] = "OK">
     	
        <cftry>
        	
            <!--- Make sure a valid email address was passed --->
			<cfif NOT isValid('email', rc.emailAddress)>
                <cfset rc.response["errorMessage"] = "Please enter a valid email address">
                <cfset rc.response["status"] = "error">
            <cfelse>
            	
                <!--- find the user details --->
                <cfquery name="rc.getuserDetails">
                    SELECT *
                    FROM tbl_users 
                    WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">
                </cfquery>
                
                <!--- if the user exists update there user record, The reset password request is valid for 24 hours --->
                <cfif rc.getuserDetails.recordcount>
                    
                    <!--- create a password token --->
                    <cfset rc.passwordToken = createUUID()>

                    <cfquery name="rc.getPayslips">
                        UPDATE tbl_users 
                        SET passwordResetToken = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.passwordToken#">, 
                        passwordResetDate = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#dateAdd('d', 1, now())#">
                        WHERE emailAddress = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.emailAddress#">
                    </cfquery>
                    
		    		<!--- send an email to the users with instrcutions of how to reset there password --->
		    		<cfmail from="noreply@mustrememberthat.com" to="#rc.emailAddress#" subject="Must Remember That Password Reset" type="html" server="mail.flintmedia.co.uk" username="noreply@mustrememberthat.com" password="Popesabbots1!" bcc="james@maisyweb.co.uk">
                        <p>Hello #rc.getuserDetails.firstname#</p>
	                    <p>We have sent you this email because you have requested that your Must Remember That password be reset.<br/>To get back into your Must Remember That account you'll need to create a new password.</p>
	                    <p>Here's how to do that
	                    <ul>
	                    	<li>Enter the requested information and follow the on screen instrcutions.</li>
	                        <li>You have 24 hours to complete this task.</li>
	                    </ul>
	                    </p>
	                    <p>Reset you password now: <cfoutput>http://#cgi.http_host##variables.fw.buildUrl("main.passwordreset")#&token=#rc.passwordToken#</cfoutput></p>
                    </cfmail>
                    <!--- output the following message weather an user was found or not. --->
                    <cfset rc.response["successMessage"] = "An email has been sent to " & rc.emailAddress & " with further instructions on how to reset your password">
                <cfelse>
                    <cfset rc.response["errorMessage"] = "Sorry, but you're request could not be processed at this time. Please try again or contact our support team ">
                    <cfset rc.response["status"] = "error">
                </cfif>
				
			</cfif>
            
            <cfcatch type="any">
                <cfset rc.response["errorMessage"] = "Sorry, but you're request could not be processed at this time. Please try again or contact our support team ">
                <cfset rc.response["status"] = "error">
            </cfcatch>
            
        </cftry>
    	
        <cfcontent reset="true">
        <cfset variables.fw.renderData( "json", rc.response )>
        
    </cffunction>

    <cffunction name="getCategories" access="remote" returntype="string" returnformat="plain">
        
        <cfargument name="mainCategoryId" default="1">

        <cfquery name="rc.getuserDetails">
            SELECT *
            FROM tbl_categories 
            WHERE mainCategoryId = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.mainCategoryId#">
            ORDER BY title
        </cfquery>

        <cfsavecontent variable="selectOutput">
            <cfoutput query="rc.getuserDetails"><option value="#id#">#title#</option></cfoutput>
        </cfsavecontent>
          
        <cfreturn selectOutput>

    </cffunction>

    <cffunction name="searchReminders" access="remote" returntype="string" returnformat="plain">
        
        <cfargument name="rc">

        <cfquery name="rc.getReminders">
            SELECT *
            FROM tbl_reminders 
            WHERE (title LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#rc.searchString#%">
            OR comments  LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#rc.searchString#%">)
            AND reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="2">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderKey#">
            AND rdate >= getdate()
            ORDER BY rdate
        </cfquery>

    </cffunction>

    <cffunction name="getReminder" access="remote" returnformat="json">
        
        <cfargument name="reminderId">

        <cfquery name="rc.getReminders">
            SELECT *
            FROM tbl_reminders 
            WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.reminderId#">
        </cfquery>

        <!--- get the main category --->
        <cfquery name="rc.getMainCat">
            SELECT *
            FROM tbl_mainCategories 
            INNER JOIN tbl_categories ON tbl_categories.mainCategoryId = tbl_mainCategories.mainCategoryId
            WHERE tbl_categories.id = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.getReminders.CATEGORY#">
        </cfquery>

        <cfset returnStruct = {
            REMINDERID = rc.getReminders.REMINDERID,
            REMINDERTYPE = rc.getReminders.REMINDERTYPE,
            REMINDERKEY = rc.getReminders.REMINDERKEY,
            TITLE = rc.getReminders.TITLE,
            MAINCATEGORY = rc.getMainCat.mainCategoryId,
            CATEGORY = rc.getReminders.CATEGORY,
            RDATE = rc.getReminders.RDATE,
            RTIME = rc.getReminders.RTIME,
            INTERVAL = rc.getReminders.INTERVAL,
            REMINDERTIME = rc.getReminders.REMINDERTIME,
            COMMENTS = rc.getReminders.COMMENTS,
            PARENTID = rc.getReminders.PARENTID,
            DATECREATED = rc.getReminders.DATECREATED,
            ISLIVE = rc.getReminders.ISLIVE,
            INTERVALPERIOD = rc.getReminders.INTERVALPERIOD,
            EVENTDATE = dateFormat(rc.getReminders.EVENTDATE, 'mm/dd/yyyy'),
            EVENTDISPLAYDATE = dateFormat(rc.getReminders.EVENTDATE, 'MMMM, dd yyyy'),
            EVENTTIME = timeFormat(rc.getReminders.EVENTTIME, 'HH:mm'),
            FOREVER = rc.getReminders.FOREVER
        }>

        <cfreturn returnStruct>

    </cffunction>

    <cffunction name="safeurl" access="remote" returnformat="json">
        
        <cfquery name="rc.getDefaultReminder">
            SELECT *
            FROM tbl_categories
            WHERE urlSafe = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.safeurl#">
        </cfquery>

        <cfset rc.title = rc.getDefaultReminder.defaultReminderTitle>
        <cfset rc.maincategory = rc.getDefaultReminder.mainCategoryId>
        <cfset rc.category = rc.getDefaultReminder.id>
        <cfset rc.remindertime = 'd'>

        <cfset returnStruct = {
            TITLE = rc.getDefaultReminder.defaultReminderTitle,
            MAINCATEGORY = rc.getDefaultReminder.mainCategoryId,
            CATEGORY = rc.getDefaultReminder.id,
            REMINDERTIME =  'd'
        }>

    <cfreturn returnStruct>

    </cffunction>

    <cffunction name="searchPersonalReminders" access="remote" returntype="string" returnformat="plain">
        
        <cfargument name="rc">

        <cfquery name="rc.getReminders">
            SELECT *
            FROM tbl_reminders 
            WHERE (title LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#rc.searchString#%">
            OR comments  LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#rc.searchString#%">)
            AND reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderKey#">
            AND rdate >= getdate()
            AND isLive = 1
            ORDER BY rdate
        </cfquery>

    </cffunction>

    <cffunction name="searchGroupReminders" access="remote" returntype="string" returnformat="plain">
        
        <cfargument name="rc">

        <cfquery name="rc.getReminders">
            SELECT *
            FROM tbl_reminders 
            WHERE (title LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#rc.searchString#%">
            OR comments  LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#rc.searchString#%">)
            AND reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="3">
            AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderKey#">
            AND rdate >= getdate()
            AND isLive = 1
            ORDER BY rdate
        </cfquery>

    </cffunction>

    <cffunction name="unsubscribe" access="remote" returntype="string" returnformat="plain">

        <cfargument name="rc">

        <!--- first make sure the user is not already subscribed --->
        <cfquery name="chkSubscription">
            delete FROM tbl_subscriptions
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">
            AND businessId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.businessId#">
        </cfquery>
        
        <cfset rc.response["successMessage"] = "You have successfuly unsubscribed to this page.">
        
        <cfset variables.fw.renderData( "json", rc.response )>

    </cffunction>

    <cffunction name="subscribe" access="remote" returntype="string" returnformat="plain">

        <cfargument name="rc">

        <!--- first make sure the user is not already subscribed --->
        <cfquery name="chkSubscription">
            SELECT *
            FROM tbl_subscriptions
            WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">
            AND businessId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.businessId#">
        </cfquery>
        
        <cfif NOT chkSubscription.recordcount>
            <cfif NOT structKeyExists(RC, 'subscriptionType')>
                <cfset rc.subscriptionType = 1>
            </cfif>
            <cfquery name="setReminders">
                INSERT INTO tbl_subscriptions(userId, businessId, subscriptionType)
                VALUES
                (
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.userId#">,
                    <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.businessId#">,
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.subscriptionType#">
                )
            </cfquery>
        </cfif>

        <!--- now check to see if we have to add all the current reminders --->
        <cfif structKeyExists(RC, 'allreminders') AND rc.allreminders EQ 1>
            
            <!--- get a list of all future reminders for this business --->
            <cfquery name="rc.getReminderList">
                SELECT rem.*, tbl_categories.title AS catTitle, overall_count = COUNT(*) OVER(),
                (select count(*) from tbl_reminders where parentid= rem.reminderid ) as ChildCount
                FROM tbl_reminders rem
                INNER JOIN tbl_categories ON tbl_categories.id = rem.category
                WHERE reminderType = <cfqueryparam cfsqltype="cf_sql_integer" value="2">
                AND reminderKey = <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.businessId#">
                AND  isLive = <cfqueryparam cfsqltype="cf_sql_integer" value="1">
                AND rdate >= getdate()
                ORDER BY rdate
            </cfquery>

            <cfset rc.reminderList = valueList(rc.getReminderList.reminderId)>

            <cfloop list="#rc.reminderList#" index="i">
            
                <!--- get the reminder --->
                <cfquery name="rc.getReminder">
                    SELECT * 
                    FROM tbl_reminders
                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#i#">
                </cfquery>

                <cfset rc.rdate = rc.getReminder.rdate>
                <cfset rc.rtime = rc.getReminder.rtime>
                <cfset rc.interval = rc.getReminder.interval>
                <cfset rc.reminderId = createUUID()>
                <cfset rc.reminderTime = rc.getReminder.reminderTime>
                <cfset rc.intervalPeriod = rc.getReminder.intervalPeriod>
                <cfset rc.title = rc.getReminder.title>
                <cfset rc.category = rc.getReminder.category>
                <cfset rc.forever = rc.getReminder.forever>
                <cfset rc.reminderActualTime = ''>

                <cfobject component="controllers.users" name="request.userObject" type="component">
                <cfset createSchedule = request.userObject.createSchedule(rc)>

                <cfquery name="rc.createReminder">
                    INSERT INTO tbl_reminders
                    (
                        reminderId,
                        reminderType,
                        reminderKey,
                        title,
                        category,
                        interval,
                        remindertime,
                        rdate,
                        rtime,
                        intervalPeriod,
                        isLive,
                        eventDate,
                        eventTime,
                        forever
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminderId#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.title#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.category#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.interval#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.remindertime#">,
                        <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                        <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.intervalPeriod#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                        <cfqueryparam cfsqltype="cf_sql_date" value="#rc.rdate#">,
                        <cfqueryparam cfsqltype="cf_sql_time" value="#rc.rtime#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#rc.forever#">
                    )
                </cfquery>

            </cfloop>

        </cfif>    
        
        <cfset rc.response["successMessage"] = "You have successfuly subscribed to this page.">
        
        <cfset variables.fw.renderData( "json", rc.response )>

    </cffunction>

    <cffunction name="batchReminder" access="remote" returntype="string" returnformat="plain">
        
        <cfargument name="rc" default="1">
        
        <!--- loop over the selected reminders and save to the users database --->
        <cfset rc.reminders = DeserializeJSON(rc.reminders)>
        <cfloop from="1" to="#arrayLen(rc.reminders)#" index="i">

            <cfset rc.reminderId = createUUID()>

            <cfquery name="setReminders">
                INSERT INTO tbl_reminders(reminderId, reminderType, reminderKey, title, category, rdate, rtime, interval, remindertime, comments, parentId, eventDate, eventTime, forever)
                SELECT '#rc.reminderId#' AS reminderId, 1 AS reminderType, '#rc.userId#' AS reminderKey, title, category, rdate, rtime, interval, remindertime, comments, '#rc.reminders[i]#', eventDate, eventTime, forever
                FROM tbl_reminders 
                WHERE reminderId =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#rc.reminders[i]#">
            </cfquery>
            
        </cfloop>
        
        <cfif arrayLen(rc.reminders) EQ 1>
            <cfset rc.response["successMessage"] = "The reminder has been added to your personal list. <br/><a class='alert-link' href='/index.cfm?action=users.reminders'>Your Reminders</a>">
        <cfelse>
            <cfset rc.response["successMessage"] = "The selected reminders have been added to your personal list. <br/><a class='alert-link' href='/index.cfm?action=users.reminders'>Your Reminders</a>">
        </cfif>
        
        <cfset variables.fw.renderData( "json", rc.response )>

    </cffunction>

    <cffunction name="setReminderSessions" access="remote" returntype="string" returnformat="plain">
        
        <cfargument name="rc" default="1">
        <cfset rc.reminders = DeserializeJSON(rc.reminders)>
        <cfset session.reminderArray = rc.reminders>
        
        <cfif arrayLen(rc.reminders) EQ 1>
            <cfset rc.response["successMessage"] = "The reminder has been added to your personal list. <br/><a class='alert-link' href='/index.cfm?action=users.reminders'>Your Reminders</a>">
        <cfelse>
            <cfset rc.response["successMessage"] = "The selected reminders have been added to your personal list. <br/><a class='alert-link' href='/index.cfm?action=users.reminders'>Your Reminders</a>">
        </cfif>
        
        <cfset variables.fw.renderData( "json", rc.response )>

    </cffunction>

    <cffunction name="checkSessionExists" access="remote" returntype="string" returnformat="plain">
        
        <cfargument name="rc" default="1">

        <cfif structKeyExists(session, 'auth') AND structKeyExists(session.auth, 'isLoggedIn') AND session.auth.isLoggedIn>
            Logged In
        <cfelse>
            <cfheader statuscode="400" statustext="Bad request. session timedout.">
        </cfif>
     </cffunction>

     <cffunction name="imgCrop" access="remote" returntype="string" returnformat="plain">
        
        <cfsetting showdebugoutput="false">
        <!--- hide debugoutput. You dont want to 
            return the debug information as well --->

        <cfparam name="url.imgX"    type="numeric"  default="10" />
        <cfparam name="url.imgY"    type="numeric"  default="10" />
        <cfparam name="url.width"   type="numeric"  default="50" />
        <cfparam name="url.height"  type="numeric"  default="50" />
        <cfparam name="url.imgLoc"  type="string"   default="" />
        <!--- set the params for the image dimensions --->

        <cfif len(url.imgLoc)>

            <cfif NOT isNumeric(url.imgX)>
                <cfset url.imgX = 10>
            </cfif>
            <cfif NOT isNumeric(url.imgY)>
                <cfset url.imgY = 10>
            </cfif>
            <cfif NOT isNumeric(url.width)>
                <cfset url.width = 50>
            </cfif>
            <cfif NOT isNumeric(url.height)>
                <cfset url.height = 50>
            </cfif>

            <!--- save the details to the database --->
            <cfquery name="updateUserImage">
                UPDATE tbl_users
                   SET imgX = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.imgX#">
                      ,imgY = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.imgY#">
                      ,imageWidth = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.width#">
                      ,imageHeight = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.height#">
                 WHERE userId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">
            </cfquery>
                
            <!--- read the image and create a ColdFusion image object --->
            <cfimage source="#expandPath('/')##url.imgLoc#" name="originalImage" />

            <!--- crop the image using the supplied coords from the url request --->
            <cfset ImageCrop(originalImage, url.imgX, url.imgY, url.width, url.height) />

            <!--- write the revised/cropped image to the browser
             to display on the calling page --->
            <cfimage source="#originalImage#" action="writeToBrowser" />

        </cfif>
        
     </cffunction>

    <cffunction name="modalReminder" access="remote" returnformat="plain" returntype="string">

        <cfif not len(trim(arguments.rc.title))>
            <cfheader statusCode="500" statusText="status text" >
            Please supply a title
            <cfabort>
        </cfif>
        <cfif not structKeyExists(arguments.rc, 'category')>
            <cfheader statusCode="500" statusText="status text" >
            Please supply a category
            <cfabort>
        </cfif>
        <cfif NOT isValid('date', arguments.rc.rdate)>
            <cfheader statusCode="500" statusText="status text" >
            Please supply a date
            <cfabort>
        </cfif> 
        <cfif not len(trim(arguments.rc.rtime))>
            <cfheader statusCode="500" statusText="status text" >
            Please supply a time
            <cfabort>
        </cfif>

        <cftry>
            
            <cfobject component="controllers.users" name="request.userObject" type="component">
            <cfset createSchedule = request.userObject.createSchedule(arguments.rc)>

            <cfif structKeyExists(arguments.rc, 'reminderId') AND len(arguments.rc.reminderId)>
                 
                 <cfquery name="rc.createReminder">
                    UPDATE tbl_reminders
                    SET
                    title = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.title#">,
                    category = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.category#">,
                    interval = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.interval#">,
                    reminderTime = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.remindertime#">,
                    rdate = <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                    rtime = <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                    eventDate = <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.rc.rdate#">,
                    eventTime = <cfqueryparam cfsqltype="cf_sql_time" value="#arguments.rc.rtime#">,
                    comments = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.comments#">,
                    intervalPeriod = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.rc.intervalPeriod#">
                    WHERE reminderId = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.reminderId#">
                </cfquery>
            
            <cfelse>
                
                <cfif arguments.rc.interval EQ 'daily2'>
                    <cfset arguments.rc.interval = 'daily'>
                    <cfset arguments.rc.forever = 1>
                <cfelse>
                    <cfset arguments.rc.forever = 0>
                </cfif>

                <cfquery name="rc.createReminder">
                    INSERT INTO tbl_reminders
                    (
                        reminderId,
                        reminderType,
                        reminderKey,
                        title,
                        category,
                        interval,
                        remindertime,
                        rdate,
                        rtime,
                        intervalPeriod,
                        isLive,
                        eventDate,
                        eventTime,
                        forever
                    )
                    VALUES
                    (
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#createUUID()#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.auth.userId#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.title#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.category#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.interval#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.rc.remindertime#">,
                        <cfqueryparam cfsqltype="cf_sql_date" value="#createSchedule.reminderdate#">,
                        <cfqueryparam cfsqltype="cf_sql_time" value="#createSchedule.remindertime#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.rc.intervalPeriod#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="1">,
                        <cfqueryparam cfsqltype="cf_sql_date" value="#arguments.rc.rdate#">,
                        <cfqueryparam cfsqltype="cf_sql_time" value="#arguments.rc.rtime#">,
                        <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.rc.forever#">
                    )
                </cfquery>    
            </cfif>

            <cfcatch type="any">
                <cfheader statusCode="500" statusText="status text" >
                <cfoutput>#cfcatch.message#</cfoutput>
                <cfabort>
            </cfcatch>
                
        </cftry>

        <cfreturn "ok">

    </cffunction>

</cfcomponent>